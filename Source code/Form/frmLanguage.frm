VERSION 5.00
Object = "{A8E5842E-102B-4289-9D57-3B3F5B5E15D3}#18.3#0"; "CODEJO~1.OCX"
Begin VB.Form frmLanguage 
   Appearance      =   0  'Flat
   BackColor       =   &H00FFFFFF&
   BorderStyle     =   1  'Fixed Single
   Caption         =   "FINGERSPOT ADD ON"
   ClientHeight    =   1995
   ClientLeft      =   45
   ClientTop       =   390
   ClientWidth     =   4875
   FillColor       =   &H00FFFFFF&
   BeginProperty Font 
      Name            =   "Segoe UI"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   ForeColor       =   &H00FFFFFF&
   Icon            =   "frmLanguage.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1995
   ScaleWidth      =   4875
   StartUpPosition =   1  'CenterOwner
   Begin XtremeSuiteControls.ComboBox cmbBahasa 
      Height          =   315
      Left            =   600
      TabIndex        =   1
      Top             =   600
      Width           =   3495
      _Version        =   1179651
      _ExtentX        =   6165
      _ExtentY        =   556
      _StockProps     =   77
      ForeColor       =   8421504
      Style           =   2
      Appearance      =   2
      UseVisualStyle  =   0   'False
   End
   Begin XtremeSuiteControls.PushButton btn 
      Height          =   375
      Index           =   0
      Left            =   2280
      TabIndex        =   2
      Top             =   1200
      Width           =   1815
      _Version        =   1179651
      _ExtentX        =   3201
      _ExtentY        =   661
      _StockProps     =   79
      Caption         =   "OK"
      ForeColor       =   8421504
      BackColor       =   -2147483644
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Segoe UI"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Transparent     =   -1  'True
      Appearance      =   2
      Picture         =   "frmLanguage.frx":058A
   End
   Begin XtremeSuiteControls.Label lbl 
      Height          =   195
      Index           =   0
      Left            =   600
      TabIndex        =   0
      Top             =   240
      Width           =   1785
      _Version        =   1179651
      _ExtentX        =   3149
      _ExtentY        =   344
      _StockProps     =   79
      Caption         =   "Please Select Language"
      ForeColor       =   8421504
      BackColor       =   -2147483633
      Transparent     =   -1  'True
      AutoSize        =   -1  'True
   End
End
Attribute VB_Name = "frmLanguage"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub btn_Click(Index As Integer)
    INIConfigWrite "System", "Language", cmbBahasa.Text
    
    Language = INIConfigRead("System", "Language", "")
    INILanFile = App.Path & "\Lang\" & Language & ".ini"
    
    Unload Me
End Sub

Private Sub Form_Load()
    Dim i As Integer
    Dim nama As String
    
'    For i = 0 To NumberLang - 1
'        nama = "Lang" & Trim(str(i + 1))
'        nama = INIConfigRead("Language", nama, nama)
'        cmbBahasa.AddItem nama, i, nama
'    Next
    
    cmbBahasa.AddItem "Indonesia", 0, "Indonesia"
    cmbBahasa.AddItem "Inggris", 1, "Inggris"
    
    cmbBahasa.ListIndex = 0
End Sub

Private Sub SubTabDetail_SelectedChanged(ByVal Item As XtremeSuiteControls.ITabControlItem)

End Sub
