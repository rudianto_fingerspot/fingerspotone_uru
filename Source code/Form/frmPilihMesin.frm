VERSION 5.00
Object = "{A8E5842E-102B-4289-9D57-3B3F5B5E15D3}#18.3#0"; "CODEJO~2.OCX"
Object = "{D2BBCEE2-EBE7-4DA2-8B98-2E5F1ECA275B}#1.0#0"; "iGrid650_10Tec.ocx"
Begin VB.Form frmPilihMesin 
   Appearance      =   0  'Flat
   BackColor       =   &H00E0E0E0&
   BorderStyle     =   1  'Fixed Single
   Caption         =   "FingerspotOne AddOn"
   ClientHeight    =   5835
   ClientLeft      =   45
   ClientTop       =   390
   ClientWidth     =   13605
   FillColor       =   &H00FFFFFF&
   BeginProperty Font 
      Name            =   "Segoe UI"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   ForeColor       =   &H00FFFFFF&
   Icon            =   "frmPilihMesin.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5835
   ScaleWidth      =   13605
   StartUpPosition =   1  'CenterOwner
   Begin VB.TextBox txtTimezone 
      Appearance      =   0  'Flat
      Height          =   3435
      Left            =   3840
      Locked          =   -1  'True
      MultiLine       =   -1  'True
      ScrollBars      =   3  'Both
      TabIndex        =   5
      Text            =   "frmPilihMesin.frx":058A
      Top             =   3960
      Visible         =   0   'False
      Width           =   4215
   End
   Begin XtremeSuiteControls.GroupBox GroupBox1TT 
      Height          =   5880
      Index           =   0
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   13575
      _Version        =   1179651
      _ExtentX        =   23945
      _ExtentY        =   10372
      _StockProps     =   79
      ForeColor       =   -2147483630
      BackColor       =   16777215
      UseVisualStyle  =   -1  'True
      BorderStyle     =   2
      Begin iGrid650_10Tec.iGrid dbg 
         Height          =   4245
         Left            =   360
         TabIndex        =   1
         Tag             =   "AlignLeft"
         Top             =   840
         Width           =   12915
         _ExtentX        =   22781
         _ExtentY        =   7488
         AllowSorting    =   0   'False
         BorderType      =   1
         FlatCellControls=   -1  'True
         ForeColor       =   4210752
         GridLineColor   =   12632256
      End
      Begin XtremeSuiteControls.PushButton btn 
         Height          =   375
         Index           =   0
         Left            =   360
         TabIndex        =   2
         Top             =   5280
         Width           =   1815
         _Version        =   1179651
         _ExtentX        =   3201
         _ExtentY        =   661
         _StockProps     =   79
         Caption         =   "0"
         ForeColor       =   16711680
         BackColor       =   -2147483644
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Segoe UI"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Transparent     =   -1  'True
         Appearance      =   2
      End
      Begin XtremeSuiteControls.Label lbl 
         Height          =   435
         Index           =   0
         Left            =   360
         TabIndex        =   4
         Tag             =   "AlignTop"
         Top             =   240
         Width           =   12900
         _Version        =   1179651
         _ExtentX        =   22754
         _ExtentY        =   767
         _StockProps     =   79
         Caption         =   "0"
         ForeColor       =   0
         BackColor       =   8421504
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Alignment       =   4
         Transparent     =   -1  'True
         WordWrap        =   -1  'True
      End
      Begin XtremeSuiteControls.Label lbl_row_count 
         Height          =   225
         Index           =   0
         Left            =   9720
         TabIndex        =   3
         Tag             =   "AnchorCenterHLeft"
         Top             =   5100
         Width           =   2430
         _Version        =   1179651
         _ExtentX        =   4286
         _ExtentY        =   397
         _StockProps     =   79
         Caption         =   "lbl_row_count"
         ForeColor       =   4210752
         BackColor       =   -2147483633
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Segoe UI"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Alignment       =   1
         Transparent     =   -1  'True
      End
   End
End
Attribute VB_Name = "frmPilihMesin"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private status0 As String

Private Sub dbg_CellCheckChange(ByVal lRow As Long, ByVal lCol As Long, eNewCheckState As iGrid650_10Tec.ECellCheckState, bCancel As Boolean)
    Dim Jml As Integer
    Dim sn_device As String
    
    Jml = Val(GetValueField("SELECT count(*) AS Jml FROM t_ordering WHERE status_ordering = '0' ", "Jml"))
    If (dbg.CellValue(lRow, lCol) = status0) Or (Jml > 0) Then
        bCancel = True
        Exit Sub
    End If
    
    If eNewCheckState = igCheckStateGrayed Then
        eNewCheckState = igCheckStateUnchecked
    End If
    
    sn_device = dbg.CellValue(lRow, 1)
    If lCol = 2 Then
        If (eNewCheckState = igCheckStateUnchecked) And (dbg.CellCheckState(lRow, 5) = igCheckStateChecked) Then
            MsgBox INILanRead("errMsg", "msg17", "", sn_device, "", "")
            dbg.CellCheckState(lRow, 5) = igCheckStateUnchecked
        End If
    ElseIf lCol = 5 Then
        If (eNewCheckState = igCheckStateChecked) And (dbg.CellCheckState(lRow, 2) = igCheckStateUnchecked) Then
            MsgBox INILanRead("errMsg", "msg16", "", sn_device, "", "")
            eNewCheckState = igCheckStateUnchecked
        End If
    End If
End Sub

Public Function GetPayment() As Boolean
    On Error GoTo BugError
    
    Dim str1 As String
    Dim str2 As String
    Dim str3 As String
    
    Dim sn_device As String
    Dim RecRow As Long
    Dim Jml As Integer
    
    GetPayment = False
    
    Dim nCnt As Long
    Dim sn_str1 As String
    Dim sn_str2 As String
    Dim sn_str3 As String
    Dim sn_str4 As String
    
    frmWizard.sn_mesin_fitur1 = ""
    frmWizard.sn_mesin_fitur2 = ""
    frmWizard.sn_mesin_fitur3 = ""
    frmWizard.sn_mesin_fitur4 = ""
    
    frmWizard.jml_mesin_fitur1 = 0
    frmWizard.jml_mesin_fitur2 = 0
    frmWizard.jml_mesin_fitur3 = 0
    frmWizard.jml_mesin_fitur4 = 0
    
    sn_str1 = ""
    sn_str2 = ""
    sn_str3 = ""
    sn_str4 = ""
    status0 = INILanRead(Me.Name, "status0", "", "", "", "")
    
    With dbg
        .BeginUpdate
        For RecRow = 1 To .RowCount
            sn_device = .CellValue(RecRow, 1)
            
            If (.CellCheckState(RecRow, 2) = igCheckStateChecked) And (.CellValue(RecRow, 2) <> status0) Then
                frmWizard.jml_mesin_fitur1 = frmWizard.jml_mesin_fitur1 + 1
                sn_str1 = sn_str1 & sn_device & ";"
            End If
            
            If (.CellCheckState(RecRow, 3) = igCheckStateChecked) And (.CellValue(RecRow, 3) <> status0) Then
                frmWizard.jml_mesin_fitur2 = frmWizard.jml_mesin_fitur2 + 1
                sn_str2 = sn_str2 & sn_device & ";"
            End If
            
            If (.CellCheckState(RecRow, 4) = igCheckStateChecked) And (.CellValue(RecRow, 4) <> status0) Then
                frmWizard.jml_mesin_fitur3 = frmWizard.jml_mesin_fitur3 + 1
                sn_str3 = sn_str3 & sn_device & ";"
            End If
            
            If (.CellCheckState(RecRow, 5) = igCheckStateChecked) And (.CellValue(RecRow, 5) <> status0) Then
                frmWizard.jml_mesin_fitur4 = frmWizard.jml_mesin_fitur4 + 1
                sn_str4 = sn_str4 & sn_device & ";"
            End If
            
            DoEvents
        Next RecRow
        .EndUpdate
    End With
    
    frmWizard.sn_mesin_fitur1 = sn_str1
    frmWizard.sn_mesin_fitur2 = sn_str2
    frmWizard.sn_mesin_fitur3 = sn_str3
    frmWizard.sn_mesin_fitur4 = sn_str4
    
    If frmWizard.jml_mesin_fitur1 > 0 Then
        frmWizard.chb(0).value = xtpChecked
    Else
        Jml = Val(GetValueField("SELECT count(*) AS Jml FROM t_ordering WHERE addon_id = 1 ", "Jml"))
        If Jml = 0 Then
            frmWizard.chb(0).value = xtpUnchecked
        End If
    End If
    
    If frmWizard.jml_mesin_fitur2 > 0 Then
        frmWizard.chb(1).value = xtpChecked
    Else
        Jml = Val(GetValueField("SELECT count(*) AS Jml FROM t_ordering WHERE addon_id = 2 ", "Jml"))
        If Jml = 0 Then
            frmWizard.chb(1).value = xtpUnchecked
        End If
    End If
    
    If frmWizard.jml_mesin_fitur3 > 0 Then
        frmWizard.chb(2).value = xtpChecked
    Else
        Jml = Val(GetValueField("SELECT count(*) AS Jml FROM t_ordering WHERE addon_id = 3 ", "Jml"))
        If Jml = 0 Then
            frmWizard.chb(2).value = xtpUnchecked
        End If
    End If
    
    If frmWizard.jml_mesin_fitur4 > 0 Then
        frmWizard.chb(3).value = xtpChecked
    Else
        Jml = Val(GetValueField("SELECT count(*) AS Jml FROM t_ordering WHERE addon_id = 4 ", "Jml"))
        If Jml = 0 Then
            frmWizard.chb(3).value = xtpUnchecked
        End If
    End If
    
    GetPayment = True
    
    Exit Function
        
BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Function

Private Sub btn_Click(Index As Integer)
    frmWizard.StatusBolehGetPrice = True
    
    GetPayment
    DoEvents
    
    frmWizard.GetPrice 'HitungHarga
    DoEvents
    
    Unload Me
End Sub

Private Sub Form_Load()
    On Error GoTo Err1
    
    Dim str1 As String
    Dim SNAutoSyncUser As String
    Dim ACAutoSyncUser As String
    
    Dim ComboFieldID() As String
    Dim ComboFieldName() As String
    Dim ComboSQL() As String
    
    ChangeFont Me
    SetLang Me
    
    status0 = INILanRead(Me.Name, "status0", "", "", "", "")
    DoEvents
    
    SetGrid dbg, dbg.Name, Me.Name, False, "", lbl_row_count(0), "", txtTimezone.Text, 1, ComboFieldID(), ComboFieldName(), ComboSQL()
    DoEvents
    
    LoadDevice
    DoEvents
    
    Exit Sub
Err1:
    Err.Clear
    Resume Next
End Sub

Private Sub LoadDevice()
    On Error GoTo Err1
    
    Dim rs As cRecordset
    Dim Jml As Integer
    
    Dim sn_device As String
    Dim active_code_realtime As String
    Dim active_code_autosync As String
    Dim active_code_sdk As String
    Dim activation_code As String
    Dim timezone As String
    Dim selisih As String
    
    Dim str1 As String
    Dim str2 As String
    Dim str3 As String
    
    Dim status_fitur1 As String
    Dim status_fitur2 As String
    Dim status_fitur3 As String
    Dim status_fitur4 As String
    
    dbg.Clear
    status0 = INILanRead(Me.Name, "status0", "", "", "", "")
    DoEvents
    
    strSQL = "SELECT * FROM t_device Where status_aktif = '1' AND comm_type < 4 " & _
        "ORDER BY sn_device ASC "
    Set rs = getRecordSet_Data(strSQL, True)
    Do While Not rs.EOF
        sn_device = rs.Fields("sn_device").value
        active_code_realtime = rs.Fields("active_code_realtime").value
        active_code_autosync = rs.Fields("active_code_autosync").value
        active_code_sdk = rs.Fields("active_code_sdk").value
        activation_code = rs.Fields("activation_code").value
        
        '1. Fitur Realtime Scanlog
        Jml = Val(GetValueField("SELECT count(*) AS Jml FROM t_ordering WHERE addon_id = 1 AND status_ordering = '1' AND sn_device = """ & sn_device & """ ", "Jml"))
        If Jml = 0 Then
            Jml = Val(GetValueField("SELECT count(*) AS Jml FROM t_ordering WHERE addon_id = 1 AND status_ordering = '0' AND sn_device = """ & sn_device & """ ", "Jml"))
            If Jml = 0 Then
                Jml = Val(GetValueField("SELECT count(*) AS Jml FROM t_ordering WHERE status_ordering = '0' ", "Jml"))
                If Jml = 0 Then
                    If InStr(frmWizard.sn_mesin_fitur1, sn_device & ";") > 0 Then
                        status_fitur1 = "[True]"
                        'frmWizard.chb(0).value = xtpChecked
                    Else
                        status_fitur1 = "[False]"
                    End If
                Else
                    If frmWizard.chb(0).value = xtpChecked Then
                        status_fitur1 = "[True]"
                    Else
                        status_fitur1 = "[False]"
                    End If
                End If
            Else
                status_fitur1 = "[True]"
            End If
        Else
            status_fitur1 = status0
        End If
        
        '2. Fitur Auto Sinkron User Mesin
        Jml = Val(GetValueField("SELECT count(*) AS Jml FROM t_ordering WHERE addon_id = 2 AND status_ordering = '1' AND sn_device = """ & sn_device & """ ", "Jml"))
        If Jml = 0 Then
            Jml = Val(GetValueField("SELECT count(*) AS Jml FROM t_ordering WHERE addon_id = 2 AND status_ordering = '0' AND sn_device = """ & sn_device & """ ", "Jml"))
            If Jml = 0 Then
                Jml = Val(GetValueField("SELECT count(*) AS Jml FROM t_ordering WHERE status_ordering = '0' ", "Jml"))
                If Jml = 0 Then
                    If InStr(frmWizard.sn_mesin_fitur2, sn_device & ";") > 0 Then
                        status_fitur2 = "[True]"
                        'frmWizard.chb(1).value = xtpChecked
                    Else
                        status_fitur2 = "[False]"
                    End If
                Else
                    If frmWizard.chb(1).value = xtpChecked Then
                        status_fitur2 = "[True]"
                    Else
                        status_fitur2 = "[False]"
                    End If
                End If
            Else
                status_fitur2 = "[True]"
            End If
        Else
            status_fitur2 = status0
        End If
        
        '3. Fitur Auto Sinkron Waktu Mesin
        Jml = Val(GetValueField("SELECT count(*) AS Jml FROM t_ordering WHERE addon_id = 3 AND status_ordering = '1' AND sn_device = """ & sn_device & """ ", "Jml"))
        If Jml = 0 Then
            Jml = Val(GetValueField("SELECT count(*) AS Jml FROM t_ordering WHERE addon_id = 3 AND status_ordering = '0' AND sn_device = """ & sn_device & """ ", "Jml"))
            If Jml = 0 Then
                Jml = Val(GetValueField("SELECT count(*) AS Jml FROM t_ordering WHERE status_ordering = '0' ", "Jml"))
                If Jml = 0 Then
                    If InStr(frmWizard.sn_mesin_fitur3, sn_device & ";") > 0 Then
                        status_fitur3 = "[True]"
                        'frmWizard.chb(2).value = xtpChecked
                    Else
                        status_fitur3 = "[False]"
                    End If
                Else
                    If frmWizard.chb(2).value = xtpChecked Then
                        status_fitur3 = "[True]"
                    Else
                        status_fitur3 = "[False]"
                    End If
                End If
            Else
                status_fitur3 = "[True]"
            End If
        Else
            status_fitur3 = status0
        End If

        '4. Notifikasi SMS Scanlog
        Jml = Val(GetValueField("SELECT count(*) AS Jml FROM t_ordering WHERE addon_id = 4 AND status_ordering = '1' AND sn_device = """ & sn_device & """ ", "Jml"))
        If Jml = 0 Then
            Jml = Val(GetValueField("SELECT count(*) AS Jml FROM t_ordering WHERE addon_id = 4 AND status_ordering = '0' AND sn_device = """ & sn_device & """ ", "Jml"))
            If Jml = 0 Then
                Jml = Val(GetValueField("SELECT count(*) AS Jml FROM t_ordering WHERE status_ordering = '0' ", "Jml"))
                If Jml = 0 Then
                    If InStr(frmWizard.sn_mesin_fitur4, sn_device & ";") > 0 Then
                        status_fitur4 = "[True]"
                        'frmWizard.chb(3).value = xtpChecked
                    Else
                        status_fitur4 = "[False]"
                    End If
                Else
                    If frmWizard.chb(3).value = xtpChecked Then
                        status_fitur4 = "[True]"
                    Else
                        status_fitur4 = "[False]"
                    End If
                End If
            Else
                status_fitur4 = "[True]"
            End If
        Else
            status_fitur4 = status0
        End If

        'str1 = sn_device & ";" & status_fitur1 & ";" & status_fitur2 & ";" & status_fitur3 & ";" & timezone & ";" & selisih & ";" & status_fitur4 & ";"
        str1 = sn_device & ";" & status_fitur1 & ";" & status_fitur2 & ";" & status_fitur3 & ";" & status_fitur4 & ";"
        InsertGrid dbg, dbg.RowCount + 1, lbl_row_count(0), "", False, &H0&, &H404040, False, Null, Null, "", str1
        
        DoEvents
        rs.MoveNext
    Loop
    DoEvents
    
    'Centang yang sudah dibeli
    Dim RecRow As Long
    With dbg
        .BeginUpdate
        For RecRow = 1 To .RowCount
            '1 :Realtime Scanlog
            If .CellValue(RecRow, 2) = status0 Then
                .CellCheckState(RecRow, 2) = igCheckStateChecked
            End If
            
            '2 :Auto Sinkron User
            If .CellValue(RecRow, 3) = status0 Then
                .CellCheckState(RecRow, 3) = igCheckStateChecked
            End If
            
            '3 :Auto Sinkron Tanggal Jam Mesin
            If .CellValue(RecRow, 4) = status0 Then
                .CellCheckState(RecRow, 4) = igCheckStateChecked
            End If
            
            '4 :SMS
            If .CellValue(RecRow, 5) = status0 Then
                .CellCheckState(RecRow, 5) = igCheckStateChecked
            End If
            
            DoEvents
        Next RecRow
        .EndUpdate
    End With
    DoEvents
        
    Exit Sub
Err1:
    'MsgBox Err.Description
    Err.Clear
    Resume Next
End Sub
