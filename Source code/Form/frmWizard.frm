VERSION 5.00
Object = "{86683A0C-A3FA-4412-B044-51B918DBEE5C}#1.0#0"; "RealSvrOcxTcp.ocx"
Object = "{A8E5842E-102B-4289-9D57-3B3F5B5E15D3}#18.3#0"; "Codejock.Controls.Unicode.v18.3.0.ocx"
Object = "{D2BBCEE2-EBE7-4DA2-8B98-2E5F1ECA275B}#1.0#0"; "iGrid650_10Tec.ocx"
Object = "{48E59290-9880-11CF-9754-00AA00C00908}#1.0#0"; "MSINET.ocx"
Begin VB.Form frmWizard 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "FingerspotOne UareU"
   ClientHeight    =   9840
   ClientLeft      =   45
   ClientTop       =   390
   ClientWidth     =   14415
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmWizard.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   9840
   ScaleWidth      =   14415
   StartUpPosition =   2  'CenterScreen
   Begin VB.PictureBox Picture1 
      Appearance      =   0  'Flat
      BackColor       =   &H00FFC0C0&
      FillColor       =   &H00E0E0E0&
      ForeColor       =   &H80000008&
      Height          =   5055
      Left            =   -1800
      ScaleHeight     =   5025
      ScaleWidth      =   6945
      TabIndex        =   8
      Top             =   2880
      Visible         =   0   'False
      Width           =   6975
      Begin VB.PictureBox Picture2 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         Enabled         =   0   'False
         ForeColor       =   &H80000008&
         Height          =   400
         Left            =   4440
         ScaleHeight     =   375
         ScaleWidth      =   2145
         TabIndex        =   14
         Top             =   3000
         Width           =   2175
         Begin XtremeSuiteControls.CheckBox chb 
            Height          =   255
            Index           =   0
            Left            =   240
            TabIndex        =   15
            Top             =   60
            Width           =   255
            _Version        =   1179651
            _ExtentX        =   450
            _ExtentY        =   450
            _StockProps     =   79
            BackColor       =   16777215
            Appearance      =   1
         End
         Begin XtremeSuiteControls.CheckBox chb 
            Height          =   255
            Index           =   1
            Left            =   720
            TabIndex        =   16
            Top             =   60
            Width           =   255
            _Version        =   1179651
            _ExtentX        =   450
            _ExtentY        =   450
            _StockProps     =   79
            BackColor       =   16777215
            Appearance      =   1
         End
         Begin XtremeSuiteControls.CheckBox chb 
            Height          =   255
            Index           =   2
            Left            =   1200
            TabIndex        =   17
            Top             =   60
            Width           =   255
            _Version        =   1179651
            _ExtentX        =   450
            _ExtentY        =   450
            _StockProps     =   79
            BackColor       =   16777215
            Appearance      =   1
         End
         Begin XtremeSuiteControls.CheckBox chb 
            Height          =   255
            Index           =   3
            Left            =   1680
            TabIndex        =   18
            Top             =   60
            Width           =   255
            _Version        =   1179651
            _ExtentX        =   450
            _ExtentY        =   450
            _StockProps     =   79
            BackColor       =   16777215
            Appearance      =   1
         End
      End
      Begin VB.PictureBox picSample 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         ForeColor       =   &H80000008&
         Height          =   2535
         Left            =   4440
         ScaleHeight     =   2505
         ScaleWidth      =   2145
         TabIndex        =   13
         Top             =   480
         Width           =   2175
      End
      Begin XtremeSuiteControls.PushButton PushButton 
         Height          =   375
         Index           =   0
         Left            =   4440
         TabIndex        =   12
         Top             =   4440
         Width           =   2175
         _Version        =   1179651
         _ExtentX        =   3836
         _ExtentY        =   661
         _StockProps     =   79
         Caption         =   "0"
         ForeColor       =   0
         BackColor       =   -2147483635
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Appearance      =   12
      End
      Begin XtremeSuiteControls.ComboBox cmb_mesin 
         Height          =   315
         Left            =   240
         TabIndex        =   19
         Tag             =   "AnchorCenterHRight"
         Top             =   480
         Width           =   2820
         _Version        =   1179651
         _ExtentX        =   4974
         _ExtentY        =   556
         _StockProps     =   77
         ForeColor       =   0
         BackColor       =   16777215
         Style           =   2
         Appearance      =   2
         UseVisualStyle  =   0   'False
      End
      Begin XtremeSuiteControls.PushButton PushButton 
         Height          =   375
         Index           =   1
         Left            =   240
         TabIndex        =   22
         Top             =   4440
         Width           =   2175
         _Version        =   1179651
         _ExtentX        =   3836
         _ExtentY        =   661
         _StockProps     =   79
         Caption         =   "1"
         ForeColor       =   0
         BackColor       =   -2147483635
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Enabled         =   0   'False
         Appearance      =   12
      End
      Begin XtremeSuiteControls.GroupBox GroupBox1TT 
         Height          =   450
         Index           =   1
         Left            =   240
         TabIndex        =   28
         Top             =   3720
         Width           =   6375
         _Version        =   1179651
         _ExtentX        =   11245
         _ExtentY        =   794
         _StockProps     =   79
         BackColor       =   16777215
         UseVisualStyle  =   -1  'True
         BorderStyle     =   2
         Begin XtremeSuiteControls.Label lbl 
            Height          =   195
            Index           =   8
            Left            =   120
            TabIndex        =   29
            Top             =   120
            Width           =   120
            _Version        =   1179651
            _ExtentX        =   212
            _ExtentY        =   344
            _StockProps     =   79
            Caption         =   "8"
            BackColor       =   16777215
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            AutoSize        =   -1  'True
         End
      End
      Begin XtremeSuiteControls.Label lbl 
         Height          =   195
         Index           =   9
         Left            =   240
         TabIndex        =   40
         Top             =   3240
         Width           =   120
         _Version        =   1179651
         _ExtentX        =   212
         _ExtentY        =   344
         _StockProps     =   79
         Caption         =   "9"
         ForeColor       =   16711680
         BackColor       =   16761024
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         AutoSize        =   -1  'True
      End
      Begin XtremeSuiteControls.Label lbl 
         Height          =   195
         Index           =   7
         Left            =   240
         TabIndex        =   27
         Top             =   2880
         Width           =   90
         _Version        =   1179651
         _ExtentX        =   159
         _ExtentY        =   344
         _StockProps     =   79
         Caption         =   "7"
         BackColor       =   16761024
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         AutoSize        =   -1  'True
      End
      Begin XtremeSuiteControls.Label lbl 
         Height          =   195
         Index           =   6
         Left            =   240
         TabIndex        =   26
         Top             =   2520
         Width           =   90
         _Version        =   1179651
         _ExtentX        =   159
         _ExtentY        =   344
         _StockProps     =   79
         Caption         =   "6"
         BackColor       =   16761024
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         AutoSize        =   -1  'True
      End
      Begin XtremeSuiteControls.Label lbl 
         Height          =   195
         Index           =   5
         Left            =   240
         TabIndex        =   25
         Top             =   2160
         Width           =   90
         _Version        =   1179651
         _ExtentX        =   159
         _ExtentY        =   344
         _StockProps     =   79
         Caption         =   "5"
         BackColor       =   16761024
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         AutoSize        =   -1  'True
      End
      Begin XtremeSuiteControls.Label lbl 
         Height          =   195
         Index           =   4
         Left            =   240
         TabIndex        =   24
         Top             =   1800
         Width           =   90
         _Version        =   1179651
         _ExtentX        =   159
         _ExtentY        =   344
         _StockProps     =   79
         Caption         =   "4"
         BackColor       =   16761024
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         AutoSize        =   -1  'True
      End
      Begin XtremeSuiteControls.Label lbl 
         Height          =   195
         Index           =   3
         Left            =   240
         TabIndex        =   23
         Top             =   1440
         Width           =   90
         _Version        =   1179651
         _ExtentX        =   159
         _ExtentY        =   344
         _StockProps     =   79
         Caption         =   "3"
         BackColor       =   16761024
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         AutoSize        =   -1  'True
      End
      Begin XtremeSuiteControls.Label lbl 
         Height          =   195
         Index           =   2
         Left            =   240
         TabIndex        =   21
         Top             =   960
         Width           =   90
         _Version        =   1179651
         _ExtentX        =   159
         _ExtentY        =   344
         _StockProps     =   79
         Caption         =   "2"
         BackColor       =   16761024
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         AutoSize        =   -1  'True
      End
      Begin XtremeSuiteControls.Label lbl 
         Height          =   195
         Index           =   1
         Left            =   240
         TabIndex        =   20
         Top             =   240
         Width           =   120
         _Version        =   1179651
         _ExtentX        =   212
         _ExtentY        =   344
         _StockProps     =   79
         Caption         =   "1"
         BackColor       =   16761024
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         AutoSize        =   -1  'True
      End
   End
   Begin RealSvrOcxTcpLib.RealSvrOcxTcp FKRealSvrTcp 
      Height          =   480
      Left            =   12960
      TabIndex        =   1
      Top             =   -480
      Width           =   480
      _Version        =   65536
      _ExtentX        =   847
      _ExtentY        =   847
      _StockProps     =   0
   End
   Begin XtremeSuiteControls.Resizer Resizer 
      Height          =   9855
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   14535
      _Version        =   1179651
      _ExtentX        =   25638
      _ExtentY        =   17383
      _StockProps     =   1
      BackColor       =   14737632
      ControlCount    =   5
      Control(0).Caption=   "TabDetail"
      Control(0).Width=   100
      Control(0).Height=   100
      Control(1).Caption=   "Picture1"
      Control(1).Y    =   100
      Control(1).Width=   100
      Control(2).Caption=   "Picture2"
      Control(2).Height=   100
      Control(3).Caption=   "PushButton1(0)"
      Control(3).X    =   100
      Control(4).Caption=   "PushButton1(1)"
      Control(4).X    =   100
      Begin XtremeSuiteControls.TabControl sub_TabDetail 
         Height          =   3480
         Left            =   5640
         TabIndex        =   30
         Top             =   6240
         Width           =   8535
         _Version        =   1179651
         _ExtentX        =   15055
         _ExtentY        =   6138
         _StockProps     =   68
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Appearance      =   4
         Color           =   2
         PaintManager.FixedTabWidth=   300
         PaintManager.ButtonMargin=   "10,5,10,5"
         ItemCount       =   2
         Item(0).Caption =   "0"
         Item(0).ControlCount=   1
         Item(0).Control(0)=   "sub_tdp(0)"
         Item(1).Caption =   "1"
         Item(1).ControlCount=   2
         Item(1).Control(0)=   "sub_tdp(1)"
         Item(1).Control(1)=   "sub_tdp(5)"
         Begin XtremeSuiteControls.TabControlPage sub_tdp 
            Height          =   2955
            Index           =   0
            Left            =   30
            TabIndex        =   31
            Top             =   495
            Width           =   8475
            _Version        =   1179651
            _ExtentX        =   14949
            _ExtentY        =   5212
            _StockProps     =   1
            BackColor       =   16777215
            Page            =   0
            Begin VB.TextBox txtIsi2 
               Appearance      =   0  'Flat
               BackColor       =   &H8000000F&
               BeginProperty Font 
                  Name            =   "Segoe UI"
                  Size            =   9
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H00000000&
               Height          =   2205
               Left            =   240
               Locked          =   -1  'True
               MultiLine       =   -1  'True
               ScrollBars      =   2  'Vertical
               TabIndex        =   36
               Top             =   240
               Visible         =   0   'False
               Width           =   6015
            End
            Begin VB.PictureBox picSampleVer 
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               ForeColor       =   &H80000008&
               Height          =   2610
               Left            =   5880
               ScaleHeight     =   2580
               ScaleWidth      =   2265
               TabIndex        =   34
               Top             =   0
               Visible         =   0   'False
               Width           =   2295
            End
            Begin XtremeSuiteControls.ListBox ListBox 
               Height          =   2925
               Left            =   0
               TabIndex        =   35
               Top             =   0
               Width           =   8175
               _Version        =   1179651
               _ExtentX        =   14420
               _ExtentY        =   5159
               _StockProps     =   77
               ForeColor       =   -2147483640
               BackColor       =   -2147483643
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Segoe UI"
                  Size            =   9
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Appearance      =   5
               UseVisualStyle  =   0   'False
            End
         End
         Begin XtremeSuiteControls.TabControlPage sub_tdp 
            Height          =   2955
            Index           =   1
            Left            =   -69970
            TabIndex        =   32
            Top             =   495
            Visible         =   0   'False
            Width           =   8475
            _Version        =   1179651
            _ExtentX        =   14949
            _ExtentY        =   5212
            _StockProps     =   1
            BackColor       =   16777215
            Page            =   1
            Begin iGrid650_10Tec.iGrid dbg_scanlog 
               Height          =   2925
               Left            =   0
               TabIndex        =   37
               Tag             =   "AlignLeft"
               Top             =   0
               Width           =   8355
               _ExtentX        =   14737
               _ExtentY        =   5159
               BorderType      =   1
               Editable        =   0   'False
               FlatCellControls=   -1  'True
               ForeColor       =   4210752
               FullRowSelect   =   -1  'True
               GridLineColor   =   12632256
            End
            Begin XtremeSuiteControls.Label lbl_row_count 
               Height          =   225
               Index           =   1
               Left            =   0
               TabIndex        =   38
               Tag             =   "AnchorCenterHLeft"
               Top             =   0
               Width           =   2430
               _Version        =   1179651
               _ExtentX        =   4286
               _ExtentY        =   397
               _StockProps     =   79
               Caption         =   "lbl_row_count"
               ForeColor       =   4210752
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Segoe UI"
                  Size            =   9
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Alignment       =   1
               Transparent     =   -1  'True
            End
         End
         Begin XtremeSuiteControls.TabControlPage sub_tdp 
            Height          =   2955
            Index           =   5
            Left            =   -69970
            TabIndex        =   33
            Top             =   495
            Visible         =   0   'False
            Width           =   8475
            _Version        =   1179651
            _ExtentX        =   14949
            _ExtentY        =   5212
            _StockProps     =   1
            BackColor       =   -2147483643
            Page            =   2
         End
      End
      Begin iGrid650_10Tec.iGrid dbg 
         Height          =   3885
         Left            =   5640
         TabIndex        =   6
         Tag             =   "AlignLeft"
         Top             =   1920
         Width           =   8535
         _ExtentX        =   15055
         _ExtentY        =   6853
         AllowSorting    =   0   'False
         BorderType      =   1
         Editable        =   0   'False
         FlatCellControls=   -1  'True
         ForeColor       =   4210752
         GridLineColor   =   12632256
      End
      Begin XtremeSuiteControls.FlatEdit edtSearch 
         Height          =   375
         Index           =   2
         Left            =   240
         TabIndex        =   2
         Top             =   240
         Width           =   3285
         _Version        =   1179651
         _ExtentX        =   5794
         _ExtentY        =   661
         _StockProps     =   77
         ForeColor       =   8421504
         BackColor       =   16777215
         Text            =   "Cari"
         BackColor       =   16777215
      End
      Begin XtremeSuiteControls.PushButton btnSearch 
         Height          =   375
         Index           =   2
         Left            =   3600
         TabIndex        =   3
         Top             =   240
         Width           =   1215
         _Version        =   1179651
         _ExtentX        =   2143
         _ExtentY        =   661
         _StockProps     =   79
         Caption         =   "PushButton2"
         Enabled         =   0   'False
         Appearance      =   12
      End
      Begin XtremeSuiteControls.TreeView Tree 
         Height          =   8970
         Index           =   2
         Left            =   240
         TabIndex        =   4
         Top             =   720
         Width           =   5115
         _Version        =   1179651
         _ExtentX        =   9022
         _ExtentY        =   15822
         _StockProps     =   77
         ForeColor       =   4473924
         BackColor       =   16777215
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Checkboxes      =   -1  'True
         BackColor       =   16777215
         ForeColor       =   4473924
         Appearance      =   12
         IconSize        =   16
         SelectionBackColor=   14925219
         SelectionForeColor=   4473924
      End
      Begin XtremeSuiteControls.GroupBox GroupBox1TT 
         Height          =   1215
         Index           =   0
         Left            =   5640
         TabIndex        =   9
         Top             =   240
         Width           =   8535
         _Version        =   1179651
         _ExtentX        =   15055
         _ExtentY        =   2143
         _StockProps     =   79
         BackColor       =   16777215
         UseVisualStyle  =   -1  'True
         BorderStyle     =   2
         Begin VB.PictureBox Picture1TT 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   0  'None
            ForeColor       =   &H80000008&
            Height          =   615
            Index           =   0
            Left            =   90
            ScaleHeight     =   41
            ScaleMode       =   0  'User
            ScaleWidth      =   32
            TabIndex        =   10
            Top             =   105
            Width           =   615
         End
         Begin XtremeSuiteControls.Label lbl 
            Height          =   1035
            Index           =   0
            Left            =   840
            TabIndex        =   11
            Tag             =   "AlignTop"
            Top             =   120
            Width           =   7620
            _Version        =   1179651
            _ExtentX        =   13441
            _ExtentY        =   1826
            _StockProps     =   79
            Caption         =   "0"
            ForeColor       =   0
            BackColor       =   8421504
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Alignment       =   4
            Transparent     =   -1  'True
            WordWrap        =   -1  'True
         End
      End
      Begin XtremeSuiteControls.PushButton btn_refresh 
         Height          =   375
         Index           =   0
         Left            =   5000
         TabIndex        =   39
         Top             =   240
         Width           =   375
         _Version        =   1179651
         _ExtentX        =   661
         _ExtentY        =   661
         _StockProps     =   79
         ForeColor       =   0
         BackColor       =   14737632
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Appearance      =   2
         Picture         =   "frmWizard.frx":058A
         BorderGap       =   0
      End
      Begin XtremeSuiteControls.Label lbl_row_count 
         Height          =   225
         Index           =   0
         Left            =   11640
         TabIndex        =   7
         Tag             =   "AnchorCenterHLeft"
         Top             =   5880
         Width           =   2430
         _Version        =   1179651
         _ExtentX        =   4286
         _ExtentY        =   397
         _StockProps     =   79
         Caption         =   "lbl_row_count"
         ForeColor       =   4210752
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Segoe UI"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Alignment       =   1
         Transparent     =   -1  'True
      End
      Begin XtremeSuiteControls.Label LabelTitle 
         Height          =   300
         Index           =   0
         Left            =   5640
         TabIndex        =   5
         Top             =   1560
         Width           =   165
         _Version        =   1179651
         _ExtentX        =   291
         _ExtentY        =   529
         _StockProps     =   79
         Caption         =   "0"
         ForeColor       =   16711680
         BackColor       =   14737632
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         AutoSize        =   -1  'True
      End
   End
   Begin InetCtlsObjects.Inet Inet1 
      Left            =   13320
      Top             =   0
      _ExtentX        =   1005
      _ExtentY        =   1005
      _Version        =   393216
   End
   Begin XtremeSuiteControls.TrayIcon TrayIcon 
      Left            =   360
      Top             =   0
      _Version        =   1179651
      _ExtentX        =   423
      _ExtentY        =   423
      _StockProps     =   16
      Text            =   "FingerspotOne UareU"
      Picture         =   "frmWizard.frx":0B24
   End
   Begin XtremeSuiteControls.CommonDialog CommonDialog 
      Left            =   0
      Top             =   0
      _Version        =   1179651
      _ExtentX        =   423
      _ExtentY        =   423
      _StockProps     =   4
   End
   Begin VB.Menu mnuTray 
      Caption         =   "mnuTray"
      Visible         =   0   'False
      Begin VB.Menu mnuTrayShow 
         Caption         =   "Hide/Show"
      End
      Begin VB.Menu mnuSep 
         Caption         =   "-"
      End
      Begin VB.Menu mnuExit 
         Caption         =   "Exit"
      End
   End
End
Attribute VB_Name = "frmWizard"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private WithEvents FPReg As FlexCodeSDK_FTM.FinFPReg
Attribute FPReg.VB_VarHelpID = -1
Private WithEvents FPVer As FlexCodeSDK_FTM.FinFPVer
Attribute FPVer.VB_VarHelpID = -1

Private sn_device() As String
Private name_device() As String
Private ACode() As String
Private VCode() As String
    
Private CommandReg As Integer
Private RowTemp As Long
Private PinReg As String
Private getFPTemplate As String
Private getSamples As Integer
Private regDstop As Boolean
Private regUlang As Boolean
Private ModeReg As Boolean

Private CommandVer As Integer
Private PinVer As String
Private DeviceSNVer As String
Private indexSearch(10) As Long
Private mfilter1 As String
Private mfilter2 As String

Private Status0 As String
Private Status1 As String

Private PosTopNow As Long
Private PosLeftNow As Long
Private PosTopReg As Long
Private PosLeftReg As Long
       
Public CloseForm As Boolean
Public Minimized As Boolean

Private Sub RegVisible(visi As Boolean)
    Picture1.Visible = visi
    Resizer.Visible = Not visi
    lbl(8).Caption = ""
    getFPTemplate = ""
    cmb_mesin.Text = ""
    
    chb(0).value = xtpUnchecked
    chb(1).value = xtpUnchecked
    chb(2).value = xtpUnchecked
    chb(3).value = xtpUnchecked
    picSample = LoadPicture("")
    
    If visi = False Then
        ModeReg = False
        Me.BorderStyle = vbFixedSingle
        Me.Height = 10275
        Me.Width = 14505
        Me.Top = PosTopNow
        Me.Left = PosLeftNow
    Else
        ModeReg = True
        Me.BorderStyle = vbBSNone
        Me.Height = 5535
        Me.Width = 6975
        Me.Top = PosTopReg
        Me.Left = PosLeftReg
    End If
End Sub

Private Sub ButtonEnabled(Ena As Boolean)
    'Button(0).Enabled = Ena
End Sub

Private Sub btn_refresh_Click(index As Integer)
    Select Case index
        Case 0
            SetTreeEmployee "", ""
            DoEvents
            
            If Tree(2).Nodes.Count > 0 Then
                Tree_NodeClick 2, Tree(2).Nodes(1)
            End If
    End Select
End Sub

Private Sub btnSearch_Click(index As Integer)
    edtSearch_KeyPress index, 13
End Sub

Private Sub cmb_mesin_Click()
    If Trim(cmb_mesin.Text) <> "" Then
        lbl(2).Caption = INILanRead(Me.Name, "lbl.2", "", name_device(cmb_mesin.ListIndex), "", "")
        PushButton(1).Enabled = True
    End If
End Sub

Private Sub edtSearch_GotFocus(index As Integer)
    If edtSearch(index).Text = edtSearch(index).Tag Then
        edtSearch(index).Text = ""
        edtSearch(index).ForeColor = &H0&
    End If
End Sub

Private Sub edtSearch_KeyPress(index As Integer, KeyAscii As Integer)
    If KeyAscii <> 13 Then
        btnSearch(index).Enabled = True
        Exit Sub
    End If
    
    edtSearch(index).Text = Trim(edtSearch(index).Text)
    If edtSearch(index).Text = "" Then
        If indexSearch(index) > 1 Then Tree(index).Nodes(indexSearch(index) - 1).Text = Replace(Tree(index).Nodes(indexSearch(index) - 1).Text, "Blue", "Black")
        indexSearch(index) = 0
    End If
    If edtSearch(index).Text = "" Then Exit Sub
    
    Dim i As Integer
    Dim j As Integer
    Dim k As Boolean
    Dim fields() As String
    j = Tree(index).Nodes.Count
    
    If indexSearch(index) = 0 Then indexSearch(index) = 1
    If indexSearch(index) > 1 Then Tree(index).Nodes(indexSearch(index) - 1).Text = Replace(Tree(index).Nodes(indexSearch(index) - 1).Text, "Blue", "Black")
    
    k = False
    For i = indexSearch(index) To j
        fields() = Split(Tree(index).Nodes(i).Tag, "-")
        If InStr(1, UCase(fields(2)), UCase(edtSearch(index).Text)) > 0 Or InStr(1, UCase(fields(3)), UCase(edtSearch(index).Text)) > 0 Then
            Tree(index).Nodes(i).Selected = True
            Tree(index).Nodes(i).Expanded = True
            Tree(index).Nodes(i).Text = Replace(Tree(index).Nodes(i).Text, "Black", "Blue")
            indexSearch(index) = i + 1
            k = True
            Exit For
        End If
    Next
    
    If Not k Then
        indexSearch(index) = 0
        MsgBoxLanguageFile "Err2"
    End If
End Sub

Private Sub edtSearch_LostFocus(index As Integer)
    edtSearch(index).Text = Trim(edtSearch(index).Text)
    If edtSearch(index).Text = "" Then
        edtSearch(index).Text = edtSearch(index).Tag
        edtSearch(index).ForeColor = &H808080
        If indexSearch(index) > 1 Then Tree(index).Nodes(indexSearch(index) - 1).Text = Replace(Tree(index).Nodes(indexSearch(index) - 1).Text, "Blue", "Black")
            indexSearch(index) = 0
    End If
End Sub

Private Sub Form_Unload(Cancel As Integer)
    On Error GoTo Err1
    
    If Not CloseForm Then
        If ModeReg = True Then
            Cancel = 1
            PushButton_Click 0
            Exit Sub
        End If
        
        MinimizeToTray
        Cancel = 1
        
        SetToolTip "FingerspotOne UareU"
        ShowMessage 0, "FingerspotOne UareU", "FingerspotOne", 1
    Else
        If CommandReg <> 0 Then
            FPReg.FPRegistrationStop
            CommandReg = 0
        End If
        DoEvents
        
        If CommandVer <> 0 Then
            FPVer.FPVerificationStop
            CommandVer = 0
        End If
        DoEvents
        
        FrmUnload
    End If
    
    Exit Sub
Err1:
    Err.Clear
    Resume Next
End Sub

Private Sub AddFP_template()
    On Error GoTo Err1
    
    Dim rs As cRecordset
    Dim i As Long
    Dim pin As String
    Dim template_index As Integer
    Dim template_text As String
    
    If CommandVer <> 0 Then
        FPVer.FPVerificationStop
        CommandVer = 0
    End If
    DoEvents
    
    FPVer.FPListClear
    DoEvents
    
    strSQL = "select * from t_template_uru " & _
        "Order By pin "
    Set rs = getRecordSet_Data(strSQL, True)
    If rs.RecordCount > 0 Then
        Do While Not rs.EOF
            pin = rs.fields("pin").value
            template_index = rs.fields("template_index").value
            template_text = rs.fields("template_text").value
            
            FPVer.FPLoad pin, template_index, template_text, "4JJ 1"
            DoEvents
            
            rs.MoveNext
        Loop
    End If
    
    If CommandVer = 0 Then
        CommandVer = 1
        FPVer.WorkingInBackground True
        FPVer.FPVerificationStart
    End If
    DoEvents
    
    Exit Sub
Err1:
    Err.Clear
    Resume Next
End Sub

'Verifikasi
Private Sub FPVer_FPVerificationID(ByVal ID As String, ByVal FingerNr As FlexCodeSDK_FTM.FingerNumber)
    Dim str1 As String
    
    PinVer = ID
    str1 = INILanRead("errMsg", "msg4", "", ID, DateTimeToStr_DB(Now, "dd-MM-yyyy HH:mm:ss"), "")
    TulisLog str1
    DoEvents
    
    SetToolTip "FingerspotOne UareU"
    ShowMessage 1, str1, "FingerspotOne UareU", 1
    DoEvents
End Sub

Private Sub FPVer_FPVerificationImage()
    picSampleVer = LoadPicture(App.Path & "\FPTemp.BMP")
End Sub

Private Sub FPVer_FPDeviceSN(DeviceSN As String)
    DeviceSNVer = DeviceSN
End Sub

Private Sub FPVer_FPVerificationStatus(ByVal status As FlexCodeSDK_FTM.VerificationStatus)
    Dim gagal As Boolean
    
    gagal = False
    Select Case status
        Case v_ActivationIncorrect
            TulisLog INILanRead("General", "ProsesVer0", "", "", "", "") '"Activation / verification code is incorrent or not set"
            '"Start Verify"
            gagal = True
            CommandVer = 0
            
        Case v_FPListEmpty
            TulisLog INILanRead("General", "ProsesVer1", "", "", "", "") '"Please add some templates"
            '"Start Verify"
            gagal = True
            CommandVer = 0
            
        Case v_FPListFull
            TulisLog INILanRead("General", "ProsesVer2", "", "", "", "") '"Max 2000 templates"
            
        Case v_FPDevFull
            TulisLog INILanRead("General", "ProsesVer3", "", "", "", "") '"Max 10 devices"
            
        Case v_MultiplelMatch
            TulisLog INILanRead("General", "ProsesVer4", "", "", "", "") '"Multiple match"
            
        Case v_NoDevice
            TulisLog INILanRead("General", "ProsesVer5", "", "", "", "") '"Please connect the device to USB port"
            '"Start Verify"
            gagal = True
            CommandVer = 0
            
        Case v_NotMatch
            TulisLog INILanRead("General", "ProsesVer6", "", "", "", "") '"Not match"
            
        Case v_OK
            'TulisLog INILanRead("General", "ProsesVer7", "", "", "", "") '"Match"
            InsertScanLog PinVer, Now, DeviceSNVer
            
            PinVer = ""
            DeviceSNVer = ""
            
        Case v_PoorImageQuality
            TulisLog INILanRead("General", "ProsesVer8", "", "", "", "") '"Poor image quality"
            
        Case v_VerificationFailed
            TulisLog INILanRead("General", "ProsesVer9", "", "", "", "") '"Verification fail"
            
        Case v_VerifyCaptureFingerTouch
            'TulisLog INILanRead("General", "ProsesVer10", "", "", "", "") '"Finger touch"
            
        Case v_VerifyCaptureStop
            TulisLog INILanRead("General", "ProsesVer11", "", "", "", "") '"Stop Verify"
            '"Start Verify"
            gagal = True
            CommandVer = 0
    End Select
    DoEvents
  
    If gagal = True Then
        If CommandVer = 0 Then
            CommandVer = 1
            FPVer.WorkingInBackground True
            FPVer.FPVerificationStart
        End If
    End If
End Sub

Private Sub InsertScanLog(pin As String, scan_datetime As Date, SN As String)
    On Error GoTo Err1
    
    Dim last_update_date As String
    Dim dateStr As String
    Dim tglStr As String
    
    pin = Trim(pin)
    last_update_date = DateTimeToStr_DB(Now, DateTimeFormatDatabase)
    
    dateStr = DateTimeToStr_DB(scan_datetime, DateTimeFormatDatabase)
    dateStr = Mid(dateStr, 1, 17) & "00"
    
    tglStr = DateTimeToStr_DB(scan_datetime, DateFormatDatabase)
    
    strSQL = "REPLACE INTO t_att_log (scan_date, pin, sn, verify_mode, inout_mode, " & _
        "inout_mode2, ip_public, ip_device, id_device, port_device) " & _
        "VALUES ('" & dateStr & "', '" & pin & "', '" & SN & "', 0, 0, 0, '', '', 0, 0) "
    ExecuteSQLite_Data strSQL
    DoEvents
    
    Dim rs As cRecordset
    Dim nik As String
    Dim nama As String
    Dim jabatan As String
    Dim kantor As String
    Dim str1 As String
    Dim reg As Integer
    
    Set rs = QueryDataEmployee3(pin)
    Do While Not rs.EOF
        pin = rs.fields("pin").value
        nik = rs.fields("nik").value
        nama = rs.fields("name_employee").value
        jabatan = rs.fields("name_position").value
        kantor = rs.fields("name_office").value
        reg = rs.fields("reg").value
        
        str1 = DateTimeToStr_DB(Now, "dd-MM-yyyy HH:mm:ss") & ";" & pin & ";" & nik & ";" & nama & ";" & jabatan & ";" & kantor & ";"
        InsertGrid dbg_scanlog, dbg_scanlog.RowCount + 1, lbl_row_count(1), "", False, &H0&, &H404040, False, Null, Null, "", str1
            
        DoEvents
        rs.MoveNext
    Loop
    
    dbg_scanlog.SortObject.Clear
    dbg_scanlog.SortObject.AddItem 1, igSortDesc, igSortByCellTextUseCase
    dbg_scanlog.Sort
    CellBackColorAfterSort dbg_scanlog
    
    If dbg_scanlog.RowCount > 100 Then
        dbg_scanlog.RemoveRow dbg_scanlog.RowCount, 1
    End If
    DoEvents
    
    Exit Sub
Err1:
    Err.Clear
    Resume Next
End Sub

Private Sub LoadAwal()
    On Error GoTo Err1
    
    Set FPReg = New FlexCodeSDK_FTM.FinFPReg
    FPReg.PictureSamplePath = App.Path & "\FPTemp.BMP"
    FPReg.PictureSampleHeight = picSample.Height
    FPReg.PictureSampleWidth = picSample.Width
    DoEvents
    
    Set FPVer = New FlexCodeSDK_FTM.FinFPVer
    FPVer.PictureSamplePath = App.Path & "\FPTemp.BMP"
    FPVer.PictureSampleHeight = picSample.Height
    FPVer.PictureSampleWidth = picSample.Width
    DoEvents
    
    Status0 = INILanRead("General", "Status0", "", "", "", "")
    Status1 = INILanRead("General", "Status1", "", "0", "", "")
    DoEvents
    
    Picture1.Top = 0
    Picture1.Left = 0
    DoEvents
        
    Picture1TT(0).Picture = LoadPicture(App.Path & "\Img\help_button.bmp")
    DoEvents
    
    edtSearch(2).Tag = edtSearch(2).Text
    
    PushButton(1).Enabled = False
    RowTemp = 0
    PinReg = ""
    PinVer = ""
    DeviceSNVer = ""
    regDstop = True
    regUlang = False
    ModeReg = False
    PushButton(1).ForeColor = vbBlue
    
    CommandVer = 0
    CommandReg = 0
    DoEvents
    
    Dim rs As cRecordset
    Dim i As Integer
    Dim str1 As String
    
    i = 0
    cmb_mesin.Clear
    strSQL = "select * from t_device where comm_type = 5 " & _
        "Order By sn_device, name_device "
    Set rs = getRecordSet_Data(strSQL, True)
    If rs.RecordCount > 0 Then
        ReDim sn_device(rs.RecordCount)
        ReDim name_device(rs.RecordCount)
        ReDim ACode(rs.RecordCount)
        ReDim VCode(rs.RecordCount)
        
        Do While Not rs.EOF
            sn_device(i) = rs.fields("sn_device").value
            name_device(i) = rs.fields("name_device").value
            ACode(i) = rs.fields("activation_code").value
            VCode(i) = rs.fields("device_key_code").value
            
            cmb_mesin.AddItem sn_device(i), i, sn_device(i)
            i = i + 1
            DoEvents
            
            If Not FPVer.AddDeviceInfo(rs.fields("sn_device").value, rs.fields("device_key_code").value, rs.fields("activation_code").value) Then
                str1 = INILanRead("errMsg", "msg3", "", sn_device(i), "", "")
                TulisLog str1
            End If
            DoEvents
            
            rs.MoveNext
        Loop
    End If
    
    AddFP_template
    DoEvents
    
    Exit Sub
Err1:
    Err.Clear
    Resume Next
End Sub
    
Private Sub Form_Load()
    On Error GoTo Err1
    
    ChangeFont Me
    DoEvents
    
    SetLang Me
    DoEvents
    
    sub_TabDetail.ItemCount = 2
    sub_TabDetail.Item(0).Selected = True
    sub_TabDetail.EnableMarkup = True
    sub_TabDetail(0).Caption = "<TextBlock><TextBlock HorizontalAlignment='Center' VerticalAlignment='Center' Margin='0,0,0,0'></TextBlock>" & _
        "<TextBlock HorizontalAlignment='Center' VerticalAlignment='Center'>" & INILanRead(Me.Name, "sub_TabDetail.0", "") & "</TextBlock></TextBlock>"
    sub_TabDetail(1).Caption = "<TextBlock><TextBlock HorizontalAlignment='Center' VerticalAlignment='Center' Margin='0,0,0,0'></TextBlock>" & _
        "<TextBlock HorizontalAlignment='Center' VerticalAlignment='Center'>" & INILanRead(Me.Name, "sub_TabDetail.1", "") & "</TextBlock></TextBlock>"
    DoEvents
    
    ListBox.Left = 0
    ListBox.Top = 0
    ListBox.Height = sub_tdp(0).Height
    ListBox.Width = sub_tdp(0).Width
    DoEvents
    
    dbg_scanlog.Left = 0
    dbg_scanlog.Top = 0
    dbg_scanlog.Height = sub_tdp(1).Height
    dbg_scanlog.Width = sub_tdp(1).Width
    DoEvents
    
    LoadAwal
    DoEvents
    
    Dim ComboFieldID() As String
    Dim ComboFieldName() As String
    Dim ComboSQL() As String
    
    SetGrid dbg, dbg.Name, Me.Name, False, "", lbl_row_count(0), "", "", 1, ComboFieldID(), ComboFieldName(), ComboSQL()
    DoEvents
    
    SetGrid dbg_scanlog, dbg_scanlog.Name, Me.Name, True, "", lbl_row_count(1), "", "", 1, ComboFieldID(), ComboFieldName(), ComboSQL()
    DoEvents
    
    SetTreeEmployee "", ""
    DoEvents
    
    If Tree(2).Nodes.Count > 0 Then
        Tree_NodeClick 2, Tree(2).Nodes(1)
    End If
    
    Exit Sub
Err1:
    Err.Clear
    Resume Next
End Sub

Public Sub ClearTree(TheTree As XtremeSuiteControls.TreeView)
    Dim i As Integer
    Dim j As Integer
    j = TheTree.Nodes.Count
    For i = j To 1 Step -1
       TheTree.Nodes.Remove i
    Next
End Sub

Public Sub SetTreeEmployee(SQLWhere1 As String, SQLWhere2 As String)
    Dim strSQL1 As String
    Dim strSQL2 As String
    Dim rs As cRecordset
    Dim rsE As cRecordset
    Dim strUserOffice1 As String
    Dim strUserOffice2 As String
    Dim strUserEmp1 As String
    Dim strUserEmp2 As String
    
    ClearTree Tree(2)

    Tree(2).Appearance = xtpAppearanceOffice2013
    Tree(2).EnableMarkup = True
    Tree(2).Checkboxes = False 'True
    Tree(2).Icons.LoadBitmap App.Path & "\images\icon_classtree7.png", Array(1, 2, 3, 4, 5, 6, 7, 8), xtpImageNormal
    
    If UserID <> vbNullString Then
        If id_data_privilege_login <> 1 Then
            strUserOffice1 = " AND id_structure in (SELECT id_structure FROM t_data_privilege_detail " & _
            " WHERE id_data_privilege in (SELECT id_data_privilege FROM t_user WHERE name_user = '" & name_user_login & "')) "
            strUserOffice2 = " AND b.id_structure in (SELECT id_structure FROM t_data_privilege_detail " & _
            " WHERE id_data_privilege in (SELECT id_data_privilege FROM t_user WHERE name_user = '" & name_user_login & "')) "
            
            strUserEmp1 = " AND a.id_structure in (SELECT id_structure FROM t_data_privilege_detail " & _
            " WHERE id_data_privilege in (SELECT id_data_privilege FROM t_user WHERE name_user = '" & name_user_login & "')) "
            strUserEmp2 = " AND a.id_structure in (SELECT id_structure FROM t_data_privilege_detail " & _
            " WHERE id_data_privilege in (SELECT id_data_privilege FROM t_user WHERE name_user = '" & name_user_login & "')) "
        Else
            strUserOffice1 = vbNullString
            strUserOffice2 = vbNullString
            strUserEmp1 = vbNullString
            strUserEmp2 = vbNullString
        End If
    Else
        strUserOffice1 = vbNullString
        strUserOffice2 = vbNullString
        strUserEmp1 = vbNullString
        strUserEmp2 = vbNullString
    End If
    
    If SQLWhere1 = vbNullString Then
        If SQLWhere2 = vbNullString Then
            strSQL1 = "SELECT id_office, name_office, parentid_office,parentpath_structure" & _
                " FROM t_office WHERE active = 1 " & _
                strUserOffice1 & _
                " ORDER BY parentpath_structure ASC "
                '" ORDER BY name_office, parentid_office ASC"
                
            strSQL2 = "SELECT a.id_employee, a.name_employee, a.pin, a.id_office,a.parentpath_structure" & _
                " FROM t_employee AS a WHERE a.active=1 " & _
                strUserEmp1 & _
                " ORDER BY a.name_employee ASC"
            
        Else
           strSQL1 = "SELECT a.id_office, b.name_office, b.parentid_office,a.parentpath_structure" & _
                " FROM t_employee as a JOIN t_office as b ON a.id_office = b.id_office" & _
                " WHERE a.active=1 AND a.id_employee IN (SELECT id_employee FROM t_employee_detail WHERE " & SQLWhere2 & ")" & _
                strUserOffice2 & _
                " GROUP BY a.id_office " & _
                " ORDER BY b.parentpath_structure ASC "
                '" ORDER BY b.name_office, b.parentid_office  ASC "
            
            strSQL2 = "SELECT a.id_employee, a.name_employee, a.pin, a.id_office,a.parentpath_structure" & _
                " FROM t_employee AS a WHERE  a.active=1 AND a.id_employee IN (SELECT id_employee FROM t_employee_detail WHERE " & SQLWhere2 & ")" & _
                strUserEmp2 & _
                " ORDER BY a.name_employee ASC"
            
        End If
    Else
        If SQLWhere2 = vbNullString Then
            strSQL1 = "SELECT a.id_office, b.name_office,b.parentid_office, a.parentpath_structure" & _
                " FROM t_employee as a JOIN t_office as b ON a.id_office = b.id_office" & _
                " WHERE  a.active=1 AND a.id_employee IN (SELECT id_employee FROM t_employee WHERE " & SQLWhere1 & ")" & _
                strUserOffice2 & _
                " GROUP BY a.id_office " & _
                " ORDER BY b.parentpath_structure ASC "
                '" ORDER BY b.name_office,b.parentid_office ASC "

            strSQL2 = "SELECT a.id_employee, a.name_employee, a.pin, a.id_office, a.parentpath_structure" & _
                  " FROM t_employee AS a" & _
                  " WHERE  a.active=1 AND a.id_employee IN (SELECT id_employee FROM t_employee WHERE " & SQLWhere1 & ")" & _
                  strUserEmp2 & _
                  " ORDER BY a.name_employee ASC"
        Else
            strSQL1 = "SELECT a.id_office, b.name_office, b.parentid_office, a.parentpath_structure" & _
                " FROM t_employee as a JOIN t_office as b ON a.id_office = b.id_office" & _
                " WHERE  a.active=1 AND a.id_employee IN (SELECT id_employee FROM t_employee WHERE " & SQLWhere1 & ") " & _
                " AND a.id_employee IN (SELECT id_employee FROM t_employee_detail WHERE " & SQLWhere2 & ") " & _
                strUserOffice2 & _
                " GROUP BY a.id_office " & _
                " ORDER BY b.parentpath_structure ASC "
                '" ORDER BY b.name_office, b.parentid_office ASC "

            strSQL2 = "SELECT a.id_employee, a.name_employee, a.pin, a.id_office, a.parentpath_structure" & _
                  " FROM t_employee AS a" & _
                  " WHERE  a.active=1 AND a.id_employee IN (SELECT id_employee FROM t_employee WHERE " & SQLWhere1 & ") " & _
                  " AND a.id_employee IN (SELECT id_employee FROM t_employee_detail WHERE " & SQLWhere2 & ") " & _
                  strUserEmp2 & _
                  " ORDER BY a.name_employee ASC"

        End If
    End If
    
    If SQLWhere1 = vbNullString And SQLWhere2 = vbNullString Then
    Tree(2).Nodes.Add 0, xtpTreeViewChild, 0, _
        "<TextBlock Foreground='Black'>" & INILanRead("General", "ShowAllOfficeOnEmployeeTree", "...") & "</TextBlock>", 3
        Tree(2).Nodes(Tree(2).Nodes.Count).Tag = "O" & "-0-" & INILanRead("General", "ShowAllOfficeOnEmployeeTree", "...") & "-0-0-0-"
    End If
    Set rs = getRecordSet_Data(strSQL1, True)
    Do While Not rs.EOF
        Tree(2).Nodes.Add CStr(rs.fields("parentid_office").value), xtpTreeViewChild, CStr(rs.fields("id_office").value), _
        "<TextBlock Foreground='Black'>" & rs.fields("name_office").value & "</TextBlock>", 3
        Tree(2).Nodes(Tree(2).Nodes.Count).Tag = "O" & "-" & rs.fields("id_office").value & "-" & _
        rs.fields("name_office") & "-0-0-" & rs.fields("parentpath_structure") & "-"
        
        rs.MoveNext
    Loop
    
    Set rsE = getRecordSet_Data(strSQL2, True)
    Do While Not rsE.EOF
        Tree(2).Nodes.Add CStr(rsE.fields("id_office").value), xtpTreeViewChild, "E" & CStr(rsE.fields("id_employee").value), _
        "<TextBlock Foreground='Black'>" & rsE.fields("pin").value & "-" & rsE.fields("name_employee").value & "</TextBlock>", 1
        Tree(2).Nodes(Tree(2).Nodes.Count).Tag = "E" & "-" & rsE.fields("id_employee").value & "-" & _
            rsE.fields("name_employee") & "-" & rsE.fields("pin") & "-" & rsE.fields("id_office") & "-" & rsE.fields("parentpath_structure") & "-"
        rsE.MoveNext
    Loop

    'Clipboard.Clear
    'Clipboard.SetText strSQL2
    'Tree(2).Checkboxes = True
    Dim fields() As String
    
    If SQLWhere1 = vbNullString And SQLWhere2 = vbNullString Then
        If rs.RecordCount > 0 Then
            fields = Split(Tree(2).Nodes(1).Tag, "-")
            Tree(2).Nodes(1).Text = Replace(Tree(2).Nodes(1).Text, fields(2), "<Bold>" & fields(2) & "</Bold>")
            Tree(2).Nodes(1).Expanded = True
            Tree(2).Nodes(1).Selected = True
            LabelTitle(0).Caption = fields(2)
        Else
            LabelTitle(0).Caption = vbNullString
        End If
    Else
        If rs.RecordCount > 0 Then
            Tree(2).SetFocus
            Tree(2).Nodes(1).Selected = True
        End If
    End If
    
    Set rs = Nothing
    Set rsE = Nothing
End Sub

Private Sub PushButton_Click(index As Integer)
    Select Case index
    Case 0
        RegVisible False
        PushButton(1).Enabled = False
        
        PushButton(1).Caption = INILanRead(Me.Name, "PushButton.1", "", "", "", "")
        PushButton(1).ForeColor = vbBlue
                
        If CommandReg <> 0 Then
            If dbg.CellValue(RowTemp, 1) = dbg.ColHeaderText(1) Then
                dbg.CellForeColor(RowTemp, 1) = vbBlue
            End If
            
            CommandReg = 0
            RowTemp = 0
            PinReg = ""
            FPReg.FPRegistrationStop
            
            If CommandVer = 0 Then
                CommandVer = 1
                FPVer.WorkingInBackground True
                FPVer.FPVerificationStart
            End If
        End If
    Case 1
        If Trim(cmb_mesin.Text) <> "" Then
            If CommandReg = 0 Then
                If CommandVer <> 0 Then
                    CommandVer = 0
                    FPVer.FPVerificationStop
                End If
                DoEvents
                
                CommandReg = 1
                getSamples = 3
                FPReg.DeviceInfo sn_device(cmb_mesin.ListIndex), VCode(cmb_mesin.ListIndex), ACode(cmb_mesin.ListIndex)
                FPReg.FPRegistrationStart "4JJ 1"
                regDstop = False
                PushButton(1).Caption = INILanRead(Me.Name, "PushButton.1b", "", "", "", "")
                PushButton(1).ForeColor = vbRed
            Else
                CommandReg = 0
                regDstop = True
                FPReg.FPRegistrationStop
                PushButton(1).Caption = INILanRead(Me.Name, "PushButton.1", "", "", "", "")
                PushButton(1).ForeColor = vbBlue
                
                If CommandVer = 0 Then
                    CommandVer = 1
                    FPVer.WorkingInBackground True
                    FPVer.FPVerificationStart
                End If
            End If
        End If
    End Select
End Sub

'Registrasi
Private Sub FPReg_FPRegistrationImage()
    picSample = LoadPicture(App.Path & "\FPTemp.BMP")
End Sub

Private Sub FPReg_FPRegistrationStatus(ByVal status As FlexCodeSDK_FTM.RegistrationStatus)
    Dim gagal As Boolean
    Dim addList As Boolean
    Dim lastupdate_date As String
    Dim jml As Long
    
    gagal = False
    addList = False
    Select Case status
        Case r_OK
            'Simpan Template FP
            UpdateInfo INILanRead("General", "ProsesReg0", "", "", "", ""), 1 '"Registration Success
            TulisLog INILanRead("General", "ProsesReg0", "", "", "", "")
            
            lastupdate_date = DateTimeToStr_DB(Now, DateTimeFormatDatabase)
            
            jml = GetJmlRecord("SELECT count(*) AS jml FROM t_template_uru WHERE pin = """ & PinReg & """ ", "jml")
            
            If jml >= 10 Then
                strSQL = "REPLACE INTO t_template_uru (id_group, pin, template_index, template_text, last_update) " & vbNewLine & _
                    "VALUES(1, """ & PinReg & """, " & CStr(0) & ", """ & getFPTemplate & """, """ & lastupdate_date & """); "
                ExecuteSQLite_Data strSQL
                DoEvents
                
                dbg.CellValue(RowTemp, 1) = INILanRead("General", "Status1", "", CStr(jml), "", "")
                dbg.CellForeColor(RowTemp, 1) = vbBlack
                DoEvents
            Else
                strSQL = "INSERT INTO t_template_uru (id_group, pin, template_index, template_text, last_update) " & vbNewLine & _
                    "VALUES(1, """ & PinReg & """, " & CStr(jml) & ", """ & getFPTemplate & """, """ & lastupdate_date & """); "
                ExecuteSQLite_Data strSQL
                DoEvents
                
                dbg.CellValue(RowTemp, 1) = INILanRead("General", "Status1", "", CStr(jml + 1), "", "")
                dbg.CellForeColor(RowTemp, 1) = vbBlack
                DoEvents
            End If
            
            'Ditambahkan Template FP yang baru ke dalam List Verifikasi
            addList = FPVer.FPLoad(PinReg, jml, getFPTemplate, "4JJ 1")
            DoEvents
            
            PushButton_Click 0
                        
            'If regUlang = True Then
            '    AddFP_template
            'End If
                        
        Case r_RegistrationFailed
            UpdateInfo INILanRead("General", "ProsesReg1", "", "", "", ""), 4 '"Registration Fail"
            CommandReg = 0
            FPReg.FPRegistrationStop
            gagal = True
            
        Case r_NoDevice
            UpdateInfo INILanRead("General", "ProsesReg2", "", "", "", ""), 4 '"Please connect the device to USB port"
            CommandReg = 0
            FPReg.FPRegistrationStop
            gagal = True
              
        Case r_PoorImageQuality
            UpdateInfo INILanRead("General", "ProsesReg3", "", "", "", ""), 4 '"Poor image quality"
            CommandReg = 0
            FPReg.FPRegistrationStop
            gagal = True
              
        Case r_ActivationIncorrect
            UpdateInfo INILanRead("General", "ProsesReg4", "", "", "", ""), 4 '"Activation / verification code is incorrent or not set"
            CommandReg = 0
            FPReg.FPRegistrationStop
            gagal = True
              
        Case r_RegistrationCaptureStart
            UpdateInfo INILanRead("General", "ProsesReg5", "", "", "", ""), 1 '"Registration Start"
              
        Case r_RegistrationCaptureStop
            If regDstop = True Then
                UpdateInfo INILanRead("General", "ProsesReg6", "", "", "", ""), 4 '"Registration Stop"
            End If
    End Select
  
    If gagal = True Then
        chb(0).value = xtpUnchecked
        chb(1).value = xtpUnchecked
        chb(2).value = xtpUnchecked
        chb(3).value = xtpUnchecked
        picSample = LoadPicture("")
        PushButton(1).Caption = INILanRead(Me.Name, "PushButton.1", "", "", "", "")
        PushButton(1).ForeColor = vbBlue
        
        If CommandVer = 0 Then
            CommandVer = 1
            FPVer.WorkingInBackground True
            FPVer.FPVerificationStart
        End If
    End If
End Sub

Private Sub FPReg_FPRegistrationTemplate(ByVal FPTemplate As String)
    getFPTemplate = FPTemplate
End Sub

Private Sub FPReg_FPSamplesNeeded(ByVal Samples As Integer)
    If Samples < 4 Then
        chb(getSamples - Samples).value = xtpChecked
    End If
    
    UpdateInfo INILanRead("General", "ProsesReg7", "", str(Samples), "", ""), 1 '"Samples Needed " & str(Samples)
End Sub

Public Sub UpdateInfo(vsDialog As String, ColorCode As Integer)
    lbl(8).Caption = vsDialog
    lbl(8).ForeColor = QBColor(ColorCode)
End Sub

Private Sub TrayIcon_DblClick()
    MinimizeToTray
End Sub

Private Sub TrayIcon_MouseUp(Button As Integer, Shift As Integer, x As Single, y As Single)
    If (Button = 2) Then Me.PopupMenu mnuTray
End Sub

Private Sub mnuTrayShow_Click()
    MinimizeToTray
End Sub

Public Sub mnuExit_Click()
    CloseForm = True
    Unload Me
End Sub

Public Sub MinimizeToTray()
    On Error GoTo BugError
    
    If Not Minimized Then
        Me.Hide
        Minimized = True
        mnuTrayShow.Caption = "Show"
    Else
        Me.Show
        Minimized = False
        mnuTrayShow.Caption = "Hide"
    End If
    
    Exit Sub
        
BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Sub

Public Sub SetToolTip(TipStr As String)
    TrayIcon.Text = TipStr
End Sub

Public Sub ShowMessage(IDMessage As Integer, StrTitle As String, StrMessage As String, StyleIndex As Integer)
    'StyleIndex = 0: None, 1: Info, 2: Warning, 3: Error
    TrayIcon.ShowBalloonTip 0, StrTitle, StrMessage, StyleIndex
End Sub

Public Sub TulisLog(str_value As String)
    On Error GoTo BugError

    Dim field() As String
    Dim i As Integer
    Dim str1 As String
    Dim str2 As String
    Dim str3 As String
    Dim dateStr As String

    dateStr = DateTimeToStr_DB(Now, "dd-MM-yyyy HH:mm")

    str3 = dateStr & " : " & str_value & vbNewLine
    str1 = txtIsi2.Text
    field = Split(str1, vbNewLine)
    txtIsi2.Text = ""
    ListBox.Clear
    ListBox.AddItem str3
    For i = 0 To UBound(field) - 1
        If i <= 50 Then
            str2 = field(i)
            str3 = str3 & str2 & vbNewLine
            ListBox.AddItem str2
        Else
            Exit For
        End If
    Next i
    txtIsi2.Text = ""
    txtIsi2.Text = str3
    DoEvents
    
    Exit Sub
        
BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Sub

Private Sub SetTreeUnBold(mediatree As XtremeSuiteControls.TreeView)
    Dim i As Integer
    For i = 1 To mediatree.Nodes.Count
        If InStr(1, mediatree.Nodes(i).Text, "<Bold>") > 0 Then
            mediatree.Nodes(i).Text = Replace(mediatree.Nodes(i).Text, "<Bold>", "")
            mediatree.Nodes(i).Text = Replace(mediatree.Nodes(i).Text, "</Bold>", "")
        End If
    Next
End Sub

Private Function GetAllPinByOffice2(IDOffice As String) As String
    Dim rs As cRecordset
    Dim strT As String
    Set rs = getRecordSet_Data("SELECT pin FROM t_employee WHERE id_office = '" & IDOffice & "' " & _
                          " OR id_office IN (SELECT id_office FROM t_office WHERE parentid_office = '" & IDOffice & "') ", True)
    Do While Not rs.EOF
        strT = strT & rs("pin").value & "-"
        rs.MoveNext
    Loop
    GetAllPinByOffice2 = strT
End Function

Private Sub Tree_NodeClick(index As Integer, ByVal Node As XtremeSuiteControls.TreeViewNode)
    Dim fields() As String
    Dim strKey As String
    Select Case index
    Case 1
         
    Case 2
        If Tree(index).Nodes.Count > 0 Then
            UserID = Trim(CStr(id_user_login))
            QueryGrid
        End If
    End Select
End Sub

Private Sub dbg_Click(ByVal lRowIfAny As Long, ByVal lColIfAny As Long)
    If lRowIfAny = 0 Then Exit Sub
    If dbg.RowCount = 0 Then Exit Sub
    
    Dim DelMsgText As String
    Dim str1 As String
    Dim pin As String
    Dim nik As String
    Dim nama As String
    Dim jabatan As String
    Dim kantor As String
    Dim i As Integer
    Dim jml As Long
    
    Dim Aa As String
    Dim msg1 As String
    
    With dbg
        If lColIfAny = 1 Then
            str1 = .CellValue(lRowIfAny, 1)
            pin = .CellValue(lRowIfAny, 2)
            nik = .CellValue(lRowIfAny, 3)
            nama = .CellValue(lRowIfAny, 4)
            jabatan = .CellValue(lRowIfAny, 5)
            kantor = .CellValue(lRowIfAny, 6)
            
            PosTopNow = Me.Top
            PosLeftNow = Me.Left
            PosTopReg = PosTopNow + (Round(Me.Height / 2, 0) - Round(Picture1.Height / 2, 0))
            PosLeftReg = PosLeftNow + (Round(Me.Width / 2, 0) - Round(Picture1.Width / 2, 0))
            DoEvents
            
            lbl(3).Caption = INILanRead(Me.Name, "lbl.3", "", pin, "", "")
            lbl(4).Caption = INILanRead(Me.Name, "lbl.4", "", nik, "", "")
            lbl(5).Caption = INILanRead(Me.Name, "lbl.5", "", nama, "", "")
            lbl(6).Caption = INILanRead(Me.Name, "lbl.6", "", jabatan, "", "")
            lbl(7).Caption = INILanRead(Me.Name, "lbl.7", "", kantor, "", "")
            DoEvents
            
            If str1 = .ColHeaderText(1) Then
                RowTemp = lRowIfAny
                PinReg = pin
                
                jml = GetJmlRecord("SELECT count(*) AS jml FROM t_template_uru WHERE pin = """ & PinReg & """ ", "jml")
                lbl(9).Caption = INILanRead(Me.Name, "lbl.9", "", CStr(jml), "", "")
                
                RegVisible True
            Else
                msg1 = INILanRead("errMsg", "msg2", "", pin, "", "")
                Aa = MsgBox(msg1, vbYesNo + vbDefaultButton1, App.Title)
                If Aa = vbYes Then
                    RowTemp = lRowIfAny
                    PinReg = pin
                    regUlang = True
                    
                    jml = GetJmlRecord("SELECT count(*) AS jml FROM t_template_uru WHERE pin = """ & PinReg & """ ", "jml")
                    lbl(9).Caption = INILanRead(Me.Name, "lbl.9", "", CStr(jml), "", "")
                    
                    RegVisible True
                End If
            End If
        End If
    End With
End Sub

Public Sub QueryGrid()
    Dim fields() As String
    Dim xGrid As iGrid
    Dim strGrid As String
    Dim col As Integer
    Dim Row As Integer
    Dim strPIN As String
    Dim StartDate As String
    Dim endDate As String
    Dim strOT As String
    Dim strScheduleID As String
    Dim TipeSched As String

    fields() = Split(Tree(2).SelectedItem.Tag, "-")
    'MsgBox Tree(2).SelectedItem.Tag
    LabelTitle(0).Caption = fields(2)
    
    Dim rs As cRecordset
    Dim pin As String
    Dim nik As String
    Dim nama As String
    Dim jabatan As String
    Dim kantor As String
    Dim id_value As String
    Dim reg As Integer
    
    Dim str1 As String
    Dim str2 As String
    
    id_value = fields(1)
    
    dbg.Clear
    If LCase(fields(0)) = "o" Then
        Set rs = QueryDataEmployee1(fields(1), mfilter1, mfilter2)
    Else
        Set rs = QueryDataEmployee2(id_value)
    End If
    
    Do While Not rs.EOF
        pin = rs.fields("pin").value
        nik = rs.fields("nik").value
        nama = rs.fields("name_employee").value
        jabatan = rs.fields("name_position").value
        kantor = rs.fields("name_office").value
        reg = rs.fields("reg").value
        
        If reg = 0 Then
            str1 = "Header;" & pin & ";" & nik & ";" & nama & ";" & jabatan & ";" & kantor & ";"
            InsertGrid dbg, dbg.RowCount + 1, lbl_row_count(0), "", False, &H0&, &H404040, False, Null, Null, "", str1
        Else
            str2 = INILanRead("General", "Status1", "", CStr(reg), "", "")
            str1 = str2 & ";" & pin & ";" & nik & ";" & nama & ";" & jabatan & ";" & kantor & ";"
            InsertGrid dbg, dbg.RowCount + 1, lbl_row_count(0), "", False, &H0&, &H404040, False, Null, Null, "", str1
        End If
        
        DoEvents
        rs.MoveNext
    Loop
End Sub

Public Function QueryDataEmployee1(IDOffice As String, Optional mfilter1 As String = "", Optional mfilter2 As String = "") As cRecordset
    Dim rs As cRecordset
    Dim strAnd As String
    Dim strWhere1 As String
    Dim strWhere2 As String
    Dim strUserWhere As String
    
    If UserID <> "" Then
        If id_data_privilege_login <> 1 Then
            strUserWhere = " AND id_structure IN (" & getId_Structure(id_data_privilege_login) & ") "
        Else
            strUserWhere = ""
        End If
    Else
        strUserWhere = " AND id_structure IN ('XXX') "
    End If
    
    If IDOffice <> "" Then
        strAnd = " AND (a.id_office = '" & IDOffice & "' OR a.id_office IN (SELECT id_office FROM t_office WHERE parentid_office = '" & IDOffice & "')) "
    Else
        strAnd = ""
    End If
    
    If mfilter1 <> "" Then
        strWhere1 = " AND a.id_employee IN (SELECT id_employee FROM t_employee WHERE " & mfilter1 & ") "
    Else
        strWhere1 = ""
    End If
    
    If mfilter2 <> "" Then
        strWhere2 = " AND a.id_employee IN (SELECT id_employee FROM t_employee_detail WHERE " & mfilter2 & ") "
    Else
        strWhere2 = ""
    End If
             
    strSQL = "SELECT a.id_employee, ifnull((SELECT count(*) FROM t_template_uru WHERE pin = a.pin), 0) as reg, " & _
             "CAST(a.pin AS integer) AS pin, CAST(a.nik as integer) AS nik, a.name_employee, " & _
             "a.id_position, p.name_position, a.id_office, o.name_office, " & _
             "a.gender, a.status_employee, strftime('%d-%m-%Y',a.start_date) as start_date, " & _
             "a.working_year || ' " & INILanRead("General", "Lang.Year.0") & " ' || a.working_month || ' " & INILanRead("General", "Lang.Month.0") & "' AS masa, " & _
             "a.last_education, a.marital,a.religion,a.qty_children,a.type_blood,(SELECT shortname_bank FROM t_bank WHERE id_bank = a.name_bank) as namebank " & _
             "FROM view_employee AS a " & _
             "LEFT JOIN t_position AS p ON p.id_position = a.id_position " & _
             "LEFT JOIN t_office AS o ON o.id_office = a.id_office " & _
             "WHERE a.active=1 " & strAnd & " " & strWhere1 & " " & strWhere2 & " " & strUserWhere
    'Clipboard.Clear
    'Clipboard.SetText strSQL
    
    Set rs = getRecordSet_Data(strSQL, False)
    Set QueryDataEmployee1 = rs
    
End Function

Public Function QueryDataEmployee2(id_employee As String) As cRecordset
    Dim rs As cRecordset
    Dim strAnd As String
    
    strAnd = " AND a.id_employee = " & id_employee & " "
             
    strSQL = "SELECT a.id_employee, ifnull((SELECT count(*) FROM t_template_uru WHERE pin = a.pin), 0) as reg, " & _
             "CAST(a.pin AS integer) AS pin, CAST(a.nik as integer) AS nik, a.name_employee, " & _
             "a.id_position, p.name_position, a.id_office, o.name_office, " & _
             "a.gender, a.status_employee, strftime('%d-%m-%Y',a.start_date) as start_date, " & _
             "a.working_year || ' " & INILanRead("General", "Lang.Year.0") & " ' || a.working_month || ' " & INILanRead("General", "Lang.Month.0") & "' AS masa, " & _
             "a.last_education, a.marital,a.religion,a.qty_children,a.type_blood,(SELECT shortname_bank FROM t_bank WHERE id_bank = a.name_bank) as namebank " & _
             "FROM view_employee AS a " & _
             "LEFT JOIN t_position AS p ON p.id_position = a.id_position " & _
             "LEFT JOIN t_office AS o ON o.id_office = a.id_office " & _
             "WHERE a.active=1 " & strAnd & " "
    'Clipboard.Clear
    'Clipboard.SetText strSQL
    
    Set rs = getRecordSet_Data(strSQL, False)
    Set QueryDataEmployee2 = rs
    
End Function

Public Function QueryDataEmployee3(pin As String) As cRecordset
    Dim rs As cRecordset
    Dim strAnd As String
    
    strAnd = " AND a.pin = " & pin & " "
             
    strSQL = "SELECT a.id_employee, ifnull((SELECT count(*) FROM t_template_uru WHERE pin = a.pin), 0) as reg, " & _
             "CAST(a.pin AS integer) AS pin, CAST(a.nik as integer) AS nik, a.name_employee, " & _
             "a.id_position, p.name_position, a.id_office, o.name_office, " & _
             "a.gender, a.status_employee, strftime('%d-%m-%Y',a.start_date) as start_date, " & _
             "a.working_year || ' " & INILanRead("General", "Lang.Year.0") & " ' || a.working_month || ' " & INILanRead("General", "Lang.Month.0") & "' AS masa, " & _
             "a.last_education, a.marital,a.religion,a.qty_children,a.type_blood,(SELECT shortname_bank FROM t_bank WHERE id_bank = a.name_bank) as namebank " & _
             "FROM view_employee AS a " & _
             "LEFT JOIN t_position AS p ON p.id_position = a.id_position " & _
             "LEFT JOIN t_office AS o ON o.id_office = a.id_office " & _
             "WHERE a.active=1 " & strAnd & " "
    'Clipboard.Clear
    'Clipboard.SetText strSQL
    
    Set rs = getRecordSet_Data(strSQL, False)
    Set QueryDataEmployee3 = rs
    
End Function

Public Function getId_Structure(id_data_privilege As Integer) As String
    Dim rs As cRecordset
    Dim id_str As String
    
    strSQL = "SELECT GROUP_CONCAT(id_structure,',') AS id_str FROM ( " & vbNewLine & _
        "SELECT id_structure " & vbNewLine & _
        "From t_data_privilege_detail " & vbNewLine & _
        "WHERE id_data_privilege = " & Trim(CStr(id_data_privilege)) & ") "
    Set rs = getRecordSet_Data(strSQL, True)
        
    id_str = ""
    Do While Not rs.EOF
        id_str = rs.fields("id_str").value
        
        rs.MoveNext
    Loop
    DoEvents
    
    getId_Structure = id_str
End Function


