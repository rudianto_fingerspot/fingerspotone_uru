Attribute VB_Name = "mdlSQLite"
Option Explicit

Public Function getRecordSet_Data(strSQL As String, Optional ReadOnly As Boolean = True) As cRecordset
    On Error GoTo Err1
    If FileExists(PathDB_data) Then
        Dim Cnn As cConnection
        Dim rs As cRecordset
    
        Set Cnn = New_c.connection
        Cnn.OpenDB PathDB_data, PassDB
        'MsgBox Cnn.UniqueID64
        
        Set rs = Cnn.OpenRecordset(strSQL, ReadOnly)
        Set getRecordSet_Data = rs
        Set Cnn = Nothing
    End If
    
    Exit Function
Err1:
    'MsgBox strSQL
    Err.Clear
    Resume Next
End Function

Public Function getRecordSet_SMS(strSQL As String, Optional ReadOnly As Boolean = True) As cRecordset
    On Error GoTo Err1
    If FileExists(PathDB_sms) Then
        Dim Cnn As cConnection
        Dim rs As cRecordset
    
        Set Cnn = New_c.connection
        Cnn.OpenDB PathDB_sms
        'MsgBox Cnn.UniqueID64
        
        Set rs = Cnn.OpenRecordset(strSQL, ReadOnly)
        Set getRecordSet_SMS = rs
        Set Cnn = Nothing
    End If
    
    Exit Function
Err1:
    'MsgBox strSQL
    Err.Clear
    Resume Next
End Function

Public Function ExecuteSQLite_Data(SQL As String) As Boolean
    On Error GoTo Err1
    
    If FileExists(PathDB_data) Then
        Dim Cnn As cConnection
        Set Cnn = New_c.connection
        
        Cnn.OpenDB PathDB_data, PassDB
        Dim bStatus As Boolean
        bStatus = True
        
        SQL = Replace(SQL, """", "'")
        Cnn.Execute SQL
            
        If Cnn.AffectedRows = 0 Then
            bStatus = False
        End If
        Set Cnn = Nothing
    End If
    ExecuteSQLite_Data = bStatus
    
    Exit Function
Err1:
    'MsgBox strSQL
    Err.Clear
    Resume Next
End Function

Public Function ExecuteSQLite_SMS(SQL As String) As Boolean
    On Error GoTo Err1
    
    If FileExists(PathDB_sms) Then
        Dim Cnn As cConnection
        Set Cnn = New_c.connection
        
        Cnn.OpenDB PathDB_sms
        Dim bStatus As Boolean
        bStatus = True
        
        SQL = Replace(SQL, """", "'")
        Cnn.Execute SQL
            
        If Cnn.AffectedRows = 0 Then
            bStatus = False
        End If
        Set Cnn = Nothing
    End If
    ExecuteSQLite_SMS = bStatus
    
    Exit Function
Err1:
    'MsgBox strSQL
    Err.Clear
    Resume Next
End Function

Public Function CreatedDate() As String
    CreatedDate = DateTimeToStr_DB(Now, DateTimeFormatDatabase)
End Function

Public Function getParamater(parameterName) As String
    Dim rs As cRecordset
    
    Set rs = getRecordSet_Data("SELECT value_param FROM t_param WHERE id_param = '" & parameterName & "'")
    If rs.RecordCount > 0 Then
        getParamater = rs.fields("value_param").value
    Else
        getParamater = ""
    End If
End Function

Public Function setParamater(parameterName As String, parameterValue As String) As String
    Dim str1 As String
    
    str1 = "DELETE FROM t_param WHERE id_param = """ & parameterName & """ "
    ExecuteSQLite_Data str1
    
    str1 = "INSERT INTO t_param (id_param, value_param) VALUES (""" & parameterName & """, """ & parameterValue & """) "
    ExecuteSQLite_Data str1
End Function

Public Function DateTimeToStr_DB(dt As Date, format_str As String) As String
    Dim s As String

    s = CStr(Format(dt, format_str))
    s = Replace(s, ".", ":")

    DateTimeToStr_DB = s
End Function

Public Function GetJmlRecord(sqlCek As String, fieldJml As String) As Long
    Dim rs As cRecordset
    
    GetJmlRecord = 0
    strSQL = sqlCek
    Set rs = getRecordSet_Data(strSQL, True)
    Do While Not rs.EOF
        GetJmlRecord = rs.fields(fieldJml).value
        rs.MoveNext
    Loop
End Function
