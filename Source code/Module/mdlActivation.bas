Attribute VB_Name = "mdlActivation"
Option Explicit

Private Declare Function GetPrivateProfileString Lib "kernel32" Alias "GetPrivateProfileStringA" (ByVal lpApplicationName As String, ByVal lpKeyName As Any, ByVal lpDefault As String, ByVal lpReturnedString As String, ByVal nSize As Long, ByVal lpFileName As String) As Long
Private Declare Function WritePrivateProfileString Lib "kernel32" Alias "WritePrivateProfileStringA" (ByVal lpApplicationName As String, ByVal lpKeyName As Any, ByVal lpString As String, ByVal lpFileName As String) As Long

Private Declare Function GetUserName Lib "advapi32.dll" Alias "GetUserNameA" (ByVal lpBuffer As String, nSize As Long) As Long
Private Declare Function GetComputerName Lib "kernel32" Alias "GetComputerNameA" (ByVal lpBuffer As String, nSize As Long) As Long

Public pIniFileSensor As String
Private bytBlock() As Byte

Public Sub WriteStringDevice(Section As String, key As String, value As String)
    WritePrivateProfileString Section, key, value, pIniFileSensor
End Sub

Public Function CheckKeyRevoRT2(opt As Long, SN As String, AC As String) As Boolean
    On Error GoTo BugError
    
    Dim str1 As String
    Dim temp As String
    Dim key As String
    Dim Jns As String
    Dim l As Byte
    Dim Act As String
    
    Dim aKey2(27) As Byte
    For l = 0 To 27
      aKey2(l) = Choose(l + 1, &HA5, &H2B, &H4E, &H28, &H39, &H12, &H5A, &H74, &H4E, &H6B, &H43, &H49, &H6A, &H7E, &H4E, &H53, &H4E, &H53, &H35, &H74, &H63, &H48, &H37, &H72, &H34, &H41, &HDD, &HBD)
    Next
    
    Act = Replace(AC, "-", "")
    str1 = ""
    str1 = Mid(Act, 3, 5)
    Act = Mid(Act, 8, Len(Act))
    key = GetKeyAct(Act)
    
    temp = MD5(key & SN & StrConv(aKey2, vbUnicode) & SN & key & str1)
    key = ""
    
    If opt = 1 Then
        Jns = "A1"
    ElseIf opt = 2 Then
        Jns = "B1"
    ElseIf opt = 3 Then
        Jns = "C1"
    ElseIf opt = 4 Then
        Jns = "D1"
    ElseIf opt = 5 Then
        Jns = "E1"
    Else
        Jns = "F1"
    End If
    
    temp = Jns & str1 & Mid(temp, 3, 7) & Mid(temp, 15, 10)
    If temp = Jns & str1 & Act Then
        CheckKeyRevoRT2 = True
    Else
        CheckKeyRevoRT2 = False
    End If
    
    Exit Function
        
BugError:
    Err.Clear
    Resume Next
End Function

Private Function HashANSI(strData) As String
    Dim Hash As New MD5Hash

    bytBlock = StrConv(strData, vbFromUnicode)
    HashANSI = Hash.HashBytes(bytBlock)
End Function

Public Function GetKeyRevoRT2(opt As Integer, SN As String) As String
    On Error GoTo BugError
    
    Dim str1 As String
    Dim str2 As String
    Dim temp As String
    Dim key As String
    Dim Jns As String
    Dim l As Byte
    Dim aKey2(27) As Byte
    
    For l = 0 To 27
      aKey2(l) = Choose(l + 1, &HA5, &H2B, &H4E, &H28, &H39, &H12, &H5A, &H74, &H4E, &H6B, &H43, &H49, &H6A, &H7E, &H4E, &H53, &H4E, &H53, &H35, &H74, &H63, &H48, &H37, &H72, &H34, &H41, &HDD, &HBD)
    Next
    
    str1 = GetRandomKey("ACT")
    str1 = Mid(str1, 4, 5)
    temp = HashANSI(key & SN & StrConv(aKey2, vbUnicode) & SN & key & str1)
    key = ""
    
    If opt = 1 Then
        Jns = "A1"
    ElseIf opt = 2 Then
        Jns = "B1"
    ElseIf opt = 3 Then
        Jns = "C1"
    ElseIf opt = 4 Then
        Jns = "D1"
    ElseIf opt = 5 Then
        Jns = "E1"
    Else
        Jns = "F1"
    End If
    
    temp = Jns & str1 & Mid(temp, 3, 7) & Mid(temp, 15, 10)
    
    Dim i As Integer
    
    GetKeyRevoRT2 = temp
    
    Exit Function
        
BugError:
    Err.Clear
    Resume Next
End Function

Public Function GetKeyAct(AC As String) As String
    On Error GoTo BugError
    
    Dim temp As String
    Dim Temp2 As String
    Dim i As Byte
    
    For i = 1 To Len(AC)
        If InStr(1, "1234567890ABCDEF", Mid(AC, i, 1)) = 0 Then
            temp = temp & Mid(AC, i, 1)
        End If
    Next
    
    GetKeyAct = temp
    
    Exit Function
        
BugError:
    Err.Clear
    Resume Next
End Function

Public Function GetRandomKey(keyValues As String) As String
    On Error GoTo BugError
    
    GetRandomKey = keyValues & 1 + Int(Rnd() * 10000000)
    
    Exit Function
        
BugError:
    Err.Clear
    Resume Next
End Function

Public Function check_spelling_fio_device_sn(device_sn As String) As String
    On Error GoTo BugError
    
    Dim check_1 As String
    
    check_spelling_fio_device_sn = device_sn
    check_1 = Mid(device_sn, 1, 3)
    
    If (LCase(check_1) = LCase("fi0")) Or (LCase(check_1) = LCase("f10")) _
    Or (LCase(check_1) = LCase("f1o")) Or (LCase(check_1) = LCase("fl0")) _
    Or (LCase(check_1) = LCase("flo")) Or (LCase(check_1) = LCase("fio")) Then
        check_spelling_fio_device_sn = "FIO" & Mid(device_sn, 4, Len(device_sn))
    End If
    
    Exit Function
        
BugError:
    Err.Clear
    Resume Next
End Function

Public Function SystemSerialNumber() As String
    On Error GoTo BugError
    
    Dim mother_boards As Variant
    Dim board As Variant
    Dim wmi As Variant
    Dim serial_numbers As String

    ' Get the Windows Management Instrumentation object.
    Set wmi = GetObject("WinMgmts:")

    ' Get the "base boards" (mother boards).
    Set mother_boards = wmi.InstancesOf("Win32_BaseBoard")
    For Each board In mother_boards
        serial_numbers = serial_numbers & ", " & _
            board.SerialNumber
    Next board
    If Len(serial_numbers) > 0 Then serial_numbers = _
        Mid$(serial_numbers, 3)

    SystemSerialNumber = serial_numbers
    
    Exit Function
        
BugError:
    Err.Clear
    Resume Next
End Function

Public Function CpuId() As String
    On Error GoTo BugError
    
    Dim computer As String
    Dim wmi As Variant
    Dim processors As Variant
    Dim cpu As Variant
    Dim cpu_ids As String

    computer = "."
    Set wmi = GetObject("winmgmts:" & _
        "{impersonationLevel=impersonate}!\\" & _
        computer & "\root\cimv2")
    Set processors = wmi.ExecQuery("Select * from " & _
        "Win32_Processor")

    For Each cpu In processors
        cpu_ids = cpu_ids & ", " & cpu.ProcessorId
    Next cpu
    If Len(cpu_ids) > 0 Then cpu_ids = Mid$(cpu_ids, 3)

    CpuId = cpu_ids
    
    Exit Function
        
BugError:
    Err.Clear
    Resume Next
End Function

Public Function ReturnUserName() As String
    On Error GoTo BugError
    
    ReturnUserName = Space$(255)
    Call GetUserName(ReturnUserName, Len(ReturnUserName))
    
    Exit Function
        
BugError:
    Err.Clear
    Resume Next
End Function
 
Public Function ReturnComputerName() As String
    On Error GoTo BugError
    
    ReturnComputerName = Space$(255)
    Call GetComputerName(ReturnComputerName, Len(ReturnComputerName))
    
    Exit Function
        
BugError:
    Err.Clear
    Resume Next
End Function
 
'***************************************************************************************************************************************

Function ShiftRight(ByVal lngNumber As Long, ByVal intNumBits As Integer) As Long
    ShiftRight = lngNumber \ 2 ^ intNumBits 'note the integer division op
End Function

Function ShiftLeft(ByVal lngNumber As Long, ByVal intNumBits As Integer) As Long
    ShiftLeft = lngNumber * 2 ^ intNumBits
End Function

'***************************************************************************************************************************************
