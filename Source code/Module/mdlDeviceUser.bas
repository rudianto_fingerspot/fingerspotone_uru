Attribute VB_Name = "mdlDeviceUser"
Option Explicit

Public devNeo As Device
Public deviceConn As Zd2911DeviceConnection
Public fpBytes() As Byte
Private deviceTool As Zd2911Tools
Private gUsers() As UserExt

Private CnTemp As cConnection
Private mnCommHandleIndex As Long
Private mbGetState As Boolean
Private optConn As Integer

Private last_connect As Date
Private flagDev As String
Private pinTemp As String
Private jmlEnroll As Integer
Private count_user As Integer
Private count_manager As Integer

Private strTmpPwdDefault As String
Private strTmpPwd As String
Private strTmpPwd2 As String
Private strTmpCard As String
Public isValidation As Boolean

Const strPwdNeoDefault = "15907342"
    
Private gPassEncryptKey As Long
Private OnBits(0 To 31) As Long

Const PWD_DATA_SIZE = 40
Const FP_DATA_SIZE = 1680
Const FACE_DATA_SIZE = 20080
Const VEIN_DATA_SIZE = 3080
Const PALMVEIN_DATA_SIZE = 20080
Private mbytEnrollData(FACE_DATA_SIZE - 1) As Byte
Private mlngPasswordData As Long

Private list_tgl_upload() As String
Private execRecCount As Integer
Private SQL1 As String
Private SQL2 As String

Public HapusDulu As Boolean
Public pinUnreg As String
Public pinUnregMesin As String
Public pinUnSetName As String
Public pinUnSetNameMesin As String

Public CnAttlog As cConnection
Public MaxAttLogDL As Integer

Public Const vnLicense = 1261
Public Const Angka As String = "0123456789"
Public Const Huruf As String = "qwertyuioplkjhgfdsazxcvbnmQWERTYUIOPLKJHGFDSAZXCVBNM"

Public EnrollDataDefault() As Byte

Public gbOpenFlag As Boolean
Public gnCommHandleIndex As Long
Public FKFirmware As String
Public GetDevInfo As Boolean

Public PathDBAttlog As String

Public vnname_device As String
Public vnactivation_code As String
Public vnstatus_aktif As String

Public deviceType As Integer
    
Public vnMachineNumber As Long
Public vnCommPort As Long
Public vnCommBaudrate As Long
Public vstrTelNumber As String
Public vnWaitDialTime As Long
Public vpszIPAddress As String
Public vpszNetPort As Long
Public vpszNetPassword As Long
Public vnTimeOut As Long
Public vnProtocolType As Long
Public TCPIPFlag As Boolean

Public Function InitDevice(SN As String, ByRef optConn As Integer, useFD As Boolean) As Boolean
    On Error GoTo BugError
       
    InitDevice = False
    
    Dim RecstAtt As New cRecordset
    Dim id_group As Integer
    Dim master_group As String
    
    strSQL = "SELECT d.*, dt.firmware, dt.dev_type FROM t_device AS d " & _
        "LEFT JOIN t_device_type AS dt ON dt.id_device_type = d.id_device_type " & _
        "WHERE comm_type < 4 AND sn_device = """ & SN & """ "
    Set RecstAtt = getRecordSet_Data(strSQL, True)
        
    If RecstAtt.RecordCount = 0 Then
        Exit Function
    End If
    
    With RecstAtt
        Do While Not .EOF
            deviceType = .fields("dev_type").value
            
            vnname_device = .fields("name_device").value
            vnactivation_code = .fields("active_code").value
            optConn = .fields("comm_type").value
            vnMachineNumber = .fields("dev_id").value
            vpszNetPassword = .fields("comm_key").value
            vpszIPAddress = .fields("ip_address").value
            vpszNetPort = .fields("ethernet_port").value
            vnCommPort = .fields("serial_port").value
            vnCommBaudrate = .fields("baud_rate").value
            FKFirmware = .fields("firmware").value
            id_group = .fields("id_group").value
            master_group = .fields("master_group").value
            vnstatus_aktif = .fields("status_aktif").value
            
            .MoveNext
        Loop
    End With
    
    InitDevice = True
    
    Exit Function
        
BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Function

Public Function cmdOpenCommRevo(optConn As Integer) As Boolean
    On Error GoTo BugError
    
    cmdOpenCommRevo = False
    
    Dim vnResultCode As Long
    Dim last_connect As Date
    
    If optConn = 1 Then 'NetworkDevice
        vnTimeOut = 5000
        TCPIPFlag = True
        
        If TCPIPFlag Then
            vnProtocolType = PROTOCOL_TCPIP
        Else
            vnProtocolType = PROTOCOL_UDP
        End If
        gnCommHandleIndex = FK_ConnectNet(vnMachineNumber, vpszIPAddress, vpszNetPort, vnTimeOut, vnProtocolType, vpszNetPassword, vnLicense)
    ElseIf optConn = 2 Then 'USBDevice
        gnCommHandleIndex = FK_ConnectUSB(vnMachineNumber, vnLicense)
    ElseIf optConn = 3 Then 'SerialDevice
        vstrTelNumber = ""
        vnWaitDialTime = 0
        
        vnTimeOut = 5000
        gnCommHandleIndex = FK_ConnectComm(vnMachineNumber, vnCommPort, vnCommBaudrate, vstrTelNumber, vnWaitDialTime, vnLicense, vnTimeOut)
    End If
    
    If gnCommHandleIndex > 0 Then
        gbOpenFlag = True
        cmdOpenCommRevo = True
        last_connect = Now
    Else
        vnResultCode = gnCommHandleIndex
        gbOpenFlag = False
    End If
    
    Exit Function
        
BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Function

Public Function cmdOpenCommNeo(optConn As Integer) As Boolean
    On Error GoTo BugError
    
    cmdOpenCommNeo = False
    
    Set devNeo = New Device
    devNeo.DN = vnMachineNumber
    devNeo.password = Trim(CStr(vpszNetPassword))
    devNeo.ConnectionModel = 5
    
    If optConn = 2 Then 'USBDevice
        devNeo.CommunicationType = CommunicationType.CommunicationType_Usb
    ElseIf optConn = 3 Then 'SerialDevice
        devNeo.CommunicationType = CommunicationType.CommunicationType_Serial
        devNeo.SerialPort = vnCommPort
        devNeo.Baudrate = vnCommBaudrate
    ElseIf optConn = 1 Then 'NetworkDevice
        devNeo.CommunicationType = CommunicationType.CommunicationType_Tcp
        devNeo.IpAddress = Trim(vpszIPAddress)
        devNeo.IpPort = vpszNetPort
    End If
    
    Set deviceConn = New Zd2911DeviceConnection
    If deviceConn.Open(devNeo) > 0 Then
        cmdOpenCommNeo = True
        Set deviceTool = New Zd2911Tools
    End If
    DoEvents
    
    Exit Function
        
BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Function

Public Function cmdOpenComm(optConn As Integer) As Boolean
    On Error GoTo BugError
    
    cmdOpenComm = False
    
    If deviceType = 2 Then
        cmdOpenComm = cmdOpenCommNeo(optConn)
    ElseIf deviceType = 3 Then
        cmdOpenComm = cmdOpenCommRevo(optConn)
    End If
    
    Exit Function
        
BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Function

Public Sub cmdCloseComm()
    On Error GoTo BugError
    
    If gbOpenFlag = True Then
        FK_DisConnect gnCommHandleIndex
        gbOpenFlag = False
    End If
    
    Exit Sub
        
BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Sub

Public Sub cmdCloseCommNeo()
    On Error GoTo BugError
    
    deviceConn.Close
    Set devNeo = Nothing
    Set deviceConn = Nothing
    Set deviceTool = Nothing
    
    Exit Sub
        
BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Sub

Public Function GetDeviceStatus(IndexStatus As Integer, mnCommHandleIndex As Long, ByRef strValue As String, _
    ByRef intValue As Long) As Boolean
    On Error GoTo BugError
    
    GetDeviceStatus = False
    
    Dim vnStatusIndex As Long
    Dim vnValue As Long
    Dim vnResultCode As Long

    intValue = 0
    DoEvents

    vnResultCode = FK_EnableDevice(mnCommHandleIndex, 0)
    If vnResultCode <> RUN_SUCCESS Then
        'DebugText gstrNoDevice
        Exit Function
    End If

    vnStatusIndex = IndexStatus + 1
    vnResultCode = FK_GetDeviceStatus(mnCommHandleIndex, vnStatusIndex, vnValue)
    If vnResultCode = RUN_SUCCESS Then
        GetDeviceStatus = True
        
        intValue = vnValue
        Select Case vnStatusIndex
            Case GET_MANAGERS:  strValue = "Manager count = " & vnValue
            Case GET_USERS:  strValue = "User count = " & vnValue
            Case GET_FPS:  strValue = "Fp count = " & vnValue
            Case GET_PSWS:  strValue = "Password count = " & vnValue
            Case GET_SLOGS:  strValue = "SLog count = " & vnValue
            Case GET_GLOGS:  strValue = "GLog count = " & vnValue
            Case GET_ASLOGS:  strValue = "All SLog count = " & vnValue
            Case GET_AGLOGS:  strValue = "All GLog count = " & vnValue
            Case GET_CARDS:  strValue = "Card count = " & vnValue
            Case GET_FACES:  strValue = "Face count = " & vnValue
            Case Else: strValue = "--"
        End Select
    Else
        strValue = ReturnResultPrint(vnResultCode)
    End If

    FK_EnableDevice mnCommHandleIndex, 1
    
    Exit Function
        
BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Function

Public Function GetDeviceInfo(IndexStatus As Integer, mnCommHandleIndex As Long, ByRef strValue As String, _
    ByRef intValue As Long) As Boolean
    On Error GoTo BugError
    
    GetDeviceInfo = False
    
    Dim vnInfoIndex As Long
    Dim vnValue As Long
    Dim vnResultCode As Long

    intValue = 0
    DoEvents

    vnResultCode = FK_EnableDevice(mnCommHandleIndex, 0)
    If vnResultCode <> RUN_SUCCESS Then
        Exit Function
    End If

    vnInfoIndex = IndexStatus + 1
    If vnInfoIndex = 11 Then
        vnInfoIndex = DI_VERIFY_KIND
    ElseIf vnInfoIndex = 12 Then
        vnInfoIndex = DI_MULTIUSERS
    End If
    
    vnResultCode = FK_GetDeviceInfo(mnCommHandleIndex, vnInfoIndex, vnValue)
    If vnResultCode = RUN_SUCCESS Then
        GetDeviceInfo = True
        
        intValue = vnValue
        Select Case vnInfoIndex
            Case DI_MANAGERS:  strValue = "ManagerCount = " & vnValue
            Case DI_MACHINENUM:  strValue = "Machine Num = " & vnValue
            Case DI_LANGAUGE:  strValue = "Language = " & vnValue
            Case DI_POWEROFF_TIME:  strValue = "PowerOffTime = " & vnValue
            Case DI_LOCK_CTRL:  strValue = "LockOperate = " & vnValue
            Case DI_GLOG_WARNING:  strValue = "GLogWarning = " & vnValue
            Case DI_SLOG_WARNING:  strValue = "SLogWarning = " & vnValue
            Case DI_VERIFY_INTERVALS:  strValue = "ReVerifyTime = " & vnValue
            Case DI_RSCOM_BPS:  strValue = "Baudrate(" & vnValue & ") : "
                If vnValue = BPS_9600 Then
                    strValue = strValue & "9600"
                ElseIf vnValue = BPS_19200 Then
                    strValue = strValue & "19200"
                ElseIf vnValue = BPS_38400 Then
                    strValue = strValue & "38400"
                ElseIf vnValue = BPS_57600 Then
                    strValue = strValue & "57600"
                ElseIf vnValue = BPS_115200 Then
                    strValue = strValue & "115200"
                Else
                    strValue = strValue & "--"
                End If
            Case DI_VERIFY_KIND: strValue = "VerifyKind = "
                If vnValue = 0 Then
                    strValue = strValue & "F / P / C"
                ElseIf vnValue = 1 Then
                    strValue = strValue & "F + P"
                ElseIf vnValue = 2 Then
                    strValue = strValue & "F + C"
                ElseIf vnValue = 3 Then
                    strValue = strValue & "C"
                End If
            Case DI_DATE_SEPARATE: strValue = "DateSeperate = " & vnValue
            Case DI_MULTIUSERS: strValue = "MultiUsers = " & vnValue
            Case Else: strValue = "--"
        End Select
    Else
        strValue = ReturnResultPrint(vnResultCode)
    End If

    FK_EnableDevice mnCommHandleIndex, 1
    
    Exit Function
        
BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Function

Public Function GetDeviceTime(ByRef device_time As Date, mnCommHandleIndex As Long) As Boolean
    On Error GoTo BugError
    
    GetDeviceTime = False
    
    Dim vdwDate As Date
    Dim strDataTime As String
    Dim vnResultCode As Long

    DoEvents

    vnResultCode = FK_EnableDevice(mnCommHandleIndex, 0)
    If vnResultCode <> RUN_SUCCESS Then
        Exit Function
    End If

    vnResultCode = FK_GetDeviceTime(mnCommHandleIndex, vdwDate)
    If vnResultCode = RUN_SUCCESS Then
        GetDeviceTime = True
        device_time = vdwDate
        
        strDataTime = "GetDeviceDateTime = " & Format(vdwDate, "Long Date") & ", Time = " & Format(vdwDate, "Long Time")
    End If

    FK_EnableDevice mnCommHandleIndex, 1
    
    Exit Function
        
BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Function

Public Function GetDeviceTimeNeo(ByRef device_time As Date) As Boolean
    On Error GoTo BugError
    
    GetDeviceTimeNeo = False
    
    Dim vdwDate As Date
    Dim strDataTime As String
    Dim extraProperty() As Byte, extraData() As Byte
    Dim result As Boolean
    
    Dim yy As Long
    Dim mm As Long
    Dim dd As Long
    Dim hh As Long
    Dim nn As Long
    Dim ss As Long
    
    result = deviceConn.GetPropertyExt_2(DeviceProperty_DeviceTime, extraProperty, devNeo, extraData)
    DoEvents
    If result Then
        GetDeviceTimeNeo = True
        yy = CLng(extraData(0)) + BeginYear
        mm = CLng(extraData(1))
        dd = CLng(extraData(2))
        hh = CLng(extraData(3))
        nn = CLng(extraData(4))
        ss = CLng(extraData(5))
        
        vdwDate = DateSerial(yy, mm, dd) & " " & TimeSerial(hh, nn, ss)
        device_time = vdwDate
        
        strDataTime = "GetDeviceDateTime = " & Format(vdwDate, "Long Date") & ", Time = " & Format(vdwDate, "Long Time")
    End If
    
    Exit Function
        
BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Function

Public Function DeviceInfo(SN As String, mnCommHandleIndex As Long) As Boolean
    On Error GoTo BugError
       
    DeviceInfo = False
    
    Dim i As Integer
    Dim J As Integer
    Dim strValue As String
    Dim intValue As Long
    
    Dim manager_count As Long
    Dim user_count As Long
    Dim fp_count As Long
    Dim pwd_count As Long
    Dim card_count As Long
    Dim face_count As Long
    Dim new_attlog_count As Long
    Dim all_attlog_count As Long
    Dim interval_scan As Long
    Dim combination_verify As String
    Dim dateStr As String
    Dim device_time As Date
    Dim device_timeStr As String
    Dim count_new_attlog As Integer
    
    DoEvents
    
    manager_count = 0
    user_count = 0
    fp_count = 0
    pwd_count = 0
    card_count = 0
    face_count = 0
    new_attlog_count = 0
    all_attlog_count = 0
    interval_scan = 0
    combination_verify = ""
    count_new_attlog = 0
    last_connect = Now
    
    strSQL = "REPLACE INTO t_device_info (sn_device, manager_count, user_count, " & _
        "fp_count, pwd_count, card_count, face_count, new_attlog_count, all_attlog_count, " & _
        "interval_scan, combination_verify, device_time, last_connect) "
    For i = 0 To 9
        If GetDeviceStatus(i, mnCommHandleIndex, strValue, intValue) Then
            J = i + 1
            Select Case J
                Case GET_MANAGERS: manager_count = intValue
                Case GET_USERS: user_count = intValue
                Case GET_FPS: fp_count = intValue
                Case GET_PSWS: pwd_count = intValue
                Case GET_SLOGS: intValue = 0
                Case GET_GLOGS: new_attlog_count = intValue
                Case GET_ASLOGS: intValue = 0
                Case GET_AGLOGS: all_attlog_count = intValue
                Case GET_CARDS: card_count = intValue
                Case GET_FACES: face_count = intValue
                Case Else: intValue = 0
        End Select
        End If
    Next
    count_new_attlog = new_attlog_count
    count_user = user_count
    
    If GetDeviceInfo(7, mnCommHandleIndex, strValue, intValue) Then
        interval_scan = intValue
    End If
    
    If GetDeviceInfo(10, mnCommHandleIndex, strValue, intValue) Then
        If intValue = 0 Then
            combination_verify = "F / P / C"
        ElseIf intValue = 1 Then
            combination_verify = "F + P"
        ElseIf intValue = 2 Then
            combination_verify = "F + C"
        ElseIf intValue = 3 Then
            combination_verify = "C"
        End If
    End If
    
    GetDeviceTime device_time, mnCommHandleIndex
    device_timeStr = DateTimeToStr_DB(device_time, DateTimeFormatDatabase)
    dateStr = DateTimeToStr_DB(last_connect, DateTimeFormatDatabase)
    
    strSQL = strSQL & "VALUES (""" & SN & """, " & manager_count & ", " & user_count & ", " & _
        fp_count & ", " & pwd_count & ", " & card_count & ", " & face_count & ", " & _
        new_attlog_count & ", " & all_attlog_count & ", " & interval_scan & ", """ & _
        combination_verify & """, """ & device_timeStr & """, """ & dateStr & """) "
    ExecuteSQLite_Data strSQL
    
    DeviceInfo = True
    
    Exit Function
        
BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Function

Public Function DeviceInfoNeo(SN As String) As Boolean
    On Error GoTo BugError
       
    DeviceInfoNeo = False
    
    Dim i As Integer
    Dim J As Integer
    Dim strValue As String
    Dim intValue As Long
    
    Dim manager_count As Long
    Dim user_count As Long
    Dim fp_count As Long
    Dim pwd_count As Long
    Dim card_count As Long
    Dim face_count As Long
    Dim new_attlog_count As Long
    Dim all_attlog_count As Long
    Dim new_slog_count  As Long
    Dim all_slog_count  As Long
    Dim interval_scan As Long
    Dim combination_verify As String
    Dim dateStr As String
    Dim device_time As Date
    Dim device_timeStr As String
    Dim count_new_attlog As Integer
    
    manager_count = 0
    user_count = 0
    fp_count = 0
    pwd_count = 0
    card_count = 0
    face_count = 0
    new_attlog_count = 0
    all_attlog_count = 0
    interval_scan = 0
    combination_verify = ""
    count_new_attlog = 0
    last_connect = Now
    new_slog_count = 0
    all_slog_count = 0
    
    Dim extraProperty() As Byte, extraData() As Byte
    Dim result As Boolean
    
    strSQL = "REPLACE INTO t_device_info (sn_device, manager_count, user_count, " & _
        "fp_count, pwd_count, card_count, face_count, new_attlog_count, all_attlog_count, " & _
        "interval_scan, combination_verify, device_time, last_connect) "
    
    result = deviceConn.GetPropertyExt_2(DeviceProperty_Status, extraProperty, devNeo, extraData)
    DoEvents
    If result Then
        user_count = deviceTool.GetStringByNum(extraData, 0, NumberType_UInt32Bit)
        manager_count = deviceTool.GetStringByNum(extraData, 4, NumberType_UInt32Bit)
        fp_count = deviceTool.GetStringByNum(extraData, 8, NumberType_UInt32Bit)
        card_count = deviceTool.GetStringByNum(extraData, 12, NumberType_UInt32Bit)
        pwd_count = deviceTool.GetStringByNum(extraData, 16, NumberType_UInt32Bit)
        new_slog_count = deviceTool.GetStringByNum(extraData, 20, NumberType_UInt32Bit)
        new_attlog_count = deviceTool.GetStringByNum(extraData, 24, NumberType_UInt32Bit)
        all_slog_count = deviceTool.GetStringByNum(extraData, 28, NumberType_UInt32Bit)
        all_attlog_count = deviceTool.GetStringByNum(extraData, 32, NumberType_UInt32Bit)
    End If
    count_new_attlog = new_attlog_count
    count_user = manager_count + user_count
    count_manager = manager_count
    
    If count_manager > 1 Then
        user_count = count_user - 1
        count_manager = 1
        manager_count = 1
    End If
    
    combination_verify = "F / P / C"
    
    GetDeviceTimeNeo device_time
    device_timeStr = DateTimeToStr_DB(device_time, DateTimeFormatDatabase)
    dateStr = DateTimeToStr_DB(last_connect, DateTimeFormatDatabase)
    
    strSQL = strSQL & "VALUES (""" & SN & """, " & manager_count & ", " & user_count & ", " & _
        fp_count & ", " & pwd_count & ", " & card_count & ", " & face_count & ", " & _
        new_attlog_count & ", " & all_attlog_count & ", " & interval_scan & ", """ & _
        combination_verify & """, """ & device_timeStr & """, """ & dateStr & """) "
    ExecuteSQLite_Data strSQL
    
    DeviceInfoNeo = True
    
    Exit Function
        
BugError:
    MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Function

Private Sub SaveTemplate(SN As String, pin As String, anBackupNumber As Long, vbytEnrollData() As Byte, _
    rs_template As cRecordset)
    On Error GoTo BugError

    rs_template.AddNew
    rs_template!sn_device = SN
    rs_template!pin = Trim(pin)
    rs_template!template_index = anBackupNumber
    rs_template!template_blob = vbytEnrollData
    rs_template!last_update = last_connect
    
    Exit Sub

BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Sub

Private Sub FuncSaveTemplate(SN As String, pin As String, anBackupNumber As Long, mnCommHandleIndex As Long, _
    rs_template As cRecordset)
    On Error GoTo BugError
    
    Dim vnii As Long
    Dim vnLen As Long
    Dim vbytEnrollData() As Byte
    Dim FPData() As Byte
    
    Dim sama As Boolean
    Dim strTmp As String
    Dim x As Integer
    Dim i As Integer
    Dim pwd As String
    
    sama = False
    If anBackupNumber = BACKUP_PSW Then
        ReDim vbytEnrollData(PWD_DATA_SIZE - 1)
        
        CopyMemory vbytEnrollData(0), mbytEnrollData(0), PWD_DATA_SIZE
        strTmp = ShowByteArrayToString(vbytEnrollData, PWD_DATA_SIZE)
        If strTmp = strTmpPwd Or strTmp = strTmpPwdDefault Or strTmp = strTmpPwd2 Then
            sama = True
        End If
    ElseIf anBackupNumber = BACKUP_CARD Then
        ReDim vbytEnrollData(PWD_DATA_SIZE - 1)
        CopyMemory vbytEnrollData(0), mbytEnrollData(0), PWD_DATA_SIZE
        
        strTmp = ShowByteArrayToString(vbytEnrollData, PWD_DATA_SIZE)
        If strTmp = strTmpCard Then
            sama = True
        End If
    ElseIf anBackupNumber >= BACKUP_FP_0 And anBackupNumber <= BACKUP_FP_9 Then
        ReDim vbytEnrollData(FP_DATA_SIZE - 1)
        CopyMemory vbytEnrollData(0), mbytEnrollData(0), FP_DATA_SIZE
    ElseIf anBackupNumber = BACKUP_FACE Then
        ReDim vbytEnrollData(FACE_DATA_SIZE - 1)
        CopyMemory vbytEnrollData(0), mbytEnrollData(0), FACE_DATA_SIZE
    ElseIf anBackupNumber >= BACKUP_PALMVEIN_0 And anBackupNumber <= BACKUP_PALMVEIN_3 Then
        ReDim vbytEnrollData(PALMVEIN_DATA_SIZE - 1)
        CopyMemory vbytEnrollData(0), mbytEnrollData(0), PALMVEIN_DATA_SIZE
    ElseIf anBackupNumber = BACKUP_VEIN_0 Then
        ReDim vbytEnrollData(VEIN_DATA_SIZE - 1)
        CopyMemory vbytEnrollData(0), mbytEnrollData(0), VEIN_DATA_SIZE
    End If
    
    If isValidation Then sama = False
    
    If sama = False Then
        SaveTemplate SN, pin, anBackupNumber, vbytEnrollData, rs_template
        
        strSQL = "DELETE FROM t_template_log WHERE sn_device = """ & SN & """ AND pin = """ & pin & """ "
        ExecuteSQLite_Data strSQL
        
        strSQL = "REPLACE INTO t_template_log (sn_device, pin, status, last_update) " & vbNewLine & _
            "VALUES (""" & SN & """, """ & pin & """, 2, """ & CreatedDate() & """) "
        ExecuteSQLite_Data strSQL
    End If
    
    Exit Sub
        
BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Sub

Public Function GetAllUser(SN As String, mbGetState As Boolean, mnCommHandleIndex As Long) As Boolean
    On Error GoTo BugError
    
    Dim vEnrollNumber As Long
    Dim vStrEnrollNumber As String
    Dim vBackupNumber As Long
    Dim vPrivilege As Long
    Dim vnEnableFlag As Long
    Dim vnMessRet As Long
    Dim vTitle As String
    Dim vnResultCode As Long
    
    GetAllUser = False
    pinTemp = ""
    jmlEnroll = 0
    DoEvents
    
    vnResultCode = FK_EnableDevice(mnCommHandleIndex, 0)
    If vnResultCode <> RUN_SUCCESS Then
        Exit Function
    End If

    vnResultCode = FK_ReadAllUserID(mnCommHandleIndex)
    If vnResultCode <> RUN_SUCCESS Then
        FK_EnableDevice mnCommHandleIndex, 1
        Exit Function
    End If
    
    Dim RecstAtt As cRecordset
    strSQL = "select * from t_template_revo where pin = '0' and template_index = 10 "
    Set RecstAtt = getRecordSet_Data(strSQL, True)
    Do While RecstAtt.EOF = False
        EnrollDataDefault = RecstAtt!template_blob
        RecstAtt.MoveNext
    Loop
    strTmpPwdDefault = ShowByteArrayToString(EnrollDataDefault, PWD_DATA_SIZE)
    strTmpPwd = "0000000000000000317A2B1944DF54D5A10000000000000000000000000000000000000000000000"
    strTmpPwd2 = "00000000000000003030303030303030300000000000000000000000000000000000000000000000"
    strTmpCard = "00000000000000000030303030303030300000000000000000000000000000000000000000000000"
    
    strSQL = "DELETE FROM t_template_revo_new WHERE sn_device = """ & SN & """ "
    ExecuteSQLite_Data strSQL
    strSQL = "DELETE FROM t_template_revo_new WHERE sn_device NOT IN (SELECT sn_device FROM t_device) "
    ExecuteSQLite_Data strSQL
    
    Dim rs_template As cRecordset
    strSQL = "Select * From t_template_revo_new WHERE sn_device = """ & SN & """ "
    Set rs_template = getRecordSet_Data(strSQL, False)
    
'---- Get Enroll data and save into database -------------
    mbGetState = True
    Dim vnResultSupportStringID As Long
    vnResultSupportStringID = FK_GetIsSupportStringID(mnCommHandleIndex)
    If vnResultSupportStringID >= RUN_SUCCESS Then
        Do
            If vnResultSupportStringID = USER_ID_LEN13_1 Then
                vStrEnrollNumber = Space(USER_ID_LEN13_1)
            Else
                vStrEnrollNumber = Space(USER_ID_LEN)
            End If
            
            vnResultCode = FK_GetAllUserID_StringID(mnCommHandleIndex, vStrEnrollNumber, vBackupNumber, vPrivilege, vnEnableFlag)
            If vnResultCode <> RUN_SUCCESS Then
                If vnResultCode = RUNERR_DATAARRAY_END Then
                    vnResultCode = RUN_SUCCESS
                End If
                Exit Do
            End If
            
            vStrEnrollNumber = Trim(vStrEnrollNumber)
            vStrEnrollNumber = getAngka(vStrEnrollNumber, False)
            
            vnResultCode = FK_GetEnrollData_StringID(mnCommHandleIndex, vStrEnrollNumber, vBackupNumber, vPrivilege, _
                mbytEnrollData(0), mlngPasswordData)
        
            If vnResultCode = RUN_SUCCESS Then
                FuncSaveTemplate SN, vStrEnrollNumber, vBackupNumber, mnCommHandleIndex, rs_template
            End If
            
            DoEvents
        Loop
    Else
        Do
            vnResultCode = FK_GetAllUserID(mnCommHandleIndex, vEnrollNumber, vBackupNumber, vPrivilege, vnEnableFlag)
            If vnResultCode <> RUN_SUCCESS Then
                If vnResultCode = RUNERR_DATAARRAY_END Then
                    vnResultCode = RUN_SUCCESS
                End If
                Exit Do
            End If
            
            vStrEnrollNumber = Trim(CStr(vEnrollNumber))
            
            vnResultCode = FK_GetEnrollData(mnCommHandleIndex, vEnrollNumber, vBackupNumber, vPrivilege, mbytEnrollData(0), _
                mlngPasswordData)
        
            If vnResultCode = RUN_SUCCESS Then
                FuncSaveTemplate SN, vStrEnrollNumber, vBackupNumber, mnCommHandleIndex, rs_template
            End If
            
            DoEvents
        Loop
    End If
    mbGetState = False
    DoEvents
    
    If vnResultCode = RUN_SUCCESS Then
        rs_template.UpdateBatch
        
        GetAllUser = True
    End If

    FK_EnableDevice mnCommHandleIndex, 1
    
    Exit Function
        
BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Function

Public Function GetAllUserNeo(sn_device As String) As Boolean
    On Error GoTo BugError
    
    Dim ada As Boolean
    Dim pin As String
    Dim nama_alias As String
    Dim privilege_id As Integer
    Dim Privilege_name As String
    Dim strTmp As String
    Dim dateStr As String
    
    strSQL = "DELETE FROM t_template_neo_new WHERE sn_device = """ & sn_device & """ "
    ExecuteSQLite_Data strSQL
    strSQL = "DELETE FROM t_template_neo_new WHERE sn_device NOT IN (SELECT sn_device FROM t_device) "
    ExecuteSQLite_Data strSQL
    
    SQL1 = ""
    SQL2 = ""
    execRecCount = 0
    dateStr = getCurrentDT
    ada = False
    
    Dim extraProperty() As Byte, extraData() As Byte, userIDBytes() As Byte
    Dim deviceStatus() As Byte
    Dim userList() As UserExt
    Dim u As Variant
    Dim result As Boolean
    
    deviceStatus = deviceTool.GetBytesByNum(CStr(DeviceBusy), NumberType_Int32Bit)
    result = deviceConn.SetPropertyExt_2(DeviceProperty_Enable, extraProperty, devNeo, deviceStatus)
    DoEvents
    If result Then
        userIDBytes = deviceTool.GetBytes("0")
        result = deviceConn.GetPropertyExt_2(DeviceProperty_Enrolls, userIDBytes, devNeo, extraData)
        DoEvents
        If result Then
            Dim SN As Long
            SN = 1
            userList = deviceTool.GetAllUserExtWithoutEnroll(extraData)
            For Each u In userList
                Dim enrollCol As Variant
                Dim enroll As EnrollExt
                Dim user As UserExt
                ReDim Preserve gUsers(SN) As UserExt
                
                Set user = u
                Set gUsers(SN) = u
                enrollCol = user.Enrolls
                Set enroll = enrollCol(0)
                result = deviceConn.GetPropertyExt(UserProperty_Enroll, extraProperty, user, extraData)
                DoEvents
                If result Then
                    Dim i As Long
                    
                    pin = user.DIN
                    nama_alias = user.UserName
                    For i = 0 To MaxFingerprintCount - 1
                        If 0 <> deviceTool.BitCheck(CLng(enroll.EnrollType), i) Then
                            strTmp = ReadFPData(pin, i)
                
                            If strTmp <> "" Then
                                SaveTemplateNeoToDatabase sn_device, pin, Trim(CStr(i)), strTmp, dateStr
                                ada = True
                            End If
                        Else
                            Exit For
                        End If
                        DoEvents
                    Next
                    
                    If 0 <> deviceTool.BitCheck(CLng(enroll.EnrollType), 10) Then
                        strTmp = ReadPwdData(pin)
                        If strTmp <> "" Then
                            SaveTemplateNeoToDatabase sn_device, pin, "10", strTmp, dateStr
                            ada = True
                        End If
                        DoEvents
                    End If
                    
                    If 0 <> deviceTool.BitCheck(CLng(enroll.EnrollType), 11) Then
                        strTmp = ReadCardData(pin)
                        If strTmp <> "" Then
                            SaveTemplateNeoToDatabase sn_device, pin, "11", strTmp, dateStr
                            ada = True
                        End If
                        DoEvents
                    End If
                    
                    privilege_id = user.Privilege
                    Privilege_name = GetPrivilegeName(user.Privilege)
                    DoEvents
                Else
                    deviceStatus = deviceTool.GetBytesByNum(CStr(DeviceIdle), NumberType_Int32Bit)
                    result = deviceConn.SetPropertyExt_2(DeviceProperty_Enable, extraProperty, devNeo, deviceStatus)
                    DoEvents
                    Exit Function
                End If
                
                SN = SN + 1
            Next
        End If
        
        deviceStatus = deviceTool.GetBytesByNum(CStr(DeviceIdle), NumberType_Int32Bit)
        result = deviceConn.SetPropertyExt_2(DeviceProperty_Enable, extraProperty, devNeo, deviceStatus)
        DoEvents
    End If
    
    If ada = True Then
        If execRecCount > 0 Then
            If Trim(SQL1) <> "" Then
                ExecuteSQLite_Data SQL1
            End If
            
            SQL1 = ""
            SQL2 = ""
            execRecCount = 0
        End If
    End If

    Exit Function
        
BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Function

Public Function GetAllUserTemplate(SN As String, mbGetState As Boolean, mnCommHandleIndex As Long) As Boolean
    On Error GoTo BugError
    
    Dim vEnrollNumber As Long
    Dim vStrEnrollNumber As String
    Dim vBackupNumber As Long
    Dim vPrivilege As Long
    Dim vnEnableFlag As Long
    Dim vnMessRet As Long
    Dim vTitle As String
    Dim vnResultCode As Long
    
    GetAllUserTemplate = False
    pinTemp = ""
    jmlEnroll = 0
    DoEvents
    
    vnResultCode = FK_EnableDevice(mnCommHandleIndex, 0)
    If vnResultCode <> RUN_SUCCESS Then
        Exit Function
    End If

    vnResultCode = FK_ReadAllUserID(mnCommHandleIndex)
    If vnResultCode <> RUN_SUCCESS Then
        FK_EnableDevice mnCommHandleIndex, 1
        Exit Function
    End If
    
    Dim RecstAtt As cRecordset
    strSQL = "select * from t_template_revo where pin = '0' and template_index = 10 "
    Set RecstAtt = getRecordSet_Data(strSQL, True)
    Do While RecstAtt.EOF = False
        EnrollDataDefault = RecstAtt!template_blob
        RecstAtt.MoveNext
    Loop
    strTmpPwdDefault = ShowByteArrayToString(EnrollDataDefault, PWD_DATA_SIZE)
    strTmpPwd = "0000000000000000317A2B1944DF54D5A10000000000000000000000000000000000000000000000"
    strTmpPwd2 = "00000000000000003030303030303030000000000000000000000000000000000000000000000000"
    strTmpCard = "00000000000000000030303030303030300000000000000000000000000000000000000000000000"
    
    strSQL = "DELETE FROM t_template_revo_new WHERE sn_device = """ & SN & """ "
    ExecuteSQLite_Data strSQL
    strSQL = "DELETE FROM t_template_revo_new WHERE sn_device NOT IN (SELECT sn_device FROM t_device) "
    ExecuteSQLite_Data strSQL
    
    Dim rs_template As cRecordset
    strSQL = "Select * From t_template_revo_new WHERE sn_device = """ & SN & """ "
    Set rs_template = getRecordSet_Data(strSQL, False)
    
'---- Get Enroll data and save into database -------------
    mbGetState = True
    Dim vnResultSupportStringID As Long
    vnResultSupportStringID = FK_GetIsSupportStringID(mnCommHandleIndex)
    If vnResultSupportStringID >= RUN_SUCCESS Then
        Do
            If vnResultSupportStringID = USER_ID_LEN13_1 Then
                vStrEnrollNumber = Space(USER_ID_LEN13_1)
            Else
                vStrEnrollNumber = Space(USER_ID_LEN)
            End If
            
            vnResultCode = FK_GetAllUserID_StringID(mnCommHandleIndex, vStrEnrollNumber, vBackupNumber, vPrivilege, vnEnableFlag)
            If vnResultCode <> RUN_SUCCESS Then
                If vnResultCode = RUNERR_DATAARRAY_END Then
                    vnResultCode = RUN_SUCCESS
                End If
                Exit Do
            End If
            
            vStrEnrollNumber = Trim(vStrEnrollNumber)
            vStrEnrollNumber = getAngka(vStrEnrollNumber, False)
            
            If InStr(pinUnreg, "#" & vStrEnrollNumber & "#") > 0 Then
                vnResultCode = FK_GetEnrollData_StringID(mnCommHandleIndex, vStrEnrollNumber, vBackupNumber, vPrivilege, _
                    mbytEnrollData(0), mlngPasswordData)
            
                If vnResultCode = RUN_SUCCESS Then
                    FuncSaveTemplate SN, vStrEnrollNumber, vBackupNumber, mnCommHandleIndex, rs_template
                End If
            End If
            
            DoEvents
        Loop
    Else
        Do
            vnResultCode = FK_GetAllUserID(mnCommHandleIndex, vEnrollNumber, vBackupNumber, vPrivilege, vnEnableFlag)
            If vnResultCode <> RUN_SUCCESS Then
                If vnResultCode = RUNERR_DATAARRAY_END Then
                    vnResultCode = RUN_SUCCESS
                End If
                Exit Do
            End If
            
            vStrEnrollNumber = Trim(CStr(vEnrollNumber))
            
            If InStr(pinUnreg, "#" & vStrEnrollNumber & "#") > 0 Then
                vnResultCode = FK_GetEnrollData(mnCommHandleIndex, vEnrollNumber, vBackupNumber, vPrivilege, mbytEnrollData(0), _
                    mlngPasswordData)
            
                If vnResultCode = RUN_SUCCESS Then
                    FuncSaveTemplate SN, vStrEnrollNumber, vBackupNumber, mnCommHandleIndex, rs_template
                End If
            End If
            
            DoEvents
        Loop
    End If
    mbGetState = False
    DoEvents
    
    If vnResultCode = RUN_SUCCESS Then
        rs_template.UpdateBatch
        
        GetAllUserTemplate = True
    End If

    FK_EnableDevice mnCommHandleIndex, 1
    
    Exit Function
        
BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Function

Public Function GetUserUnReg(SN As String, mbGetState As Boolean, mnCommHandleIndex As Long) As Boolean
    On Error GoTo BugError
    
    Dim field() As String
    Dim str1 As String
    Dim str2 As String
    Dim i As Integer
    Dim J As Integer
    Dim ada As Boolean
    
    Dim vEnrollNumber As Long
    Dim vStrEnrollNumber As String
    Dim vBackupNumber As Long
    Dim vPrivilege As Long
    Dim vnEnableFlag As Long
    Dim vnMessRet As Long
    Dim vnResultCode As Long
    
    GetUserUnReg = False
    DoEvents
    
    vnResultCode = FK_EnableDevice(mnCommHandleIndex, 0)
    If vnResultCode <> RUN_SUCCESS Then
        Exit Function
    End If

    Dim RecstAtt As cRecordset
    strSQL = "select * from t_template_revo where pin = '0' and template_index = 10 "
    Set RecstAtt = getRecordSet_Data(strSQL, True)
    Do While RecstAtt.EOF = False
        EnrollDataDefault = RecstAtt!template_blob
        RecstAtt.MoveNext
    Loop
    strTmpPwdDefault = ShowByteArrayToString(EnrollDataDefault, PWD_DATA_SIZE)
    strTmpPwd = "0000000000000000317A2B1944DF54D5A10000000000000000000000000000000000000000000000"
    strTmpPwd2 = "00000000000000003030303030303030300000000000000000000000000000000000000000000000"
    strTmpCard = "00000000000000000030303030303030300000000000000000000000000000000000000000000000"
    
    strSQL = "DELETE FROM t_template_revo_new WHERE sn_device = """ & SN & """ "
    ExecuteSQLite_Data strSQL
    strSQL = "DELETE FROM t_template_revo_new WHERE sn_device NOT IN (SELECT sn_device FROM t_device) "
    ExecuteSQLite_Data strSQL
    
    Dim rs_template As cRecordset
    strSQL = "Select * From t_template_revo_new WHERE sn_device = """ & SN & """ "
    Set rs_template = getRecordSet_Data(strSQL, False)
    
    ada = False
    field = Split(pinUnregMesin & ",", ",")
    Dim vnResultSupportStringID As Long
    
    vnResultSupportStringID = FK_GetIsSupportStringID(mnCommHandleIndex)
    If vnResultSupportStringID >= RUN_SUCCESS Then
        If vnResultSupportStringID = USER_ID_LEN13_1 Then
            vStrEnrollNumber = Space(USER_ID_LEN13_1)
        Else
            vStrEnrollNumber = Space(USER_ID_LEN)
        End If
        
        For i = 0 To UBound(field) - 1
            str1 = field(i)
            If Trim(str1) <> "" And Len(Trim(str1)) <= 9 Then
                vStrEnrollNumber = str1
                For J = 0 To 9
                    vBackupNumber = J
                    vnResultCode = FK_GetEnrollData_StringID(mnCommHandleIndex, vStrEnrollNumber, vBackupNumber, vPrivilege, mbytEnrollData(0), mlngPasswordData)
                    If vnResultCode = RUN_SUCCESS Then
                        FuncSaveTemplate SN, vStrEnrollNumber, vBackupNumber, mnCommHandleIndex, rs_template
                        ada = True
                    Else
                        Exit For
                    End If
                Next
                
                For J = BACKUP_PSW To BACKUP_PALMVEIN_3
                    vBackupNumber = J
                    vnResultCode = FK_GetEnrollData_StringID(mnCommHandleIndex, vStrEnrollNumber, vBackupNumber, vPrivilege, mbytEnrollData(0), mlngPasswordData)
                    If vnResultCode = RUN_SUCCESS Then
                        FuncSaveTemplate SN, vStrEnrollNumber, vBackupNumber, mnCommHandleIndex, rs_template
                        ada = True
                    Else
                        If J >= BACKUP_PALMVEIN_1 Then
                            Exit For
                        End If
                    End If
                Next
            End If
            DoEvents
        Next
    Else
        For i = 0 To UBound(field) - 1
            str1 = field(i)
            If Trim(str1) <> "" And Len(Trim(str1)) <= 8 Then
                vEnrollNumber = Val(str1)
                
                For J = 0 To 9
                    vBackupNumber = J
                    vnResultCode = FK_GetEnrollData(mnCommHandleIndex, vEnrollNumber, vBackupNumber, vPrivilege, mbytEnrollData(0), mlngPasswordData)
                    If vnResultCode = RUN_SUCCESS Then
                        FuncSaveTemplate SN, CStr(vEnrollNumber), vBackupNumber, mnCommHandleIndex, rs_template
                        ada = True
                    Else
                        Exit For
                    End If
                Next
                
                For J = BACKUP_PSW To BACKUP_PALMVEIN_3
                    vBackupNumber = J
                    vnResultCode = FK_GetEnrollData(mnCommHandleIndex, vEnrollNumber, vBackupNumber, vPrivilege, mbytEnrollData(0), mlngPasswordData)
                    If vnResultCode = RUN_SUCCESS Then
                        FuncSaveTemplate SN, CStr(vEnrollNumber), vBackupNumber, mnCommHandleIndex, rs_template
                        ada = True
                    Else
                        If J >= BACKUP_PALMVEIN_1 Then
                            Exit For
                        End If
                    End If
                Next
            End If
            DoEvents
        Next
    End If
    
    If ada = True Then
        rs_template.UpdateBatch
        
        GetUserUnReg = True
    End If

    FK_EnableDevice mnCommHandleIndex, 1
    
    Exit Function
        
BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Function

Public Function GetUserUnRegNeo(SN As String) As Boolean
    On Error GoTo BugError
    
    Dim field() As String
    Dim str1 As String
    Dim str2 As String
    Dim i As Integer
    Dim J As Long
    Dim ada As Boolean
    
    Dim pin As String
    Dim strTmp As String
    Dim dateStr As String
    
    strSQL = "DELETE FROM t_template_neo_new WHERE sn_device = """ & SN & """ "
    ExecuteSQLite_Data strSQL
    strSQL = "DELETE FROM t_template_neo_new WHERE sn_device NOT IN (SELECT sn_device FROM t_device) "
    ExecuteSQLite_Data strSQL
    
    SQL1 = ""
    SQL2 = ""
    execRecCount = 0
    dateStr = getCurrentDT
    
    ada = False
    field = Split(pinUnregMesin & ",", ",")
    For i = 0 To UBound(field) - 1
        str1 = field(i)
        If Trim(str1) <> "" Then
            pin = Trim(str1)
            For J = 0 To 9
                strTmp = ReadFPData(pin, J)
                
                If strTmp <> "" Then
                    SaveTemplateNeoToDatabase SN, pin, Trim(CStr(J)), strTmp, dateStr
                    
                    strSQL = "DELETE FROM t_template_log_neo WHERE sn_device = """ & SN & """ AND pin = """ & pin & """ "
                    ExecuteSQLite_Data strSQL
                    
                    strSQL = "REPLACE INTO t_template_log_neo (sn_device, pin, status, last_update) " & vbNewLine & _
                        "VALUES (""" & SN & """, """ & pin & """, 2, """ & CreatedDate() & """) "
                    ExecuteSQLite_Data strSQL
                    
                    ada = True
                Else
                    Exit For
                End If
            Next
            
            strTmp = ReadPwdData(pin)
            If strTmp <> "" Then
                SaveTemplateNeoToDatabase SN, pin, "10", strTmp, dateStr
                
                strSQL = "DELETE FROM t_template_log_neo WHERE sn_device = """ & SN & """ AND pin = """ & pin & """ "
                ExecuteSQLite_Data strSQL
                
                strSQL = "REPLACE INTO t_template_log_neo (sn_device, pin, status, last_update) " & vbNewLine & _
                    "VALUES (""" & SN & """, """ & pin & """, 2, """ & CreatedDate() & """) "
                ExecuteSQLite_Data strSQL
                
                ada = True
            End If
            
            strTmp = ReadCardData(pin)
            If strTmp <> "" Then
                SaveTemplateNeoToDatabase SN, pin, "11", strTmp, dateStr
                
                strSQL = "DELETE FROM t_template_log_neo WHERE sn_device = """ & SN & """ AND pin = """ & pin & """ "
                ExecuteSQLite_Data strSQL
                
                strSQL = "REPLACE INTO t_template_log_neo (sn_device, pin, status, last_update) " & vbNewLine & _
                    "VALUES (""" & SN & """, """ & pin & """, 2, """ & CreatedDate() & """) "
                ExecuteSQLite_Data strSQL
                
                ada = True
            End If
        End If
        DoEvents
    Next
    
    If ada = True Then
        If execRecCount > 0 Then
            If Trim(SQL1) <> "" Then
                ExecuteSQLite_Data SQL1
            End If
            
            SQL1 = ""
            SQL2 = ""
            execRecCount = 0
        End If
    End If

    Exit Function
        
BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Function

Private Function ReadFPData(pin As String, template_index As Long) As String
    On Error GoTo BugError
    
    ReadFPData = ""
    
    Dim strFingerprint As String
    
    Dim extraProperty() As Byte, extraData() As Byte
    Dim deviceStatus() As Byte, tmpFP(MaxFingerprintLength - 1) As Byte
    Dim user As UserExt
    Dim enroll(0) As EnrollExt
    Dim result As Boolean
    Dim tmpFpBytes() As Byte 'for sample ConvertFingerprintToString
    
    strFingerprint = ""
    
    If "" = Trim(pin) Then
        Exit Function
    End If
    
    If False = IsNumeric(Trim(pin)) Then
        Exit Function
    End If
    
    deviceStatus = deviceTool.GetBytesByNum(CStr(DeviceBusy), NumberType_Int32Bit)
    result = deviceConn.SetPropertyExt_2(DeviceProperty_Enable, extraProperty, devNeo, deviceStatus)
    DoEvents
    If result Then
        extraProperty = deviceTool.GetBytesByNum(CStr(CLng(UserEnrollCommand_ReadFingerprint)), NumberType_Int32Bit)
        Set user = New UserExt
        Set enroll(0) = New EnrollExt
        user.DIN = Trim(pin)
        enroll(0).DIN = user.DIN
        enroll(0).EnrollType = template_index
        enroll(0).SetFingerprint tmpFP
        user.SetEnrolls enroll
        
        result = deviceConn.GetPropertyExt(UserProperty_UserEnroll, extraProperty, user, extraData)
        If result Then
            Dim enrollCol As Variant
            Dim tmp As EnrollExt
            Dim fp As Variant
            
            enrollCol = user.Enrolls
            Set tmp = enrollCol(0)
            fp = tmp.Fingerprint
            fpBytes = fp
            
            tmpFpBytes = fp
            strFingerprint = deviceTool.ConvertFingerprintToString(tmpFpBytes) 'convert fp data to string
        End If
        
        deviceStatus = deviceTool.GetBytesByNum(CStr(DeviceIdle), NumberType_Int32Bit)
        result = deviceConn.SetPropertyExt_2(DeviceProperty_Enable, extraProperty, devNeo, deviceStatus)
        DoEvents
    End If
    
    ReadFPData = strFingerprint
    
    Exit Function
        
BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Function

Private Function WriteFPData(pin As String, template_index As Long, template_str As String) As Boolean
    On Error GoTo BugError
    
    WriteFPData = False
    
    Dim extraProperty() As Byte, extraData() As Byte
    Dim deviceStatus() As Byte, fp() As Byte
    Dim user As UserExt
    Dim enroll(0) As EnrollExt
    Dim result As Boolean
    Dim tmpFpBytes() As Byte 'for sample ConvertStringFingerprintToBytes
    
    If "" = Trim(pin) Then
        Exit Function
    End If
    
    If False = IsNumeric(Trim(pin)) Then
        Exit Function
    End If
    
    If "" = Trim(template_str) Then
        Exit Function
    End If
    
    deviceStatus = deviceTool.GetBytesByNum(CStr(DeviceBusy), NumberType_Int32Bit)
    result = deviceConn.SetPropertyExt_2(DeviceProperty_Enable, extraProperty, devNeo, deviceStatus)
    DoEvents
    If result Then
        ReDim fp((template_index + 1) * MaxFingerprintLength - 1) As Byte
        extraProperty = deviceTool.GetBytesByNum(CStr(CLng(UserEnrollCommand_WriteFingerprint)), NumberType_Int32Bit)
        Set user = New UserExt
        Set enroll(0) = New EnrollExt
        user.DIN = Trim(pin)
        enroll(0).DIN = user.DIN
        enroll(0).EnrollType = template_index
        
        tmpFpBytes = deviceTool.ConvertStringFingerprintToBytes(template_str)
        fpBytes = tmpFpBytes
        SetFingerprintData template_index * MaxFingerprintLength, fp
        
        enroll(0).SetFingerprint fp
        
        user.SetEnrolls enroll
        result = deviceConn.SetPropertyExt(UserProperty_UserEnroll, extraProperty, user, extraData)
        WriteFPData = result
        
        deviceStatus = deviceTool.GetBytesByNum(CStr(DeviceIdle), NumberType_Int32Bit)
        result = deviceConn.SetPropertyExt_2(DeviceProperty_Enable, extraProperty, devNeo, deviceStatus)
        DoEvents
    End If
    
    Exit Function
        
BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Function

Private Function ReadPwdData(pin As String) As String
    On Error GoTo BugError
    
    ReadPwdData = ""
    
    Dim extraProperty() As Byte, extraData() As Byte
    Dim user As UserExt
    Dim enroll(0) As EnrollExt
    Dim result As Boolean
    Dim strPassword As String
    
    strPassword = ""
    
    If "" = Trim(pin) Then
        Exit Function
    End If
    
    If False = IsNumeric(Trim(pin)) Then
        Exit Function
    End If
    
    extraProperty = deviceTool.GetBytesByNum(CStr(CLng(UserEnrollCommand_ReadPassword)), NumberType_Int32Bit)
    Set user = New UserExt
    Set enroll(0) = New EnrollExt
    user.DIN = Trim(pin)
    enroll(0).DIN = user.DIN
    user.SetEnrolls enroll
    
    result = deviceConn.GetPropertyExt(UserProperty_UserEnroll, extraProperty, user, extraData)
    If result Then
        Dim enrollCol As Variant
        Dim tmp As EnrollExt
        enrollCol = user.Enrolls
        Set tmp = enrollCol(0)
        strPassword = Trim(tmp.password)
        
        If strPassword = strPwdNeoDefault Then
            strPassword = ""
        End If
    End If
    
    ReadPwdData = strPassword
    
    Exit Function
        
BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Function

Private Function WritePwdData(pin As String, strPassword As String) As Boolean
    On Error GoTo BugError
    
    WritePwdData = False
    
    Dim extraProperty() As Byte, extraData() As Byte
    Dim user As UserExt
    Dim enroll(0) As EnrollExt
    Dim result As Boolean
    
    If "" = Trim(pin) Then
        Exit Function
    End If
    
    If False = IsNumeric(Trim(pin)) Then
        Exit Function
    End If
    
    If "" = Trim(strPassword) Then
        Exit Function
    End If
    
    If False = IsNumeric(Trim(strPassword)) Then
        Exit Function
    End If
    
    extraProperty = deviceTool.GetBytesByNum(CStr(CLng(UserEnrollCommand_WritePassword)), NumberType_Int32Bit)
    Set user = New UserExt
    Set enroll(0) = New EnrollExt
    user.DIN = Trim(pin)
    enroll(0).DIN = user.DIN
    enroll(0).password = Trim(strPassword)
    user.SetEnrolls enroll
    
    result = deviceConn.SetPropertyExt(UserProperty_UserEnroll, extraProperty, user, extraData)
    WritePwdData = result
    
    Exit Function
        
BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Function

Private Function ReadCardData(pin As String) As String
    On Error GoTo BugError
    
    ReadCardData = ""
    
    Dim extraProperty() As Byte, extraData() As Byte
    Dim user As UserExt
    Dim enroll(0) As EnrollExt
    Dim result As Boolean
    Dim strCardID As String
    
    strCardID = ""
    
    If "" = Trim(pin) Then
        Exit Function
    End If
    
    If False = IsNumeric(Trim(pin)) Then
        Exit Function
    End If
    
    extraProperty = deviceTool.GetBytesByNum(CStr(CLng(UserEnrollCommand_ReadCard)), NumberType_Int32Bit)
    Set user = New UserExt
    Set enroll(0) = New EnrollExt
    user.DIN = Trim(pin)
    enroll(0).DIN = user.DIN
    user.SetEnrolls enroll
    
    result = deviceConn.GetPropertyExt(UserProperty_UserEnroll, extraProperty, user, extraData)
    If result Then
        Dim enrollCol As Variant
        Dim tmp As EnrollExt
        enrollCol = user.Enrolls
        Set tmp = enrollCol(0)
        strCardID = Trim(tmp.CardID)
    End If
    
    ReadCardData = strCardID
    
    Exit Function
        
BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Function

Private Function WriteCardData(pin As String, strCardID As String) As String
    On Error GoTo BugError
    
    WriteCardData = False
    
    Dim extraProperty() As Byte, extraData() As Byte
    Dim user As UserExt
    Dim enroll(0) As EnrollExt
    Dim result As Boolean
    
    If "" = Trim(pin) Then
        Exit Function
    End If
    
    If False = IsNumeric(Trim(pin)) Then
        Exit Function
    End If
    
    If "" = Trim(strCardID) Then
        Exit Function
    End If
    
    If False = IsNumeric(Trim(strCardID)) Then
        Exit Function
    End If
    
    extraProperty = deviceTool.GetBytesByNum(CStr(CLng(UserEnrollCommand_WriteCard)), NumberType_Int32Bit)
    Set user = New UserExt
    Set enroll(0) = New EnrollExt
    user.DIN = Trim(pin)
    enroll(0).DIN = user.DIN
    enroll(0).CardID = Trim(strCardID)
    user.SetEnrolls enroll
    
    result = deviceConn.SetPropertyExt(UserProperty_UserEnroll, extraProperty, user, extraData)
    WriteCardData = result
    
    Exit Function
        
BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Function

Public Function SaveTemplateNeoToDatabase(sn_device As String, pin As String, template_index As String, template_str As String, last_update As String) As Boolean
    On Error GoTo BugError
    
    execRecCount = execRecCount + 1
    
    If execRecCount = 1 Then
        SQL1 = "REPLACE INTO t_template_neo_new (sn_device, pin, template_index, template_str, last_update) "
        SQL2 = "VALUES ('" & sn_device & "', '" & pin & "', " & template_index & ", '" & template_str & "', '" & last_update & "') "
    Else
        SQL2 = ", ('" & sn_device & "', '" & pin & "', " & template_index & ", '" & template_str & "', '" & last_update & "') "
    End If
    SQL1 = SQL1 + SQL2
    
    If execRecCount >= 20 Then
        If Trim(SQL1) <> "" Then
            ExecuteSQLite_Data SQL1
        End If
        
        SQL1 = ""
        SQL2 = ""
        execRecCount = 0
    End If
      
    SaveTemplateNeoToDatabase = True
    
    Exit Function
        
BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Function

Public Function DownloadUser(sn_device As String) As Boolean
    On Error GoTo BugError
    
    Dim berhasil As Boolean
            
    If InitDevice(sn_device, optConn, False) = True Then
        If cmdOpenComm(optConn) = True Then
            Select Case deviceType
            Case 2 'Mesin Neo
                If GetDevInfo = True Then
                    berhasil = DeviceInfoNeo(sn_device)
                Else
                    berhasil = True
                End If
                
                If berhasil = True Then
                    berhasil = GetUserUnRegNeo(sn_device)
                End If
                
                cmdCloseCommNeo
                DownloadUser = True
            Case 3 'Mesin Revo
                mnCommHandleIndex = gnCommHandleIndex
                mbGetState = gbOpenFlag
                
                If GetDevInfo = True Then
                    berhasil = DeviceInfo(sn_device, mnCommHandleIndex)
                Else
                    berhasil = True
                End If
                
                If berhasil = True Then
                    berhasil = GetUserUnReg(sn_device, mbGetState, mnCommHandleIndex)
                End If
                
                cmdCloseComm
                DownloadUser = True
            End Select
        Else
            DownloadUser = False
        End If
    Else
        DownloadUser = False
    End If
    
    Set CnTemp = Nothing
    
    Exit Function
        
BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Function

Public Function DownloadAllUser(sn_device As String) As Boolean
    On Error GoTo BugError
    
    Dim berhasil As Boolean
            
    If InitDevice(sn_device, optConn, False) = True Then
        If cmdOpenComm(optConn) = True Then
            Select Case deviceType
            Case 2 'Mesin Neo
                If GetDevInfo = True Then
                    berhasil = DeviceInfoNeo(sn_device)
                Else
                    berhasil = True
                End If
                
                If berhasil = True Then
                    berhasil = GetAllUserNeo(sn_device)
                End If
                
                cmdCloseCommNeo
                DownloadAllUser = True
            Case 3 'Mesin Revo
                mnCommHandleIndex = gnCommHandleIndex
                mbGetState = gbOpenFlag
                
                If GetDevInfo = True Then
                    berhasil = DeviceInfo(sn_device, mnCommHandleIndex)
                Else
                    berhasil = True
                End If
                
                If berhasil = True Then
                    berhasil = GetAllUser(sn_device, mbGetState, mnCommHandleIndex)
                End If
                
                cmdCloseComm
                DownloadAllUser = True
            End Select
        Else
            DownloadAllUser = False
        End If
    Else
        DownloadAllUser = False
    End If
    
    Set CnTemp = Nothing
    
    Exit Function
        
BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Function

Public Function ValidasiAllMesin(ArrSN() As String) As Boolean
    Dim x As Integer
    Dim str1 As String
    Dim str2 As String
    Dim msg1 As String
    Dim Aa As String
    
    Dim sn_device As String
    Dim Jml As Integer
    
    ValidasiAllMesin = False
    
    msg1 = INILanRead("ErrMsg", "msg4", "...")
    msg1 = Replace(msg1, "/n", vbNewLine)
    str1 = ""
    For x = 0 To UBound(ArrSN) - 1
        sn_device = Trim(ArrSN(x))
        Jml = Val(GetValueField("SELECT count(*) AS Jml FROM t_device WHERE sn_device = '" & sn_device & "' AND comm_type < 4 ", "Jml"))
        If Jml > 0 Then
            ValidasiMesin sn_device, str2
            str1 = str1 & str2
        End If
        DoEvents
    Next
    
    If str1 <> "" Then
        msg1 = Replace(msg1, "?1", str1)
        msg1 = Replace(msg1, "/n", vbNewLine)
        Aa = MsgBox(msg1, vbYesNo + vbDefaultButton1, App.Title) ' & " : " & "msg4"
        If Aa = vbYes Then
            ValidasiAllMesin = True
            
            For x = 0 To UBound(ArrSN) - 1
                sn_device = Trim(ArrSN(x))
                Jml = Val(GetValueField("SELECT count(*) AS Jml FROM t_device WHERE sn_device = '" & sn_device & "' AND comm_type < 4 ", "Jml"))
                If Jml > 0 Then
                    KonfirmasiValidasiMesin sn_device
                End If
                DoEvents
            Next
        End If
    End If
    
    ValidasiAllMesin = True
End Function

Public Function CekPinNotInEmp(sn_device As String, fnCommHandleIndex As Long) As String
    On Error GoTo BugError
    
    Dim rs As cRecordset
    
    Dim pin As String
    Dim nama As String
    Dim str1 As String
    Dim str2 As String
    Dim str3 As String
    
    CekPinNotInEmp = ""
    str1 = INILanRead("ErrMsg", "msg5", "...")
    str3 = ""
    
    strSQL = "SELECT DISTINCT pin FROM t_template_revo_new WHERE pin <> '99999999' AND sn_device = """ & sn_device & """ " & _
        "AND pin NOT IN (SELECT DISTINCT pin FROM t_employee WHERE active = 1) "
    Set rs = getRecordSet_Data(strSQL, True)
    If rs.RecordCount > 0 Then
        Do While Not rs.EOF
            pin = rs.fields("pin").value
            
            If InStr(str2, pin) = 0 Then
                nama = GetUserName(pin, fnCommHandleIndex)
                str2 = str1
                str2 = Replace(str2, "?1", pin)
                str2 = Replace(str2, "?2", nama)
                str2 = Replace(str2, "?3", sn_device)
                str3 = str3 & str2 & vbNewLine
            End If
            
            DoEvents
            rs.MoveNext
        Loop
    End If
    
    CekPinNotInEmp = str3
    
    Exit Function
        
BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Function

Public Function CekPinNotInEmpNeo(sn_device As String) As String
    On Error GoTo BugError
    
    Dim rs As cRecordset
    
    Dim pin As String
    Dim nama As String
    Dim str1 As String
    Dim str2 As String
    Dim str3 As String
    
    CekPinNotInEmpNeo = ""
    str1 = INILanRead("ErrMsg", "msg5", "...")
    str3 = ""
    
    strSQL = "SELECT DISTINCT pin FROM t_template_neo_new WHERE pin <> '99999999' AND sn_device = """ & sn_device & """ " & _
        "AND pin NOT IN (SELECT DISTINCT pin FROM t_employee WHERE active = 1) "
    Set rs = getRecordSet_Data(strSQL, True)
    If rs.RecordCount > 0 Then
        Do While Not rs.EOF
            pin = rs.fields("pin").value
            
            If InStr(str2, pin) = 0 Then
                nama = GetUserNameNeo(pin)
                str2 = str1
                str2 = Replace(str2, "?1", pin)
                str2 = Replace(str2, "?2", nama)
                str2 = Replace(str2, "?3", sn_device)
                str3 = str3 & str2 & vbNewLine
            End If
            
            DoEvents
            rs.MoveNext
        Loop
    End If
    
    CekPinNotInEmpNeo = str3
    
    Exit Function
        
BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Function

Public Function ValidasiMesin(sn_device As String, ByRef msg1 As String) As Boolean
    On Error GoTo BugError
    
    Dim berhasil As Boolean
    Dim name_device As String
    
    Dim str1 As String
    Dim str2 As String
    Dim str3 As String
    
    ValidasiMesin = False
    msg1 = ""
    name_device = vnname_device
    
    If InitDevice(sn_device, optConn, False) = True Then
        If vnstatus_aktif = "1" Then
            If cmdOpenComm(optConn) = True Then
                Select Case deviceType
                Case 2 'Mesin Neo
                    str1 = GetValueField("select * from t_device_info where sn_device = """ & sn_device & """ ", "manager_count")
                    str2 = INILanRead("frmValidasiMesin", "Err1.Device.1", "")
                    str2 = Replace(str2, "?1", name_device)
                    str2 = Replace(str2, "?2", str1)
                    str3 = name_device & ";" & str2 & ";" & "[1];"
                    
                    str1 = GetValueField("select * from t_device_info where sn_device = """ & sn_device & """ ", "user_count")
                    str2 = INILanRead("ErrMsg", "Err1.Device.2", "")
                    str2 = Replace(str2, "?1", name_device)
                    str2 = Replace(str2, "?2", str1)
                    str3 = name_device & ";" & str2 & ";" & "[1];"
                    
                    str1 = GetValueField("select * from t_device_info where sn_device = """ & sn_device & """ ", "all_attlog_count")
                    str2 = INILanRead("ErrMsg", "Err1.Device.3", "")
                    str2 = Replace(str2, "?1", name_device)
                    str2 = Replace(str2, "?2", str1)
                    str3 = name_device & ";" & str2 & ";" & "[1];"
                    
                    berhasil = GetAllUserNeo(sn_device)
                    
                    str1 = GetValueField("select count(*) as Jml from " & _
                        "(select DISTINCT pin from t_template_neo_new where pin <> '99999999' and pin not in (select pin from t_employee WHERE active =1))", "Jml")
                    str2 = INILanRead("ErrMsg", "Err1.Device.4", "")
                    str2 = Replace(str2, "?1", name_device)
                    str2 = Replace(str2, "?2", str1)
                    str3 = name_device & ";" & str2 & ";" & "[1];"
                    
                    str1 = GetValueField("select count(*) as Jml from " & _
                        "(select DISTINCT pin from t_employee where active = 1 AND pin not in (select pin from t_template_neo_new))", "Jml")
                    str2 = INILanRead("ErrMsg", "Err1.Device.5", "")
                    str2 = Replace(str2, "?1", name_device)
                    str2 = Replace(str2, "?2", str1)
                    str3 = name_device & ";" & str2 & ";" & "[1];"
                    
                    str2 = INILanRead("ErrMsg", "Err1.Device.9", "")
                    str2 = Replace(str2, "?1", name_device)
                    str3 = name_device & ";" & str2 & ";" & "[1];"
                    
                    msg1 = CekPinNotInEmpNeo(sn_device)
                    DoEvents
                    
                    cmdCloseCommNeo
                Case 3 'Mesin Revo
                    mnCommHandleIndex = gnCommHandleIndex
                    mbGetState = gbOpenFlag
                    
                    str1 = GetValueField("select * from t_device_info where sn_device = """ & sn_device & """ ", "manager_count")
                    str2 = INILanRead("frmValidasiMesin", "Err1.Device.1", "")
                    str2 = Replace(str2, "?1", name_device)
                    str2 = Replace(str2, "?2", str1)
                    str3 = name_device & ";" & str2 & ";" & "[1];"
                    
                    str1 = GetValueField("select * from t_device_info where sn_device = """ & sn_device & """ ", "user_count")
                    str2 = INILanRead("ErrMsg", "Err1.Device.2", "")
                    str2 = Replace(str2, "?1", name_device)
                    str2 = Replace(str2, "?2", str1)
                    str3 = name_device & ";" & str2 & ";" & "[1];"
                    
                    str1 = GetValueField("select * from t_device_info where sn_device = """ & sn_device & """ ", "all_attlog_count")
                    str2 = INILanRead("ErrMsg", "Err1.Device.3", "")
                    str2 = Replace(str2, "?1", name_device)
                    str2 = Replace(str2, "?2", str1)
                    str3 = name_device & ";" & str2 & ";" & "[1];"
                    
                    berhasil = GetAllUser(sn_device, mbGetState, mnCommHandleIndex)
                    
                    str1 = GetValueField("select count(*) as Jml from " & _
                        "(select DISTINCT pin from t_template_revo_new where pin <> '99999999' and pin not in (select pin from t_employee WHERE active =1))", "Jml")
                    str2 = INILanRead("ErrMsg", "Err1.Device.4", "")
                    str2 = Replace(str2, "?1", name_device)
                    str2 = Replace(str2, "?2", str1)
                    str3 = name_device & ";" & str2 & ";" & "[1];"
                    
                    str1 = GetValueField("select count(*) as Jml from " & _
                        "(select DISTINCT pin from t_employee where active = 1 AND pin not in (select pin from t_template_revo_new))", "Jml")
                    str2 = INILanRead("ErrMsg", "Err1.Device.5", "")
                    str2 = Replace(str2, "?1", name_device)
                    str2 = Replace(str2, "?2", str1)
                    str3 = name_device & ";" & str2 & ";" & "[1];"
                    
                    str2 = INILanRead("ErrMsg", "Err1.Device.9", "")
                    str2 = Replace(str2, "?1", name_device)
                    str3 = name_device & ";" & str2 & ";" & "[1];"
                    
                    msg1 = CekPinNotInEmp(sn_device, mnCommHandleIndex)
                    DoEvents
                    
                    cmdCloseComm
                    ValidasiMesin = True
                End Select
            End If
        Else
            ValidasiMesin = True
        End If
    End If
    
    Set CnTemp = Nothing
    
    Exit Function
        
BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Function

Public Function KonfirmasiValidasiMesin(sn_device As String) As Boolean
    On Error GoTo BugError
    
    Dim berhasil As Boolean
    Dim str1 As String
    Dim str2 As String
    Dim str3 As String
    
    KonfirmasiValidasiMesin = False
    
    If InitDevice(sn_device, optConn, False) = True Then
        If vnstatus_aktif = "1" Then
            If cmdOpenComm(optConn) = True Then
                Select Case deviceType
                Case 2 'Mesin Neo
                    KonfirmasiValidasiMesin = True
                    DeletePinNotInEmpNeo sn_device
                    DoEvents
                    
                    berhasil = DeviceInfoNeo(sn_device)
                    DoEvents
                    
                    cmdCloseCommNeo
                Case 3 'Mesin Revo
                    mnCommHandleIndex = gnCommHandleIndex
                    mbGetState = gbOpenFlag
                    
                    KonfirmasiValidasiMesin = True
                    DeletePinNotInEmp sn_device, mnCommHandleIndex
                    DoEvents
                    
                    berhasil = DeviceInfo(sn_device, mnCommHandleIndex)
                    DoEvents
                    
                    cmdCloseComm
                End Select
            Else
                MsgBoxLanguageFile "Err1.Device.16", vnname_device, sn_device
            End If
        Else
            KonfirmasiValidasiMesin = True
        End If
    End If
    
    Set CnTemp = Nothing
    
    Exit Function
        
BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Function

Public Function DeletePinNotInEmp(sn_device As String, mnCommHandleIndex As Long) As Boolean
    Dim rs As cRecordset
    
    Dim sukses As Boolean
    Dim pin As String
    Dim str1 As String
    Dim str2 As String
    Dim str3 As String
    Dim template_index As Long
    
    strSQL = "SELECT * FROM t_template_revo_new WHERE pin <> '99999999' AND sn_device = """ & sn_device & """ " & _
        "AND pin NOT IN (SELECT DISTINCT pin FROM t_employee WHERE active = 1) "
    Set rs = getRecordSet_Data(strSQL, True)
    If rs.RecordCount > 0 Then
        Do While Not rs.EOF
            pin = rs.fields("pin").value
            template_index = rs.fields("template_index").value
            
            sukses = DeleteEnrollDataByPIN(pin, template_index, mnCommHandleIndex)
            
            DoEvents
            rs.MoveNext
        Loop
    End If
End Function

Public Function DeletePinNotInEmpNeo(sn_device As String) As Boolean
    Dim rs As cRecordset
    
    Dim sukses As Boolean
    Dim pin As String
    Dim str1 As String
    Dim str2 As String
    Dim str3 As String
    
    strSQL = "SELECT DISTINCT pin FROM t_template_neo_new WHERE pin <> '99999999' AND sn_device = """ & sn_device & """ " & _
        "AND pin NOT IN (SELECT DISTINCT pin FROM t_employee WHERE active = 1) "
    Set rs = getRecordSet_Data(strSQL, True)
    If rs.RecordCount > 0 Then
        Do While Not rs.EOF
            pin = rs.fields("pin").value
            
            sukses = DeleteEnrollDataByPIN_Neo(pin)
            
            DoEvents
            rs.MoveNext
        Loop
    End If
End Function

Public Function DeleteEnrollDataByPIN(pin As String, template_index As Long, mnCommHandleIndex As Long) As Boolean
    On Error GoTo BugError
    
    Dim vEnrollNumber As Long
    Dim vBackupNumber As Long
    Dim vnResultCode As Long
    Dim vStrEnrollNumber As String
    Dim J As Long
    
    DeleteEnrollDataByPIN = False
    vnResultCode = FK_EnableDevice(mnCommHandleIndex, 0)
    If vnResultCode <> RUN_SUCCESS Then
        Exit Function
    End If

    vBackupNumber = template_index
    
    Dim vnResultSupportStringID As Long
    vnResultSupportStringID = FK_GetIsSupportStringID(mnCommHandleIndex)
    If vnResultSupportStringID >= RUN_SUCCESS Then
        If vnResultSupportStringID = USER_ID_LEN13_1 Then
            vStrEnrollNumber = Space(USER_ID_LEN13_1)
        Else
            vStrEnrollNumber = Space(USER_ID_LEN)
        End If
        
        vStrEnrollNumber = pin
        
        vnResultCode = FK_DeleteEnrollData_StringID(mnCommHandleIndex, vStrEnrollNumber, vBackupNumber)
    Else
        vStrEnrollNumber = pin
        vEnrollNumber = Val(vStrEnrollNumber)
        
        vnResultCode = FK_DeleteEnrollData(mnCommHandleIndex, vEnrollNumber, vBackupNumber)
    End If
    
    If vnResultCode = RUN_SUCCESS Then
        DeleteEnrollDataByPIN = True
    End If

    FK_EnableDevice mnCommHandleIndex, 1
    
    Exit Function
        
BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Function

Public Function DeleteEnrollDataByPIN_Neo(pin As String) As Boolean
    On Error GoTo BugError
    
    DeleteEnrollDataByPIN_Neo = False
    
    Dim extraProperty() As Byte, extraData() As Byte
    Dim result As Boolean
    
    If False = IsNumeric(Trim(pin)) Then
        Exit Function
    End If
    
    extraData = deviceTool.GetBytes(Trim(pin))
    result = deviceConn.SetPropertyExt_2(DeviceProperty_Enrolls, extraProperty, devNeo, extraData)
    DoEvents
    DeleteEnrollDataByPIN_Neo = result
    
    Exit Function
        
BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Function

Public Function MengunciMesin(ByRef msg_str As String, sandi_kunci_mesin As String, SN As String) As Boolean
    Dim rs As cRecordset
    
    Dim name_device As String
    Dim activation_code As String
    Dim sukses As Boolean
    Dim pin As String
    Dim pwd As String
    Dim str1 As String
    Dim str2 As String
    Dim str3 As String
    Dim Serial_Number As String
    
    MengunciMesin = False
    sukses = CekKoneksiMesin(SN)
    DoEvents
    
    name_device = GetValueField("SELECT * FROM t_device WHERE sn_device = '" & SN & "' ", "name_device")
    msg_str = ""
    str3 = DateTimeToStr_DB(Now, DateTimeFormatDatabase)
    If sukses = False Then
        Dim Aa As String
            
        str1 = INILanRead("errMsg", "Err1.Device.10", "")
        str1 = Replace(str1, "?1", name_device)
        str1 = Replace(str1, "?2", SN)
        Aa = MsgBox(str1, vbYesNo + vbDefaultButton1, App.Title & " : " & "Err1.Device.10")
        If Aa <> vbYes Then
            Exit Function
        Else
            MengunciMesin = True
            Exit Function
        End If
    Else
        pin = "99999999"
        
        Select Case deviceType
        Case 2 'Mesin Neo
            pwd = GetRandomKunciMesinNeo()
        Case 3 'Mesin Revo
            str1 = GetRandomKunciMesin()
            str2 = GetValueField("select id_group from t_template_revo where pin = """ & str1 & """ ", "id_group")
            pwd = str2
        End Select
        
        'Jika berhasil mengunci mesin
        If KunciMesin(SN, pin, pwd) = True Then
            strSQL = "Update t_device Set device_key_code = """ & Trim(sandi_kunci_mesin) & """, " & _
                "admin_pin = """ & pin & """, admin_pwd = """ & pwd & """ where sn_device = """ & SN & """ "
            ExecuteSQLite_Data strSQL
            
            strSQL = "Update t_ambil_kunci Set status_reset = 1 " & _
                "Where sn_device = """ & SN & """ "
            ExecuteSQLite_Data strSQL

            SetParam "kode_kunci_mesin", Trim(sandi_kunci_mesin)
            
            msg_str = msg_str & INILanRead("errMsg", "Err1.Device.11", "") & vbNewLine
            msg_str = Replace(msg_str, "?1", name_device)
        Else
            strSQL = "Replace Into t_ambil_kunci (id_user, sn_device, tgl_ambil, status_reset) " & _
                "Values (" & UserID & ", """ & SN & """, """ & str3 & """, 2) "
            ExecuteSQLite_Data strSQL
            
            msg_str = msg_str & INILanRead("errMsg", "Err1.Device.12", "") & vbNewLine
            msg_str = Replace(msg_str, "?1", name_device)
        End If
    End If
    
    MengunciMesin = True
End Function

Public Function KunciMesin(sn_device As String, pin As String, id_group As String) As Boolean
    On Error GoTo BugError
    
    Dim berhasil As Boolean
    
    If InitDevice(sn_device, optConn, False) = True Then
        If cmdOpenComm(optConn) = True Then
            Select Case deviceType
            Case 2 'Mesin Neo
                If GetDevInfo = True Then
                    berhasil = DeviceInfoNeo(sn_device)
                Else
                    berhasil = True
                End If
                
                If berhasil = True Then
                    berhasil = SetAdminKunciMesinNeo(sn_device, pin, id_group)
                End If
                
                cmdCloseCommNeo
                KunciMesin = True
            Case 3 'Mesin Revo
                mnCommHandleIndex = gnCommHandleIndex
                mbGetState = gbOpenFlag
                
                If GetDevInfo = True Then
                    berhasil = DeviceInfo(sn_device, mnCommHandleIndex)
                Else
                    berhasil = True
                End If
                
                If berhasil = True Then
                    berhasil = SetAdminKunciMesin(sn_device, pin, id_group, mbGetState, mnCommHandleIndex)
                End If
                
                cmdCloseComm
                KunciMesin = True
            End Select
        Else
            KunciMesin = False
        End If
    Else
        KunciMesin = False
    End If
    
    Set CnTemp = Nothing
    
    Exit Function
        
BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Function

Public Function SetAdminKunciMesin(SN As String, pin As String, id_group As String, mbGetState As Boolean, _
    mnCommHandleIndex As Long) As Boolean
    On Error GoTo BugError
    
    SetAdminKunciMesin = False
    
    Dim vEnrollNumber As Long
    Dim vStrEnrollNumber As String
    Dim vBackupNumber As Long
    Dim vPrivilege As Long
    Dim vEnrollName As String
    
    Dim vnMessRet As Long
    Dim vStr As String
    Dim vTitle As String
    Dim vnIsSupported As Long
    Dim vnResultCode As Long
    Dim vbRet As Boolean
    
    DoEvents
    
    vnResultCode = FK_EnableDevice(mnCommHandleIndex, 0)
    If vnResultCode <> RUN_SUCCESS Then
        Exit Function
    End If

    Dim RecstAtt As New cRecordset
    
    Dim countTemplate As Integer
    Dim vbytEnrollData() As Byte
    Static x As Long
    mbGetState = True
    Dim vnResultSupportStringID As Long
        
    strSQL = "SELECT * from t_template_revo where id_group = " & id_group & " "
    Set RecstAtt = getRecordSet_Data(strSQL, True)
    
    If RecstAtt.RecordCount = 0 Then
        FK_EnableDevice mnCommHandleIndex, 1
        SetAdminKunciMesin = True
        Exit Function
    End If
    
    With RecstAtt
        vnResultSupportStringID = FK_GetIsSupportStringID(mnCommHandleIndex)
        If vnResultSupportStringID >= RUN_SUCCESS Then
            If vnResultSupportStringID = USER_ID_LEN13_1 Then
                vStrEnrollNumber = Space(USER_ID_LEN13_1)
            Else
                vStrEnrollNumber = Space(USER_ID_LEN)
            End If
            
            Do While .EOF = False
                vEnrollName = Space(256)
                vStrEnrollNumber = pin
                vEnrollName = "F1"
                vPrivilege = MP_MANAGER_1
                vBackupNumber = BACKUP_PSW
                
                If vBackupNumber <> -1 Then
                    ZeroMemory mbytEnrollData(0), UBound(mbytEnrollData) + 1
                    If vBackupNumber = BACKUP_PSW Or vBackupNumber = BACKUP_CARD Then
                        vbytEnrollData = RecstAtt!template_blob
                        CopyMemory mbytEnrollData(0), vbytEnrollData(0), PWD_DATA_SIZE
                    ElseIf vBackupNumber >= BACKUP_FP_0 And vBackupNumber <= BACKUP_FP_9 Then
                        vbytEnrollData = RecstAtt!template_blob
                        CopyMemory mbytEnrollData(0), vbytEnrollData(0), FP_DATA_SIZE
                    ElseIf vBackupNumber = BACKUP_FACE Then
                        vbytEnrollData = RecstAtt!template_blob
                        CopyMemory mbytEnrollData(0), vbytEnrollData(0), FACE_DATA_SIZE
                    ElseIf vBackupNumber >= BACKUP_PALMVEIN_0 And vBackupNumber <= BACKUP_PALMVEIN_3 Then
                        vbytEnrollData = RecstAtt!template_blob
                        CopyMemory mbytEnrollData(0), vbytEnrollData(0), PALMVEIN_DATA_SIZE
                    ElseIf vBackupNumber = BACKUP_VEIN_0 Then
                        vbytEnrollData = RecstAtt!template_blob
                        CopyMemory mbytEnrollData(0), vbytEnrollData(0), VEIN_DATA_SIZE
                    End If
                    
                    vnResultCode = FK_DeleteEnrollData_StringID(mnCommHandleIndex, vStrEnrollNumber, vBackupNumber)
                    
                    Call FK_IsSupportedEnrollData(mnCommHandleIndex, vBackupNumber, vnIsSupported)
                    
                    If vnIsSupported <> 0 Then
                        vnResultCode = FK_PutEnrollData_StringID(mnCommHandleIndex, vStrEnrollNumber, vBackupNumber, _
                            vPrivilege, mbytEnrollData(0), mlngPasswordData)
                                        
                        If vnResultCode <> RUN_SUCCESS Then
                            vStr = "SetAllEnrollData Error"
                        Else
                            strSQL = "Update t_ambil_kunci Set status_reset = 1 " & _
                                "Where sn_device = """ & SN & """ And status_reset = 0 "
                            ExecuteSQLite_Data strSQL
                        End If
                    End If
                End If
                DoEvents
                
                If Trim(vEnrollName) <> "" Then
                    'vnResultCode = FK_SetUserName_StringID(mnCommHandleIndex, vStrEnrollNumber, Trim(vEnrollName))
                    SetUserName vStrEnrollNumber, vEnrollName, mnCommHandleIndex
                End If
                DoEvents
                
                .MoveNext
            Loop
        Else
            Do While .EOF = False
                vEnrollName = Space(256)
                vStrEnrollNumber = pin
                vEnrollNumber = Val(vStrEnrollNumber)
                vEnrollName = "F1"
                vPrivilege = MP_MANAGER_1
                vBackupNumber = BACKUP_PSW
                
                If vBackupNumber <> -1 Then
                    ZeroMemory mbytEnrollData(0), UBound(mbytEnrollData) + 1
                    If vBackupNumber = BACKUP_PSW Or vBackupNumber = BACKUP_CARD Then
                        vbytEnrollData = RecstAtt!template_blob
                        CopyMemory mbytEnrollData(0), vbytEnrollData(0), PWD_DATA_SIZE
                    ElseIf vBackupNumber >= BACKUP_FP_0 And vBackupNumber <= BACKUP_FP_9 Then
                        vbytEnrollData = RecstAtt!template_blob
                        CopyMemory mbytEnrollData(0), vbytEnrollData(0), FP_DATA_SIZE
                    ElseIf vBackupNumber = BACKUP_FACE Then
                        vbytEnrollData = RecstAtt!template_blob
                        CopyMemory mbytEnrollData(0), vbytEnrollData(0), FACE_DATA_SIZE
                    ElseIf vBackupNumber >= BACKUP_PALMVEIN_0 And vBackupNumber <= BACKUP_PALMVEIN_3 Then
                        vbytEnrollData = RecstAtt!template_blob
                        CopyMemory mbytEnrollData(0), vbytEnrollData(0), PALMVEIN_DATA_SIZE
                    ElseIf vBackupNumber = BACKUP_VEIN_0 Then
                        vbytEnrollData = RecstAtt!template_blob
                        CopyMemory mbytEnrollData(0), vbytEnrollData(0), VEIN_DATA_SIZE
                    End If
                    
                    vnResultCode = FK_DeleteEnrollData(mnCommHandleIndex, vEnrollNumber, vBackupNumber)
                    
                    Call FK_IsSupportedEnrollData(mnCommHandleIndex, vBackupNumber, vnIsSupported)
                    
                    If vnIsSupported <> 0 Then
                        vnResultCode = FK_PutEnrollData(mnCommHandleIndex, vEnrollNumber, vBackupNumber, vPrivilege, _
                            mbytEnrollData(0), mlngPasswordData)
                                        
                        If vnResultCode <> RUN_SUCCESS Then
                            vStr = "SetAllEnrollData Error"
                        Else
                            strSQL = "Update t_ambil_kunci Set status_reset = 1 " & _
                                "Where sn_device = """ & SN & """ And status_reset = 0 "
                            ExecuteSQLite_Data strSQL
                        End If
                    End If
                End If
                DoEvents
                
                If Trim(vEnrollName) <> "" Then
                    'vnResultCode = FK_SetUserName(mnCommHandleIndex, vEnrollNumber, Trim(vEnrollName))
                    SetUserName Trim(CStr(vEnrollNumber)), vEnrollName, mnCommHandleIndex
                End If
                DoEvents
                
                .MoveNext
            Loop
        End If
    End With
    mbGetState = False
    
    If vnResultCode = RUN_SUCCESS Then
        vnResultCode = FK_SaveEnrollData(mnCommHandleIndex)
        If vnResultCode = RUN_SUCCESS Then
            SetAdminKunciMesin = True
        End If
    End If

    FK_EnableDevice mnCommHandleIndex, 1
    
    Exit Function
        
BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Function

Public Function SetAdminKunciMesinNeo(SN As String, pin As String, pwd As String) As Boolean
    On Error GoTo BugError
    
    SetAdminKunciMesinNeo = False
    
    Dim result As Boolean
    
    result = WritePwdData(pin, pwd)
    If result Then
        result = SetUserPrivilegeNeo(pin, 3)
        If result Then
            result = SetUserNameNeo(pin, "FOne")
            If result Then
                SetAdminKunciMesinNeo = True
            
                strSQL = "Update t_ambil_kunci Set status_reset = 1 " & _
                    "Where sn_device = """ & SN & """ And status_reset = 0 "
                ExecuteSQLite_Data strSQL
            End If
        End If
    End If
    
    Exit Function
        
BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Function

Public Function SetUserPrivilegeNeo(pin As String, Privilege As Long) As Boolean
    On Error GoTo BugError
    
    SetUserPrivilegeNeo = False
    
    Dim extraProperty() As Byte, extraData() As Byte
    Dim user As UserExt
    Dim result As Boolean
    
    If False = IsNumeric(Trim(pin)) Then
        Exit Function
    End If
    
    Set user = New UserExt
    user.DIN = Trim(pin)
    user.Privilege = GetPrivilege(Privilege)
    result = deviceConn.SetPropertyExt(UserProperty_Privilege, extraProperty, user, extraData)
    SetUserPrivilegeNeo = result
    
    Exit Function
        
BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Function

Public Function SetUserNameNeo(pin As String, UserName As String) As Boolean
    On Error GoTo BugError
    
    SetUserNameNeo = False
    
    Dim extraProperty() As Byte, extraData() As Byte
    Dim user As UserExt
    Dim result As Boolean
    
    If False = IsNumeric(Trim(pin)) Then
        Exit Function
    End If
    
    If "" = Trim(UserName) Then
        Exit Function
    End If
    
    Set user = New UserExt
    user.DIN = Trim(pin)
    user.UserName = Trim(UserName)
    result = deviceConn.SetPropertyExt(UserProperty_UserName, extraProperty, user, extraData)
    SetUserNameNeo = result
    
    Exit Function
        
BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Function

Public Function GetUserNameNeo(pin As String) As String
    On Error GoTo BugError
    
    GetUserNameNeo = ""
    
    Dim extraProperty() As Byte, extraData() As Byte
    Dim user As UserExt
    Dim result As Boolean
    
    If "" = Trim(pin) Then
        Exit Function
    End If
    
    If False = IsNumeric(Trim(pin)) Then
        Exit Function
    End If
    
    Set user = New UserExt
    user.DIN = Trim(pin)
    result = deviceConn.GetPropertyExt(UserProperty_UserName, extraProperty, user, extraData)
    If result Then
        GetUserNameNeo = Trim(user.UserName)
    End If
     
    Exit Function
        
BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Function

Public Function SetUserName(pin As String, nama As String, fnCommHandleIndex As Long)
    On Error GoTo BugError
    
    SetUserName = False
    
    Dim vEnrollNumber As Long
    Dim vStrEnrollNumber As String
    Dim vnResultCode As Long
    
    vnResultCode = FK_EnableDevice(fnCommHandleIndex, 0)
    If vnResultCode <> RUN_SUCCESS Then
        Exit Function
    End If
    
    vStrEnrollNumber = Trim(pin)
    nama = Trim(nama)
    nama = Mid(nama, 1, 15)
    
    If FK_GetIsSupportStringID(fnCommHandleIndex) >= RUN_SUCCESS Then
        vnResultCode = FK_SetUserName_StringID(fnCommHandleIndex, vStrEnrollNumber, nama)
    Else
        vEnrollNumber = Val(vStrEnrollNumber)
        vnResultCode = FK_SetUserName(fnCommHandleIndex, vEnrollNumber, nama)
    End If
    
    If vnResultCode = RUN_SUCCESS Then
        SetUserName = True
    End If

    FK_EnableDevice fnCommHandleIndex, 1
    
    Exit Function
        
BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Function

Private Function GetUserName(pin As String, fnCommHandleIndex As Long) As String
    On Error GoTo BugError
    
    GetUserName = ""
    
    Dim vEnrollNumber As Long
    Dim vStrEnrollNumber As String
    Dim vName As String
    Dim vnResultCode As Long
    Dim vName1 As String
    Dim vnResultSupportStringID As Long
    
    vnResultCode = FK_EnableDevice(fnCommHandleIndex, 0)
    If vnResultCode <> RUN_SUCCESS Then
        'MsgBox gstrNoDevice
        Exit Function
    End If
    
    vName = Space(256)
    vStrEnrollNumber = Trim(pin)
    
    vnResultSupportStringID = FK_GetIsSupportStringID(mnCommHandleIndex)
    If vnResultSupportStringID >= RUN_SUCCESS Then
        vnResultCode = FK_GetUserName_StringID(fnCommHandleIndex, vStrEnrollNumber, vName)
    Else
        vEnrollNumber = Val(vStrEnrollNumber)
        vnResultCode = FK_GetUserName(fnCommHandleIndex, vEnrollNumber, vName)
    End If
    
    If vnResultCode = RUN_SUCCESS Then
        GetUserName = vName
    End If

    FK_EnableDevice fnCommHandleIndex, 1
    
    Exit Function
        
BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Function

Public Function SetAllUserTemplateFilter(SN As String, mbGetState As Boolean, mnCommHandleIndex As Long) As Boolean
    On Error GoTo BugError
    
    SetAllUserTemplateFilter = False
    
    Dim vEnrollNumber As Long
    Dim vStrEnrollNumber As String
    Dim vBackupNumber As Long
    Dim vPrivilege As Long
    Dim vEnrollName As String
    
    Dim vnMessRet As Long
    Dim vStr As String
    Dim vTitle As String
    Dim vnIsSupported As Long
    Dim vnResultCode As Long
    Dim vbRet As Boolean
    
    DoEvents
    
    vnResultCode = FK_EnableDevice(mnCommHandleIndex, 0)
    If vnResultCode <> RUN_SUCCESS Then
        Exit Function
    End If

    Dim RecstAtt As New cRecordset
    
    Dim countTemplate As Integer
    Dim vbytEnrollData() As Byte
    Static x As Long
    mbGetState = True
    
    strSQL = "SELECT e.pin, e.alias, e.name_employee, d.id_data_privilege " & vbNewLine & _
        ", ifnull(tmp.template_index, -1) as idx, tmp.template_blob " & vbNewLine & _
        "FROM t_employee AS e " & vbNewLine & _
        "INNER JOIN t_template_revo_backup AS tmp ON tmp.pin = e.pin " & vbNewLine & _
        "LEFT JOIN t_employee_detail AS d ON d.id_employee = e.id_employee " & vbNewLine & _
        "Where e.Active = 1 " & vbNewLine & _
        "And e.pin IN (Select pin From t_template_log Where status = 1 " & vbNewLine & _
        "And sn_device In (Select sn_device From t_device Where status_aktif = '1')) "
    Set RecstAtt = getRecordSet_Data(strSQL, True)
    
    If RecstAtt.RecordCount = 0 Then
        FK_EnableDevice mnCommHandleIndex, 1
        SetAllUserTemplateFilter = True
        Exit Function
    End If
    
    Dim vnResultSupportStringID As Long
    With RecstAtt
        vnResultSupportStringID = FK_GetIsSupportStringID(mnCommHandleIndex)
        If vnResultSupportStringID >= RUN_SUCCESS Then
            If vnResultSupportStringID = USER_ID_LEN13_1 Then
                vStrEnrollNumber = Space(USER_ID_LEN13_1)
            Else
                vStrEnrollNumber = Space(USER_ID_LEN)
            End If
            
            Do While .EOF = False
                vEnrollName = Space(256)
                vEnrollName = .fields("alias").value
                vStrEnrollNumber = .fields("pin").value
                vPrivilege = MP_NONE
                vBackupNumber = .fields("idx").value
                
                If vBackupNumber <> -1 Then
                    ZeroMemory mbytEnrollData(0), UBound(mbytEnrollData) + 1
                    If vBackupNumber = BACKUP_PSW Or vBackupNumber = BACKUP_CARD Then
                        vbytEnrollData = RecstAtt!template_blob
                        CopyMemory mbytEnrollData(0), vbytEnrollData(0), PWD_DATA_SIZE
                    ElseIf vBackupNumber >= BACKUP_FP_0 And vBackupNumber <= BACKUP_FP_9 Then
                        vbytEnrollData = RecstAtt!template_blob
                        CopyMemory mbytEnrollData(0), vbytEnrollData(0), FP_DATA_SIZE
                    ElseIf vBackupNumber = BACKUP_FACE Then
                        vbytEnrollData = RecstAtt!template_blob
                        CopyMemory mbytEnrollData(0), vbytEnrollData(0), FACE_DATA_SIZE
                    ElseIf vBackupNumber >= BACKUP_PALMVEIN_0 And vBackupNumber <= BACKUP_PALMVEIN_3 Then
                        vbytEnrollData = RecstAtt!template_blob
                        CopyMemory mbytEnrollData(0), vbytEnrollData(0), PALMVEIN_DATA_SIZE
                    ElseIf vBackupNumber = BACKUP_VEIN_0 Then
                        vbytEnrollData = RecstAtt!template_blob
                        CopyMemory mbytEnrollData(0), vbytEnrollData(0), VEIN_DATA_SIZE
                    End If
                    
                    vnResultCode = FK_DeleteEnrollData_StringID(mnCommHandleIndex, vStrEnrollNumber, vBackupNumber)
                    
                    Call FK_IsSupportedEnrollData(mnCommHandleIndex, vBackupNumber, vnIsSupported)
                    
                    If vnIsSupported <> 0 Then
                        vnResultCode = FK_PutEnrollData_StringID(mnCommHandleIndex, vStrEnrollNumber, vBackupNumber, _
                            vPrivilege, mbytEnrollData(0), mlngPasswordData)
                                        
                        If vnResultCode <> RUN_SUCCESS Then
                            vStr = "SetAllEnrollData Error"
                        Else
                            strSQL = "DELETE FROM t_template_log WHERE sn_device = """ & SN & """ AND pin = """ & vStrEnrollNumber & """ "
                            ExecuteSQLite_Data strSQL
        
                            strSQL = "REPLACE INTO t_template_log (sn_device, pin, status, last_update) " & vbNewLine & _
                                "VALUES (""" & SN & """, """ & vStrEnrollNumber & """, 2, """ & CreatedDate() & """) "
                            ExecuteSQLite_Data strSQL
                        End If
                    End If
                End If
                DoEvents
                
                If Trim(vEnrollName) <> "" Then
                    'vnResultCode = FK_SetUserName_StringID(mnCommHandleIndex, vStrEnrollNumber, Trim(vEnrollName))
                    SetUserName vStrEnrollNumber, vEnrollName, mnCommHandleIndex
                End If
                
                DoEvents
                .MoveNext
            Loop
        Else
            Do While .EOF = False
                vEnrollName = Space(256)
                vEnrollName = .fields("alias").value
                vStrEnrollNumber = .fields("pin").value
                vEnrollNumber = Val(vStrEnrollNumber)
                vPrivilege = MP_NONE
                vBackupNumber = .fields("idx").value
                
                If vBackupNumber <> -1 Then
                    ZeroMemory mbytEnrollData(0), UBound(mbytEnrollData) + 1
                    If vBackupNumber = BACKUP_PSW Or vBackupNumber = BACKUP_CARD Then
                        vbytEnrollData = RecstAtt!template_blob
                        CopyMemory mbytEnrollData(0), vbytEnrollData(0), PWD_DATA_SIZE
                    ElseIf vBackupNumber >= BACKUP_FP_0 And vBackupNumber <= BACKUP_FP_9 Then
                        vbytEnrollData = RecstAtt!template_blob
                        CopyMemory mbytEnrollData(0), vbytEnrollData(0), FP_DATA_SIZE
                    ElseIf vBackupNumber = BACKUP_FACE Then
                        vbytEnrollData = RecstAtt!template_blob
                        CopyMemory mbytEnrollData(0), vbytEnrollData(0), FACE_DATA_SIZE
                    ElseIf vBackupNumber >= BACKUP_PALMVEIN_0 And vBackupNumber <= BACKUP_PALMVEIN_3 Then
                        vbytEnrollData = RecstAtt!template_blob
                        CopyMemory mbytEnrollData(0), vbytEnrollData(0), PALMVEIN_DATA_SIZE
                    ElseIf vBackupNumber = BACKUP_VEIN_0 Then
                        vbytEnrollData = RecstAtt!template_blob
                        CopyMemory mbytEnrollData(0), vbytEnrollData(0), VEIN_DATA_SIZE
                    End If
                    
                    vnResultCode = FK_DeleteEnrollData(mnCommHandleIndex, vEnrollNumber, vBackupNumber)
                    
                    Call FK_IsSupportedEnrollData(mnCommHandleIndex, vBackupNumber, vnIsSupported)
                    
                    If vnIsSupported <> 0 Then
                        vnResultCode = FK_PutEnrollData(mnCommHandleIndex, vEnrollNumber, vBackupNumber, vPrivilege, _
                            mbytEnrollData(0), mlngPasswordData)
                                        
                        If vnResultCode <> RUN_SUCCESS Then
                            vStr = "SetAllEnrollData Error"
                        Else
                            strSQL = "DELETE FROM t_template_log WHERE sn_device = """ & SN & """ AND pin = """ & vStrEnrollNumber & """ "
                            ExecuteSQLite_Data strSQL
                            
                            strSQL = "REPLACE INTO t_template_log (sn_device, pin, status, last_update) " & vbNewLine & _
                                "VALUES (""" & SN & """, """ & vStrEnrollNumber & """, 2, """ & CreatedDate() & """) "
                            ExecuteSQLite_Data strSQL
                        End If
                    End If
                End If
                DoEvents
                
                If Trim(vEnrollName) <> "" Then
                    'vnResultCode = FK_SetUserName(mnCommHandleIndex, vEnrollNumber, Trim(vEnrollName))
                    SetUserName Trim(CStr(vEnrollNumber)), vEnrollName, mnCommHandleIndex
                End If
                            
                DoEvents
                .MoveNext
            Loop
        End If
    End With
    mbGetState = False
    
    If vnResultCode = RUN_SUCCESS Then
        vnResultCode = FK_SaveEnrollData(mnCommHandleIndex)
        If vnResultCode = RUN_SUCCESS Then
            SetAllUserTemplateFilter = True
        End If
    End If

    FK_EnableDevice mnCommHandleIndex, 1
    
    Exit Function
        
BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Function

Public Function SetAllUserTemplateFilterNeo(SN As String) As Boolean
    On Error GoTo BugError
    
    SetAllUserTemplateFilterNeo = False
    
    Dim pin As String
    Dim UserName As String
    Dim Privilege As Long
    Dim template_index As Long
    Dim template_str As String
    Dim result As Boolean
    
    Dim RecstAtt As New cRecordset
    
    strSQL = "SELECT e.pin, e.alias, e.name_employee, d.id_data_privilege " & vbNewLine & _
        ", ifnull(tmp.template_index, -1) as idx, tmp.template_str " & vbNewLine & _
        "FROM t_employee AS e " & vbNewLine & _
        "INNER JOIN t_template_neo_backup AS tmp ON tmp.pin = e.pin " & vbNewLine & _
        "LEFT JOIN t_employee_detail AS d ON d.id_employee = e.id_employee " & vbNewLine & _
        "Where e.Active = 1 " & vbNewLine & _
        "And e.pin IN (Select pin From t_template_log_neo Where status = 1 " & vbNewLine & _
        "And sn_device In (Select sn_device From t_device Where status_aktif = '1')) "
    Set RecstAtt = getRecordSet_Data(strSQL, True)
    
    If RecstAtt.RecordCount = 0 Then
        SetAllUserTemplateFilterNeo = True
        Exit Function
    End If
    
    With RecstAtt
        Do While .EOF = False
            pin = .fields("pin").value
            UserName = Trim(.fields("alias").value)
            template_index = .fields("idx").value
            template_str = Trim(.fields("template_str").value)
            Privilege = 1
            
            If template_index <> -1 Then
                If (template_index >= 0) And (template_index <= 9) Then
                    result = WriteFPData(pin, template_index, template_str)
                ElseIf template_index = 10 Then
                    result = WritePwdData(pin, template_str)
                ElseIf template_index = 11 Then
                    result = WriteCardData(pin, template_str)
                End If
                
                If result Then
                    result = SetUserPrivilegeNeo(pin, Privilege)
                    If result Then
                        If UserName <> "" Then result = SetUserNameNeo(pin, UserName)
                        If result Then
                            strSQL = "DELETE FROM t_template_log_neo WHERE sn_device = """ & SN & """ AND pin = """ & pin & """ "
                            ExecuteSQLite_Data strSQL
        
                            strSQL = "REPLACE INTO t_template_log_neo (sn_device, pin, status, last_update) " & vbNewLine & _
                                "VALUES (""" & SN & """, """ & pin & """, 2, """ & CreatedDate() & """) "
                            ExecuteSQLite_Data strSQL
                        End If
                    End If
                End If
            End If
            DoEvents
            
            .MoveNext
        Loop
    End With
    
    SetAllUserTemplateFilterNeo = True
    
    Exit Function
        
BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Function

Public Function SetAllUserDefault(SN As String, mbGetState As Boolean, mnCommHandleIndex As Long) As Boolean
    On Error GoTo BugError
    
    SetAllUserDefault = False
    
    Dim vEnrollName As String
    Dim vEnrollNumber As Long
    Dim vStrEnrollNumber As String
    Dim vBackupNumber As Long
    Dim vPrivilege As Long
    
    Dim vnMessRet As Long
    Dim vStr As String
    Dim vTitle As String
    Dim vnIsSupported As Long
    Dim vnResultCode As Long
    Dim vbRet As Boolean
    
    vnResultCode = FK_EnableDevice(mnCommHandleIndex, 0)
    If vnResultCode <> RUN_SUCCESS Then
        Exit Function
    End If

    Dim RecstAtt As New cRecordset
    
    Dim countTemplate As Integer
    Dim vbytEnrollData() As Byte
    Static x As Long
    mbGetState = True
    
    strSQL = "select * from t_template_revo where pin = '0' and template_index = 10 "
    Set RecstAtt = getRecordSet_Data(strSQL, True)
    Do While RecstAtt.EOF = False
        EnrollDataDefault = RecstAtt!template_blob
        RecstAtt.MoveNext
    Loop
    strTmpPwdDefault = ShowByteArrayToString(EnrollDataDefault, PWD_DATA_SIZE)
    strTmpPwd = "0000000000000000317A2B1944DF54D5A10000000000000000000000000000000000000000000000"
    strTmpPwd2 = "00000000000000003030303030303030300000000000000000000000000000000000000000000000"
    strTmpCard = "00000000000000000030303030303030300000000000000000000000000000000000000000000000"
    
    strSQL = "SELECT t.pin, e.alias, e.name_employee, d.id_data_privilege " & vbNewLine & _
        "FROM t_template_log AS t " & vbNewLine & _
        "LEFT JOIN t_employee AS e ON e.pin = t.pin " & vbNewLine & _
        "LEFT JOIN t_employee_detail AS d ON d.id_employee = e.id_employee " & vbNewLine & _
        "Where t.status = 0 And t.sn_device = """ & SN & """ "
    Set RecstAtt = getRecordSet_Data(strSQL, True)
    
    If RecstAtt.RecordCount = 0 Then
        FK_EnableDevice mnCommHandleIndex, 1
        SetAllUserDefault = True
        Exit Function
    End If
    
    Dim vnResultSupportStringID As Long
    With RecstAtt
        vnResultSupportStringID = FK_GetIsSupportStringID(mnCommHandleIndex)
        If vnResultSupportStringID >= RUN_SUCCESS Then
            If vnResultSupportStringID = USER_ID_LEN13_1 Then
                vStrEnrollNumber = Space(USER_ID_LEN13_1)
            Else
                vStrEnrollNumber = Space(USER_ID_LEN)
            End If
            
            Do While .EOF = False
                vEnrollName = Space(256)
                vStrEnrollNumber = .fields("pin").value
                vEnrollName = .fields("alias").value
                vPrivilege = MP_NONE
                vBackupNumber = BACKUP_PSW
                
                ZeroMemory mbytEnrollData(0), UBound(mbytEnrollData) + 1
                If vBackupNumber = BACKUP_PSW Or vBackupNumber = BACKUP_CARD Then
                    CopyMemory mbytEnrollData(0), EnrollDataDefault(0), PWD_DATA_SIZE
                End If
                
                vnResultCode = FK_DeleteEnrollData_StringID(mnCommHandleIndex, vStrEnrollNumber, vBackupNumber)
                
                Call FK_IsSupportedEnrollData(mnCommHandleIndex, vBackupNumber, vnIsSupported)
                If vnIsSupported <> 0 Then
                    vnResultCode = FK_PutEnrollData_StringID(mnCommHandleIndex, vStrEnrollNumber, vBackupNumber, _
                        vPrivilege, mbytEnrollData(0), mlngPasswordData)
                                    
                    If vnResultCode <> RUN_SUCCESS Then
                        vStr = "SetAllEnrollData Error"
                    Else
                        strSQL = "Update t_template_log Set status = 1 Where sn_device = """ & SN & """ And pin = """ & vStrEnrollNumber & """ "
                        ExecuteSQLite_Data strSQL
                    End If
                End If
                DoEvents
                
                If Trim(vEnrollName) <> "" Then
                    'FK_EnableDevice mnCommHandleIndex, 0
                    'vnResultCode = FK_SetUserName_StringID(mnCommHandleIndex, vStrEnrollNumber, Trim(vEnrollName))
                    'FK_EnableDevice mnCommHandleIndex, 1
                    SetUserName vStrEnrollNumber, vEnrollName, mnCommHandleIndex
                End If
                DoEvents
                
                .MoveNext
            Loop
        Else
            Do While .EOF = False
                vEnrollName = Space(256)
                vStrEnrollNumber = .fields("pin").value
                vEnrollNumber = Val(vStrEnrollNumber)
                vEnrollName = .fields("alias").value
                vPrivilege = MP_NONE
                vBackupNumber = BACKUP_PSW
                
                ZeroMemory mbytEnrollData(0), UBound(mbytEnrollData) + 1
                If vBackupNumber = BACKUP_PSW Or vBackupNumber = BACKUP_CARD Then
                    CopyMemory mbytEnrollData(0), EnrollDataDefault(0), PWD_DATA_SIZE
                End If
                
                vnResultCode = FK_DeleteEnrollData(mnCommHandleIndex, vEnrollNumber, vBackupNumber)
                
                Call FK_IsSupportedEnrollData(mnCommHandleIndex, vBackupNumber, vnIsSupported)
                If vnIsSupported <> 0 Then
                    vnResultCode = FK_PutEnrollData(mnCommHandleIndex, vEnrollNumber, vBackupNumber, vPrivilege, _
                        mbytEnrollData(0), mlngPasswordData)
                                    
                    If vnResultCode <> RUN_SUCCESS Then
                        vStr = "SetAllEnrollData Error"
                    Else
                        strSQL = "Update t_template_log Set status = 1 Where sn_device = """ & SN & """ And pin = """ & vStrEnrollNumber & """ "
                        ExecuteSQLite_Data strSQL
                    End If
                End If
                DoEvents
                
                If Trim(vEnrollName) <> "" Then
                    SetUserName CStr(vEnrollNumber), vEnrollName, mnCommHandleIndex
                End If
                DoEvents
                
                .MoveNext
            Loop

        End If
    End With
    mbGetState = False
    
    If vnResultCode = RUN_SUCCESS Then
        vnResultCode = FK_SaveEnrollData(mnCommHandleIndex)
        If vnResultCode = RUN_SUCCESS Then
            SetAllUserDefault = True
        End If
    End If

    FK_EnableDevice mnCommHandleIndex, 1
    
    Exit Function
        
BugError:
    'MsgBox Err.Description, , Err.number
    Err.Clear
    Resume Next
End Function

Public Function SetAllUserDefaultNeo(SN As String) As Boolean
    On Error GoTo BugError
    
    SetAllUserDefaultNeo = False
    
    Dim pin As String
    Dim UserName As String
    Dim Privilege As Long
    
    Dim RecstAtt As New cRecordset
    Dim result As Boolean
            
    strSQL = "SELECT t.pin, e.alias, e.name_employee, d.id_data_privilege " & vbNewLine & _
        "FROM t_template_log_neo AS t " & vbNewLine & _
        "LEFT JOIN t_employee AS e ON e.pin = t.pin " & vbNewLine & _
        "LEFT JOIN t_employee_detail AS d ON d.id_employee = e.id_employee " & vbNewLine & _
        "Where t.status = 0 And t.sn_device = """ & SN & """ "
    Set RecstAtt = getRecordSet_Data(strSQL, True)
    
    If RecstAtt.RecordCount = 0 Then
        Exit Function
    End If
    
    With RecstAtt
        Do While .EOF = False
            pin = .fields("pin").value
            UserName = .fields("alias").value
            Privilege = 1
            
            result = WritePwdData(pin, strPwdNeoDefault)
            If result Then
                result = SetUserPrivilegeNeo(pin, Privilege)
                If result Then
                    If UserName <> "" Then result = SetUserNameNeo(pin, UserName)
                    If result Then
                        strSQL = "Update t_template_log_neo Set status = 1 Where sn_device = """ & SN & """ And pin = """ & pin & """ "
                        ExecuteSQLite_Data strSQL
                    End If
                End If
            End If
            DoEvents
            
            .MoveNext
        Loop
    End With
    
    Exit Function
        
BugError:
    'MsgBox Err.Description, , Err.number
    Err.Clear
    Resume Next
End Function

Public Function UploadUser(sn_device As String) As Boolean
    On Error GoTo BugError
    
    UploadUser = False
    Dim berhasil As Boolean
    
    berhasil = False
    If InitDevice(sn_device, optConn, False) = True Then
        If cmdOpenComm(optConn) = True Then
            Select Case deviceType
            Case 2 'Mesin Neo
                If GetDevInfo = True Then
                    berhasil = DeviceInfoNeo(sn_device)
                Else
                    berhasil = True
                End If
                
                berhasil = SetAllUserDefaultNeo(sn_device)
                
                cmdCloseCommNeo
            Case 3 'Mesin Revo
                mnCommHandleIndex = gnCommHandleIndex
                mbGetState = gbOpenFlag
                
                If GetDevInfo = True Then
                    berhasil = DeviceInfo(sn_device, mnCommHandleIndex)
                Else
                    berhasil = True
                End If
                
                berhasil = SetAllUserDefault(sn_device, mbGetState, mnCommHandleIndex)
                
                cmdCloseComm
            End Select
        Else
            berhasil = False
        End If
    Else
        berhasil = False
    End If
    
    Set CnTemp = Nothing
    
    UploadUser = berhasil
    
    Exit Function
        
BugError:
    'MsgBox Err.Description, , Err.Number
    berhasil = False
    Err.Clear
    Resume Next
End Function

Public Function UploadUser2(sn_device As String) As Boolean
    On Error GoTo BugError
    
    Dim berhasil As Boolean
    
    UploadUser2 = False
    If InitDevice(sn_device, optConn, False) = True Then
        If cmdOpenComm(optConn) = True Then
            Select Case deviceType
            Case 2 'Mesin Neo
                If GetDevInfo = True Then
                    berhasil = DeviceInfoNeo(sn_device)
                Else
                    berhasil = True
                End If
                
                berhasil = SetAllUserTemplateFilterNeo(sn_device)
                DoEvents
                
                cmdCloseCommNeo
            Case 3 'Mesin Revo
                mnCommHandleIndex = gnCommHandleIndex
                mbGetState = gbOpenFlag
                
                If GetDevInfo = True Then
                    berhasil = DeviceInfo(sn_device, mnCommHandleIndex)
                Else
                    berhasil = True
                End If
                
                berhasil = SetAllUserTemplateFilter(sn_device, mbGetState, mnCommHandleIndex)
                DoEvents
                
                cmdCloseComm
            End Select
        Else
            berhasil = False
        End If
    Else
        berhasil = False
    End If
    
    Set CnTemp = Nothing
    
    UploadUser2 = berhasil
    
    Exit Function
        
BugError:
    'MsgBox Err.Description, , Err.Number
    berhasil = False
    Err.Clear
    Resume Next
End Function

Public Function DeleteUserRegUlang(sn_device As String) As Boolean
    On Error GoTo BugError
    
    Dim berhasil As Boolean
            
    If InitDevice(sn_device, optConn, False) = True Then
        If cmdOpenComm(optConn) = True Then
            Select Case deviceType
            Case 2 'Mesin Neo
                If GetDevInfo = True Then
                    berhasil = DeviceInfoNeo(sn_device)
                Else
                    berhasil = True
                End If
                
                If berhasil = True Then
                    berhasil = DeleteEnrollRegUlangNeo(sn_device)
                End If
                
                cmdCloseCommNeo
            Case 3 'Mesin Revo
                mnCommHandleIndex = gnCommHandleIndex
                mbGetState = gbOpenFlag
                
                If GetDevInfo = True Then
                    berhasil = DeviceInfo(sn_device, mnCommHandleIndex)
                Else
                    berhasil = True
                End If
                
                If berhasil = True Then
                    berhasil = DeleteEnrollRegUlang(sn_device, mnCommHandleIndex)
                End If
                
                cmdCloseComm
                DeleteUserRegUlang = True
            End Select
        Else
            DeleteUserRegUlang = False
        End If
    Else
        DeleteUserRegUlang = False
    End If
    
    Set CnTemp = Nothing
    
    Exit Function
        
BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Function

Private Sub GetUnreg(sn_device As String, status As String)
    On Error GoTo BugError
    
    Dim rs As cRecordset
    Dim str1 As String
    
    Dim pin As String
    
    Select Case deviceType
    Case 2 'Mesin Neo
        strSQL = "SELECT e.pin, e.nik, e.name_employee " & _
            "FROM t_employee AS e " & _
            "Where e.active = 1 AND e.pin IN (SELECT DISTINCT pin FROM t_template_log_neo " & _
            "WHERE (" & status & ") AND sn_device = """ & sn_device & """ " & _
            "AND sn_device IN (Select sn_device From t_device Where status_aktif = '1')) " & _
            "AND pin NOT IN (Select pin from t_template_neo_backup) " & _
            "ORDER BY e.pin ASC "
    Case 3 'Mesin Revo
        strSQL = "SELECT e.pin, e.nik, e.name_employee " & _
            "FROM t_employee AS e " & _
            "Where e.active = 1 AND e.pin IN (SELECT DISTINCT pin FROM t_template_log " & _
            "WHERE (" & status & ") AND sn_device = """ & sn_device & """ " & _
            "AND sn_device IN (Select sn_device From t_device Where status_aktif = '1')) " & _
            "AND pin NOT IN (Select pin from t_template_revo_backup) " & _
            "ORDER BY e.pin ASC "
    End Select
    
    Set rs = getRecordSet_Data(strSQL, True)
    With rs
        Do While Not .EOF
            pin = .fields("pin").value
            
            If InStr(pinUnreg, "#" & pin & "#") = 0 Then
                pinUnreg = pinUnreg & "#" & pin & "#"
                pinUnregMesin = pinUnregMesin & pin & ","
            End If
            
            DoEvents
            .MoveNext
        Loop
    End With
    
    Exit Sub
        
BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Sub

Private Sub GetRegDev(sn_device As String, status As String)
    Dim rs As cRecordset
    
    Dim pin As String
    
    Select Case deviceType
    Case 2 'Mesin Neo
        strSQL = "SELECT e.pin, e.nik, e.name_employee " & _
            "FROM t_employee AS e " & _
            "Where e.active = 1 AND e.pin IN (SELECT DISTINCT pin FROM t_template_log_neo " & _
            "WHERE (" & status & ") AND sn_device = """ & sn_device & """ " & _
            "AND sn_device IN (Select sn_device From t_device Where status_aktif = '1')) " & _
            "ORDER BY e.pin ASC "
    Case 3 'Mesin Revo
        strSQL = "SELECT e.pin, e.nik, e.name_employee " & _
            "FROM t_employee AS e " & _
            "Where e.active = 1 AND e.pin IN (SELECT DISTINCT pin FROM t_template_log " & _
            "WHERE (" & status & ") AND sn_device = """ & sn_device & """ " & _
            "AND sn_device IN (Select sn_device From t_device Where status_aktif = '1')) " & _
            "ORDER BY e.pin ASC "
    End Select
    
    Set rs = getRecordSet_Data(strSQL, True)
    Do While Not rs.EOF
        pin = rs.fields("pin").value
        
        If InStr(pinUnreg, "#" & pin & "#") = 0 Then
            pinUnreg = pinUnreg & "#" & pin & "#"
            pinUnregMesin = pinUnregMesin & pin & ","
        End If
        
        DoEvents
        rs.MoveNext
    Loop
End Sub

Private Sub CheckAllDevice()
    On Error GoTo BugError
    
    Dim rs As cRecordset
    Dim sn_device As String
    Dim name_device As String
    Dim sukses As Boolean
    
    pinUnreg = ""
    pinUnregMesin = ""
    
    strSQL = "SELECT d.*, dt.firmware, dt.dev_type FROM t_device AS d " & _
        "LEFT JOIN t_device_type AS dt ON dt.id_device_type = d.id_device_type " & _
        "Where d.status_aktif = '1' AND d.comm_type < 4 "
    Set rs = getRecordSet_Data(strSQL, True)
    Do While Not rs.EOF
        sn_device = rs.fields("sn_device").value
        name_device = rs.fields("name_device").value
        deviceType = rs.fields("dev_type").value
        
        GetUnreg sn_device, "status <> 2"
        GetRegDev sn_device, "status = 3"
        
        DoEvents
        rs.MoveNext
    Loop
    DoEvents
    
    If Len(pinUnregMesin) > 0 Then
        pinUnregMesin = Mid(pinUnregMesin, 1, Len(pinUnregMesin) - 1)
    End If
    
    Exit Sub
        
BugError:
    'MsgBox Err.Description, , Err.Number
    sukses = False
    Err.Clear
    Resume Next
End Sub

Public Function GetNewEmpDev() As Boolean
    On Error GoTo BugError
    
    Dim rs As cRecordset
    Dim sn_device As String
    Dim name_device As String
    
    Select Case deviceType
    Case 2 'Mesin Neo
        'Hapus mesin yang tidak ada
        strSQL = "DELETE FROM t_template_log_neo WHERE sn_device NOT IN (SELECT sn_device FROM t_device) "
        ExecuteSQLite_Data strSQL
        
        'Hapus mesin yang tipe koneksi cloud
        strSQL = "DELETE FROM t_template_log_neo WHERE sn_device IN (SELECT sn_device FROM t_device WHERE comm_type = 4) "
        ExecuteSQLite_Data strSQL
        
        'Upload user template default ke semua mesin baru == pegawai baru ke semua mesin
        strSQL = "SELECT d.*, dt.firmware, dt.dev_type FROM t_device AS d " & _
            "LEFT JOIN t_device_type AS dt ON dt.id_device_type = d.id_device_type " & _
            "WHERE d.comm_type < 4 "
            'kondisi ini diperbaiki untuk mengatasi jika mesin tidak aktif semua - d.status_aktif=1
            '"WHERE d.status_aktif = '1' AND d.comm_type < 4 "
            
        Set rs = getRecordSet_Data(strSQL, True)
        Do While Not rs.EOF
            sn_device = rs.fields("sn_device").value
            name_device = rs.fields("name_device").value
            
            'status : 0 = Belum, 1 = Default, 3 = Sudah
            strSQL = "Replace Into t_template_log_neo (sn_device, pin, status, last_update) " & _
                "SELECT """ & sn_device & """, pin, 0, """ & CreatedDate() & """ FROM t_employee " & _
                "WHERE active = 1 AND pin NOT IN (SELECT DISTINCT pin FROM t_template_log_neo " & _
                "WHERE sn_device = """ & sn_device & """)"
            ExecuteSQLite_Data strSQL
            
            rs.MoveNext
        Loop
        DoEvents
    Case 3 'Mesin Revo
        'Hapus mesin yang tidak ada
        strSQL = "DELETE FROM t_template_log WHERE sn_device NOT IN (SELECT sn_device FROM t_device) "
        ExecuteSQLite_Data strSQL
        
        'Hapus mesin yang tipe koneksi cloud
        strSQL = "DELETE FROM t_template_log WHERE sn_device IN (SELECT sn_device FROM t_device WHERE comm_type = 4) "
        ExecuteSQLite_Data strSQL
        
        'Upload user template default ke semua mesin baru == pegawai baru ke semua mesin
        strSQL = "SELECT d.*, dt.firmware, dt.dev_type FROM t_device AS d " & _
            "LEFT JOIN t_device_type AS dt ON dt.id_device_type = d.id_device_type " & _
            "WHERE d.comm_type < 4 "
            'kondisi ini diperbaiki untuk mengatasi jika mesin tidak aktif semua - d.status_aktif=1
            '"WHERE d.status_aktif = '1' AND d.comm_type < 4 "
            
        Set rs = getRecordSet_Data(strSQL, True)
        Do While Not rs.EOF
            sn_device = rs.fields("sn_device").value
            name_device = rs.fields("name_device").value
            
            'status : 0 = Belum, 1 = Default, 3 = Sudah
            strSQL = "Replace Into t_template_log (sn_device, pin, status, last_update) " & _
                "SELECT """ & sn_device & """, pin, 0, """ & CreatedDate() & """ FROM t_employee " & _
                "WHERE active = 1 AND pin NOT IN (SELECT DISTINCT pin FROM t_template_log " & _
                "WHERE sn_device = """ & sn_device & """)"
            ExecuteSQLite_Data strSQL
            
            rs.MoveNext
        Loop
        DoEvents
    End Select
    
    Exit Function
        
BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Function

Public Function SinkronUser() As Boolean
    On Error GoTo BugError
    
    Dim rs As cRecordset
    Dim str1 As String
    
    Dim sn_device As String
    Dim name_device As String
    Dim activation_code As String
    Dim sukses As Boolean
    
    Dim nama_rombel As String
    Dim pin As String
    Dim nis As String
    Dim alias As String
    Dim jenis_kelamin As String
    
    Dim admin_pin As String
    Dim admin_pwd As String
        
    Dim Jml As Integer
    
    SinkronUser = False
    
    'Neo
    'Hapus mesin yang tidak terdaftar
    strSQL = "DELETE FROM t_template_log_neo WHERE sn_device NOT IN (SELECT sn_device FROM t_device) "
    ExecuteSQLite_Data strSQL
    
    'Hapus mesin yang bukan tipe mesin Neo
    strSQL = "DELETE FROM t_template_log_neo WHERE sn_device NOT IN (SELECT sn_device FROM t_device AS d " & _
        "LEFT JOIN t_device_type AS dt ON dt.id_device_type = d.id_device_type " & _
        "WHERE dt.dev_type = 2) "
    ExecuteSQLite_Data strSQL
    
    'Hapus mesin yang tipe koneksi cloud
    strSQL = "DELETE FROM t_template_log_neo WHERE sn_device IN (SELECT sn_device FROM t_device WHERE comm_type = 4) "
    ExecuteSQLite_Data strSQL
    
    'Revo
    'Hapus mesin yang tidak terdaftar
    strSQL = "DELETE FROM t_template_log WHERE sn_device NOT IN (SELECT sn_device FROM t_device) "
    ExecuteSQLite_Data strSQL
    
    'Hapus mesin yang bukan tipe mesin Revo
    strSQL = "DELETE FROM t_template_log WHERE sn_device NOT IN (SELECT sn_device FROM t_device AS d " & _
        "LEFT JOIN t_device_type AS dt ON dt.id_device_type = d.id_device_type " & _
        "WHERE dt.dev_type = 3) "
    ExecuteSQLite_Data strSQL
    
    'Hapus mesin yang tipe koneksi cloud
    strSQL = "DELETE FROM t_template_log WHERE sn_device IN (SELECT sn_device FROM t_device WHERE comm_type = 4) "
    ExecuteSQLite_Data strSQL
    
    'Cek koneksi semua mesin
    strSQL = "SELECT d.*, dt.firmware, dt.dev_type FROM t_device AS d " & _
        "LEFT JOIN t_device_type AS dt ON dt.id_device_type = d.id_device_type " & _
        "WHERE d.status_aktif = '1' AND d.comm_type < 4 "
    Set rs = getRecordSet_Data(strSQL, True)
    Do While Not rs.EOF
        sn_device = rs.fields("sn_device").value
        name_device = rs.fields("name_device").value
        'activation_code = rs.fields("activation_code").value
        activation_code = rs.fields("active_code").value
        deviceType = rs.fields("dev_type").value
        
        If CheckKeyRevoRT2(0, sn_device, activation_code) Then
            sukses = CekKoneksiMesin(sn_device)
            If sukses = True Then
                cmdCloseComm
            Else
                Exit Do
            End If
        Else
            sukses = False
            Exit Do
        End If
        
        'If sukses = True Then SetDevTime Now, sn_device
        DoEvents
        
        rs.MoveNext
    Loop
    DoEvents
    
    If sukses = False Then
        MsgBoxLanguageFile "Err1.Device.16", name_device, sn_device
        Exit Function
    End If
    
    'Delete user semua mesin yang registrasi ulang
    strSQL = "SELECT d.*, dt.firmware, dt.dev_type FROM t_device AS d " & _
        "LEFT JOIN t_device_type AS dt ON dt.id_device_type = d.id_device_type " & _
        "Where d.status_aktif = '1' AND d.comm_type < 4 "
    Set rs = getRecordSet_Data(strSQL, True)
    Do While Not rs.EOF
        sn_device = rs.fields("sn_device").value
        name_device = rs.fields("name_device").value
        deviceType = rs.fields("dev_type").value
        
        DeleteUserRegUlang sn_device
        DoEvents
        
        rs.MoveNext
    Loop
    DoEvents
    
    'Download user ke semua mesin yang sudah registrasi
    strSQL = "SELECT d.*, dt.firmware, dt.dev_type FROM t_device AS d " & _
        "LEFT JOIN t_device_type AS dt ON dt.id_device_type = d.id_device_type " & _
        "Where d.status_aktif = '1' AND d.comm_type < 4 "
    Set rs = getRecordSet_Data(strSQL, True)
    Do While Not rs.EOF
        sn_device = rs.fields("sn_device").value
        name_device = rs.fields("name_device").value
        deviceType = rs.fields("dev_type").value
        
        pinUnreg = ""
        pinUnregMesin = ""
        
        GetRegDev sn_device, "status = 1"
        
        If Len(pinUnregMesin) > 0 Then
            pinUnregMesin = Mid(pinUnregMesin, 1, Len(pinUnregMesin) - 1)
        End If
        
        If Len(pinUnregMesin) > 0 Then
            DownloadUser sn_device
        End If
        
        Select Case deviceType
        Case 2 'Mesin Neo
            strSQL = "REPLACE INTO t_template_neo_backup(sn_device, pin, template_index, template_str, last_update) " & _
                "SELECT '0', pin, template_index, template_str, last_update FROM t_template_neo_new WHERE sn_device = """ & sn_device & """ "
            ExecuteSQLite_Data strSQL
        Case 3 'Mesin Revo
            strSQL = "REPLACE INTO t_template_revo_backup(sn_device, pin, template_index, template_blob, last_update) " & _
                "SELECT '0', pin, template_index, template_blob, last_update FROM t_template_revo_new WHERE sn_device = """ & sn_device & """ "
            ExecuteSQLite_Data strSQL
        End Select
        DoEvents
        
        rs.MoveNext
    Loop
    DoEvents
    
    'Upload user template default ke semua mesin baru == pegawai baru ke semua mesin
    strSQL = "SELECT d.*, dt.firmware, dt.dev_type FROM t_device AS d " & _
        "LEFT JOIN t_device_type AS dt ON dt.id_device_type = d.id_device_type " & _
        "WHERE d.status_aktif = '1' AND d.comm_type < 4 "
    Set rs = getRecordSet_Data(strSQL, True)
    Do While Not rs.EOF
        sn_device = rs.fields("sn_device").value
        name_device = rs.fields("name_device").value
        deviceType = rs.fields("dev_type").value
        
        Select Case deviceType
        Case 2 'Mesin Neo
            'status : 0 = Belum, 1 = Default, 3 = Sudah
            strSQL = "Replace Into t_template_log_neo (sn_device, pin, status) " & _
                "SELECT """ & sn_device & """, pin, 0 FROM t_employee " & _
                "WHERE active = 1 AND pin NOT IN (SELECT DISTINCT pin FROM t_template_log_neo " & _
                "WHERE sn_device = """ & sn_device & """)"
            ExecuteSQLite_Data strSQL
        Case 3 'Mesin Revo
            'status : 0 = Belum, 1 = Default, 3 = Sudah
            strSQL = "Replace Into t_template_log (sn_device, pin, status) " & _
                "SELECT """ & sn_device & """, pin, 0 FROM t_employee " & _
                "WHERE active = 1 AND pin NOT IN (SELECT DISTINCT pin FROM t_template_log " & _
                "WHERE sn_device = """ & sn_device & """)"
            ExecuteSQLite_Data strSQL
        End Select
        
        UploadUser sn_device
        DoEvents
        
        rs.MoveNext
    Loop
    DoEvents
    
    Dim auto_ganti_kunci_mesin_saat_sync As String
    Dim auto_delete_admin_mesin As String
    Dim msg_sudah_dikunci As String
    Dim kode_kunci_mesin As String
    
    auto_ganti_kunci_mesin_saat_sync = LoadParam("auto_ganti_kunci_mesin_saat_sync")
    auto_delete_admin_mesin = LoadParam("auto_delete_admin_mesin")
    kode_kunci_mesin = LoadParam("kode_kunci_mesin")
    
    'Upload user template yang sudah registrasi ke semua mesin >> Distribusikan template ke semua mesin
    strSQL = "SELECT d.*, dt.firmware, dt.dev_type FROM t_device AS d " & _
        "LEFT JOIN t_device_type AS dt ON dt.id_device_type = d.id_device_type " & _
        "WHERE d.status_aktif = '1' AND d.comm_type < 4 "
    Set rs = getRecordSet_Data(strSQL, True)
    Do While Not rs.EOF
        sn_device = rs.fields("sn_device").value
        name_device = rs.fields("name_device").value
        admin_pin = rs.fields("admin_pin").value
        admin_pwd = rs.fields("admin_pwd").value
        deviceType = rs.fields("dev_type").value
        
        UploadUser2 sn_device
        DoEvents
        
        DeleteAdmin sn_device
        DoEvents
        
        If auto_ganti_kunci_mesin_saat_sync <> "" Then
            If auto_ganti_kunci_mesin_saat_sync = "1" Then
                MengunciMesin msg_sudah_dikunci, kode_kunci_mesin, sn_device
            Else
                KunciMesin sn_device, admin_pin, Val(admin_pwd)
            End If
        Else
            SetParam "auto_ganti_kunci_mesin_saat_sync", "1"
            MengunciMesin msg_sudah_dikunci, kode_kunci_mesin, sn_device
        End If
        DoEvents
                
        rs.MoveNext
    Loop
    DoEvents
    
    Dim tgl_sinkron_terakhir As String
    
    tgl_sinkron_terakhir = DateTimeToStr_DB(Now, DateTimeFormatDatabase)
    SetParam "tgl_sinkron_terakhir", tgl_sinkron_terakhir
    
    'CheckAllDevice
    
    SinkronUser = True
    'MsgBoxLanguageFile "Err1.Device.17"
    
    Exit Function
        
BugError:
    'MsgBox Err.Description, , Err.Number
    sukses = False
    Err.Clear
    Resume Next
End Function

Public Function DeleteEnrollData(SN As String, mnCommHandleIndex As Long) As Boolean
    On Error GoTo BugError
    
    Dim vEnrollNumber As Long
    Dim vBackupNumber As Long
    Dim vnResultCode As Long
    Dim vStrEnrollNumber As String
    
    Dim RecstAtt As cRecordset
    Dim JmlDelTmp As Integer
    Dim i As Integer
    
    DeleteEnrollData = False
    DoEvents
    
    vnResultCode = FK_EnableDevice(mnCommHandleIndex, 0)
    If vnResultCode <> RUN_SUCCESS Then
        Exit Function
    End If
    DoEvents
    
    strSQL = "SELECT t.pin, e.alias, e.name_employee " & vbNewLine & _
        "FROM t_template_log AS t " & vbNewLine & _
        "LEFT JOIN t_employee AS e ON e.pin = t.pin " & vbNewLine & _
        "Where t.status = 3 And t.sn_device = """ & SN & """ "
    Set RecstAtt = getRecordSet_Data(strSQL, True)
    
    If RecstAtt.RecordCount = 0 Then
        FK_EnableDevice mnCommHandleIndex, 1
        DeleteEnrollData = True
        Exit Function
    End If
    
    Dim vnResultSupportStringID As Long
    With RecstAtt
        vnResultSupportStringID = FK_GetIsSupportStringID(mnCommHandleIndex)
        If vnResultSupportStringID >= RUN_SUCCESS Then
            If vnResultSupportStringID = USER_ID_LEN13_1 Then
                vStrEnrollNumber = Space(USER_ID_LEN13_1)
            Else
                vStrEnrollNumber = Space(USER_ID_LEN)
            End If
            
            Do While .EOF = False
                vStrEnrollNumber = .fields("pin").value
                
                JmlDelTmp = 0
                For i = 0 To 9
                    vBackupNumber = i
                    vnResultCode = FK_DeleteEnrollData_StringID(mnCommHandleIndex, vStrEnrollNumber, vBackupNumber)
                    If vnResultCode = RUN_SUCCESS Then
                        JmlDelTmp = JmlDelTmp + 1
                        
                        strSQL = "Delete From t_template_revo_backup Where pin = """ & vStrEnrollNumber & """ And template_index = " & CStr(vBackupNumber) & " "
                        ExecuteSQLite_Data strSQL
                    Else
                        Exit For
                    End If
                Next
                
                For i = BACKUP_PSW To BACKUP_PALMVEIN_3
                    vBackupNumber = i
                    vnResultCode = FK_DeleteEnrollData_StringID(mnCommHandleIndex, vStrEnrollNumber, vBackupNumber)
                    If vnResultCode = RUN_SUCCESS Then
                        JmlDelTmp = JmlDelTmp + 1
                        
                        strSQL = "Delete From t_template_revo_backup Where pin = """ & vStrEnrollNumber & """ And template_index = " & CStr(vBackupNumber) & " "
                        ExecuteSQLite_Data strSQL
                    Else
                        If i >= BACKUP_PALMVEIN_1 Then
                            Exit For
                        End If
                    End If
                Next
                
                If JmlDelTmp > 0 Then
                    strSQL = "DELETE FROM t_template_log WHERE sn_device = """ & SN & """ AND pin = """ & vStrEnrollNumber & """ "
                    ExecuteSQLite_Data strSQL
                    
                    strSQL = "REPLACE INTO t_template_log (sn_device, pin, status, last_update) " & vbNewLine & _
                        "VALUES (""" & SN & """, """ & vStrEnrollNumber & """, 0, """ & CreatedDate() & """) "
                    ExecuteSQLite_Data strSQL
                End If
                
                .MoveNext
            Loop
        Else
            Do While .EOF = False
                vStrEnrollNumber = .fields("pin").value
                vEnrollNumber = Val(vStrEnrollNumber)
                
                JmlDelTmp = 0
                For i = 0 To 9
                    vBackupNumber = i
                    vnResultCode = FK_DeleteEnrollData(mnCommHandleIndex, vEnrollNumber, vBackupNumber)
                    If vnResultCode = RUN_SUCCESS Then
                        JmlDelTmp = JmlDelTmp + 1
                        
                        strSQL = "Delete From t_template_revo_backup Where pin = """ & vStrEnrollNumber & """ And template_index = " & CStr(vBackupNumber) & " "
                        ExecuteSQLite_Data strSQL
                    Else
                        Exit For
                    End If
                Next
                
                For i = BACKUP_PSW To BACKUP_PALMVEIN_3
                    vBackupNumber = i
                    vnResultCode = FK_DeleteEnrollData(mnCommHandleIndex, vEnrollNumber, vBackupNumber)
                    If vnResultCode = RUN_SUCCESS Then
                        JmlDelTmp = JmlDelTmp + 1
                        
                        strSQL = "Delete From t_template_revo_backup Where pin = """ & vStrEnrollNumber & """ And template_index = " & CStr(vBackupNumber) & " "
                        ExecuteSQLite_Data strSQL
                    Else
                        If i >= BACKUP_PALMVEIN_1 Then
                            Exit For
                        End If
                    End If
                Next
                
                If JmlDelTmp > 0 Then
                    strSQL = "DELETE FROM t_template_log WHERE sn_device = """ & SN & """ AND pin = """ & vStrEnrollNumber & """ "
                    ExecuteSQLite_Data strSQL
                    
                    strSQL = "REPLACE INTO t_template_log (sn_device, pin, status, last_update) " & vbNewLine & _
                        "VALUES (""" & SN & """, """ & vStrEnrollNumber & """, 0, """ & CreatedDate() & """) "
                    ExecuteSQLite_Data strSQL
                End If
                
                DoEvents
                .MoveNext
            Loop

        End If
    End With
    
    FK_EnableDevice mnCommHandleIndex, 1
    DeleteEnrollData = True
    
    Exit Function
        
BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Function

Public Function DeleteEnrollRegUlang(SN As String, mnCommHandleIndex As Long) As Boolean
    On Error GoTo BugError
    
    Dim vEnrollNumber As Long
    Dim vBackupNumber As Long
    Dim vnResultCode As Long
    Dim vStrEnrollNumber As String
    
    Dim RecstAtt As cRecordset
    Dim JmlDelTmp As Integer
    Dim i As Integer
    
    DeleteEnrollRegUlang = False
    DoEvents
    
    vnResultCode = FK_EnableDevice(mnCommHandleIndex, 0)
    If vnResultCode <> RUN_SUCCESS Then
        Exit Function
    End If
    DoEvents
    
    strSQL = "SELECT DISTINCT t.pin, e.alias, e.name_employee " & vbNewLine & _
        "FROM t_template_log AS t " & vbNewLine & _
        "LEFT JOIN t_employee AS e ON e.pin = t.pin " & vbNewLine & _
        "Where t.status = 3 And t.sn_device = """ & SN & """ "
    Set RecstAtt = getRecordSet_Data(strSQL, True)
    
    If RecstAtt.RecordCount = 0 Then
        FK_EnableDevice mnCommHandleIndex, 1
        DeleteEnrollRegUlang = True
        Exit Function
    End If
    
    Dim vnResultSupportStringID As Long
    With RecstAtt
        vnResultSupportStringID = FK_GetIsSupportStringID(mnCommHandleIndex)
        If vnResultSupportStringID >= RUN_SUCCESS Then
            If vnResultSupportStringID = USER_ID_LEN13_1 Then
                vStrEnrollNumber = Space(USER_ID_LEN13_1)
            Else
                vStrEnrollNumber = Space(USER_ID_LEN)
            End If
            
            Do While .EOF = False
                vStrEnrollNumber = .fields("pin").value
                
                JmlDelTmp = 0
                For i = 0 To 9
                    vBackupNumber = i
                    vnResultCode = FK_DeleteEnrollData_StringID(mnCommHandleIndex, vStrEnrollNumber, vBackupNumber)
                    If vnResultCode = RUN_SUCCESS Then
                        JmlDelTmp = JmlDelTmp + 1
                    Else
                        Exit For
                    End If
                Next
                
                For i = BACKUP_PSW To BACKUP_PALMVEIN_3
                    vBackupNumber = i
                    vnResultCode = FK_DeleteEnrollData_StringID(mnCommHandleIndex, vStrEnrollNumber, vBackupNumber)
                    If vnResultCode = RUN_SUCCESS Then
                        JmlDelTmp = JmlDelTmp + 1
                    Else
                        If i >= BACKUP_PALMVEIN_1 Then
                            Exit For
                        End If
                    End If
                Next
                
                strSQL = "DELETE FROM t_template_log WHERE sn_device = """ & SN & """ AND pin = """ & vStrEnrollNumber & """ "
                ExecuteSQLite_Data strSQL
                    
                strSQL = "REPLACE INTO t_template_log (sn_device, pin, status, last_update) " & vbNewLine & _
                    "VALUES (""" & SN & """, """ & vStrEnrollNumber & """, 0, """ & CreatedDate() & """) "
                ExecuteSQLite_Data strSQL
                DoEvents
                
                .MoveNext
            Loop
        Else
            Do While .EOF = False
                vStrEnrollNumber = .fields("pin").value
                vEnrollNumber = Val(vStrEnrollNumber)
                
                JmlDelTmp = 0
                For i = 0 To 9
                    vBackupNumber = i
                    vnResultCode = FK_DeleteEnrollData(mnCommHandleIndex, vEnrollNumber, vBackupNumber)
                    If vnResultCode = RUN_SUCCESS Then
                        JmlDelTmp = JmlDelTmp + 1
                    Else
                        Exit For
                    End If
                Next
                
                For i = BACKUP_PSW To BACKUP_PALMVEIN_3
                    vBackupNumber = i
                    vnResultCode = FK_DeleteEnrollData(mnCommHandleIndex, vEnrollNumber, vBackupNumber)
                    If vnResultCode = RUN_SUCCESS Then
                        JmlDelTmp = JmlDelTmp + 1
                    Else
                        If i >= BACKUP_PALMVEIN_1 Then
                            Exit For
                        End If
                    End If
                Next
                
                strSQL = "DELETE FROM t_template_log WHERE sn_device = """ & SN & """ AND pin = """ & vStrEnrollNumber & """ "
                ExecuteSQLite_Data strSQL
                    
                strSQL = "REPLACE INTO t_template_log (sn_device, pin, status, last_update) " & vbNewLine & _
                    "VALUES (""" & SN & """, """ & vStrEnrollNumber & """, 0, """ & CreatedDate() & """) "
                ExecuteSQLite_Data strSQL
                DoEvents
                
                .MoveNext
            Loop

        End If
    End With
    
    FK_EnableDevice mnCommHandleIndex, 1
    DeleteEnrollRegUlang = True
    
    Exit Function
        
BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Function

Public Function DeleteEnrollRegUlangNeo(SN As String) As Boolean
    On Error GoTo BugError
    
    DeleteEnrollRegUlangNeo = False
    
    Dim RecstAtt As cRecordset
    Dim pin As String
    Dim result As Boolean
    
    strSQL = "SELECT DISTINCT t.pin, e.alias, e.name_employee " & vbNewLine & _
        "FROM t_template_log_neo AS t " & vbNewLine & _
        "LEFT JOIN t_employee AS e ON e.pin = t.pin " & vbNewLine & _
        "Where t.status = 3 And t.sn_device = """ & SN & """ "
    Set RecstAtt = getRecordSet_Data(strSQL, True)
    
    If RecstAtt.RecordCount = 0 Then
        DeleteEnrollRegUlangNeo = True
        Exit Function
    End If
    
    With RecstAtt
        Do While .EOF = False
            pin = .fields("pin").value
            
            result = DeleteEnrollDataByPIN_Neo(pin)
            
            If result = True Then
                strSQL = "DELETE FROM t_template_log_neo WHERE sn_device = """ & SN & """ AND pin = """ & pin & """ "
                ExecuteSQLite_Data strSQL
                    
                strSQL = "REPLACE INTO t_template_log_neo (sn_device, pin, status, last_update) " & vbNewLine & _
                    "VALUES (""" & SN & """, """ & pin & """, 0, """ & CreatedDate() & """) "
                ExecuteSQLite_Data strSQL
            End If
            
            .MoveNext
        Loop
    End With
    
    DeleteEnrollRegUlangNeo = True
    
    Exit Function
        
BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Function

Private Function ConvertVerify(VerifyMode As Long) As Long
    On Error GoTo BugError

    ConvertVerify = VerifyMode
    Select Case VerifyMode
        Case 1073741824
            ConvertVerify = LOG_FACEVERIFY
        Case 268435456
            ConvertVerify = LOG_FPVERIFY
        Case 805306368
            ConvertVerify = LOG_CARDVERIFY
        Case 536870912
            ConvertVerify = LOG_PASSVERIFY
            
        Case 553648128
            ConvertVerify = LOG_FPPASS_VERIFY   '4
        Case 603979776
            ConvertVerify = LOG_FACEPASSVERIFY  '22
        Case 822083584
            ConvertVerify = LOG_FPCARD_VERIFY   '5
        Case 872415232
            ConvertVerify = LOG_FACECARDVERIFY  '21
        Case 1879048192
            ConvertVerify = LOG_VEINVERIFY      '30
        Case Else
            ConvertVerify = VerifyMode
    End Select
    
    Exit Function
        
BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Function

Private Function ConvertIoMode(IoMode As Long) As Long
  On Error GoTo BugError

    ConvertIoMode = IoMode
    Select Case IoMode
        Case 2321
            ConvertIoMode = 1
        Case 2322
            ConvertIoMode = 2
        Case 2323
            ConvertIoMode = 3
        Case 2324
            ConvertIoMode = 4
        Case 2325
            ConvertIoMode = 5
        Case 2326
            ConvertIoMode = 6
        Case 2327
            ConvertIoMode = 7
        Case 2328
            ConvertIoMode = 8
        Case 2329
            ConvertIoMode = 9
        Case 2330
            ConvertIoMode = 10
            
        Case 2337
            ConvertIoMode = 1
        Case 2338
            ConvertIoMode = 2
        Case 2339
            ConvertIoMode = 3
        Case 2340
            ConvertIoMode = 4
        Case 2341
            ConvertIoMode = 5
        Case 2342
            ConvertIoMode = 6
        Case 2343
            ConvertIoMode = 7
        Case 2344
            ConvertIoMode = 8
        Case 2345
            ConvertIoMode = 9
        Case 2346
            ConvertIoMode = 10
            
        Case 17
            ConvertIoMode = 1
        Case 18
            ConvertIoMode = 2
        Case 19
            ConvertIoMode = 3
        Case 20
            ConvertIoMode = 4
        Case 21
            ConvertIoMode = 5
        Case 22
            ConvertIoMode = 6
        Case 23
            ConvertIoMode = 7
        Case 24
            ConvertIoMode = 8
        Case 25
            ConvertIoMode = 9
        Case 26
            ConvertIoMode = 10

        Case 33
            ConvertIoMode = 1
        Case 34
            ConvertIoMode = 2
        Case 35
            ConvertIoMode = 3
        Case 36
            ConvertIoMode = 4
        Case 37
            ConvertIoMode = 5
        Case 38
            ConvertIoMode = 6
        Case 39
            ConvertIoMode = 7
        Case 40
            ConvertIoMode = 8
        Case 41
            ConvertIoMode = 9
        Case 42
            ConvertIoMode = 10
        Case Else
            ConvertIoMode = IoMode
    End Select
    
    Exit Function
        
BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Function

Private Function GetVerifyName(VerifyMode As Long) As String
  On Error GoTo BugError

    GetVerifyName = "--"
    Select Case VerifyMode
        Case LOG_FPVERIFY
            GetVerifyName = "FP"
        Case LOG_FPVERIFY
            GetVerifyName = "Fp Verify"
        Case LOG_PASSVERIFY
            GetVerifyName = "Pass Verify"
        Case LOG_CARDVERIFY
            GetVerifyName = "Card Verify"
        Case LOG_FPPASS_VERIFY
            GetVerifyName = "Pass + Fp Verify"
        Case LOG_FPCARD_VERIFY
            GetVerifyName = "Card + Fp Verify"
        Case LOG_PASSFP_VERIFY
            GetVerifyName = "Pass + Fp Verify"
        Case LOG_CARDFP_VERIFY
            GetVerifyName = "Card + Fp Verify"
        Case LOG_CARDPASS_VERIFY
            GetVerifyName = "Card + Pass Verify"

        Case LOG_VER_CLOSE_DOOR
            GetVerifyName = "Door Close"
        Case LOG_VER_OPEN_HAND
            GetVerifyName = "Hand Open"
        Case LOG_VER_PROG_OPEN
            GetVerifyName = "Open by PC"
        Case LOG_VER_PROG_CLOSE
            GetVerifyName = "Close by PC"
        Case LOG_VER_OPEN_IREGAL
            GetVerifyName = "Iregal Open"
        Case LOG_VER_CLOSE_IREGAL
            GetVerifyName = "Iregal Close"
        Case LOG_VER_OPEN_COVER
            GetVerifyName = "Cover Open"
        Case LOG_VER_CLOSE_COVER
            GetVerifyName = "Cover Close"

        Case LOG_FACEVERIFY
            GetVerifyName = "Face Verify"
        Case LOG_FACECARDVERIFY
            GetVerifyName = "Face + Card Verify"
        Case LOG_FACEPASSVERIFY
            GetVerifyName = "Face + Pass Verify"
        Case LOG_CARDFACEVERIFY
            GetVerifyName = "Card + Face Verify"
        Case LOG_PASSFACEVERIFY
            GetVerifyName = "Pass + Face Verify"

        Case LOG_VEINVERIFY
            GetVerifyName = "Vein Verify"
        Case LOG_VEINCARDVERIFY
            GetVerifyName = "Vein + Card Verify"
        Case LOG_VEINPASSVERIFY
            GetVerifyName = "Vein + Pass Verify"
        Case LOG_CARDVEINVERIFY
            GetVerifyName = "Card + Vein Verify"
        Case LOG_PASSVEINVERIFY
            GetVerifyName = "Pass + Vein Verify"
        Case LOG_OPEN_THREAT
            GetVerifyName = "Door Open as threat"
        Case Else
            GetVerifyName = "--"
    End Select

    Exit Function

BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Function

Public Function SaveGLogDataToDatabase(SN As String, pin As String, scan_datetime As Date, _
    verify_type As Long, io_mode As Long, inout_mode2 As Long) As Boolean
    On Error GoTo BugError
    
    Dim dateStr As String
    Dim i As Integer
    Dim db_fileName As String
    Dim BulanMM As String
    
    verify_type = ConvertVerify(verify_type)
    io_mode = ConvertIoMode(io_mode)
    
    execRecCount = execRecCount + 1
    
    dateStr = CStr(Format(scan_datetime, "YYYY-MM-DD HH:mm"))
    dateStr = Replace(dateStr, ".", ":")
    
    If execRecCount = 1 Then
        SQL1 = "REPLACE INTO t_att_log (scan_date, pin, sn, verify_mode, inout_mode, inout_mode2) "
        SQL2 = "VALUES ('" & dateStr & "', '" & pin & "', '" & SN & "', " & verify_type & ", " & io_mode & ", " & inout_mode2 & ") "
    Else
        SQL2 = ", ('" & dateStr & "', '" & pin & "', '" & SN & "', " & verify_type & ", " & io_mode & ", " & inout_mode2 & ") "
    End If
    SQL1 = SQL1 + SQL2
    
    If execRecCount >= MaxAttLogDL Then
        If Trim(SQL1) <> "" Then
            ExecuteSQLite_Data SQL1
        End If
        
        SQL1 = ""
        SQL2 = ""
        execRecCount = 0
    End If
      
    SaveGLogDataToDatabase = True
    
    Exit Function
        
BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Function

Public Function DownloadAttlog(sn_device As String, NewData As Boolean, Optional abUSBFlag As Boolean = False) As Boolean
    On Error GoTo BugError
    
    Dim berhasil As Boolean
            
    DownloadAttlog = False
    berhasil = False
    
    If InitDevice(sn_device, optConn, False) = True Then
        If vnstatus_aktif = "1" Then
            If cmdOpenComm(optConn) = True Then
                Select Case deviceType
                Case 2 'Mesin Neo
                    If GetDevInfo = True Then
                        berhasil = DeviceInfoNeo(sn_device)
                    Else
                        berhasil = True
                    End If
                    
                    If berhasil = True Then
                        berhasil = GetGeneralLogDataNeo(sn_device, NewData, abUSBFlag)
                        DownloadAttlog = berhasil
                    End If
                    
                    cmdCloseCommNeo
                Case 3 'Mesin Revo
                    mnCommHandleIndex = gnCommHandleIndex
                    mbGetState = gbOpenFlag
                    
                    If GetDevInfo = True Then
                        berhasil = DeviceInfo(sn_device, mnCommHandleIndex)
                    Else
                        berhasil = True
                    End If
                    
                    If berhasil = True Then
                        berhasil = GetGeneralLogData(sn_device, mnCommHandleIndex, NewData, abUSBFlag)
                        DownloadAttlog = berhasil
                    End If
                    
                    cmdCloseComm
                End Select
            Else
                MsgBoxLanguageFile "Err1.Device.16", vnname_device, sn_device
            End If
        Else
            DownloadAttlog = True
        End If
    End If
    
    Set CnTemp = Nothing
    
    Exit Function
        
BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Function

Public Function GetGeneralLogData(SN As String, mnCommHandleIndex As Long, NewData As Boolean, Optional abUSBFlag As Boolean = False) As Boolean
    On Error GoTo BugError
    
    Dim vStrEnrollNumber As String
    Dim vSEnrollNumber As Long
    Dim vVerifyMode As Long
    Dim vInOutMode As Long
    Dim vWorkCode As Long
    Dim vdwDate As Date
    Dim vnCount As Long
    Dim vstrFileName As String
    Dim vdBeginDate As Date
    Dim vdEndDate As Date
    Dim vstrTmp As String
    Dim vbRet As Boolean
    Dim vnReadMark As Long
    Dim vnFileNum As Integer
    Dim vstrFileData As String
    Dim vnResultCode As Long
    Dim vnYear As Long
    Dim vnMonth As Long
    Dim vnDay As Long
    Dim vnHour As Long
    Dim vnMinute As Long
    Dim vnSecond As Long
    
    Dim SaveFile As Boolean
    Dim db_fileName As String
    
    GetGeneralLogData = False
    DoEvents

    SQL1 = ""
    SQL2 = ""
    execRecCount = 0
    
    If abUSBFlag = True Then
        frmWizard.CommonDialog.InitDir = CurDir
        frmWizard.CommonDialog.CancelError = False
        'frmWizard.CommonDialog.Flags = cdlOFNHideReadOnly
        frmWizard.CommonDialog.Filter = "GLog Files (*.txt)|*.txt|All Files (*.*)|*.*"
        frmWizard.CommonDialog.FilterIndex = 1
        frmWizard.CommonDialog.InitDir = CurDir
        frmWizard.CommonDialog.ShowOpen
        vstrFileName = frmWizard.CommonDialog.FileName
        If vstrFileName = "" Then
            Exit Function
        End If
    Else
        vnResultCode = FK_EnableDevice(mnCommHandleIndex, 0)
        If vnResultCode <> RUN_SUCCESS Then
            'MsgBox gstrNoDevice
            Exit Function
        End If
    End If
    
    DoEvents
    
    If abUSBFlag = True Then
        vnResultCode = FK_USBLoadGeneralLogDataFromFile(mnCommHandleIndex, vstrFileName)
    Else
        If NewData = True Then
            vnReadMark = 1
        Else
            vnReadMark = 0
        End If

        'open file for save
        SaveFile = False
        If SaveFile = True Then
            vnFileNum = FreeFile
            If vnReadMark = 0 Then
                vstrFileName = App.Path & "\AllLog.txt"
            Else
                vstrFileName = App.Path & "\Log.txt"
            End If
            If Dir(vstrFileName) <> "" Then Kill vstrFileName
            vstrFileData = "No." & vbTab & "EnrNo" & vbTab & "Verify" & vbTab & "InOut" & vbTab & "DateTime" + vbCrLf
            Open vstrFileName For Binary As #vnFileNum
            Put #vnFileNum, , vstrFileData
        End If

        vnResultCode = FK_LoadGeneralLogData(mnCommHandleIndex, vnReadMark)
    End If
    DoEvents
    
    If vnResultCode <> RUN_SUCCESS Then
       'MsgBox ReturnResultPrint(vnResultCode)
       If abUSBFlag = False And SaveFile = True Then
            Close #vnFileNum
        End If
    Else
        vnCount = 1
        Dim vnResultSupportStringID As Long
        
        vnResultSupportStringID = FK_GetIsSupportStringID(mnCommHandleIndex)
        If vnResultSupportStringID >= RUN_SUCCESS Then
            Do
                If vnResultSupportStringID = USER_ID_LEN13_1 Then
                    vStrEnrollNumber = Space(USER_ID_LEN13_1)
                Else
                    vStrEnrollNumber = Space(USER_ID_LEN)
                End If
                
                vnResultCode = FK_GetGeneralLogData_StringID_Workcode(mnCommHandleIndex, vStrEnrollNumber, vVerifyMode, vInOutMode, vdwDate, vWorkCode)
                If vnResultCode <> RUN_SUCCESS Then
                    If vnResultCode = RUNERR_DATAARRAY_END Then
                        vnResultCode = RUN_SUCCESS
                    End If
                    Exit Do
                End If
                
                vStrEnrollNumber = Trim(vStrEnrollNumber)
                If abUSBFlag = False And SaveFile = True Then
                    vstrFileData = funcMakeGeneralLogFileData(vnCount, vStrEnrollNumber, 0, vVerifyMode, vInOutMode, vdwDate)
                    Put #vnFileNum, , vstrFileData
                End If
                
                vbRet = SaveGLogDataToDatabase(SN, vStrEnrollNumber, vdwDate, vVerifyMode, vInOutMode, 0)
                                   
                If vbRet = False Then Exit Do
                vnCount = vnCount + 1
            Loop
        Else
            Do
                vnResultCode = FK_GetGeneralLogData(mnCommHandleIndex, vSEnrollNumber, vVerifyMode, vInOutMode, vdwDate)
                
                If vnResultCode <> RUN_SUCCESS Then
                    If vnResultCode = RUNERR_DATAARRAY_END Then
                        vnResultCode = RUN_SUCCESS
                    End If
                    Exit Do
                End If
                
                If abUSBFlag = False And SaveFile = True Then
                    vstrFileData = funcMakeGeneralLogFileData(vnCount, Trim(CStr(vSEnrollNumber)), vSEnrollNumber, vVerifyMode, vInOutMode, vdwDate)
       
                    Put #vnFileNum, , vstrFileData
                End If
                
                vStrEnrollNumber = CStr(vSEnrollNumber)
                vbRet = SaveGLogDataToDatabase(SN, vStrEnrollNumber, vdwDate, vVerifyMode, vInOutMode, 0)
                                                      
                If vbRet = False Then Exit Do
                vnCount = vnCount + 1
            Loop
        End If
        
        If abUSBFlag = False And SaveFile = True Then
            Close #vnFileNum
        End If

        If vnResultCode = RUN_SUCCESS Then
            GetGeneralLogData = True
            
            If abUSBFlag = True Then
                'MsgBox "USBReadGeneralLogDataFromFile OK"
            Else
                'MsgBox "ReadGeneralLogData OK"
            End If
            
            If execRecCount > 0 Then
                If Trim(SQL1) <> "" Then
                    ExecuteSQLite_Data SQL1
                End If
                    
                SQL1 = ""
                SQL2 = ""
                execRecCount = 0
            End If
        Else
            'MsgBox ReturnResultPrint(vnResultCode)
        End If
    End If
    
    If abUSBFlag = False Then
        FK_EnableDevice mnCommHandleIndex, 1
    End If
    DoEvents
    
    Exit Function
        
BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Function

Public Function GetGeneralLogDataNeo(SN As String, NewData As Boolean, Optional abUSBFlag As Boolean = False) As Boolean
    On Error GoTo BugError
    
    GetGeneralLogDataNeo = False
    
    Dim vbRet As Boolean
    Dim vStrEnrollNumber As String
    Dim vVerifyMode As Long
    Dim vInOutMode As Long
    Dim vWorkCode As Long
    Dim vdwDate As Date
    
    Dim extraProperty() As Byte
    Dim deviceStatus() As Byte
    Dim flagBytes(1) As Byte
    Dim dateBytes() As Byte
    Dim result As Boolean
    Dim records() As RecordExt
    
    Dim R As Variant
    Dim s As String
    Dim a As String
    Dim i As Long
    Dim l As Long
    
    If NewData Then
        flagBytes(0) = 1 'New GLog flag
        flagBytes(1) = 1
    Else
        flagBytes(0) = 0 'All GLog flag
        flagBytes(1) = 0
    End If
    
    deviceStatus = deviceTool.GetBytesByNum(CStr(DeviceBusy), NumberType_Int32Bit)
    ReDim Preserve dateBytes(0) As Byte
    dateBytes(0) = CByte(2010 - BeginYear)
    ReDim Preserve dateBytes(1) As Byte
    dateBytes(1) = 1
    ReDim Preserve dateBytes(2) As Byte
    dateBytes(2) = 1
    ReDim Preserve dateBytes(3) As Byte
    dateBytes(3) = CByte(2099 - BeginYear)
    ReDim Preserve dateBytes(4) As Byte
    dateBytes(4) = 12
    ReDim Preserve dateBytes(5) As Byte
    dateBytes(5) = 31
    
    SQL1 = ""
    SQL2 = ""
    execRecCount = 0
    
    result = deviceConn.SetPropertyExt_2(DeviceProperty_Enable, extraProperty, devNeo, deviceStatus)
    DoEvents
    If result Then
        result = deviceConn.GetPropertyExt_2(DeviceProperty_AttRecords, flagBytes, devNeo, dateBytes)
        DoEvents
        If result Then
            records = deviceTool.GetGLogList(dateBytes)
            
            For Each R In records
                s = GetStatus(R.Verify)
                a = ""
                For i = 1 To MaxAction
                    If 0 <> deviceTool.BitCheck(R.Action, i - 1) Then
                        l = i - 1
                        Select Case i - 1
                            Case 0
                                a = a + "F" + " "
                            Case 1
                                a = a + "P" + " "
                            Case 2
                                a = a + "C" + " "
                            Case 3
                                a = a + "I" + " "
                        End Select
                    End If
                Next
                
                vStrEnrollNumber = R.DIN
                vdwDate = R.Clock
                vVerifyMode = R.Verify
                vInOutMode = l
                vbRet = SaveGLogDataToDatabase(SN, vStrEnrollNumber, vdwDate, vVerifyMode, vInOutMode, 0)
                DoEvents
            Next
        End If
        
        deviceStatus = deviceTool.GetBytesByNum(CStr(DeviceIdle), NumberType_Int32Bit)
        result = deviceConn.SetPropertyExt_2(DeviceProperty_Enable, extraProperty, devNeo, deviceStatus)
        DoEvents
    End If
    
    If execRecCount > 0 Then
        If Trim(SQL1) <> "" Then
            ExecuteSQLite_Data SQL1
        End If
            
        SQL1 = ""
        SQL2 = ""
        execRecCount = 0
    End If
    
    Exit Function
        
BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Function

Public Function OpenDB(db_fileName As String) As Boolean
    On Error GoTo BugError
    
    'Begin Masalah Koneksi Database YYYYMM
    Set CnAttlog = New cConnection
    
    If FileExists(db_fileName) Then
        CnAttlog.OpenDB db_fileName
    Else
        CnAttlog.CreateNewDB db_fileName
        If FileExists(db_fileName) Then
            CnAttlog.OpenDB db_fileName
            
            strSQL = t_att_logTxt_str
            CnAttlog.Execute strSQL
        Else
            OpenDB = False
            Exit Function
        End If
    End If
    
    OpenDB = True
    'End Masalah Koneksi Database YYYYMM
    
    Exit Function
        
BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Function

Private Function funcMakeGeneralLogFileData(anCount As Long, ByVal aStrEnrollNumber As String, aSEnrollNumber As Long, _
    aVerifyMode As Long, aInOutMode As Long, adwDate As Date) As String
    
    Dim vstrData As String
    Dim vstrDTime As String
    Dim vStrEnrollNumber As String
    
    If Len(aStrEnrollNumber) <= 0 Then
        vStrEnrollNumber = CStr(aSEnrollNumber)
    Else
        vStrEnrollNumber = aStrEnrollNumber
    End If
        
    vstrData = CStr(anCount) & vbTab & vStrEnrollNumber & vbTab & getVerifymodeToString(aVerifyMode) & vbTab & getIOmodeToString(aInOutMode) & vbTab

    vstrDTime = CStr(Year(adwDate)) & "/" & Format(Month(adwDate), "0#") & "/" & Format(Day(adwDate), "0#") & _
        " " & Format(Hour(adwDate), "0#") & ":" & Format(Minute(adwDate), "0#") & ":" & Format(Second(adwDate), "0#")
        
    funcMakeGeneralLogFileData = vstrData & vstrDTime & vbCrLf
End Function

Private Function funcMakeGeneralLogFileData_1(anCount As Long, aSEnrollNumber As Long, aVerifyMode As Long, _
    aInOutMode As Long, adwDate As Date, adwTime As Date) As String
    
    Dim vstrData As String
    Dim vstrDTime As String
    
    vstrData = CStr(anCount) & vbTab & CStr(aSEnrollNumber) & vbTab & CStr(aVerifyMode) & vbTab & CStr(aInOutMode) & vbTab

    vstrDTime = CStr(Year(adwDate)) & "/" & Format(Month(adwDate), "0#") & "/" & Format(Day(adwDate), "0#") & _
        " " & Format(Hour(adwTime), "0#") & ":" & Format(Minute(adwTime), "0#") & ":" & Format(Second(adwTime), "0#")
        
    funcMakeGeneralLogFileData_1 = vstrData & vstrDTime & vbCrLf
End Function

Private Function getVerifymodeToString(ByVal anVerifyMode As Long) As String
    Dim vstrTmp As String

    Select Case anVerifyMode
        ' Fp verify
        Case LOG_FPVERIFY           '1
            vstrTmp = "Fp"
        Case LOG_PASSVERIFY         '2
            vstrTmp = "Password"
        Case LOG_CARDVERIFY         '3
            vstrTmp = "Card"
        Case LOG_FPPASS_VERIFY      '4
            vstrTmp = "Fp+Password"
        Case LOG_FPCARD_VERIFY      '5
            vstrTmp = "Fp+Card"
        Case LOG_PASSFP_VERIFY      '6
            vstrTmp = "Password+Fp"
        Case LOG_CARDFP_VERIFY      '7
            vstrTmp = "Card+Fp"
        Case LOG_CARDPASS_VERIFY    '9
            vstrTmp = "Card+Pass"
            ' Face verify
        Case LOG_FACEVERIFY         '20
            vstrTmp = "Face"
        Case LOG_FACECARDVERIFY     '21
            vstrTmp = "Face+Card"
        Case LOG_FACEPASSVERIFY     '22
            vstrTmp = "Face+Pass"
        Case LOG_CARDFACEVERIFY     '23
            vstrTmp = "Card+Face"
        Case LOG_PASSFACEVERIFY     '23
            vstrTmp = "Pass+Face"
        Case Else
            vstrTmp = GetStringVerifyMode(anVerifyMode)
    End Select

    getVerifymodeToString = vstrTmp
End Function

Private Function getIOmodeToString(ByVal anIoMode As Long) As String
    Dim vnIoMode As Long
    Dim vnDoorMode As Long
    Dim vStr As String

    GetIoModeAndDoorMode anIoMode, vnIoMode, vnDoorMode

    If vnDoorMode = 0 Then
        vStr = "( " + str(vnIoMode) + " )"
    Else
        vStr = "( " + str(vnIoMode) + " ) &  "
    End If

    Select Case vnDoorMode
        Case LOG_CLOSE_DOOR
            vStr = vStr + "Close Door"
        Case LOG_OPEN_HAND
            vStr = vStr + "Hand Open"
        Case LOG_PROG_OPEN
            vStr = vStr + "Prog Open"
        Case LOG_PROG_CLOSE
            vStr = vStr + "Prog Close"
        Case LOG_OPEN_IREGAL
            vStr = vStr + "Illegal Open"
        Case LOG_CLOSE_IREGAL
            vStr = vStr + "Illegal Close"
        Case LOG_OPEN_COVER
            vStr = vStr + "Cover Open"
        Case LOG_CLOSE_COVER
            vStr = vStr + "Cover Close"
        Case LOG_OPEN_DOOR
            vStr = vStr + "Open door"
        Case LOG_OPEN_DOOR_THREAT
            vStr = vStr + "Open Door as Threat"
        Case LOG_FORCE_OPEN
            vStr = vStr + "Force Open"
        Case LOG_FORCE_CLOSE
            vStr = vStr + "Force Close"
    End Select
    
    If vStr = "" Then vStr = "--"
    getIOmodeToString = vStr
End Function

Public Function EmptyEnrollData(mnCommHandleIndex As Long) As Boolean
    On Error GoTo BugError
    
    Dim vnResultCode As Long

    EmptyEnrollData = False
    DoEvents

    vnResultCode = FK_EnableDevice(mnCommHandleIndex, 0)
    If vnResultCode <> RUN_SUCCESS Then
        Exit Function
    End If

    vnResultCode = FK_EmptyEnrollData(mnCommHandleIndex)
    If vnResultCode = RUN_SUCCESS Then
        EmptyEnrollData = True
    End If

    FK_EnableDevice mnCommHandleIndex, 1
    
    Exit Function
        
BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Function

Public Function EmptyEnrollDataNeo() As Boolean
    On Error GoTo BugError
    
    EmptyEnrollDataNeo = False
    
    Dim extraProperty() As Byte, extraData() As Byte
    Dim deviceStatus() As Byte
    Dim result As Boolean
    
    deviceStatus = deviceTool.GetBytesByNum(CStr(DeviceBusy), NumberType_Int32Bit)
    result = deviceConn.SetPropertyExt_2(DeviceProperty_Enable, extraProperty, devNeo, deviceStatus)
    DoEvents
    If result Then
        extraData = deviceTool.GetBytes("0")
        result = deviceConn.SetPropertyExt_2(DeviceProperty_Enrolls, extraProperty, devNeo, extraData)
        DoEvents
        EmptyEnrollDataNeo = result
        
        deviceStatus = deviceTool.GetBytesByNum(CStr(DeviceIdle), NumberType_Int32Bit)
        result = deviceConn.SetPropertyExt_2(DeviceProperty_Enable, extraProperty, devNeo, deviceStatus)
        DoEvents
    End If
    
    Exit Function
        
BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Function

Public Function EmptyGLogData(mnCommHandleIndex As Long) As Boolean
    On Error GoTo BugError
    
    Dim vnResultCode As Long

    EmptyGLogData = False
    DoEvents

    vnResultCode = FK_EnableDevice(mnCommHandleIndex, 0)
    If vnResultCode <> RUN_SUCCESS Then
        Exit Function
    End If

    vnResultCode = FK_EmptyGeneralLogData(mnCommHandleIndex)
    If vnResultCode = RUN_SUCCESS Then
        EmptyGLogData = True
    End If

    FK_EnableDevice mnCommHandleIndex, 1
    
    Exit Function
        
BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Function

Public Function EmptyGLogDataNeo() As Boolean
    On Error GoTo BugError
    
    EmptyGLogDataNeo = False
    
    Dim extraProperty() As Byte
    Dim deviceStatus() As Byte
    Dim extraData() As Byte
    Dim result As Boolean
    
    deviceStatus = deviceTool.GetBytesByNum(CStr(DeviceBusy), NumberType_Int32Bit)
    result = deviceConn.SetPropertyExt_2(DeviceProperty_Enable, extraProperty, devNeo, deviceStatus)
    DoEvents
    If result Then
        result = deviceConn.SetPropertyExt_2(DeviceProperty_AttRecords, extraProperty, devNeo, extraData)
        DoEvents
        EmptyGLogDataNeo = result
        
        deviceStatus = deviceTool.GetBytesByNum(CStr(DeviceIdle), NumberType_Int32Bit)
        result = deviceConn.SetPropertyExt_2(DeviceProperty_Enable, extraProperty, devNeo, deviceStatus)
        DoEvents
    End If
    
    Exit Function
        
BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Function

Public Function DelAllUser(sn_device As String) As Boolean
    On Error GoTo BugError
    
    Dim Hapus As Boolean
    If InitDevice(sn_device, optConn, False) = True Then
        If cmdOpenComm(optConn) = True Then
            Select Case deviceType
            Case 2 'Mesin Neo
                Hapus = EmptyEnrollDataNeo()
                If Hapus = True Then
                    EmptyGLogDataNeo
                End If
                DelAllUser = Hapus
                
                cmdCloseCommNeo
            Case 3 'Mesin Revo
                mnCommHandleIndex = gnCommHandleIndex
                mbGetState = gbOpenFlag
                
                Hapus = EmptyEnrollData(mnCommHandleIndex)
                If Hapus = True Then
                    EmptyGLogData mnCommHandleIndex
                End If
                DelAllUser = Hapus
                
                cmdCloseComm
            End Select
        Else
            DelAllUser = False
        End If
    Else
        DelAllUser = False
    End If
    
    Exit Function
        
BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Function

Public Function CekKoneksiMesin(sn_device As String) As Boolean
    On Error GoTo BugError
    
    CekKoneksiMesin = False
    
    If InitDevice(sn_device, optConn, False) = True Then
        If cmdOpenComm(optConn) = True Then
            Select Case deviceType
            Case 2 'Mesin Neo
                If GetSN_neo(sn_device) = True Then
                    CekKoneksiMesin = True
                End If
                
                cmdCloseCommNeo
            Case 3 'Mesin Revo
                mnCommHandleIndex = gnCommHandleIndex
                mbGetState = gbOpenFlag
                
                If GetSN(sn_device, mnCommHandleIndex) = True Then
                    CekKoneksiMesin = True
                End If
                
                cmdCloseComm
            End Select
        End If
    End If
    
    Set CnTemp = Nothing
    
    Exit Function
        
BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Function

Private Function GetSN_neo(sn_device As String) As Boolean
    On Error GoTo BugError
    
    GetSN_neo = False
    
    Dim vstrData As String
    Dim extraProperty() As Byte, extraData() As Byte
    Dim result As Boolean
    Dim msg_str As String
    
    ReDim Preserve extraData(0) As Byte
    ReDim Preserve extraData(1) As Byte
    ReDim Preserve extraData(2) As Byte
    ReDim Preserve extraData(3) As Byte
    
    extraData = deviceTool.GetBytesByNum(CStr(DeviceSerialNo), NumberType_Int32Bit)
    result = deviceConn.GetPropertyExt_2(DeviceProperty_SerialNo, extraProperty, devNeo, extraData)
    DoEvents
    If result Then
        vstrData = deviceTool.GetString(extraData)
        vstrData = GetStringAngkaHuruf(vstrData)
        
        sn_device = check_spelling_fio_device_sn(Trim(sn_device))
        vstrData = check_spelling_fio_device_sn(Trim(vstrData))
    
        If sn_device = vstrData Then
            GetSN_neo = True
        Else
            msg_str = INILanRead("errMsg", "Err1.Device.14", "")
            msg_str = Replace(msg_str, "?1", sn_device)
            msg_str = Replace(msg_str, "?2", vstrData)
            MsgBox msg_str, vbOKOnly, App.Title & " : " & "Err1.Device.14"
        End If
    Else
        msg_str = INILanRead("errMsg", "Err1.Device.13", "")
        MsgBox msg_str, vbOKOnly, App.Title & " : " & "Err1.Device.13"
    End If
    
    Set deviceTool = Nothing
    
    Exit Function
        
BugError:
    'MsgBox Err.Description, , Err.number
    Err.Clear
    Resume Next
End Function

Private Function GetSN(sn_device As String, fnCommHandleIndex As Long) As Boolean
    On Error GoTo BugError
    
    GetSN = False
    
    Dim vstrData As String
    Dim vnResultCode As Long
    Dim msg_str As String
    
    vnResultCode = FK_EnableDevice(fnCommHandleIndex, 0)
    If vnResultCode <> RUN_SUCCESS Then
        Exit Function
    End If

    vnResultCode = FuncGetProductData(fnCommHandleIndex, PRODUCT_SERIALNUMBER, vstrData)
    If vnResultCode = RUN_SUCCESS Then
        sn_device = check_spelling_fio_device_sn(Trim(sn_device))
        vstrData = check_spelling_fio_device_sn(Trim(vstrData))
        
        If sn_device = vstrData Then
            GetSN = True
        Else
            vstrData = Mid(vstrData, 1, Len(vstrData) - 1)
            If sn_device = vstrData Then
                GetSN = True
            Else
                msg_str = INILanRead("errMsg", "Err1.Device.14", "")
                msg_str = Replace(msg_str, "?1", sn_device)
                msg_str = Replace(msg_str, "?2", vstrData)
                MsgBox msg_str, vbOKOnly, App.Title & " : " & "Err1.Device.14"
            End If
        End If
    Else
        msg_str = INILanRead("errMsg", "Err1.Device.13", "")
        MsgBox msg_str, vbOKOnly, App.Title & " : " & "Err1.Device.13"
    End If

    FK_EnableDevice fnCommHandleIndex, 1
    
    Exit Function
        
BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Function

Private Function FuncGetProductData(fnCommHandleIndex As Long, anIndex As Long, astrItem As String) As Long
    On Error GoTo BugError
    
    Dim vstrData As String
    
    vstrData = Space(256)
    FuncGetProductData = FK_GetProductData(fnCommHandleIndex, anIndex, vstrData)
    If FuncGetProductData <> RUN_SUCCESS Then
        Exit Function
    End If
    astrItem = Trim(vstrData)
    
    Exit Function
        
BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Function

Public Function DeleteAdmin(sn_device As String) As Boolean
    On Error GoTo BugError
    
    DeleteAdmin = False
    
    Dim strQuery As String
    
    If InitDevice(sn_device, optConn, False) = True Then
        If cmdOpenComm(optConn) = True Then
            Select Case deviceType
            Case 2 'Mesin Neo
                DeleteAdmin = BenumbManagerNeo(sn_device)
                DoEvents
                
                cmdCloseCommNeo
            Case 3 'Mesin Revo
                mnCommHandleIndex = gnCommHandleIndex
                mbGetState = gbOpenFlag
                
                DeleteAdmin = BenumbManager(mnCommHandleIndex)
                DoEvents
                
                cmdCloseComm
            End Select
        End If
    End If
    
    Exit Function
        
BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Function

Public Function BenumbManager(mnCommHandleIndex As Long) As Boolean
    On Error GoTo BugError
    
    BenumbManager = False
    
    Dim vnResultCode As Long
    
    vnResultCode = FK_EnableDevice(mnCommHandleIndex, 0)
    If vnResultCode <> RUN_SUCCESS Then
        Exit Function
    End If

    vnResultCode = FK_BenumbAllManager(mnCommHandleIndex)
    
    If vnResultCode = RUN_SUCCESS Then
        BenumbManager = True
    End If
    FK_EnableDevice mnCommHandleIndex, 1
    DoEvents

    Exit Function
        
BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Function

Public Function BenumbManagerNeo(sn_device As String) As Boolean
    On Error GoTo BugError
    
    BenumbManagerNeo = False
    
    DeviceInfoNeo sn_device
    If count_manager = 0 Then
        BenumbManagerNeo = True
    Else
        Dim extraProperty() As Byte, extraData() As Byte, userIDBytes() As Byte
        Dim deviceStatus() As Byte
        Dim userList() As UserExt
        Dim u As Variant
        Dim result As Boolean
        
        Dim pin As String
        Dim Privilege_name As String
        Dim Privilege As Long
        Dim pin_admin As String
        Dim field() As String
        
        pin_admin = ""
        deviceStatus = deviceTool.GetBytesByNum(CStr(DeviceBusy), NumberType_Int32Bit)
        result = deviceConn.SetPropertyExt_2(DeviceProperty_Enable, extraProperty, devNeo, deviceStatus)
        DoEvents
        If result Then
            userIDBytes = deviceTool.GetBytes("0")
            result = deviceConn.GetPropertyExt_2(DeviceProperty_Enrolls, userIDBytes, devNeo, extraData)
            DoEvents
            If result Then
                Dim SN As Long
                SN = 1
                userList = deviceTool.GetAllUserExtWithoutEnroll(extraData)
                For Each u In userList
                    Dim enrollCol As Variant
                    Dim enroll As EnrollExt
                    Dim user As UserExt
                    ReDim Preserve gUsers(SN) As UserExt
                    
                    Set user = u
                    Set gUsers(SN) = u
                    enrollCol = user.Enrolls
                    Set enroll = enrollCol(0)
                    result = deviceConn.GetPropertyExt(UserProperty_Enroll, extraProperty, user, extraData)
                    If result Then
                        pin = user.DIN
                        Privilege_name = GetPrivilegeName(user.Privilege)
                        Privilege = user.Privilege
                        
                        If Privilege = 3 Then
                            pin_admin = pin_admin & pin & ","
                        End If
                    End If
                    
                    SN = SN + 1
                Next
            End If
            
            deviceStatus = deviceTool.GetBytesByNum(CStr(DeviceIdle), NumberType_Int32Bit)
            result = deviceConn.SetPropertyExt_2(DeviceProperty_Enable, extraProperty, devNeo, deviceStatus)
            DoEvents
        End If
        
        Dim i As Integer
        If pin_admin <> "" Then
            field = Split(pin_admin, ",")
            For i = 0 To UBound(field) - 1
                pin = field(i)
                result = SetUserPrivilegeNeo(pin, 1)
            Next
        End If
    End If
    
    Exit Function
        
BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Function

Public Function SetDevTime(dwDate As Date, sn_device As String) As Boolean
    On Error GoTo BugError
    
    SetDevTime = False
    
    Dim strQuery As String
    
    If InitDevice(sn_device, optConn, False) = True Then
        If vnstatus_aktif = "1" Then
            If cmdOpenComm(optConn) = True Then
                Select Case deviceType
                Case 2 'Mesin Neo
                    If SetDeviceTimeNeo(dwDate) = True Then
                        DeviceInfoNeo sn_device
                        SetDevTime = True
                    End If
                    
                    cmdCloseCommNeo
                Case 3 'Mesin Revo
                    mnCommHandleIndex = gnCommHandleIndex
                    mbGetState = gbOpenFlag
                    
                    If SetDeviceTime(dwDate, mnCommHandleIndex) = True Then
                        DeviceInfo sn_device, mnCommHandleIndex
                        SetDevTime = True
                    End If
                    
                    cmdCloseComm
                End Select
            Else
                MsgBoxLanguageFile "Err1.Device.16", vnname_device, sn_device
            End If
        Else
            SetDevTime = True
        End If
    End If
    
    Exit Function
        
BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Function

Public Function SetDeviceTime(vdwDate As Date, mnCommHandleIndex As Long) As Boolean
    On Error GoTo BugError
    
    SetDeviceTime = False
    
    Dim strDataTime As String
    Dim vnResultCode As Long

    vnResultCode = FK_EnableDevice(mnCommHandleIndex, 0)
    If vnResultCode <> RUN_SUCCESS Then
        Exit Function
    End If

    vnResultCode = FK_SetDeviceTime(mnCommHandleIndex, vdwDate)
    If vnResultCode = RUN_SUCCESS Then
        SetDeviceTime = True
        strDataTime = "SetDeviceDateTime = " & Format(vdwDate, "Long Date") & ", Time = " & Format(vdwDate, "Long Time")
    End If

    FK_EnableDevice mnCommHandleIndex, 1
    
    Exit Function
        
BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Function

Public Function SetDeviceTimeNeo(vdwDate As Date) As Boolean
    On Error GoTo BugError
    
    SetDeviceTimeNeo = False
    
    Dim extraProperty() As Byte, extraData() As Byte
    Dim result As Boolean
    Dim DateNow As Date
    
    DateNow = Now
    DoEvents
    
    Set_NewDateTime vdwDate
    DoEvents
    
    result = deviceConn.SetPropertyExt_2(DeviceProperty_DeviceTime, extraProperty, devNeo, extraData)
    DoEvents
    
    Set_NewDateTime DateNow
    DoEvents
    
    SetDeviceTimeNeo = result
    
    Exit Function
        
BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Function

Public Function RegEnroll(sn_device As String, name_device As String, vStrEnrollNumber As String, vBackupNumber As Long) As Boolean
    On Error GoTo BugError
    
    RegEnroll = False
    
    Dim vnResultCode As Boolean
    Dim str1 As String
    
    If InitDevice(sn_device, optConn, False) = True Then
        If cmdOpenComm(optConn) = True Then
            Select Case deviceType
            Case 2 'Mesin Neo
                vnResultCode = GetUserPIN_Neo(sn_device, vStrEnrollNumber)
                If vnResultCode = True Then
                    'RegEnroll = EnterEnrollNeo(vStrEnrollNumber, vBackupNumber)
                Else
                    vnResultCode = WritePwdData(vStrEnrollNumber, strPwdNeoDefault)
                    If vnResultCode = True Then
                        'RegEnroll = EnterEnrollNeo(vStrEnrollNumber, vBackupNumber)
                    End If
                End If
                
                cmdCloseCommNeo
            Case 3 'Mesin Revo
                mnCommHandleIndex = gnCommHandleIndex
                mbGetState = gbOpenFlag
                
                vnResultCode = GetUserPIN(sn_device, vStrEnrollNumber, mbGetState, mnCommHandleIndex)
                If vnResultCode = True Then
                    RegEnroll = EnterEnroll(vStrEnrollNumber, vBackupNumber, mnCommHandleIndex)
                Else
                    vnResultCode = SetUserPIN(sn_device, vStrEnrollNumber, mbGetState, mnCommHandleIndex)
                    If vnResultCode = True Then
                        RegEnroll = EnterEnroll(vStrEnrollNumber, vBackupNumber, mnCommHandleIndex)
                    End If
                End If
                
                cmdCloseComm
            End Select
        Else
            str1 = INILanRead("frmIsianRegFP", "msg1", "")
            str1 = Replace(str1, "?1", name_device)
            str1 = Replace(str1, "?2", sn_device)
            MsgBox str1, vbOKOnly, App.Title & " : "
        End If
    End If
    
    Exit Function
        
BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Function

Public Function GetEnrollByPIN(sn_device As String, name_device As String, vStrEnrollNumber As String) As Boolean
    On Error GoTo BugError
    
    GetEnrollByPIN = False
    
    Dim str1 As String
    
    If InitDevice(sn_device, optConn, False) = True Then
        If cmdOpenComm(optConn) = True Then
            Select Case deviceType
            Case 2 'Mesin Neo
                GetEnrollByPIN = GetUserPIN_Neo(sn_device, vStrEnrollNumber)
                
                cmdCloseCommNeo
            Case 3 'Mesin Revo
                mnCommHandleIndex = gnCommHandleIndex
                mbGetState = gbOpenFlag
                
                GetEnrollByPIN = GetUserPIN(sn_device, vStrEnrollNumber, mbGetState, mnCommHandleIndex)
                
                cmdCloseComm
            End Select
        Else
            str1 = INILanRead("errMsg", "msg44", "")
            str1 = Replace(str1, "?1", name_device)
            str1 = Replace(str1, "?2", sn_device)
            MsgBox str1, vbOKOnly, App.Title & " : "
        End If
    End If
    
    Exit Function
        
BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Function

Private Function EnterEnroll(vStrEnrollNumber As String, vBackupNumber As Long, fnCommHandleIndex As Long) As Boolean
    On Error GoTo BugError
    
    Dim vstrData As String
    Dim vnResultCode As Long
    Dim vPrivilege As Long
    Dim str1 As String
    
    EnterEnroll = False
    vnResultCode = FK_EnableDevice(fnCommHandleIndex, 0)
    If vnResultCode <> RUN_SUCCESS Then
        Exit Function
    End If

    vPrivilege = MP_NONE
    str1 = "{""cmd"":""?1"", ""param"": {""user_id"":""?2"",""backup_number"":?3,""privilege"":?4}}"
    
    str1 = Replace(str1, "?1", "enter_enroll")
    str1 = Replace(str1, "?2", vStrEnrollNumber)
    str1 = Replace(str1, "?3", vBackupNumber)
    str1 = Replace(str1, "?4", vPrivilege)
    'TextToFile str1, "JsonEnterEnroll.txt"
    
    vnResultCode = FK_HS_ExecJsonCmd(fnCommHandleIndex, str1)
    EnterEnroll = vnResultCode
    DoEvents
    
    Sleep 30
    
    FK_EnableDevice fnCommHandleIndex, 1
    
    Exit Function
        
BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Function

Private Sub Sleep(ByVal HowLong As Date)
    Dim endDate As Date
    endDate = DateAdd("s", HowLong, Now)
    While endDate > Now
        DoEvents
    Wend
End Sub

Public Function GetUserPIN(SN As String, pin As String, mbGetState As Boolean, mnCommHandleIndex As Long) As Boolean
    On Error GoTo BugError
    
    Dim i As Integer
    Dim J As Integer
    Dim ada As Boolean
    
    Dim vEnrollNumber As Long
    Dim vStrEnrollNumber As String
    Dim vBackupNumber As Long
    Dim vPrivilege As Long
    Dim vnEnableFlag As Long
    Dim vnMessRet As Long
    Dim vnResultCode As Long
    
    GetUserPIN = False
    
    vnResultCode = FK_EnableDevice(mnCommHandleIndex, 0)
    If vnResultCode <> RUN_SUCCESS Then
        Exit Function
    End If

    Dim RecstAtt As cRecordset
    strSQL = "select * from t_template_revo where pin = '0' and template_index = 10 "
    Set RecstAtt = getRecordSet_Data(strSQL, True)
    Do While RecstAtt.EOF = False
        EnrollDataDefault = RecstAtt!template_blob
        RecstAtt.MoveNext
    Loop
    strTmpPwdDefault = ShowByteArrayToString(EnrollDataDefault, PWD_DATA_SIZE)
    strTmpPwd = "0000000000000000317A2B1944DF54D5A10000000000000000000000000000000000000000000000"
    strTmpPwd2 = "00000000000000003030303030303030300000000000000000000000000000000000000000000000"
    strTmpCard = "00000000000000000030303030303030300000000000000000000000000000000000000000000000"
    
    strSQL = "DELETE FROM t_template_revo_new WHERE sn_device = """ & SN & """ "
    ExecuteSQLite_Data strSQL
    strSQL = "DELETE FROM t_template_revo_new WHERE sn_device NOT IN (SELECT sn_device FROM t_device) "
    ExecuteSQLite_Data strSQL
    
    'strSQL = "VACUUM "
    'ExecuteSQLite_Data strSQL
        
    Dim rs_template As cRecordset
    strSQL = "Select * From t_template_revo_new WHERE sn_device = """ & SN & """ "
    Set rs_template = getRecordSet_Data(strSQL, False)
    
    ada = False
    Dim vnResultSupportStringID As Long
    
    vnResultSupportStringID = FK_GetIsSupportStringID(mnCommHandleIndex)
    If vnResultSupportStringID >= RUN_SUCCESS Then
        If vnResultSupportStringID = USER_ID_LEN13_1 Then
            vStrEnrollNumber = Space(USER_ID_LEN13_1)
        Else
            vStrEnrollNumber = Space(USER_ID_LEN)
        End If
        
        vStrEnrollNumber = pin
        For J = 0 To 9
            vBackupNumber = J
            vnResultCode = FK_GetEnrollData_StringID(mnCommHandleIndex, vStrEnrollNumber, vBackupNumber, vPrivilege, mbytEnrollData(0), mlngPasswordData)
            If vnResultCode = RUN_SUCCESS Then
                FuncSaveTemplate SN, vStrEnrollNumber, vBackupNumber, mnCommHandleIndex, rs_template
                ada = True
            Else
                Exit For
            End If
            DoEvents
        Next
        
        For J = BACKUP_PSW To BACKUP_PALMVEIN_3
            vBackupNumber = J
            vnResultCode = FK_GetEnrollData_StringID(mnCommHandleIndex, vStrEnrollNumber, vBackupNumber, vPrivilege, mbytEnrollData(0), mlngPasswordData)
            If vnResultCode = RUN_SUCCESS Then
                FuncSaveTemplate SN, vStrEnrollNumber, vBackupNumber, mnCommHandleIndex, rs_template
                ada = True
            Else
                If J >= BACKUP_PALMVEIN_1 Then
                    Exit For
                End If
            End If
            DoEvents
        Next
        DoEvents
    Else
        vEnrollNumber = Val(pin)
        
        For J = 0 To 9
            vBackupNumber = J
            vnResultCode = FK_GetEnrollData(mnCommHandleIndex, vEnrollNumber, vBackupNumber, vPrivilege, mbytEnrollData(0), mlngPasswordData)
            If vnResultCode = RUN_SUCCESS Then
                FuncSaveTemplate SN, CStr(vEnrollNumber), vBackupNumber, mnCommHandleIndex, rs_template
                ada = True
            Else
                Exit For
            End If
            DoEvents
        Next
        
        For J = BACKUP_PSW To BACKUP_PALMVEIN_3
            vBackupNumber = J
            vnResultCode = FK_GetEnrollData(mnCommHandleIndex, vEnrollNumber, vBackupNumber, vPrivilege, mbytEnrollData(0), mlngPasswordData)
            If vnResultCode = RUN_SUCCESS Then
                FuncSaveTemplate SN, CStr(vEnrollNumber), vBackupNumber, mnCommHandleIndex, rs_template
                ada = True
            Else
                If J >= BACKUP_PALMVEIN_1 Then
                    Exit For
                End If
            End If
            DoEvents
        Next
        DoEvents
    End If
    
    If ada = True Then
        GetUserPIN = True
        rs_template.UpdateBatch
        DoEvents
        
        strSQL = "REPLACE INTO t_template_revo_backup(sn_device, pin, template_index, template_blob, last_update) " & _
            "SELECT '0', pin, template_index, template_blob, last_update FROM t_template_revo_new WHERE sn_device = """ & SN & """ "
        ExecuteSQLite_Data strSQL
        DoEvents
        
        GetRegUlang SN, pin
        DoEvents
    End If

    FK_EnableDevice mnCommHandleIndex, 1
    
    Exit Function
        
BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Function

Public Function GetUserPIN_Neo(SN As String, pin As String) As Boolean
    On Error GoTo BugError
    
    GetUserPIN_Neo = False
    
    Dim extraProperty() As Byte, extraData() As Byte, userIDBytes() As Byte
    Dim deviceStatus() As Byte
    Dim user As UserExt
    Dim result As Boolean
    
    Dim nama As String
    Dim privilege_str As String
    Dim ada As Boolean
    Dim strTmp As String
    Dim dateStr As String
    
    If "" = Trim(pin) Then
        Exit Function
    End If
    
    If False = IsNumeric(Trim(pin)) Then
        Exit Function
    End If
    
    SQL1 = ""
    SQL2 = ""
    execRecCount = 0
    ada = False
    dateStr = getCurrentDT
    
    strSQL = "DELETE FROM t_template_neo_new WHERE sn_device = """ & SN & """ "
    ExecuteSQLite_Data strSQL
    strSQL = "DELETE FROM t_template_neo_new WHERE sn_device NOT IN (SELECT sn_device FROM t_device) "
    ExecuteSQLite_Data strSQL
    
    deviceStatus = deviceTool.GetBytesByNum(CStr(DeviceBusy), NumberType_Int32Bit)
    result = deviceConn.SetPropertyExt_2(DeviceProperty_Enable, extraProperty, devNeo, deviceStatus)
    DoEvents
    If result Then
        userIDBytes = deviceTool.GetBytes(Trim(pin))
        result = deviceConn.GetPropertyExt_2(DeviceProperty_Enrolls, userIDBytes, devNeo, extraData)
        DoEvents
        If result Then
            Dim enrollCol As Variant
            Dim enroll As EnrollExt
            Set user = deviceTool.GetUserExtWithoutEnroll(extraData)
            enrollCol = user.Enrolls
            Set enroll = enrollCol(0)
            
            result = deviceConn.GetPropertyExt(UserProperty_Enroll, extraProperty, user, extraData)
            If result Then
                Dim i As Long
                ReDim Preserve gUsers(1) As UserExt
                
                Set gUsers(1) = user
                'pin = user.DIN
                nama = user.UserName
                
                For i = 0 To MaxFingerprintCount - 1
                    If 0 <> deviceTool.BitCheck(CLng(enroll.EnrollType), i) Then
                        strTmp = ReadFPData(pin, i)
            
                        If strTmp <> "" Then
                            SaveTemplateNeoToDatabase SN, pin, Trim(CStr(i)), strTmp, dateStr
                            ada = True
                        End If
                    End If
                Next
                
                If 0 <> deviceTool.BitCheck(CLng(enroll.EnrollType), 10) Then
                    strTmp = ReadPwdData(pin)
                    If strTmp <> "" Then
                        SaveTemplateNeoToDatabase SN, pin, "10", strTmp, dateStr
                        ada = True
                    End If
                End If
                
                If 0 <> deviceTool.BitCheck(CLng(enroll.EnrollType), 11) Then
                    strTmp = ReadCardData(pin)
                    If strTmp <> "" Then
                        SaveTemplateNeoToDatabase SN, pin, "11", strTmp, dateStr
                        ada = True
                    End If
                End If
                
                privilege_str = GetPrivilegeName(user.Privilege)
            End If
        End If
        
        deviceStatus = deviceTool.GetBytesByNum(CStr(DeviceIdle), NumberType_Int32Bit)
        result = deviceConn.SetPropertyExt_2(DeviceProperty_Enable, extraProperty, devNeo, deviceStatus)
        DoEvents
    End If
    
    If ada = True Then
        If execRecCount > 0 Then
            If Trim(SQL1) <> "" Then
                ExecuteSQLite_Data SQL1
            End If
            
            SQL1 = ""
            SQL2 = ""
            execRecCount = 0
        End If
    End If
    
    If ada = True Then
        GetUserPIN_Neo = True
        DoEvents
        
        strSQL = "REPLACE INTO t_template_neo_backup(sn_device, pin, template_index, template_str, last_update) " & _
            "SELECT '0', pin, template_index, template_str, last_update FROM t_template_neo_new WHERE sn_device = """ & SN & """ "
        ExecuteSQLite_Data strSQL
        DoEvents
        
        GetRegUlangNeo SN, pin
        DoEvents
    End If
    
    Exit Function
        
BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Function

Public Function GetRegUlang(SN As String, pin As String) As Boolean
    On Error GoTo BugError
    
    Dim rs As cRecordset
    Dim sn_device As String
    Dim name_device As String
    
    'Hapus mesin yang tidak ada
    strSQL = "DELETE FROM t_template_log WHERE sn_device NOT IN (SELECT sn_device FROM t_device) "
    ExecuteSQLite_Data strSQL
    
    'Hapus mesin yang tipe koneksi cloud
    strSQL = "DELETE FROM t_template_log WHERE sn_device IN (SELECT sn_device FROM t_device WHERE comm_type = 4) "
    ExecuteSQLite_Data strSQL
    
    strSQL = "DELETE FROM t_template_log WHERE pin = """ & pin & """ AND sn_device <> """ & SN & """ "
    ExecuteSQLite_Data strSQL
    
    'Upload user template default ke semua mesin baru == pegawai baru ke semua mesin
    strSQL = "SELECT d.*, dt.firmware, dt.dev_type FROM t_device AS d " & _
        "LEFT JOIN t_device_type AS dt ON dt.id_device_type = d.id_device_type " & _
        "WHERE d.status_aktif = '1' AND d.comm_type < 4 AND d.sn_device <> """ & SN & """ "
        
    Set rs = getRecordSet_Data(strSQL, True)
    Do While Not rs.EOF
        sn_device = rs.fields("sn_device").value
        name_device = rs.fields("name_device").value
        deviceType = rs.fields("dev_type").value
        
        'status : 0 = Belum, 1 = Default, 3 = Sudah
        strSQL = "Replace Into t_template_log (sn_device, pin, status, last_update) " & _
            "VALUES (""" & sn_device & """, """ & pin & """, 3, """ & CreatedDate() & """) "
        ExecuteSQLite_Data strSQL
        
        rs.MoveNext
    Loop
    DoEvents
    
    Exit Function
        
BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Function

Public Function GetRegUlangNeo(SN As String, pin As String) As Boolean
    On Error GoTo BugError
    
    Dim rs As cRecordset
    Dim sn_device As String
    Dim name_device As String
    
    'Hapus mesin yang tidak ada
    strSQL = "DELETE FROM t_template_log_neo WHERE sn_device NOT IN (SELECT sn_device FROM t_device) "
    ExecuteSQLite_Data strSQL
    
    'Hapus mesin yang tipe koneksi cloud
    strSQL = "DELETE FROM t_template_log_neo WHERE sn_device IN (SELECT sn_device FROM t_device WHERE comm_type = 4) "
    ExecuteSQLite_Data strSQL
    
    strSQL = "DELETE FROM t_template_log_neo WHERE pin = """ & pin & """ AND sn_device <> """ & SN & """ "
    ExecuteSQLite_Data strSQL
    
    'Upload user template default ke semua mesin baru == pegawai baru ke semua mesin
    strSQL = "SELECT d.*, dt.firmware, dt.dev_type FROM t_device AS d " & _
        "LEFT JOIN t_device_type AS dt ON dt.id_device_type = d.id_device_type " & _
        "WHERE d.status_aktif = '1' AND d.comm_type < 4 AND d.sn_device <> """ & SN & """ "
        
    Set rs = getRecordSet_Data(strSQL, True)
    Do While Not rs.EOF
        sn_device = rs.fields("sn_device").value
        name_device = rs.fields("name_device").value
        deviceType = rs.fields("dev_type").value
        
        'status : 0 = Belum, 1 = Default, 3 = Sudah
        strSQL = "Replace Into t_template_log_neo (sn_device, pin, status, last_update) " & _
            "VALUES (""" & sn_device & """, """ & pin & """, 3, """ & CreatedDate() & """) "
        ExecuteSQLite_Data strSQL
        
        rs.MoveNext
    Loop
    DoEvents
    
    Exit Function
        
BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Function

Public Function SetUserPIN(SN As String, pin As String, mbGetState As Boolean, mnCommHandleIndex As Long) As Boolean
    On Error GoTo BugError
    
    Dim vEnrollName As String
    Dim vEnrollNumber As Long
    Dim vStrEnrollNumber As String
    Dim vBackupNumber As Long
    Dim vPrivilege As Long
    
    Dim vnMessRet As Long
    Dim vStr As String
    Dim vTitle As String
    Dim vnIsSupported As Long
    Dim vnResultCode As Long
    Dim vbRet As Boolean
    
    SetUserPIN = False
    
    vnResultCode = FK_EnableDevice(mnCommHandleIndex, 0)
    If vnResultCode <> RUN_SUCCESS Then
        Exit Function
    End If

    Dim RecstAtt As New cRecordset
    
    Dim countTemplate As Integer
    Dim vbytEnrollData() As Byte
    Static x As Long
    mbGetState = True
    
    strSQL = "select * from t_template_revo where pin = '0' and template_index = 10 "
    Set RecstAtt = getRecordSet_Data(strSQL, True)
    Do While RecstAtt.EOF = False
        EnrollDataDefault = RecstAtt!template_blob
        RecstAtt.MoveNext
    Loop
    strTmpPwdDefault = ShowByteArrayToString(EnrollDataDefault, PWD_DATA_SIZE)
    strTmpPwd = "0000000000000000317A2B1944DF54D5A10000000000000000000000000000000000000000000000"
    strTmpPwd2 = "00000000000000003030303030303030300000000000000000000000000000000000000000000000"
    strTmpCard = "00000000000000000030303030303030300000000000000000000000000000000000000000000000"
    
    strSQL = "SELECT * FROM t_employee Where pin = """ & pin & """ "
    Set RecstAtt = getRecordSet_Data(strSQL, True)
    
    If RecstAtt.RecordCount = 0 Then
        FK_EnableDevice mnCommHandleIndex, 1
        SetUserPIN = True
        Exit Function
    End If
    
    Dim vnResultSupportStringID As Long
    With RecstAtt
        vnResultSupportStringID = FK_GetIsSupportStringID(mnCommHandleIndex)
        If vnResultSupportStringID >= RUN_SUCCESS Then
            If vnResultSupportStringID = USER_ID_LEN13_1 Then
                vStrEnrollNumber = Space(USER_ID_LEN13_1)
            Else
                vStrEnrollNumber = Space(USER_ID_LEN)
            End If
            
            Do While .EOF = False
                vEnrollName = Space(256)
                vStrEnrollNumber = .fields("pin").value
                vEnrollName = .fields("name_employee").value
                vPrivilege = MP_NONE
                vBackupNumber = BACKUP_PSW
                
                ZeroMemory mbytEnrollData(0), UBound(mbytEnrollData) + 1
                If vBackupNumber = BACKUP_PSW Or vBackupNumber = BACKUP_CARD Then
                    CopyMemory mbytEnrollData(0), EnrollDataDefault(0), PWD_DATA_SIZE
                End If
                
                Call FK_IsSupportedEnrollData(mnCommHandleIndex, vBackupNumber, vnIsSupported)
                If vnIsSupported <> 0 Then
                    vnResultCode = FK_PutEnrollData_StringID(mnCommHandleIndex, vStrEnrollNumber, vBackupNumber, _
                        vPrivilege, mbytEnrollData(0), mlngPasswordData)
                                    
                    If vnResultCode <> RUN_SUCCESS Then
                        vStr = "SetAllEnrollData Error"
                    Else
                        strSQL = "DELETE FROM t_template_log WHERE sn_device = """ & SN & """ AND pin = """ & vStrEnrollNumber & """ "
                        ExecuteSQLite_Data strSQL
                    
                        strSQL = "REPLACE INTO t_template_log (sn_device, pin, status, last_update) " & vbNewLine & _
                                "VALUES (""" & SN & """, """ & vStrEnrollNumber & """, 1, """ & CreatedDate() & """) "
                        ExecuteSQLite_Data strSQL
                    End If
                End If
                DoEvents
                
                If Trim(vEnrollName) <> "" Then
                    'vnResultCode = FK_SetUserName_StringID(mnCommHandleIndex, vStrEnrollNumber, Trim(vEnrollName))
                    SetUserName vStrEnrollNumber, vEnrollName, mnCommHandleIndex
                End If
                DoEvents
                
                .MoveNext
            Loop
        Else
            Do While .EOF = False
                vEnrollName = Space(256)
                vStrEnrollNumber = .fields("pin").value
                vEnrollNumber = Val(vStrEnrollNumber)
                vEnrollName = .fields("name_employee").value
                vPrivilege = MP_NONE
                vBackupNumber = BACKUP_PSW
                
                ZeroMemory mbytEnrollData(0), UBound(mbytEnrollData) + 1
                If vBackupNumber = BACKUP_PSW Or vBackupNumber = BACKUP_CARD Then
                    CopyMemory mbytEnrollData(0), EnrollDataDefault(0), PWD_DATA_SIZE
                End If
                
                Call FK_IsSupportedEnrollData(mnCommHandleIndex, vBackupNumber, vnIsSupported)
                If vnIsSupported <> 0 Then
                    vnResultCode = FK_PutEnrollData(mnCommHandleIndex, vEnrollNumber, vBackupNumber, vPrivilege, _
                        mbytEnrollData(0), mlngPasswordData)
                                    
                    If vnResultCode <> RUN_SUCCESS Then
                        vStr = "SetAllEnrollData Error"
                    Else
                        strSQL = "DELETE FROM t_template_log WHERE sn_device = """ & SN & """ AND pin = """ & vStrEnrollNumber & """ "
                        ExecuteSQLite_Data strSQL
                    
                        strSQL = "REPLACE INTO t_template_log (sn_device, pin, status, last_update) " & vbNewLine & _
                                "VALUES (""" & SN & """, """ & vStrEnrollNumber & """, 1, """ & CreatedDate() & """) "
                        ExecuteSQLite_Data strSQL
                    End If
                End If
                DoEvents
                
                If Trim(vEnrollName) <> "" Then
                    'vnResultCode = FK_SetUserName(mnCommHandleIndex, vEnrollNumber, Trim(vEnrollName))
                    SetUserName CStr(vEnrollNumber), vEnrollName, mnCommHandleIndex
                End If
                DoEvents
                
                .MoveNext
            Loop

        End If
    End With
    mbGetState = False
    
    If vnResultCode = RUN_SUCCESS Then
        vnResultCode = FK_SaveEnrollData(mnCommHandleIndex)
        If vnResultCode = RUN_SUCCESS Then
            SetUserPIN = True
        End If
    End If

    FK_EnableDevice mnCommHandleIndex, 1
    
    Exit Function
        
BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Function

Public Function HexToByte(inputData As String) As Byte()
    On Error GoTo BugError
    
    Dim temp() As Byte
    Dim Temp2 As String
    Dim i, J, k As Long
    
    i = 1
    k = 0
    J = Len(inputData)
    ReDim temp(J / 2)
    
    Do While i < J
        Temp2 = Mid(inputData, i, 2)
        temp(k) = Val("&h" & Temp2)
        i = i + 2
        k = k + 1
    Loop
    HexToByte = temp
    
    Exit Function
        
BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Function

Public Function mergeArrays(ByRef arr1() As Byte, arr2() As Byte) As Byte()
    On Error GoTo BugError
    
    Dim returnThis() As Byte
    Dim len1 As Integer
    Dim len2 As Integer
    Dim lenRe As Integer
    Dim counter As Integer
    Dim i As Integer
    
    len1 = UBound(arr1)
    len2 = UBound(arr2)
    lenRe = len1 + len2
    ReDim returnThis(lenRe)
    counter = 0
    
    For i = 0 To len1 - 1
        returnThis(counter) = arr1(i)
        counter = counter + 1
    Next
    
    For i = 0 To len2 - 1
        returnThis(counter) = arr2(i)
        counter = counter + 1
    Next
    
    Dim strTmp As String
    
    'strTmp = GetAscii(returnThis(), 0, UBound(returnThis))
    'MsgBox strTmp
    
    mergeArrays = returnThis

    Exit Function
        
BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Function

Public Sub FKHS3760_EncryptPwd(apOrgPwdData() As Byte, ByRef apEncPwdData() As Byte, aPwdLen As Long)
    Dim i As Integer
  
    gPassEncryptKey = 12415
    For i = 0 To aPwdLen - 1
        apEncPwdData(i) = FKHS3760_Encrypt_1Byte(apOrgPwdData(i))
    Next
End Sub

Public Function FKHS3760_Encrypt_1Byte(aByteData As Byte) As Byte
    On Error GoTo BugError
    
    Const U0 As Long = 28904
    Const U1 As Long = 35756
    Dim vCrytData As Byte

    'vCrytData = CByte(aByteData ^ (rshft(8, LongToByteArray(gPassEncryptKey))))
    vCrytData = (aByteData Xor CByte(RShiftLong(gPassEncryptKey, 8)))
    gPassEncryptKey = (vCrytData + gPassEncryptKey) * U0 + U1
    
    FKHS3760_Encrypt_1Byte = vCrytData
    
    Exit Function
        
BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Function

Public Function LongToByteArray(ByVal lng As Double) As Byte()
    Dim ByteArray(0 To 3) As Byte
    
    CopyMemory ByteArray(0), ByVal VarPtr(lng), Len(lng)
    LongToByteArray = ByteArray
End Function

Public Function BytesToNumEx(ByteArray() As Byte, UnSigned As Boolean) As Double
    On Error GoTo BugError
    
    Dim i As Integer
    Dim lng256 As Double
    Dim lngReturn As Double
        
    lng256 = 1
    lngReturn = 0
    
    For i = 0 To UBound(ByteArray) - 1
        lng256 = lng256 * 256
        lngReturn = lngReturn + (ByteArray(i) * lng256)
    Next i
    
    BytesToNumEx = lngReturn
    
    Exit Function
        
BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Function

Public Function rshft(num As Long, shifts As Byte) As Long
    rshft = num \ (2 ^ shifts)
End Function

Public Function getPWD(pwd As String) As Byte()
    On Error GoTo BugError
    
    Dim bpwd() As Byte
    Dim head() As Byte
    Dim spwd() As Byte
    Dim temp() As Byte
    Dim mpwd() As Byte
    
    'PWD_HS1:  IDC_HS1:;
    ReDim bpwd(PWD_DATA_SIZE)
    'head = StrConv("PWD_HS1:", vbFromUnicode)
    ReDim head(Len("PWD_HS1:"))
    SetAscii head, 0, "PWD_HS1:"
    'spwd = StrConv(pwd, vbFromUnicode)
    ReDim spwd(Len(pwd))
    SetAscii spwd, 0, pwd
    ReDim temp(32 - UBound(spwd))
    ReDim mpwd(32)
    mpwd = mergeArrays(spwd, temp)
    ReDim temp(32)
    FKHS3760_EncryptPwd mpwd, temp, 32
    bpwd = mergeArrays(head, temp)
    
    getPWD = bpwd
    
    Exit Function
        
BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Function
        
Private Sub MakeOnBits()
    On Error GoTo BugError
    
    Dim J As Integer
    Dim v As Long
  
    For J = 0 To 30
        v = v + (2 ^ J)
        OnBits(J) = v
    Next J
  
    OnBits(J) = v + &H80000000
    
    Exit Sub
        
BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Sub

Public Function LShiftLong(ByVal value As Long, ByVal Shift As Integer) As Long
    MakeOnBits
  
    If (value And (2 ^ (31 - Shift))) Then GoTo OverFlow
    LShiftLong = ((value And OnBits(31 - Shift)) * (2 ^ Shift))
  
    Exit Function

OverFlow:
    LShiftLong = ((value And OnBits(31 - (Shift + 1))) * (2 ^ (Shift))) Or &H80000000
End Function

Public Function RShiftLong(ByVal value As Long, ByVal Shift As Integer) As Long
    On Error GoTo BugError
    
    Dim hi As Long
    MakeOnBits
    If (value And &H80000000) Then hi = &H40000000
  
    RShiftLong = (value And &H7FFFFFFE) \ (2 ^ Shift)
    RShiftLong = (RShiftLong Or (hi \ (2 ^ (Shift - 1))))
    
    Exit Function
        
BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Function

Public Function Encrypt_1Byte(aByteData As Byte) As Byte
    Const U0 As Long = 28904
    Const U1 As Long = 35756

    Dim vCrytData As Byte
    
    'vCrytData = aByteData xor (gPassEncryptKey >> 8)
    vCrytData = (aByteData Xor CByte(RShiftLong(gPassEncryptKey, 8)))
    gPassEncryptKey = (vCrytData + gPassEncryptKey) * U0 + U1
    Encrypt_1Byte = vCrytData
End Function

Public Function LongToByte(vIn As Long) As Byte
    On Error GoTo BugError
    
    Dim i As Long
    
    Dim vOut((LenB(vIn) - 1)) As Byte
    For i = 0 To (LenB(vIn) - 1)
        vOut(i) = (Int(vIn / (2 ^ (8 * (LenB(vIn) - 1 - i))))) And ((2 ^ 8) - 1)
    Next i
    
    LongToByte = vOut
    
    Exit Function

BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Function

Public Function ByteToLong(vIn() As Byte) As Long
    On Error GoTo BugError
    
    Dim i As Long
    Dim vOut As Long
    
    For i = 0 To (UBound(vIn) - LBound(vIn))
        If (i = 0 And ((vIn(0) And &H80) > 0)) Then 'negative value on first byte ?
            vOut = vOut Or -(CLng(vIn(i)) * (2 ^ (8 * (LenB(vOut) - 1 - i))))
        Else
            vOut = vOut Or (CLng(vIn(i)) * (2 ^ (8 * (LenB(vOut) - 1 - i))))
        End If
    Next i

    Exit Function

BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Function

Public Function Decrypt_1Byte(aByteData As Byte) As Byte
    On Error GoTo BugError

    Const U0 As Long = 28904
    Const U1 As Long = 35756

    Dim vCrytData As Byte

    'vCrytData = aByteData xor (gPassEncryptKey shr 8)
    vCrytData = (aByteData Xor CByte(RShiftLong(gPassEncryptKey, 8)))
    gPassEncryptKey = (aByteData + gPassEncryptKey) * U0 + U1
    gPassEncryptKey = 12415
    Decrypt_1Byte = vCrytData

    Exit Function

BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Function

Public Sub EncryptPwd(apOrgPwdData() As Byte, ByRef apEncPwdData() As Byte, aPwdLen As Long)
    Dim i As Integer
  
    gPassEncryptKey = 12415
    For i = 0 To aPwdLen - 1
        If (i > 7) And (i < 20) Then
            apEncPwdData(i) = Encrypt_1Byte(apOrgPwdData(i))
        End If
    Next
End Sub

Public Sub DecryptPwd(apEncPwdData() As Byte, ByRef apOrgPwdData() As Byte, aPwdLen As Integer)
    Dim i As Integer
  
    gPassEncryptKey = 12415
    For i = 0 To aPwdLen - 1
        If (i > 7) And (i < 17) Then
            apOrgPwdData(i) = Decrypt_1Byte(apEncPwdData(i))
        End If
    Next
End Sub

Public Function ShowByteArrayToString(aByteArray() As Byte, ByVal aLenToShow As Integer) As String
    On Error GoTo BugError
    
    Dim aByteArray_dec() As Byte
    Dim k As Long, n As Long, i As Long
    Dim strHex As String, strLine As String
    Dim str1 As String
    
    If aLenToShow > UBound(aByteArray) + 1 Then aLenToShow = UBound(aByteArray) + 1
    
    ReDim aByteArray_dec(PWD_DATA_SIZE)
    
    DecryptPwd aByteArray, aByteArray_dec, PWD_DATA_SIZE
    str1 = ""
    For k = 0 To aLenToShow - 1
        strHex = Hex(aByteArray_dec(k))
        'strHex = Hex(aByteArray(k))
        If Len(strHex) = 1 Then strHex = "0" & strHex
        str1 = str1 & strHex
    Next
    ShowByteArrayToString = str1
    
    Exit Function
        
BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Function

Public Function ByteArrayToHex(ByRef ByteArray() As Byte) As String
    Dim LB As Long, ub As Long
    Dim l As Long, strRet As String
    Dim lonRetLen As Long, lonPos As Long
    Dim strHex As String, lonLenHex As Long
    
    LB = LBound(ByteArray)
    ub = UBound(ByteArray)
    lonRetLen = ((ub - LB) + 1) * 3
    strRet = Space$(lonRetLen)
    lonPos = 1
    
    For l = LB To ub
        strHex = Hex$(ByteArray(l))
        If Len(strHex) = 1 Then strHex = "0" & strHex
        If l <> ub Then
            Mid$(strRet, lonPos, 3) = strHex & " "
            lonPos = lonPos + 3
        Else
            Mid$(strRet, lonPos, 3) = strHex
        End If
    Next l
    
    ByteArrayToHex = strRet
End Function

Public Function ByteArrayToString(bytArray() As Byte) As String
    Dim sAns As String
    Dim iPos As String
    
    sAns = StrConv(bytArray, vbUnicode)
    iPos = InStr(sAns, Chr(0))
    If iPos > 0 Then sAns = Left(sAns, iPos - 1)
    
    ByteArrayToString = sAns
 End Function

Public Function DateTimeToStr_DB(dt As Date, format_str As String) As String
    Dim s As String
    
    s = CStr(Format(dt, format_str))
    s = Replace(s, ".", ":")
    
    DateTimeToStr_DB = s
End Function

Public Function GetRandomKunciMesin() As String
    Dim i As Integer
    
    i = 1 + Int(Rnd() * (30 - Round(Second(Now) / 2)))
    GetRandomKunciMesin = Trim(CStr(i))
End Function

Public Function GetRandomKunciMesinNeo() As String
    Dim i As Long
    
    i = 1 + Int(Rnd() * (999999 - Round(Second(Now) / 2)))
    GetRandomKunciMesinNeo = Trim(CStr(i))
End Function

Public Function getAngka(inputan As String, NoAwal0 As Boolean) As String
    Dim i As Integer
    Dim s As String
    Dim hasil As String
    
    hasil = ""
    For i = 1 To Len(inputan)
        s = Mid(inputan, i, 1)
        
        If InStr(Angka, s) > 0 Then
            If NoAwal0 Then
                If hasil = "" Then
                    If s <> "0" Then
                        hasil = hasil & s
                    End If
                Else
                    hasil = hasil & s
                End If
            Else
                hasil = hasil & s
            End If
        End If
    Next
    getAngka = hasil
End Function

Public Function GetStringAngkaHuruf(valueStr As String) As String
    Dim str1 As String
    Dim str2 As String
    Dim i As Integer
    
    str2 = ""
    For i = 1 To Len(valueStr)
        str1 = Mid(valueStr, i, 1)
        
        If InStr(Angka, str1) > 0 Then
            str2 = str2 & str1
        Else
            If InStr(Huruf, str1) > 0 Then
                str2 = str2 & str1
            Else
                If str1 = "-" Then
                    str2 = str2 & str1
                End If
            End If
        End If
    Next
    
    GetStringAngkaHuruf = str2
End Function

Public Function GetValueField(sqlCek As String, FieldValue As String) As String
    Dim rs As cRecordset
    
    GetValueField = ""
    strSQL = sqlCek
    Set rs = getRecordSet_Data(strSQL, True)
    Do While Not rs.EOF
        GetValueField = CStr(rs.fields(FieldValue).value)
        rs.MoveNext
    Loop
End Function

'Public Function GetValueFieldLanguage(sqlCek As String, FieldValue As String) As String
'    Dim rs As cRecordset
'
'    GetValueFieldLanguage = ""
'    strSQL = sqlCek
'    Set rs = getLanguageRecordSet(strSQL, True)
'    Do While Not rs.EOF
'        GetValueFieldLanguage = CStr(rs.Fields(FieldValue).value)
'        rs.MoveNext
'    Loop
'End Function

Public Function LoadParam(id_param As String) As String
    On Error GoTo BugError
    
    Dim rs As cRecordset
    Dim value_param As String
    
    LoadParam = ""
    strSQL = "select value_param From t_param Where id_param = """ & id_param & """ "
    Set rs = getRecordSet_Data(strSQL, True)
    
    If rs.RecordCount = 0 Then
        Exit Function
    Else
        value_param = rs.fields("value_param").value
        LoadParam = value_param
    End If
    
    Exit Function
        
BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Function

Public Function SetParam(id_param As String, value_param As String) As Boolean
    On Error GoTo BugError
            
    SetParam = False
    
    strSQL = "REPLACE INTO t_param (id_param, value_param) " & _
        "VALUES (""" & id_param & """, """ & value_param & """) "
    'Clipboard.Clear
    'Clipboard.SetText strSQL
    ExecuteSQLite_Data strSQL
    
    SetParam = True
    
    Exit Function
        
BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Function

Public Sub TextToFile(Msg As String, FileName As String)
    FileName = App.Path & "\" & FileName
    
    Open FileName For Output As #1
    Print #1, Msg
    Close #1
End Sub

Public Function GetJmlRecord(sqlCek As String, fieldJml As String) As Long
    Dim rs As cRecordset
    
    GetJmlRecord = 0
    strSQL = sqlCek
    Set rs = getRecordSet_Data(strSQL, True)
    Do While Not rs.EOF
        GetJmlRecord = rs.fields(fieldJml).value
        rs.MoveNext
    Loop
End Function

Public Sub DownloadOnlineUser(sn_device As String)
    On Error GoTo BugError
    
    Dim rs As cRecordset
    Dim i, ii, iii, count, loop_value, limit, Start, jml_data, last_start As Integer
    Dim jml_centang_mesin, id_server, jml_hr, Total, yy, mm, dd As Integer
    Dim strl() As String
    Dim Token, date1_str, curr_time As String
    Dim kode, apikey, SN, server_name, str_value As String
    Dim last_dl, last_up_dl, upload_date As Date
    
    Dim comm_type As Integer
    Dim name_device, activation_code, cloud_id As String
    Dim jsonText As String
    Dim response As String
    Dim APP_ID As String
    
    APP_ID = "4"
    curr_time = DateTimeToStr_DB(Now, "yyyymmddhhnnss")
        
    strSQL = "SELECT d.*, dt.firmware FROM t_device AS d " & _
        "LEFT JOIN t_device_type AS dt ON dt.id_device_type = d.id_device_type " & _
        "WHERE d.comm_type = 4 AND d.sn_device = """ & sn_device & """ "
    Set rs = getRecordSet_Data(strSQL, True)
    Do While Not rs.EOF
        comm_type = rs.fields("comm_type").value
        name_device = rs.fields("name_device").value
        'activation_code = rs.fields("activation_code").value
        activation_code = rs.fields("active_code").value
        server_name = rs.fields("server_name").value
        cloud_id = rs.fields("cloud_id").value
        apikey = rs.fields("apikey").value
        
        Token = LCase(MD5(apikey & cloud_id & curr_time & "I3hidXJuZXI0MTIzNA=="))
        limit = 100
        Start = 0
        count = 0
        jml_data = 0
        
        jsonText = "{""apikey"":""?1"",""cloud_id"":""?2"",""curr_time"":""?3"",""token"":""?4"",""APP_ID"":""?5"",""start"":""?6"",""limit"":""?7""}"
        jsonText = Replace(jsonText, "?1", apikey)
        jsonText = Replace(jsonText, "?2", cloud_id)
        jsonText = Replace(jsonText, "?3", curr_time)
        jsonText = Replace(jsonText, "?4", Token)
        jsonText = Replace(jsonText, "?5", APP_ID)
        jsonText = Replace(jsonText, "?6", Start)
        jsonText = Replace(jsonText, "?7", limit)
        
        If NetConnectStatus(frmWizard.Inet1) = True Then
            response = SendRequestPostHTTP3(server_name & "api/desktop/user_info_by_sn", jsonText, "")
            If (response = "HTTP protocol error") Or (response = "") Then
                MsgBox INILanRead("frmDataUser", "msg4", ""), vbOKOnly, App.Title & " : "
                GoTo skip
            End If
            
            If response = "Error" Then
                'MsgBox INILanRead("errMsg", "msg14", "", url_api, "", ""), vbOKOnly, App.Title & " : " & "4"
            ElseIf response <> "" Then
                str_value = GetJSONByKey(response, "success")
                If Trim(str_value) = "true" Then
                    jml_data = GetJSONByKey(response, "total")
                    ReadJsonUser response
                Else
                    kode = GetJSONByKey(response, "code")
                    If kode = "2" Then
                        MsgBox INILanRead("frmDataUser", "msg3", ""), vbOKOnly, App.Title & " : "
                        GoTo skip
                    End If
                End If
            End If
            
            If jml_data > limit Then
                loop_value = jml_data \ limit
                For iii = 1 To loop_value
                    last_start = limit
                    limit = limit + limit
                    
                    jsonText = "{""apikey"":""?1"",""cloud_id"":""?2"",""curr_time"":""?3"",""token"":""?4"",""APP_ID"":""?5"",""start"":""?6"",""limit"":""?7""}"
                    jsonText = Replace(jsonText, "?1", apikey)
                    jsonText = Replace(jsonText, "?2", cloud_id)
                    jsonText = Replace(jsonText, "?3", curr_time)
                    jsonText = Replace(jsonText, "?4", Token)
                    jsonText = Replace(jsonText, "?5", APP_ID)
                    jsonText = Replace(jsonText, "?6", last_start)
                    jsonText = Replace(jsonText, "?7", limit)
                    
                    response = SendRequestPostHTTP3(server_name & "api/desktop/user_info_by_sn", jsonText, "")
                    If (response = "HTTP protocol error") Or (response = "") Then
                        MsgBox INILanRead("frmDataUser", "msg4", ""), vbOKOnly, App.Title & " : "
                        GoTo skip
                    End If
                    
                    If response = "Error" Then
                        'MsgBox INILanRead("errMsg", "msg14", "", url_api, "", ""), vbOKOnly, App.Title & " : " & "4"
                    ElseIf response <> "" Then
                        str_value = GetJSONByKey(response, "success")
                        If Trim(str_value) = "true" Then
                            ReadJsonUser response
                        Else
                            kode = GetJSONByKey(response, "code")
                            If kode = "2" Then
                                MsgBox INILanRead("frmDataUser", "msg3", ""), vbOKOnly, App.Title & " : "
                                GoTo skip
                            End If
                        End If
                    End If
                    DoEvents
                Next
            End If
        End If
skip:
        
        DoEvents
        rs.MoveNext
    Loop
    
    Exit Sub
        
BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Sub

Public Sub ReadJsonUser(strJsonArray As String)
    Dim SQL1 As String
    Dim SQL2 As String
    Dim tgl_mulai_kerja As String
    
    Dim success As Long
    Dim json_value As String
    Dim jsonArray As New ChilkatJsonArray
    
    Dim pin, nama, rfid, pid, lid, Gid, Position, Location, Group, password, Privilege As String
    Dim Template, nik, FullName, Phone, Birthday, HiredDate As String
        
    json_value = GetJSONByKey(strJsonArray, "data")
    success = jsonArray.Load(json_value)
    
    Dim i As Long
    For i = 0 To jsonArray.SIZE - 1
        Dim jsonObj As ChilkatJsonObject
        Set jsonObj = jsonArray.ObjectAt(i)
        
        pin = jsonObj.StringOf("PIN")
        nama = jsonObj.StringOf("Name")
        rfid = jsonObj.StringOf("RFID")
        pid = jsonObj.StringOf("PID")
        lid = jsonObj.StringOf("LID")
        Gid = jsonObj.StringOf("GID")
        Position = jsonObj.StringOf("Position")
        Location = jsonObj.StringOf("Location")
        Group = jsonObj.StringOf("Group")
        password = jsonObj.StringOf("Password")
        Privilege = jsonObj.StringOf("Privilege")
        Template = jsonObj.StringOf("Template")
        nik = jsonObj.StringOf("NIK")
        FullName = jsonObj.StringOf("FullName")
        Phone = jsonObj.StringOf("Phone")
        Birthday = jsonObj.StringOf("Birthday")
        HiredDate = jsonObj.StringOf("HiredDate")
        
        'Insert Data User (Karyawan)
        'strSQL = "INSERT INTO t_employee (pin, name_employee, id_structure, parentpath_structure, id_position, id_office, id_schedule, active) " & _
        '    "VALUES (""" & pin & """, """ & nama & """, 1, '/1/2/', 1, 1, 1, active, update_by, update_date) "
        
        tgl_mulai_kerja = Replace(Format(Now, DateFormatDatabase), ".", ":")
        
        SQL1 = "INSERT INTO t_employee (name_employee, id_structure, parentpath_structure, id_position, id_office, id_schedule, " & vbNewLine & _
            "parentpath_device, pin, nik, txt_pass, rfid, alias, gender, status_employee, start_date, cutoff_date, note_contract, date_dayoff, " & vbNewLine & _
            "remain_paidoff, foto, nominal_leave, working_month, working_year, bpjs_kesehatan, bpjs_ketenagakerjaan, active, " & vbNewLine & _
            "created_by, created_date, update_by, update_date) " & vbNewLine
        SQL2 = "VALUES ('" & nama & "', 1, '/1/2/', 1, 1, 1, 0, '" & pin & "', '" & pin & "', '', " & vbNewLine & _
            "'', '" & Mid(nama, 1, 15) & "', 0, 1, '" & tgl_mulai_kerja & "', '9999-12-31', '-', '0', " & vbNewLine & _
            "0, '0', 0, 0, 0, 1, 1, 1, 1, '" & getCurrentDT & "', 1, '" & getCurrentDT & "') "
        strSQL = SQL1 + SQL2
        ExecuteSQLite_Data strSQL
        
        'SQL1 = "INSERT INTO t_employee_detail (id_employee, date_birth, num_bankaccount) " & vbNewLine
        'SQL2 = "VALUES (" & id_employee & ", '" & tgl_lahir & "', '" & no_rek & "') "
    Next i
End Sub

Public Sub ReadJSONAttlog(sn_device As String, strJsonArray As String)
    Dim success As Long
    Dim json_value As String
    Dim jsonArray As New ChilkatJsonArray
    
    Dim pin, nama, VerifyMode, IoMode, work_code, scan_date_str, dateStr As String
    Dim scan_date As Date
    Dim yy, mm, dd, hh, nn, ss As Integer
    
    json_value = GetJSONByKey(strJsonArray, "data")
    success = jsonArray.Load(json_value)
    
    Dim i As Long
    For i = 0 To jsonArray.SIZE - 1
        Dim jsonObj As ChilkatJsonObject
        Set jsonObj = jsonArray.ObjectAt(i)
        
        pin = jsonObj.StringOf("PIN")
        nama = jsonObj.StringOf("Name")
        
        VerifyMode = jsonObj.StringOf("Verify Type")
        If sn_device = "0" Then VerifyMode = "90"
        IoMode = jsonObj.StringOf("Log Type")
        work_code = jsonObj.StringOf("Work Code")
        scan_date_str = jsonObj.StringOf("Date Time")
        
        If Trim(scan_date_str) <> "" Then
            yy = Val(Mid(scan_date_str, 1, 4))
            mm = Val(Mid(scan_date_str, 6, 2))
            dd = Val(Mid(scan_date_str, 9, 2))
            hh = Val(Mid(scan_date_str, 12, 2))
            nn = Val(Mid(scan_date_str, 15, 2))
            ss = Val(Mid(scan_date_str, 18, 2))
            scan_date = DateSerial(yy, mm, dd) & " " & TimeSerial(hh, nn, ss)
        End If
        dateStr = DateTimeToStr_DB(scan_date, DateTimeFormatDatabase)
        
        'Insert Data Attlog
        strSQL = "REPLACE INTO t_att_log (scan_date, pin, sn, verify_mode, inout_mode, inout_mode2) " & _
            "VALUES ('" & dateStr & "', '" & pin & "', '" & sn_device & "', " & VerifyMode & ", " & IoMode & ", 0) "
        ExecuteSQLite_Data strSQL
    Next i
End Sub

Public Sub DownloadOnlineAttlog(sn_device As String)
    On Error GoTo BugError
    
    Dim rs As cRecordset
    Dim i, ii, iii, count, loop_value, limit, Start, jml_data, last_start As Integer
    Dim jml_centang_mesin, id_server, jml_hr, Total, yy, mm, dd As Integer
    Dim strl() As String
    Dim Token As String
    Dim date1_str, curr_time As String
    Dim kode, SN, str_value As String
    Dim last_dl As Date
    Dim last_up_dl As Date
    Dim upload_date As Date
    
    Dim tgl_upload_terakhir As Date
    Dim str2 As String
    
    Dim comm_type As Integer
    Dim name_device, activation_code, cloud_id As String
    Dim server_name As String
    Dim apikey As String
    Dim jsonText As String
    Dim response As String
    Dim APP_ID As String
    
    APP_ID = "4"
    curr_time = DateTimeToStr_DB(Now, "yyyymmddhhnnss")
    
    strSQL = "SELECT d.*, dt.firmware FROM t_device AS d " & _
        "LEFT JOIN t_device_type AS dt ON dt.id_device_type = d.id_device_type " & _
        "WHERE d.comm_type = 4 AND d.sn_device = """ & sn_device & """ "
    Set rs = getRecordSet_Data(strSQL, True)
    Do While Not rs.EOF
        comm_type = rs.fields("comm_type").value
        name_device = rs.fields("name_device").value
        activation_code = Trim(rs.fields("active_code").value)
        server_name = Trim(rs.fields("server_name").value)
        cloud_id = Trim(rs.fields("cloud_id").value)
        apikey = Trim(rs.fields("apikey").value)
        
        str2 = Mid(server_name, Len(server_name), 1)
        If str2 <> "/" Then server_name = server_name & "/"
        
        If NetConnectStatus(frmWizard.Inet1) = False Then GoTo skip
            
        tgl_upload_terakhir = GetTglTerakhir(server_name, cloud_id, apikey)
        tgl_upload_terakhir = DateAdd("d", 1, tgl_upload_terakhir)
        last_dl = GetLastDownload()
        GetTglUpload cloud_id, server_name, apikey, last_dl, tgl_upload_terakhir
        
        jml_hr = UBound(list_tgl_upload)
        If UBound(list_tgl_upload) = 0 Then GoTo skip
        Total = 0
        
        For i = 0 To jml_hr - 1
            yy = Val(Mid(list_tgl_upload(i), 1, 4))
            mm = Val(Mid(list_tgl_upload(i), 6, 2))
            dd = Val(Mid(list_tgl_upload(i), 9, 2))
            upload_date = DateSerial(yy, mm, dd) & " " & TimeSerial(0, 0, 0)
            
            date1_str = DateTimeToStr_DB(upload_date, DateFormatDatabase)
            If date1_str <> "1999-11-30" Then
                Token = LCase(MD5(cloud_id & date1_str & curr_time & apikey))
                limit = 100
                Start = 0
                count = 0
                jml_data = 0
                last_start = Start
                
                jsonText = "{""apikey"":""?1"",""cloud_id"":""?2"",""date_get"":""?3"",""curr_time"":""?4"",""token"":""?5"",""APP_ID"":""?6"",""start"":""?7"",""limit"":""?8""}"
                jsonText = Replace(jsonText, "?1", apikey)
                jsonText = Replace(jsonText, "?2", cloud_id)
                jsonText = Replace(jsonText, "?3", date1_str)
                jsonText = Replace(jsonText, "?4", curr_time)
                jsonText = Replace(jsonText, "?5", Token)
                jsonText = Replace(jsonText, "?6", APP_ID)
                jsonText = Replace(jsonText, "?7", Start)
                jsonText = Replace(jsonText, "?8", limit)
                
                response = SendRequestPostHTTP3(server_name & "api/desktop/attendance_log", jsonText, "")
                If (response = "HTTP protocol error") Or (response = "") Then
                    MsgBox INILanRead("frmDataUser", "msg4", "..."), vbOKOnly, App.Title & " : "
                    Exit For
                End If
                
                str_value = GetJSONByKey(response, "success")
                If Trim(str_value) = "true" Then
                    jml_data = GetJSONByKey(response, "total")
                    If jml_data > 0 Then
                        Total = Total + jml_data
                        ReadJSONAttlog sn_device, response
                    End If
                Else
                    kode = GetJSONByKey(response, "code")
                    If kode = "2" Then
                        MsgBox INILanRead("frmDataUser", "msg3", "..."), vbOKOnly, App.Title & " : "
                        GoTo skip
                    End If
                End If
                
                If jml_data > limit Then
                    loop_value = jml_data \ limit
                    For iii = 1 To loop_value
                        last_start = last_start + limit
                        jml_data = 0
                        
                        jsonText = "{""apikey"":""?1"",""cloud_id"":""?2"",""date_get"":""?3"",""curr_time"":""?4"",""token"":""?5"",""APP_ID"":""?6"",""start"":""?7"",""limit"":""?8""}"
                        jsonText = Replace(jsonText, "?1", apikey)
                        jsonText = Replace(jsonText, "?2", cloud_id)
                        jsonText = Replace(jsonText, "?3", date1_str)
                        jsonText = Replace(jsonText, "?4", curr_time)
                        jsonText = Replace(jsonText, "?5", Token)
                        jsonText = Replace(jsonText, "?6", APP_ID)
                        jsonText = Replace(jsonText, "?7", last_start)
                        jsonText = Replace(jsonText, "?8", limit)
                        
                        response = SendRequestPostHTTP3(server_name & "api/desktop/attendance_log", jsonText, "")
                        If (response = "HTTP protocol error") Or (response = "") Then
                            MsgBox INILanRead("frmDataUser", "msg4", "..."), vbOKOnly, App.Title & " : "
                            GoTo skip
                        End If
                        
                        str_value = GetJSONByKey(response, "success")
                        If Trim(str_value) = "true" Then
                            jml_data = GetJSONByKey(response, "total")
                            If jml_data > 0 Then
                                Total = Total + jml_data
                                ReadJSONAttlog sn_device, response
                            End If
                        Else
                            kode = GetJSONByKey(response, "code")
                            If kode = "2" Then
                                MsgBox INILanRead("frmDataUser", "msg3", "..."), vbOKOnly, App.Title & " : "
                                GoTo skip
                            End If
                        End If
                        DoEvents
                    Next
                End If
                
                str2 = DateTimeToStr_DB(upload_date, DateFormatDatabase)
                strSQL = "REPLACE INTO t_param (id_param, value_param) VALUES ('last_dl_scan_gps_date', """ & str2 & """) "
                ExecuteSQLite_Data strSQL
            End If
        Next i
skip:
        
        DoEvents
        rs.MoveNext
    Loop
    
    Exit Sub
        
BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Sub

Public Sub DownloadOnlineAttlogGPS(sn_device As String)
    On Error GoTo BugError
    
    Dim rs As cRecordset
    Dim i, ii, iii, count, loop_value, limit, Start, jml_data, last_start As Integer
    Dim jml_centang_mesin, id_server, jml_hr, Total, yy, mm, dd As Integer
    Dim strl() As String
    Dim Token As String
    Dim date1_str As String
    Dim curr_time As String
    Dim kode, SN, str_value As String
    Dim last_dl As Date
    Dim last_up_dl As Date
    Dim upload_date As Date
    
    Dim tgl_upload_terakhir As Date
    Dim str2 As String
    
    Dim comm_type As Integer
    Dim name_device, activation_code, cloud_id As String
    Dim server_name As String
    Dim apikey As String
    Dim jsonText As String
    Dim response As String
    Dim APP_ID As String
    
    APP_ID = "4"
    curr_time = DateTimeToStr_DB(Now, "yyyymmddhhnnss")
    
    strSQL = "SELECT d.*, dt.firmware FROM t_device AS d " & _
        "LEFT JOIN t_device_type AS dt ON dt.id_device_type = d.id_device_type " & _
        "WHERE d.comm_type = 4 AND d.sn_device = """ & sn_device & """ "
    Set rs = getRecordSet_Data(strSQL, True)
    Do While Not rs.EOF
        comm_type = rs.fields("comm_type").value
        name_device = rs.fields("name_device").value
        activation_code = Trim(rs.fields("active_code").value)
        server_name = Trim(rs.fields("server_name").value)
        cloud_id = "0"
        apikey = Trim(rs.fields("apikey").value)
        
        str2 = Mid(server_name, Len(server_name), 1)
        If str2 <> "/" Then server_name = server_name & "/"
        
        If NetConnectStatus(frmWizard.Inet1) = False Then GoTo skip
            
        tgl_upload_terakhir = GetTglTerakhir(server_name, cloud_id, apikey)
        tgl_upload_terakhir = DateAdd("d", 1, tgl_upload_terakhir)
        last_dl = GetLastDownload()
        GetTglUpload cloud_id, server_name, apikey, last_dl, tgl_upload_terakhir
        
        jml_hr = UBound(list_tgl_upload)
        If UBound(list_tgl_upload) = 0 Then GoTo skip
        Total = 0
        
        For i = 0 To jml_hr - 1
            yy = Val(Mid(list_tgl_upload(i), 1, 4))
            mm = Val(Mid(list_tgl_upload(i), 6, 2))
            dd = Val(Mid(list_tgl_upload(i), 9, 2))
            upload_date = DateSerial(yy, mm, dd) & " " & TimeSerial(0, 0, 0)
            
            date1_str = DateTimeToStr_DB(upload_date, DateFormatDatabase)
            If date1_str <> "1999-11-30" Then
                Token = LCase(MD5(cloud_id & date1_str & curr_time & apikey))
                limit = 100
                Start = 0
                count = 0
                jml_data = 0
                last_start = Start
                
                jsonText = "{""apikey"":""?1"",""cloud_id"":""?2"",""date_get"":""?3"",""curr_time"":""?4"",""token"":""?5"",""APP_ID"":""?6"",""start"":""?7"",""limit"":""?8""}"
                jsonText = Replace(jsonText, "?1", apikey)
                jsonText = Replace(jsonText, "?2", cloud_id)
                jsonText = Replace(jsonText, "?3", date1_str)
                jsonText = Replace(jsonText, "?4", curr_time)
                jsonText = Replace(jsonText, "?5", Token)
                jsonText = Replace(jsonText, "?6", APP_ID)
                jsonText = Replace(jsonText, "?7", Start)
                jsonText = Replace(jsonText, "?8", limit)
                
                response = SendRequestPostHTTP3(server_name & "api/desktop/attendance_log", jsonText, date1_str)
                If (response = "HTTP protocol error") Or (response = "") Then
                    MsgBox INILanRead("frmDataUser", "msg4", ""), vbOKOnly, App.Title & " : "
                    Exit For
                End If
                
                str_value = GetJSONByKey(response, "success")
                If Trim(str_value) = "true" Then
                    jml_data = GetJSONByKey(response, "total")
                    If jml_data > 0 Then
                        Total = Total + jml_data
                        ReadJSONAttlog "0", response
                    End If
                Else
                    kode = GetJSONByKey(response, "code")
                    If kode = "2" Then
                        MsgBox INILanRead("frmDataUser", "msg3", ""), vbOKOnly, App.Title & " : "
                        GoTo skip
                    End If
                End If
                
                If jml_data > limit Then
                    loop_value = jml_data \ limit
                    For iii = 1 To loop_value
                        last_start = last_start + limit
                        jml_data = 0
                        
                        jsonText = "{""apikey"":""?1"",""cloud_id"":""?2"",""date_get"":""?3"",""curr_time"":""?4"",""token"":""?5"",""APP_ID"":""?6"",""start"":""?7"",""limit"":""?8""}"
                        jsonText = Replace(jsonText, "?1", apikey)
                        jsonText = Replace(jsonText, "?2", cloud_id)
                        jsonText = Replace(jsonText, "?3", date1_str)
                        jsonText = Replace(jsonText, "?4", curr_time)
                        jsonText = Replace(jsonText, "?5", Token)
                        jsonText = Replace(jsonText, "?6", APP_ID)
                        jsonText = Replace(jsonText, "?7", last_start)
                        jsonText = Replace(jsonText, "?8", limit)
                        
                        response = SendRequestPostHTTP3(server_name & "api/desktop/attendance_log", jsonText, date1_str & "_" & Trim(CStr(iii)))
                        If (response = "HTTP protocol error") Or (response = "") Then
                            MsgBox INILanRead("frmDataUser", "msg4", ""), vbOKOnly, App.Title & " : "
                            GoTo skip
                        End If
                        
                        str_value = GetJSONByKey(response, "success")
                        If Trim(str_value) = "true" Then
                            jml_data = GetJSONByKey(response, "total")
                            If jml_data > 0 Then
                                Total = Total + jml_data
                                ReadJSONAttlog "0", response
                            End If
                        Else
                            kode = GetJSONByKey(response, "code")
                            If kode = "2" Then
                                MsgBox INILanRead("frmDataUser", "msg3", ""), vbOKOnly, App.Title & " : "
                                GoTo skip
                            End If
                        End If
                        DoEvents
                    Next
                End If
                
                str2 = DateTimeToStr_DB(upload_date, DateFormatDatabase)
                strSQL = "REPLACE INTO t_param (id_param, value_param) VALUES ('last_dl_scan_gps_date', """ & str2 & """) "
                ExecuteSQLite_Data strSQL
            End If
        Next i
skip:
        
        DoEvents
        rs.MoveNext
    Loop
    
    Exit Sub
        
BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Sub

Public Function GetTglTerakhir(server_name As String, cloud_id As String, apikey As String) As Date
    On Error GoTo BugError
            
    Dim response As String
    Dim str_value As String
    Dim Token As String
    Dim jsonText As String
    Dim kode As String
    Dim tgl As String
    Dim yy, mm, dd, hh, nn, ss As Integer
    Dim Data As String
    Dim tgl_akhir As Date
    
    GetTglTerakhir = DateAdd("d", -30, Now)
    
    If NetConnectStatus(frmWizard.Inet1) = True Then
        Token = LCase(MD5(cloud_id + apikey))
        jsonText = "{""apikey"":""?1"",""cloud_id"":""?2"",""token"":""?3""}"
        jsonText = Replace(jsonText, "?1", apikey)
        jsonText = Replace(jsonText, "?2", cloud_id)
        jsonText = Replace(jsonText, "?3", Token)
        
        response = SendRequestPostHTTP3(server_name & "api/desktop/device_last_data", jsonText, "GetTglTerakhir")
        If (response = "HTTP protocol error") Or (response = "") Then
            MsgBox INILanRead("frmDataUser", "msg4", ""), vbOKOnly, App.Title & " : "
            Exit Function
        End If
        
        str_value = GetJSONByKey(response, "success")
        If Trim(str_value) = "true" Then
            'data = GetJSONByKey(response, "data")
            tgl = GetJSONByKey(response, "data", "app_date_time")
            If tgl = "" Then
                tgl = GetJSONByKey(response, "data", "date_time")
            End If
            
            If tgl = "" Then
                tgl = GetJSONByKey(response, "data", "created_at")
            End If
              
            If Trim(tgl) <> "" Then
              yy = Val(Mid(tgl, 1, 4))
              mm = Val(Mid(tgl, 6, 2))
              dd = Val(Mid(tgl, 9, 2))
              hh = Val(Mid(tgl, 12, 2))
              nn = Val(Mid(tgl, 15, 2))
              ss = Val(Mid(tgl, 18, 2))
              tgl_akhir = DateSerial(yy, mm, dd) & " " & TimeSerial(0, 0, 0)
              GetTglTerakhir = DateAdd("d", 1, tgl_akhir)
            End If
        Else
            kode = GetJSONByKey(response, "code")
            If kode = "2" Then
                MsgBox INILanRead("frmDataUser", "msg3", ""), vbOKOnly, App.Title & " : "
                GoTo skip
            End If
        End If
    End If
    
skip:
    
    Exit Function
        
BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Function

Public Function GetLastDownload() As Date
    On Error GoTo BugError
    
    Dim terakhir_dl As Date
    
    GetLastDownload = DateAdd("d", -90, Now)
    
    Dim str1 As String
    Dim tgl As Date
    Dim yy, mm, dd As Integer
    
    str1 = getParamater("last_dl_scan_gps_date")
    If Trim(str1) <> "" Then
        yy = Val(Mid(str1, 1, 4))
        mm = Val(Mid(str1, 6, 2))
        dd = Val(Mid(str1, 9, 2))
      
        tgl = DateSerial(yy, mm, dd) & " " & TimeSerial(0, 0, 0)
        terakhir_dl = terakhir_dl = DateAdd("d", -1, tgl)
    End If
    
    Exit Function
        
BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Function

Public Sub GetTglUpload(sn_device As String, server_name As String, apikey As String, tgl1 As Date, tgl2 As Date)
    On Error GoTo BugError
            
    Dim success As Long
    Dim jsonArray As New ChilkatJsonArray
    
    Dim json_value As String
    Dim str1, str2 As String
    Dim yy, mm, dd, hh, nn, ss As Integer
    Dim jsonText As String
    Dim response As String
    Dim str_value As String
    Dim kode As String
    
    Dim i As Long
    Dim jsonObj As ChilkatJsonObject
                
    str1 = DateTimeToStr_DB(tgl1, "yyyy-mm-dd")
    str2 = DateTimeToStr_DB(tgl2, "yyyy-mm-dd")
    
    If NetConnectStatus(frmWizard.Inet1) = True Then
        jsonText = "{""apikey"":""?1"",""sn_device"":""?2"",""tgl1"":""?3"",""tgl2"":""?4""}"
        jsonText = Replace(jsonText, "?1", apikey)
        jsonText = Replace(jsonText, "?2", sn_device)
        jsonText = Replace(jsonText, "?3", str1)
        jsonText = Replace(jsonText, "?4", str2)
        
        response = SendRequestPostHTTP3(server_name & "api/desktop/date_list", jsonText, "GetTglUpload")
        If (response = "HTTP protocol error") Or (response = "") Then
            MsgBox INILanRead("frmDataUser", "msg4", ""), vbOKOnly, App.Title & " : "
            Exit Sub
        End If
        
        str_value = GetJSONByKey(response, "success")
        If Trim(str_value) = "true" Then
            json_value = GetJSONByKey(response, "data")
            success = jsonArray.Load(json_value)
            
            ReDim list_tgl_upload(jsonArray.SIZE)
            For i = 0 To jsonArray.SIZE - 1
                Set jsonObj = jsonArray.ObjectAt(i)
                'MsgBox jsonObj.StringOf("date")
                
                list_tgl_upload(i) = jsonObj.StringOf("date")
            Next i
        Else
            kode = GetJSONByKey(response, "code")
            If kode = "2" Then
                MsgBox INILanRead("frmDataUser", "msg3", ""), vbOKOnly, App.Title & " : "
                GoTo skip
            End If
        End If
    End If
    
skip:
    
    Exit Sub
        
BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Sub

Public Function GetJSONByKey(input_data As String, key_str As String, Optional subkey_str As String = "") As String
    On Error GoTo BugError
            
    Dim JSON As New ChilkatJsonObject
    Dim jsonObj As ChilkatJsonObject
    Dim success As Long
    
    GetJSONByKey = ""
    success = JSON.Load(input_data)
    GetJSONByKey = JSON.StringOf(key_str)
    
    If GetJSONByKey = "" And subkey_str <> "" Then
        Set jsonObj = JSON.ObjectOf(key_str)
        
        GetJSONByKey = jsonObj.StringOf(subkey_str)
    End If
    
    Exit Function
        
BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Function

Public Function StringJsonForURL(jsonStr As String) As String
    On Error GoTo BugError
    
    Dim JSON As New ChilkatJsonObject

    Dim success As Long
    success = JSON.Load(jsonStr)
    If (success <> 1) Then
        Debug.Print JSON.LastErrorText
        Exit Function
    End If
    
    Dim numMembers As Long
    Dim i As Long
    
    Dim name_str As String
    Dim value_str As String
    Dim iValue As Long
    Dim paramGet As String
    
    paramGet = ""
    numMembers = JSON.SIZE
    For i = 0 To numMembers - 1
        name_str = JSON.NameAt(i)
        value_str = JSON.StringAt(i)
        'Debug.Print name_str & ": " & value_str
    
        iValue = JSON.IntAt(i)
        'Debug.Print name_str & " as integer: " & iValue
        
        If paramGet = "" Then
            paramGet = "/" & value_str
        Else
            paramGet = paramGet & "/" & value_str
        End If
    Next
    StringJsonForURL = paramGet
    
    Exit Function
        
BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Function

Public Function SendRequestPostHTTP3(url As String, jsonStr As String, no_urut As String) As String
    On Error GoTo BugError
    
    Dim http As New ChilkatHttp
    
    Dim success As Long
    
    SendRequestPostHTTP3 = ""
    success = http.UnlockComponent("FNGSPT.CB4042019_acprLRCam76f")
    If (success <> 1) Then
        Debug.Print http.LastErrorText
        Exit Function
    End If
    
    Dim str1 As String
    Dim html As String
    
    str1 = StringJsonForURL(jsonStr)
    str1 = url & str1
    html = http.QuickGetStr(str1)
    SendRequestPostHTTP3 = html
    'TextToFile str1 & vbNewLine & vbNewLine & html, "SendRequestHTTP_" & no_urut & ".txt"
    
    Exit Function
        
BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Function

Public Function SendRequestPostHTTP(url_str As String, jsonStr As String) As String
    On Error GoTo BugError
            
    Dim req As New ChilkatHttpRequest
    Dim http As New ChilkatHttp
    
    Dim success As Long
    
    SendRequestPostHTTP = ""
    success = http.UnlockComponent("FNGSPT.CB4042019_acprLRCam76f")
    If (success <> 1) Then
        Debug.Print http.LastErrorText
        Exit Function
    End If
    
    http.AcceptCharset = ""
    http.UserAgent = ""
    http.AcceptLanguage = ""
    http.AllowGzip = 0
    
    'success = http.AddQuickHeader("Cookie", "JSESSIONID=1234")
    
    Dim jsonText As String
    'jsonText = EncodeJSON(jsonStr)
    jsonText = jsonStr
    
    Dim resp As ChilkatHttpResponse
    Set resp = http.PostJson(url_str, jsonText)
    If (resp Is Nothing) Then
        Debug.Print http.LastErrorText & vbCrLf
    Else
        'TextToFile url_str & vbNewLine & vbNewLine & jsonText & vbNewLine & vbNewLine & resp.BodyStr, "SendRequestHTTP.txt"
        SendRequestPostHTTP = resp.BodyStr & vbCrLf
    End If
    
    Exit Function
        
BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Function

Public Function SendRequestPostHTTP2(url_str As String, jsonStr As String) As String
    On Error GoTo BugError
            
    Dim req As New ChilkatHttpRequest
    Dim http As New ChilkatHttp
    
    Dim success As Long
    
    SendRequestPostHTTP2 = ""
    success = http.UnlockComponent("FNGSPT.CB4042019_acprLRCam76f")
    If (success <> 1) Then
        Debug.Print http.LastErrorText
        Exit Function
    End If
    
    http.SetRequestHeader "Authorization", "my-custom-computed-auth-value"
    http.SetRequestHeader "X-Pass-Timestamp", "my-custom-computed-timestamp-value"
    http.SetRequestHeader "Accept", "application/json"
    
    http.AcceptCharset = ""
    http.UserAgent = ""
    http.AcceptLanguage = ""
    http.AllowGzip = 0
    
    'success = http.AddQuickHeader("Cookie", "JSESSIONID=1234")
    
    Dim jsonText As String
    'jsonText = EncodeJSON(jsonStr)
    jsonText = jsonStr
    
    Dim resp As ChilkatHttpResponse
    'Set resp = http.PostJson(url_str, jsonText)
    Set resp = http.PostJson2(url_str, "application/json", jsonText)
    If (resp Is Nothing) Then
        Debug.Print http.LastErrorText & vbCrLf
    Else
        'TextToFile url_str & vbNewLine & vbNewLine & jsonText & vbNewLine & vbNewLine & resp.BodyStr, "SendRequestHTTP.txt"
        SendRequestPostHTTP2 = resp.BodyStr & vbCrLf
    End If
    
    Exit Function
        
BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Function

Public Function SendRequestGetHTTP(url_str As String, jsonStr As String) As String
    On Error GoTo BugError
    
    Dim req As New ChilkatHttpRequest
    Dim http As New ChilkatHttp
    
    Dim success As Long
    
    SendRequestGetHTTP = ""
    success = http.UnlockComponent("FNGSPT.CB4042019_acprLRCam76f")
    If (success <> 1) Then
        Debug.Print http.LastErrorText
        Exit Function
    End If
    
    http.AcceptCharset = ""
    http.UserAgent = ""
    http.AcceptLanguage = ""
    
    http.AllowGzip = 0
    
    'success = http.AddQuickHeader("Cookie", "JSESSIONID=1234")
    'success = http.AddQuickHeader("Content-Encoding", "identity")
    
    ' Modify the default "Accept" header:
    http.Accept = "application/jsonrequest"
    
    Dim jsonText As String
    jsonText = EncodeJSON(jsonStr)
    
    Dim responseBody As String
    responseBody = http.PutText(url_str, jsonStr, "utf-8", "application/jsonrequest", 0, 0)
    If (http.LastMethodSuccess <> 1) Then
        Debug.Print http.LastErrorText
    Else
        'Debug.Print responseBody
        'TextToFile url_str & vbNewLine & vbNewLine & jsonText & vbNewLine & vbNewLine & responseBody, "SendRequestGetHTTP.txt"
        SendRequestGetHTTP = responseBody
    End If
    
    Exit Function
        
BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Function

Public Function SendRequestGetHTTP2(url_str As String, jsonStr As String) As String
    On Error GoTo BugError
    
    Dim http As New ChilkatHttp
    Dim success As Long
    
    SendRequestGetHTTP2 = ""
    success = http.UnlockComponent("FNGSPT.CB4042019_acprLRCam76f")
    If (success <> 1) Then
        Debug.Print http.LastErrorText
        Exit Function
    End If
    
    Dim jsonText As String
    'jsonText = EncodeJSON(jsonStr)
    jsonText = StringJsonForURL(jsonStr)
    
    ' Send the HTTP GET and return the content in a string.
    Dim html As String
    html = http.QuickGetStr(url_str & jsonText)
    SendRequestGetHTTP2 = html
    'TextToFile url_str & jsonText, "SendRequestHTTP.txt"
    
    Exit Function
        
BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Function

Public Function NetConnectStatus(Inet1 As Inet) As Boolean
    On Error GoTo Offline

    Dim strTemp As String
    Dim strIP() As String
    
    'Tries to access dfbgaming.com but will get an error if no internet.
    strTemp = Inet1.OpenURL("http://www.google.com/")
    strIP = Split(strTemp, vbCrLf)
    
    'MsgBox strTemp
    If Trim(strTemp) <> "" Then
        'ONLINE
        NetConnectStatus = True
    Else
        'Local Network Connection
        NetConnectStatus = False
    End If
    
    Exit Function

Offline:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    
    'No Internet Connection
    NetConnectStatus = False
    
    'strIP = Winsock1.LocalIP
    'If strIP = "127.0.0.1" Then
        'OFFLINE
    '    NetConnectStatus = False
    'Else
        'Local Network Connection
    '    NetConnectStatus = False
    'End If
    
    Resume Next
End Function

'Public Function EncodeJSON(inputData As String) As String
'    On Error GoTo BugError
'
'    Dim json64, str1, str2, str As String
'
'    EncodeJSON = ""
'    If Len(inputData) >= 4 Then
'        json64 = EncodeBase64(inputData)
'        str1 = Mid(json64, 1, 4)
'        str2 = Mid(json64, Len(json64) - 3, 4)
'        str = str2 & Mid(json64, 5, Len(json64) - 8) & str1
'        'str = Replace(str, vbNewLine, "")
'        EncodeJSON = "FIN" & str & "SPOT"
'    End If
'
'    Exit Function
'
'BugError:
'    'MsgBox Err.Description, , Err.Number
'    Err.Clear
'    Resume Next
'End Function
'
'Public Function DecodeJSON(inputData As String) As String
'    On Error GoTo BugError
'
'    Dim json, jsonDecode, str1, str2, str As String
'
'    DecodeJSON = ""
'    If Len(inputData) >= 8 Then
'        json = Mid(inputData, 4, Len(inputData) - 7)
'        str1 = Mid(json, 1, 4)
'        str2 = Mid(json, Len(json) - 3, 4)
'        str = str2 & Mid(json, 5, Len(json) - 8) & str1
'
'        jsonDecode = DecodeBase64(str)
'        DecodeJSON = jsonDecode
'    End If
'
'    Exit Function
'
'BugError:
'    'MsgBox Err.Description, , Err.Number
'    Err.Clear
'    Resume Next
'End Function

