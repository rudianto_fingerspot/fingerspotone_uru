Attribute VB_Name = "mdlAddOnF1"
Option Explicit

Public Function GenerateACNew(chb0, chb1, chb2, chb3, chb4, chb5, chb6 As XtremeSuiteControls.CheckBox, edtACText As String) As Boolean
    On Error GoTo BugError
    
    Dim str1 As String
    Dim AC As String
    Dim active_date As String
    Dim order_code As String
    Dim RecRow As Long
    
    Dim field() As String
    Dim i As Integer
    Dim sn_device As String
    
    GenerateACNew = True
    active_date = Format(getCurrentDT, "yyyy-MM-dd hh:nn:ss")
    order_code = getParamater("order_code")
    
    'opt = 1 :Realtime Scanlog, 2 :Auto Sinkron User, 3 :Auto Sinkron Tanggal Jam Mesin, 4 :SMS
    
    '1 :Realtime Scanlog
    If chb0.value = xtpChecked Then
        field = Split(frmWizard.sn_mesin_fitur1, ";")
        For i = 0 To UBound(field) - 1
            str1 = field(i)
            If Trim(str1) <> "" Then
                sn_device = str1
                
                AC = Format(GetKeyRevoRT2(1, sn_device), "@@@@-@@@@-@@@@-@@@@-@@@@-@@@@")
                
                strSQL = "UPDATE t_device SET active_code_realtime = '" & AC & "', active_date_realtime = '" & active_date & "' " & _
                    "WHERE sn_device = '" & sn_device & "' "
                ExecuteSQLite_Data strSQL
                
                strSQL = "UPDATE t_ordering SET status_ordering = '1' " & _
                    "WHERE id_ordering = """ & order_code & """ AND addon_id = 1 AND sn_device = '" & sn_device & "' "
                ExecuteSQLite_Data strSQL
            End If
        Next i
    End If
    
    '2 :Auto Sinkron User
    If chb1.value = xtpChecked Then
        field = Split(frmWizard.sn_mesin_fitur2, ";")
        For i = 0 To UBound(field) - 1
            str1 = field(i)
            If Trim(str1) <> "" Then
                sn_device = str1
                
                AC = Format(GetKeyRevoRT2(2, sn_device), "@@@@-@@@@-@@@@-@@@@-@@@@-@@@@")
                
                strSQL = "UPDATE t_device SET activation_code = '" & AC & "' " & _
                    "WHERE sn_device = '" & sn_device & "' "
                ExecuteSQLite_Data strSQL
                
                strSQL = "UPDATE t_ordering SET status_ordering = '1' " & _
                    "WHERE id_ordering = """ & order_code & """ AND addon_id = 2 AND sn_device = '" & sn_device & "' "
                ExecuteSQLite_Data strSQL
            End If
        Next i
    End If
    
    '3 :Auto Sinkron Tanggal Jam Mesin
    If chb2.value = xtpChecked Then
        field = Split(frmWizard.sn_mesin_fitur3, ";")
        For i = 0 To UBound(field) - 1
            str1 = field(i)
            If Trim(str1) <> "" Then
                sn_device = str1
                
                AC = Format(GetKeyRevoRT2(3, sn_device), "@@@@-@@@@-@@@@-@@@@-@@@@-@@@@")
                
                strSQL = "UPDATE t_device SET active_code_autosync = '" & AC & "', active_date_autosync = '" & active_date & "' " & _
                    "WHERE sn_device = '" & sn_device & "' "
                ExecuteSQLite_Data strSQL
                
                strSQL = "UPDATE t_ordering SET status_ordering = '1' " & _
                    "WHERE id_ordering = """ & order_code & """ AND addon_id = 3 AND sn_device = '" & sn_device & "' "
                ExecuteSQLite_Data strSQL
            End If
        Next i
    End If
    
    '4 :SMS
    If chb3.value = xtpChecked Then
        field = Split(frmWizard.sn_mesin_fitur4, ";")
        For i = 0 To UBound(field) - 1
            str1 = field(i)
            If Trim(str1) <> "" Then
                sn_device = str1
                
                AC = Format(GetKeyRevoRT2(4, sn_device), "@@@@-@@@@-@@@@-@@@@-@@@@-@@@@")
                
                strSQL = "UPDATE t_device SET active_code_sdk = '" & AC & "', active_date_sdk = '" & active_date & "' " & _
                    "WHERE sn_device = '" & sn_device & "' "
                ExecuteSQLite_Data strSQL
                
                strSQL = "UPDATE t_ordering SET status_ordering = '1' " & _
                    "WHERE id_ordering = """ & order_code & """ AND addon_id = 4 AND sn_device = '" & sn_device & "' "
                ExecuteSQLite_Data strSQL
            End If
        Next i
    End If
    
    '5 :BPJS
    If chb4.value = xtpChecked Then
        sn_device = CStr(frmWizard.jml_fitur5)
        AC = Format(GetKeyRevoRT2(5, sn_device), "@@@@-@@@@-@@@@-@@@@-@@@@-@@@@")
        
        setParamater "active_code_bpjs", AC
        
        strSQL = "UPDATE t_ordering SET status_ordering = '1' " & _
            "WHERE id_ordering = """ & order_code & """ AND addon_id = 5 "
        ExecuteSQLite_Data strSQL
    End If
        
    '6 :PPH21
    If chb5.value = xtpChecked Then
        sn_device = CStr(frmWizard.jml_fitur6)
        AC = Format(GetKeyRevoRT2(6, sn_device), "@@@@-@@@@-@@@@-@@@@-@@@@-@@@@")
        
        setParamater "active_code_pph21", AC
        
        strSQL = "UPDATE t_ordering SET status_ordering = '1' " & _
            "WHERE id_ordering = """ & order_code & """ AND addon_id = 6 "
        ExecuteSQLite_Data strSQL
    End If
        
    '7 :PPH21
    If chb6.value = xtpChecked Then
        sn_device = CStr(frmWizard.jml_fitur7)
        AC = Format(GetKeyRevoRT2(7, sn_device), "@@@@-@@@@-@@@@-@@@@-@@@@-@@@@")
        
        setParamater "active_code_emp_" & order_code, AC
        
        strSQL = "UPDATE t_ordering SET status_ordering = '1' " & _
            "WHERE id_ordering = """ & order_code & """ AND addon_id = 7 "
        ExecuteSQLite_Data strSQL
    End If
        
    setParamater "active_code_" & order_code, Trim(edtACText)
    setParamater "active_code_date_" & order_code, active_date
    
    Exit Function
        
BugError:
    GenerateACNew = False
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Function

Public Function GetPaymentNew(chb0, chb1, chb2, chb3, chb4, chb5, chb6 As XtremeSuiteControls.CheckBox) As Boolean
    On Error GoTo BugError
    
    Dim str1 As String
    Dim str2 As String
    Dim str3 As String
    Dim data_str As String
    
    Dim sn_device As String
    Dim url_str As String
    Dim timezone As String
    Dim selisih As String
    Dim RecRow As Long
    
    GetPaymentNew = True
    
    Dim status0 As String
    
    status0 = INILanRead("frmWizard", "status0", "", "", "", "")
    
    Dim nCnt As Long
    Dim sn_str1 As String
    Dim sn_str2 As String
    Dim sn_str3 As String
    Dim sn_str4 As String
    Dim sn_str5 As String
    Dim sn_str6 As String
    Dim sn_str7 As String
    
    sn_str1 = ""
    sn_str2 = ""
    sn_str3 = ""
    sn_str4 = ""
    sn_str5 = ""
    sn_str6 = ""
    sn_str7 = ""
    
    If chb0.value = xtpChecked Then sn_str1 = frmWizard.sn_mesin_fitur1
    If chb1.value = xtpChecked Then sn_str2 = frmWizard.sn_mesin_fitur2
    If chb2.value = xtpChecked Then sn_str3 = frmWizard.sn_mesin_fitur3
    If chb3.value = xtpChecked Then sn_str4 = frmWizard.sn_mesin_fitur4
    If chb4.value = xtpChecked Then sn_str5 = "99"
    If chb5.value = xtpChecked Then sn_str6 = "99"
    If chb6.value = xtpChecked Then sn_str7 = "99"
    
    If (sn_str1 = "") And (sn_str2 = "") And (sn_str3 = "") And (sn_str4 = "") And (sn_str5 = "") And (sn_str6 = "") And (sn_str7 = "") Then
        GetPaymentNew = False
        MsgBox INILanRead("errMsg", "msg11", "", "", "", ""), vbOKOnly, App.Title
        Exit Function
    End If
    
    Dim CompanyName As String
    CompanyName = LoadParam("CompanyName")
    
    Dim fiturStr1 As String
    Dim fiturStr2 As String
    Dim fiturStr3 As String
    Dim fiturStr4 As String
    Dim fiturStr5 As String
    Dim fiturStr6 As String
    Dim fiturStr7 As String
    
    url_str = "http://payment.fingerspot.com/api/get_payment"
    str1 = "{""app_id"":""?1"", ""add_on"":["
    str1 = Replace(str1, "?1", "1")
    
    If (chb0.value = xtpChecked) And (sn_str1 <> "") And (frmWizard.jml_mesin_fitur1 > 0) Then
        fiturStr1 = "{""add_on_id"":""?2"",""sn"":""?3"",""qty"":""?4""},"
        fiturStr1 = Replace(fiturStr1, "?2", "1")
        If sn_str1 <> "" Then sn_str1 = Mid(sn_str1, 1, Len(sn_str1) - 1)
        fiturStr1 = Replace(fiturStr1, "?3", sn_str1)
        fiturStr1 = Replace(fiturStr1, "?4", CStr(frmWizard.jml_mesin_fitur1))
        str1 = str1 & fiturStr1
    End If
    
    If (chb1.value = xtpChecked) And (sn_str2 <> "") And (frmWizard.jml_mesin_fitur2 > 0) Then
        fiturStr2 = "{""add_on_id"":""?2"",""sn"":""?3"",""qty"":""?4""},"
        fiturStr2 = Replace(fiturStr2, "?2", "2")
        If sn_str2 <> "" Then sn_str2 = Mid(sn_str2, 1, Len(sn_str2) - 1)
        fiturStr2 = Replace(fiturStr2, "?3", sn_str2)
        fiturStr2 = Replace(fiturStr2, "?4", CStr(frmWizard.jml_mesin_fitur2))
        str1 = str1 & fiturStr2
    End If
    
    If (chb2.value = xtpChecked) And (sn_str3 <> "") And (frmWizard.jml_mesin_fitur3 > 0) Then
        fiturStr3 = "{""add_on_id"":""?2"",""sn"":""?3"",""qty"":""?4""},"
        fiturStr3 = Replace(fiturStr3, "?2", "3")
        If sn_str3 <> "" Then sn_str3 = Mid(sn_str3, 1, Len(sn_str3) - 1)
        fiturStr3 = Replace(fiturStr3, "?3", sn_str3)
        fiturStr3 = Replace(fiturStr3, "?4", CStr(frmWizard.jml_mesin_fitur3))
        str1 = str1 & fiturStr3
    End If
    
    If (chb3.value = xtpChecked) And (sn_str4 <> "") And (frmWizard.jml_mesin_fitur4 > 0) Then
        fiturStr4 = "{""add_on_id"":""?2"",""sn"":""?3"",""qty"":""?4""},"
        fiturStr4 = Replace(fiturStr4, "?2", "4")
        If sn_str4 <> "" Then sn_str4 = Mid(sn_str4, 1, Len(sn_str4) - 1)
        fiturStr4 = Replace(fiturStr4, "?3", sn_str4)
        fiturStr4 = Replace(fiturStr4, "?4", CStr(frmWizard.jml_mesin_fitur4))
        str1 = str1 & fiturStr4
    End If
    
    If (chb4.value = xtpChecked) And (frmWizard.jml_fitur5 > 0) Then
        sn_str5 = "" 'SN gak usah diisi karena tidak berhubungan denga mesin
        
        fiturStr5 = "{""add_on_id"":""?2"",""sn"":""?3"",""qty"":""?4""},"
        fiturStr5 = Replace(fiturStr5, "?2", "5")
        fiturStr5 = Replace(fiturStr5, "?3", sn_str5)
        fiturStr5 = Replace(fiturStr5, "?4", CStr(frmWizard.jml_fitur5))
        str1 = str1 & fiturStr5
    End If
    
    If (chb5.value = xtpChecked) And (frmWizard.jml_fitur6 > 0) Then
        sn_str6 = "" 'SN gak usah diisi karena tidak berhubungan denga mesin
        
        fiturStr6 = "{""add_on_id"":""?2"",""sn"":""?3"",""qty"":""?4""},"
        fiturStr6 = Replace(fiturStr6, "?2", "6")
        fiturStr6 = Replace(fiturStr6, "?3", sn_str6)
        fiturStr6 = Replace(fiturStr6, "?4", CStr(frmWizard.jml_fitur6))
        str1 = str1 & fiturStr6
    End If
    
    If (chb6.value = xtpChecked) And (frmWizard.jml_fitur7 > 0) Then
        sn_str7 = "" 'SN gak usah diisi karena tidak berhubungan denga mesin
        
        fiturStr7 = "{""add_on_id"":""?2"",""sn"":""?3"",""qty"":""?4""},"
        fiturStr7 = Replace(fiturStr7, "?2", "7")
        fiturStr7 = Replace(fiturStr7, "?3", sn_str7)
        fiturStr7 = Replace(fiturStr7, "?4", CStr(frmWizard.jml_fitur7))
        str1 = str1 & fiturStr7
    End If
    
    If Mid(str1, Len(str1), 1) = "," Then
        str1 = Mid(str1, 1, Len(str1) - 1)
    End If
    str1 = str1 & "], ""company_name"":""?CN""} "
    str1 = Replace(str1, "?CN", CompanyName)
    
    'Clipboard.Clear
    'Clipboard.SetText str1
    
    Dim order_code As String
    Dim url As String
    Dim date_ordering As String
    
    Dim field() As String
    Dim i As Integer
    
    str2 = SendRequestHTTP(False, url_str, str1)
    If str2 = "Error" Then
        GetPaymentNew = False
        MsgBox INILanRead("errMsg", "msg14", "", "", "", "")
    Else
        str3 = frmWizard.ReadJson(str2, "success")
        If str3 = "true" Then
            order_code = frmWizard.ReadJson(str2, "order_code")
            url = frmWizard.ReadJson(str2, "url")
            url = Replace(url, "\/", "\")
            date_ordering = Format(getCurrentDT, "yyyy-MM-dd hh:nn:ss")
            
            If chb0.value = xtpChecked Then
                field = Split(sn_str1 & ";", ";")
                For i = 0 To UBound(field) - 1
                    str1 = field(i)
                    If Trim(str1) <> "" Then
                        sn_device = str1
                        
                        strSQL = "INSERT INTO t_ordering(id_ordering, addon_id, sn_device, date_ordering, link_ordering, status_ordering) " & _
                            "VALUES(""" & order_code & """, 1, """ & sn_device & """, """ & date_ordering & """, """ & url & """, '0') "
                        ExecuteSQLite_Data strSQL
                    End If
                Next i
            End If
            
            If chb1.value = xtpChecked Then
                field = Split(sn_str2 & ";", ";")
                For i = 0 To UBound(field) - 1
                    str1 = field(i)
                    If Trim(str1) <> "" Then
                        sn_device = str1
                        
                        strSQL = "INSERT INTO t_ordering(id_ordering, addon_id, sn_device, date_ordering, link_ordering, status_ordering) " & _
                            "VALUES(""" & order_code & """, 2, """ & sn_device & """, """ & date_ordering & """, """ & url & """, '0') "
                        ExecuteSQLite_Data strSQL
                    End If
                Next i
            End If
            
            If chb2.value = xtpChecked Then
                field = Split(sn_str3 & ";", ";")
                For i = 0 To UBound(field) - 1
                    str1 = field(i)
                    If Trim(str1) <> "" Then
                        sn_device = str1
                        
                        strSQL = "INSERT INTO t_ordering(id_ordering, addon_id, sn_device, date_ordering, link_ordering, status_ordering) " & _
                            "VALUES(""" & order_code & """, 3, """ & sn_device & """, """ & date_ordering & """, """ & url & """, '0') "
                        ExecuteSQLite_Data strSQL
                    End If
                Next i
            End If
            
            If chb3.value = xtpChecked Then
                field = Split(sn_str4 & ";", ";")
                For i = 0 To UBound(field) - 1
                    str1 = field(i)
                    If Trim(str1) <> "" Then
                        sn_device = str1
                        
                        strSQL = "INSERT INTO t_ordering(id_ordering, addon_id, sn_device, date_ordering, link_ordering, status_ordering) " & _
                            "VALUES(""" & order_code & """, 4, """ & sn_device & """, """ & date_ordering & """, """ & url & """, '0') "
                        ExecuteSQLite_Data strSQL
                    End If
                Next i
            End If
            
            If chb4.value = xtpChecked Then
                strSQL = "INSERT INTO t_ordering(id_ordering, addon_id, sn_device, date_ordering, link_ordering, status_ordering) " & _
                    "VALUES(""" & order_code & """, 5, """ & CStr(frmWizard.jml_fitur5) & """, """ & date_ordering & """, """ & url & """, '0') "
                ExecuteSQLite_Data strSQL
            End If
                    
            If chb5.value = xtpChecked Then
                strSQL = "INSERT INTO t_ordering(id_ordering, addon_id, sn_device, date_ordering, link_ordering, status_ordering) " & _
                    "VALUES(""" & order_code & """, 6, """ & CStr(frmWizard.jml_fitur6) & """, """ & date_ordering & """, """ & url & """, '0') "
                ExecuteSQLite_Data strSQL
            End If
                    
            If chb6.value = xtpChecked Then
                strSQL = "INSERT INTO t_ordering(id_ordering, addon_id, sn_device, date_ordering, link_ordering, status_ordering) " & _
                    "VALUES(""" & order_code & """, 7, """ & CStr(frmWizard.jml_fitur7) & """, """ & date_ordering & """, """ & url & """, '0') "
                ExecuteSQLite_Data strSQL
            End If
                    
            setParamater "url_order_code", url
            setParamater "order_code", order_code
            
            DoEvents
        Else
            GetPaymentNew = False
            MsgBox INILanRead("errMsg", "msg15", "", "", "", "")
        End If
    End If
    
    Exit Function
        
BugError:
    GetPaymentNew = False
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
    
End Function

Public Function GetCancelPaymentNew() As Boolean
    On Error GoTo BugError
    
    Dim str1 As String
    Dim str2 As String
    Dim str3 As String
    Dim data_str As String
    
    Dim url_str As String
    
    GetCancelPaymentNew = True
    
    Dim order_code As String
    order_code = LoadParam("order_code")
    
    url_str = "http://payment.fingerspot.com/api/cancel_payment"
    str1 = "{""order_id"":""?1""}"
    str1 = Replace(str1, "?1", order_code)
    
    Dim url As String
    Dim date_ordering As String
    
    str2 = SendRequestHTTP(False, url_str, str1)
    If str2 = "Error" Then
        GetCancelPaymentNew = False
        MsgBox INILanRead("errMsg", "msg14", "", "", "", "")
    Else
        str3 = frmWizard.ReadJson(str2, "success")
        If str3 = "true" Then
            strSQL = "DELETE FROM t_ordering WHERE id_ordering = """ & order_code & """ "
            ExecuteSQLite_Data strSQL
            
            setParamater "url_order_code", ""
            setParamater "order_code", ""
    
            DoEvents
        Else
            GetCancelPaymentNew = False
            MsgBox INILanRead("errMsg", "msg15", "", "", "", "")
        End If
    End If
    
    Exit Function
        
BugError:
    GetCancelPaymentNew = False
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Function

Public Function CheckEmailTransaction(email As String) As Boolean
    On Error GoTo BugError
    
    Dim str1 As String
    Dim str2 As String
    Dim str3 As String
    Dim data_str As String
    
    Dim url_str As String
    
    CheckEmailTransaction = True
    
    url_str = "http://payment.fingerspot.com/api/check_email_transaction"
    str1 = "{""email"":""?1""}"
    str1 = Replace(str1, "?1", email)
    
    Dim url As String
    Dim date_ordering As String
    
    str2 = SendRequestHTTP(False, url_str, str1)
    If str2 = "Error" Then
        CheckEmailTransaction = False
        MsgBox INILanRead("errMsg", "msg14", "", "", "", "")
    Else
        str3 = frmWizard.ReadJson(str2, "success")
        If str3 = "true" Then
            setParamater "check_email_transaction", email
            
            MsgBox INILanRead("errMsg", "msg22", "", "", "", "")
            DoEvents
        Else
            CheckEmailTransaction = False
            MsgBox INILanRead("errMsg", "msg15", "", "", "", "")
        End If
    End If
    
    Exit Function
        
BugError:
    CheckEmailTransaction = False
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Function

Public Function GetTransaction(email As String, verification_code As String) As Boolean
    On Error GoTo BugError
    
    Dim str1 As String
    Dim str2 As String
    Dim str3 As String
    Dim data_str As String
    
    GetTransaction = True
    
    Dim nCnt As Long
    Dim url_str As String
    
    url_str = "http://payment.fingerspot.com/api/get_transaction"
    str1 = "{""email"":""?1"", ""verification_code"":""?2""}"
    str1 = Replace(str1, "?1", email)
    str1 = Replace(str1, "?2", verification_code)
    
    Dim order_code As String
    Dim url As String
    Dim date_ordering As String
    
    Dim field() As String
    Dim i As Integer
    
    str2 = SendRequestHTTP(False, url_str, str1)
    If str2 = "Error" Then
        GetTransaction = False
        MsgBox INILanRead("errMsg", "msg14", "", "", "", "")
    Else
        str3 = frmWizard.ReadJson(str2, "success")
        If str3 = "true" Then
            data_str = frmWizard.ReadJson(str2, "data")
            'data_str = Mid(data_str, 2, Len(data_str) - 2)
            Clipboard.Clear
            Clipboard.SetText data_str
            
            'GetTransaction = ArrayJson(data_str, "detail")
            GetTransaction = ArrayJsonTrans(data_str)
            DoEvents
        Else
            GetTransaction = False
            MsgBox INILanRead("errMsg", "msg15", "", "", "", "")
        End If
    End If
    
    Exit Function
        
BugError:
    GetTransaction = False
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Function

Private Function ArrayJsonTrans(strJsonArray As String) As Boolean
    On Error GoTo BugError
    
    ArrayJsonTrans = True
    
    Dim jsonArray As New ChilkatJsonArray
    Dim success As Long
    
    success = jsonArray.Load(strJsonArray)
    
    Dim i As Long
    i = 0
    Do While i < jsonArray.SIZE
        Dim jsonObj As ChilkatJsonObject
        Set jsonObj = jsonArray.ObjectAt(i)
        'Debug.Print i & ": " & jsonObj.StringOf("name")
        
        ArrayJsonTrans = ArrayJson(jsonObj, "detail")
        
        i = i + 1
    Loop

    Exit Function
        
BugError:
    ArrayJsonTrans = False
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Function

'Private Function ArrayJson(jsonStr As String, key_str As String) As Boolean
Private Function ArrayJson(JSON As ChilkatJsonObject, key_str As String) As Boolean
    On Error GoTo BugError
    
    ArrayJson = True
    'Clipboard.Clear
    'Clipboard.SetText jsonStr
    
'    Dim JSON As New ChilkatJsonObject
'
'    Dim success As Long
'    success = JSON.Load(jsonStr)
'    If (success <> 1) Then
'        ArrayJson = False
'        Debug.Print JSON.LastErrorText
'        Exit Function
'    End If
    
    Dim dataStr As ChilkatJsonArray
    Set dataStr = JSON.ArrayOf(key_str)
    If (JSON.LastMethodSuccess = 0) Then
        ArrayJson = False
        Exit Function
    End If
    
    Dim numdataStr As Long
    numdataStr = dataStr.SIZE
    Dim i As Long
    Dim invoice_id As String
    Dim str1 As String
    
    Dim add_on_id As String
    Dim SN As String
    Dim qty As String
    
    Dim date_ordering As String
    Dim CompanyName As String
    Dim KodeAktivasi As String
    
    date_ordering = Format(getCurrentDT, "yyyy-MM-dd hh:nn:ss")
    invoice_id = JSON.StringOf("invoice_id")
        
    i = 0
    Do While i < numdataStr
    
        Dim strObj As ChilkatJsonObject
        Set strObj = dataStr.ObjectAt(i)
        
        add_on_id = strObj.StringOf("add_on_id")
        SN = strObj.StringOf("sn")
        qty = strObj.StringOf("qty")
        
        InputAktivasiUlang date_ordering, invoice_id, add_on_id, SN, qty
        
        i = i + 1
        DoEvents
    Loop
    
    CompanyName = LoadParam("CompanyName")
    KodeAktivasi = UCase(Mid(MD5("F1n63R" & invoice_id & CompanyName & "Sp0T"), 5, 24))
    
    setParamater "order_code", invoice_id
    setParamater "active_code_" & invoice_id, Trim(KodeAktivasi)
    setParamater "active_code_date_" & invoice_id, date_ordering
        
    Exit Function
        
BugError:
    ArrayJson = False
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Function

Private Function InputAktivasiUlang(date_ordering As String, order_code As String, add_on_id As String, SN As String, qty As String) As Boolean
    On Error GoTo BugError
    
    InputAktivasiUlang = True
    
    Dim i As Integer
    Dim sn_device As String
    Dim str1 As String
    Dim field() As String
    Dim AC As String
        
    str1 = SN
    If InStr("1234", Trim(add_on_id)) > 0 Then
        str1 = str1 & ";"
        
        field = Split(str1, ";")
        For i = 0 To UBound(field) - 1
            sn_device = field(i)
            
            If Trim(sn_device) <> "" Then
                '1 :Realtime Scanlog
                If Trim(add_on_id) = "1" Then
                    AC = Format(GetKeyRevoRT2(1, sn_device), "@@@@-@@@@-@@@@-@@@@-@@@@-@@@@")
                    
                    strSQL = "UPDATE t_device SET active_code_realtime = '" & AC & "', active_date_realtime = '" & date_ordering & "' " & _
                        "WHERE sn_device = '" & sn_device & "' "
                    ExecuteSQLite_Data strSQL
                    
                    strSQL = "INSERT INTO t_ordering(id_ordering, addon_id, sn_device, date_ordering, link_ordering, status_ordering) " & _
                        "VALUES(""" & order_code & """, 1, """ & sn_device & """, """ & date_ordering & """, '', '1') "
                    ExecuteSQLite_Data strSQL
                End If
                
                '2 :Auto Sinkron User
                If Trim(add_on_id) = "2" Then
                    AC = Format(GetKeyRevoRT2(2, sn_device), "@@@@-@@@@-@@@@-@@@@-@@@@-@@@@")
                
                    strSQL = "UPDATE t_device SET activation_code = '" & AC & "' " & _
                        "WHERE sn_device = '" & sn_device & "' "
                    ExecuteSQLite_Data strSQL
                    
                    strSQL = "INSERT INTO t_ordering(id_ordering, addon_id, sn_device, date_ordering, link_ordering, status_ordering) " & _
                        "VALUES(""" & order_code & """, 2, """ & sn_device & """, """ & date_ordering & """, '', '1') "
                    ExecuteSQLite_Data strSQL
                End If
                
                '3 :Auto Sinkron Tanggal Jam Mesin
                If Trim(add_on_id) = "3" Then
                    AC = Format(GetKeyRevoRT2(3, sn_device), "@@@@-@@@@-@@@@-@@@@-@@@@-@@@@")
                    
                    strSQL = "UPDATE t_device SET active_code_autosync = '" & AC & "', active_date_autosync = '" & date_ordering & "' " & _
                        "WHERE sn_device = '" & sn_device & "' "
                    ExecuteSQLite_Data strSQL
                    
                    strSQL = "INSERT INTO t_ordering(id_ordering, addon_id, sn_device, date_ordering, link_ordering, status_ordering) " & _
                        "VALUES(""" & order_code & """, 3, """ & sn_device & """, """ & date_ordering & """, '', '1') "
                    ExecuteSQLite_Data strSQL
                End If
                
                '4 :SMS
                If Trim(add_on_id) = "4" Then
                    AC = Format(GetKeyRevoRT2(4, sn_device), "@@@@-@@@@-@@@@-@@@@-@@@@-@@@@")
                
                    strSQL = "UPDATE t_device SET active_code_sdk = '" & AC & "', active_date_sdk = '" & date_ordering & "' " & _
                        "WHERE sn_device = '" & sn_device & "' "
                    ExecuteSQLite_Data strSQL
                    
                    strSQL = "INSERT INTO t_ordering(id_ordering, addon_id, sn_device, date_ordering, link_ordering, status_ordering) " & _
                        "VALUES(""" & order_code & """, 4, """ & sn_device & """, """ & date_ordering & """, '', '1') "
                    ExecuteSQLite_Data strSQL
                End If
            End If
        Next i
    ElseIf Trim(add_on_id) = "5" Then
        '5 :BPJS
        sn_device = qty
        AC = Format(GetKeyRevoRT2(5, sn_device), "@@@@-@@@@-@@@@-@@@@-@@@@-@@@@")

        setParamater "active_code_bpjs", AC
        
        strSQL = "INSERT INTO t_ordering(id_ordering, addon_id, sn_device, date_ordering, link_ordering, status_ordering) " & _
            "VALUES(""" & order_code & """, 5, """ & qty & """, """ & date_ordering & """, '', '1') "
        ExecuteSQLite_Data strSQL
    ElseIf Trim(add_on_id) = "6" Then
        '6 :PPH21
        sn_device = qty
        AC = Format(GetKeyRevoRT2(6, sn_device), "@@@@-@@@@-@@@@-@@@@-@@@@-@@@@")

        setParamater "active_code_pph21", AC
        
        strSQL = "INSERT INTO t_ordering(id_ordering, addon_id, sn_device, date_ordering, link_ordering, status_ordering) " & _
            "VALUES(""" & order_code & """, 6, """ & qty & """, """ & date_ordering & """, '', '1') "
        ExecuteSQLite_Data strSQL
    ElseIf Trim(add_on_id) = "7" Then
        '7 :PPH21
        sn_device = qty
        AC = Format(GetKeyRevoRT2(7, sn_device), "@@@@-@@@@-@@@@-@@@@-@@@@-@@@@")

        setParamater "active_code_emp_" & order_code, AC
        
        strSQL = "INSERT INTO t_ordering(id_ordering, addon_id, sn_device, date_ordering, link_ordering, status_ordering) " & _
            "VALUES(""" & order_code & """, 7, """ & qty & """, """ & date_ordering & """, '', '1') "
        ExecuteSQLite_Data strSQL
    End If
    
    Exit Function
        
BugError:
    InputAktivasiUlang = False
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Function

Private Function SubArrayJson(jsonStr As String, key_str As String, order_code As String) As String
    On Error GoTo BugError
    
    SubArrayJson = ""
    'Clipboard.Clear
    'Clipboard.SetText jsonStr
    
    Dim JSON As New ChilkatJsonObject

    Dim success As Long
    success = JSON.Load(jsonStr)
    If (success <> 1) Then
        Debug.Print JSON.LastErrorText
        Exit Function
    End If
    
    Dim dataStr As ChilkatJsonArray
    Set dataStr = JSON.ArrayOf(key_str)
    If (JSON.LastMethodSuccess = 0) Then
        Exit Function
    End If
    
    Dim numdataStr As Long
    numdataStr = dataStr.SIZE
    Dim i As Long
    Dim date_ordering As String
    Dim add_on_id As String
    Dim sn_device As String
    Dim str1 As String
    Dim field() As String
    Dim qty As String
    Dim AC As String
    Dim CompanyName As String
    Dim KodeAktivasi As String
        
    date_ordering = Format(getCurrentDT, "yyyy-MM-dd hh:nn:ss")
    i = 0
    Do While i < numdataStr
    
        Dim strObj As ChilkatJsonObject
        Set strObj = dataStr.ObjectAt(i)
        
        'add_on_id, sn, qty
        add_on_id = Trim(strObj.StringOf("add_on_id"))
        str1 = Trim(strObj.StringOf("sn"))
        qty = Trim(strObj.StringOf("qty"))
        
        If InStr("1234", Trim(add_on_id)) > 0 Then
            str1 = str1 & ";"
            field = Split(str1, ";")
            For i = 0 To UBound(field) - 1
                sn_device = field(i)
                
                If Trim(sn_device) <> "" Then
                    '1 :Realtime Scanlog
                    If Trim(add_on_id) = "1" Then
                        AC = Format(GetKeyRevoRT2(1, sn_device), "@@@@-@@@@-@@@@-@@@@-@@@@-@@@@")
                        
                        strSQL = "UPDATE t_device SET active_code_realtime = '" & AC & "', active_date_realtime = '" & date_ordering & "' " & _
                            "WHERE sn_device = '" & sn_device & "' "
                        ExecuteSQLite_Data strSQL
                        
                        strSQL = "INSERT INTO t_ordering(id_ordering, addon_id, sn_device, date_ordering, link_ordering, status_ordering) " & _
                            "VALUES(""" & order_code & """, 1, """ & sn_device & """, """ & date_ordering & """, '', '1') "
                        ExecuteSQLite_Data strSQL
                    End If
                    
                    '2 :Auto Sinkron User
                    If Trim(add_on_id) = "2" Then
                        AC = Format(GetKeyRevoRT2(2, sn_device), "@@@@-@@@@-@@@@-@@@@-@@@@-@@@@")
                    
                        strSQL = "UPDATE t_device SET activation_code = '" & AC & "' " & _
                            "WHERE sn_device = '" & sn_device & "' "
                        ExecuteSQLite_Data strSQL
                        
                        strSQL = "INSERT INTO t_ordering(id_ordering, addon_id, sn_device, date_ordering, link_ordering, status_ordering) " & _
                            "VALUES(""" & order_code & """, 2, """ & sn_device & """, """ & date_ordering & """, '', '1') "
                        ExecuteSQLite_Data strSQL
                    End If
                    
                    '3 :Auto Sinkron Tanggal Jam Mesin
                    If Trim(add_on_id) = "3" Then
                        AC = Format(GetKeyRevoRT2(3, sn_device), "@@@@-@@@@-@@@@-@@@@-@@@@-@@@@")
                        
                        strSQL = "UPDATE t_device SET active_code_autosync = '" & AC & "', active_date_autosync = '" & date_ordering & "' " & _
                            "WHERE sn_device = '" & sn_device & "' "
                        ExecuteSQLite_Data strSQL
                        
                        strSQL = "INSERT INTO t_ordering(id_ordering, addon_id, sn_device, date_ordering, link_ordering, status_ordering) " & _
                            "VALUES(""" & order_code & """, 3, """ & sn_device & """, """ & date_ordering & """, '', '1') "
                        ExecuteSQLite_Data strSQL
                    End If
                    
                    '4 :SMS
                    If Trim(add_on_id) = "4" Then
                        AC = Format(GetKeyRevoRT2(4, sn_device), "@@@@-@@@@-@@@@-@@@@-@@@@-@@@@")
                    
                        strSQL = "UPDATE t_device SET active_code_sdk = '" & AC & "', active_date_sdk = '" & date_ordering & "' " & _
                            "WHERE sn_device = '" & sn_device & "' "
                        ExecuteSQLite_Data strSQL
                        
                        strSQL = "INSERT INTO t_ordering(id_ordering, addon_id, sn_device, date_ordering, link_ordering, status_ordering) " & _
                            "VALUES(""" & order_code & """, 4, """ & sn_device & """, """ & date_ordering & """, '', '1') "
                        ExecuteSQLite_Data strSQL
                    End If
                End If
            Next i
        ElseIf Trim(add_on_id) = "5" Then
            '5 :BPJS
            sn_device = qty
            AC = Format(GetKeyRevoRT2(5, sn_device), "@@@@-@@@@-@@@@-@@@@-@@@@-@@@@")

            setParamater "active_code_bpjs", AC
            
            strSQL = "INSERT INTO t_ordering(id_ordering, addon_id, sn_device, date_ordering, link_ordering, status_ordering) " & _
                "VALUES(""" & order_code & """, 5, """ & CStr(1) & """, """ & date_ordering & """, '', '1') "
            ExecuteSQLite_Data strSQL
        ElseIf Trim(add_on_id) = "6" Then
            '6 :PPH21
            sn_device = qty
            AC = Format(GetKeyRevoRT2(6, sn_device), "@@@@-@@@@-@@@@-@@@@-@@@@-@@@@")

            setParamater "active_code_pph21", AC
            
            strSQL = "INSERT INTO t_ordering(id_ordering, addon_id, sn_device, date_ordering, link_ordering, status_ordering) " & _
                "VALUES(""" & order_code & """, 6, """ & CStr(1) & """, """ & date_ordering & """, '', '1') "
            ExecuteSQLite_Data strSQL
        ElseIf Trim(add_on_id) = "7" Then
            '7 :PPH21
            sn_device = qty
            AC = Format(GetKeyRevoRT2(7, sn_device), "@@@@-@@@@-@@@@-@@@@-@@@@-@@@@")

            setParamater "active_code_emp_" & order_code, AC
            
            strSQL = "INSERT INTO t_ordering(id_ordering, addon_id, sn_device, date_ordering, link_ordering, status_ordering) " & _
                "VALUES(""" & order_code & """, 7, """ & qty & """, """ & date_ordering & """, '', '1') "
            ExecuteSQLite_Data strSQL
        End If
        
        CompanyName = LoadParam("CompanyName")
        KodeAktivasi = UCase(Mid(MD5("F1n63R" & order_code & CompanyName & "Sp0T"), 5, 24))
        
        setParamater "order_code", order_code
        setParamater "active_code_" & order_code, Trim(KodeAktivasi)
        setParamater "active_code_date_" & order_code, date_ordering
    
        i = i + 1
        DoEvents
    Loop
    
    Exit Function
        
BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Function
