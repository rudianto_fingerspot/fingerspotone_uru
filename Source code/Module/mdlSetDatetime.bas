Attribute VB_Name = "mdlSetDatetime"
'Put this code under the Basic Module file
Option Explicit

Public Type SYSTEMTIME
    wYear As Integer
    wMonth As Integer
    wDayOfWeek As Integer
    wDay As Integer
    wHour As Integer
    wMinute As Integer
    wSecond As Integer
    wMilliseconds As Integer
End Type
Public lpSystemTime As SYSTEMTIME
Public Declare Function SetSystemTime Lib "kernel32" (lpSystemTime As SYSTEMTIME) As Long

Public Type TIME_ZONE_INFORMATION
        Bias As Long
        StandardName(32) As Integer
        StandardDate As SYSTEMTIME
        StandardBias As Long
        DaylightName(32) As Integer
        DaylightDate As SYSTEMTIME
        DaylightBias As Long
End Type
Public lpTimeZoneInformation As TIME_ZONE_INFORMATION
Public Declare Function GetTimeZoneInformation Lib "kernel32" (lpTimeZoneInformation As TIME_ZONE_INFORMATION) As Long

'Public Sub Set_NewDateTime(ByVal lpNewDate As String, ByVal lpNewTime As String)
'Public Sub Set_NewDateTime(ByVal datetime_str As String)
Public Sub Set_NewDateTime(ByVal dt As Date)
    On Error GoTo BugError
    
    Dim xHour%
    Dim xMinute%
    Dim xSecond%
    Dim xYear%
    Dim xMonth%
    Dim xDay%
    Dim salah As Boolean
    Dim Res As Long
    Dim jam As Integer
    Dim tgl_svr As String
    
    salah = True
    
    tgl_svr = DateTimeToStr_DB(dt, "yyyy-mm-dd hh:nn:ss")
    
    'Get System Time Zone
'    GetTimeZoneInformation lpTimeZoneInformation
'
'    If Hour(lpNewTime) >= Abs((lpTimeZoneInformation.Bias / 60)) Then
'        xHour = Hour(lpNewTime) + (lpTimeZoneInformation.Bias / 60)
'    Else
'        xHour = 24 + (Hour(lpNewTime) + (lpTimeZoneInformation.Bias / 60))
'        lpNewDate = DateValue(lpNewDate) - 1
'    End If
'    xMinute = Minute(lpNewTime)
'    xSecond = Second(lpNewTime)
'    xYear = Year(lpNewDate)
'    xMonth = Month(lpNewDate)
'    xDay = Day(lpNewDate)
'
'    lpSystemTime.wYear = xYear
'    lpSystemTime.wMonth = xMonth
'    lpSystemTime.wDayOfWeek = -1
'    lpSystemTime.wDay = xDay
'    lpSystemTime.wHour = xHour
'    lpSystemTime.wMinute = xMinute
'    lpSystemTime.wSecond = xSecond
'    lpSystemTime.wMilliseconds = 0
'    'set the new time
'    SetSystemTime lpSystemTime
    
    
    GetTimeZoneInformation lpTimeZoneInformation

    If Hour(dt) >= Abs((lpTimeZoneInformation.Bias / 60)) Then
        xHour = Hour(dt) + (lpTimeZoneInformation.Bias / 60)
    Else
        xHour = 24 + (Hour(dt) + (lpTimeZoneInformation.Bias / 60))
        dt = DateValue(dt) - 1
    End If
    xMinute = Minute(dt)
    xSecond = Second(dt)
    xYear = Year(dt)
    xMonth = Month(dt)
    xDay = Day(dt)

    lpSystemTime.wYear = xYear
    lpSystemTime.wMonth = xMonth
    lpSystemTime.wDayOfWeek = -1
    lpSystemTime.wDay = xDay
    lpSystemTime.wHour = xHour
    lpSystemTime.wMinute = xMinute
    lpSystemTime.wSecond = xSecond
    lpSystemTime.wMilliseconds = 0

    'set the new time
    Res = SetSystemTime(lpSystemTime)
    
    If salah = True Then
        frmWizard.TulisLog INILanRead("errMsg", "msgLog17", "", CStr(Res) & " ~ " & tgl_svr, "", "")
    Else
        frmWizard.TulisLog INILanRead("errMsg", "msgLog18", "", CStr(Res) & " ~ " & tgl_svr, "", "")
    End If
    
    Exit Sub
        
BugError:
    salah = False
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Sub
