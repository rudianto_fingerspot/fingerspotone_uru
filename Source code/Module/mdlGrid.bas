Attribute VB_Name = "mdlGrid"
Option Explicit

Const DB_LANGUANGE_NAME = "Language"

' these vars used to store the row and col of the cell with pushed button:
Public m_lPushedRow As Long
Public m_lPushedCol As Long
Public m_bPushed As Boolean

' these vars contain the button font object and its API handle
Public m_oFontNorm As StdFont
Public m_hFontNorm As Long

' helper class to draw buttons using visual styles
Public m_cUxTheme As cUxTheme
Public m_bCanDrawVisualStyle As Boolean

' temp screen dc to calc text size
Public m_tempDC As Long
Public m_hOldFont As Long

Public fieldButton1 As String
Public fieldButton2 As String
Public fieldButton3 As String
Public fieldButton4 As String

Public Sub SetGrid(dbg As iGrid, dbgName As String, frm_name As String, autoSort As Boolean, strSetgrid As String, _
    lbl_row_count As XtremeSuiteControls.Label, nama_data As String, textStr As String, dbg_cmb_count As Integer, _
    ComboFieldID() As String, ComboFieldName() As String, ComboSQL() As String)
    
    Dim rs As cRecordset
    Dim Combo() As String
    Dim field() As String
    
    Dim s As String
    Dim str1 As String
    Dim str2 As String
    Dim str3 As String
    Dim str5 As String
    Dim lebarDbg As Long
    Dim Pos2 As Long
    Dim Pos3 As Long
    Dim cekWajibIsi As String
    
    Dim i As Integer
    Dim J As Integer
    
    Dim field_name As String
    Dim field_id As String
    
    With dbg
        If .ColCount > 0 Then .RemoveCol 1, .ColCount
        
        .BeginUpdate
        .ShowControlsInAllCells = True
        
        str1 = INILanRead(frm_name, dbgName, strSetgrid, "", "", "")
        If InStr(str1, "(*)") > 0 Then
            cekWajibIsi = True
        Else
            cekWajibIsi = False
        End If
        
        If InStr(str1, "ComboBox") > 0 Then
            dbg_cmb_count = Val(INILanRead(frm_name, dbgName & "_cmb_count", "", "", "", ""))
            For i = 1 To dbg_cmb_count
                str1 = dbgName & "_cmb" & Trim(str(i))
                str2 = INILanRead(frm_name, str1, "", "", "", "")
                Combo = Split(str2, ";")
                
                With .Combos.Add(str1)
                    For J = 0 To UBound(Combo) - 1
                        str3 = Combo(J)
                        str3 = INILanRead("Default", str3, "", "", "", "")
                        .AddItem str3, CStr(J + 1)
                    Next
                End With
            Next
        ElseIf InStr(str1, "CmbDB") > 0 Then
            For i = 1 To dbg_cmb_count
                str1 = dbgName & "_cmb" & Trim(str(i))
                field_id = ComboFieldID(i)
                field_name = ComboFieldName(i)
                strSQL = ComboSQL(i)
                Set rs = getRecordSet_Data(strSQL, True)
                
                If rs.RecordCount > 0 Then
                    With .Combos.Add(str1)
                        For J = 0 To rs.RecordCount - 1
                            str2 = rs.fields(field_id).value
                            str3 = rs.fields(field_name).value
                            .AddItem str3, str2
                        Next
                    End With
                End If
            Next
        ElseIf InStr(str1, "CmbText") > 0 Then
            For i = 1 To dbg_cmb_count
                str1 = dbgName & "_cmb" & Trim(str(i))
                Combo = Split(textStr, vbNewLine)
                
                With .Combos.Add(str1)
                    For J = 0 To UBound(Combo) - 1
                        str3 = Combo(J)
                        .AddItem str3, str3 'CStr(j + 1)
                    Next
                End With
            Next
        End If
        
        str1 = INILanRead(frm_name, dbgName, strSetgrid, "", "", "")
        field = Split(str1, ";")
        For i = 0 To UBound(field) - 1
            str2 = field(i)
            Pos2 = InStr(str2, "[")
            Pos3 = InStr(str2, "]")
            
            If Pos2 > 0 Then
                str3 = Mid(str2, 1, Pos2 - 1)
                str5 = Mid(str2, Pos2 + 1, Pos3 - Pos2 - 1)
            Else
                str3 = str2
            End If
            
            If InStr(str5, "Normal") > 0 Then
                .AddCol Trim(str(i + 1)), str3, 100
            ElseIf InStr(str5, "CheckBox") > 0 Then
                .AddCol Trim(str(i + 1)), str3, 100
                .ColDefaultCell(i + 1).eTypeFlags = igCheckBox3State
                .ColDefaultCell(i + 1).bCheckVisible = True
            ElseIf InStr(str5, "Button") > 0 Then
                .AddCol(sKey:=Trim(str(i + 1)), sHeader:=str3, lWidth:=100).eType = igCellCustomDraw
            ElseIf InStr(str5, "ComboBox") > 0 Then
                .AddCol Trim(str(i + 1)), str3, 100
                .ColDefaultCell(i + 1).eType = igCellCombo
                s = Mid(str5, Len("ComboBox") + 1, Len(str5) - Len("ComboBox"))
                If s <> "0" Then
                    s = dbgName & "_cmb" & s
                    .ColDefaultCell(i + 1).sCtrlKey = s
                End If
            ElseIf InStr(str5, "CmbDB") > 0 Then
                .AddCol Trim(str(i + 1)), str3, 100
                .ColDefaultCell(i + 1).eType = igCellCombo
                s = Mid(str5, Len("CmbDB") + 1, Len(str5) - Len("CmbDB"))
                If s <> "0" Then
                    s = dbgName & "_cmb" & s
                    .ColDefaultCell(i + 1).sCtrlKey = s
                End If
            ElseIf InStr(str5, "CmbText") > 0 Then
                .AddCol Trim(str(i + 1)), str3, 100
                .ColDefaultCell(i + 1).eType = igCellCombo
                s = Mid(str5, Len("CmbText") + 1, Len(str5) - Len("CmbText"))
                If s <> "0" Then
                    s = dbgName & "_cmb" & s
                    .ColDefaultCell(i + 1).sCtrlKey = s
                End If
            End If
            
            If cekWajibIsi = True Then
                If InStr(str2, "(*)") > 0 Then
                    .ColHeaderForeColor(i + 1) = vbBlack
                Else
                    .ColHeaderForeColor(i + 1) = vbBlue
                End If
            Else
                .ColHeaderForeColor(i + 1) = vbBlack
            End If
            
            .Header.UseXPStyles = False
            .Header.BackColor = WarnaHeader
            .Header.Font.Bold = True
            .Header.Flat = True
            .Header.Font.SIZE = 9
            .Header.Font.Name = "Century Gothic"
            .ColHeaderAlignH(i + 1) = igAlignHCenter
            .ColHeaderAlignV(i + 1) = igAlignVCenter
            .GridLineColor = &HE0E0E0
            .Font.SIZE = 8
            .Header.Height = 30
        Next
        
        If autoSort Then
            i = UBound(field) + 1
            .AddCol CStr(i), "autoSort", 100
            
            .Header.UseXPStyles = False
            .Header.BackColor = WarnaHeader
            .Header.Font.Bold = True
            .Header.Flat = True
            .Header.Font.SIZE = 9
            .Header.Font.Name = "Century Gothic"
            .ColHeaderAlignH(i + 1) = igAlignHCenter
            .ColHeaderAlignV(i + 1) = igAlignVCenter
            .GridLineColor = &HE0E0E0
            .Font.SIZE = 8
            .Header.Height = 30
            .ColVisible(i) = False
        End If
        
        .EndUpdate
    End With
    
    SetWidthCols frm_name, dbg, dbgName
    
    lbl_row_count.Caption = INILanRead("Default", "jumlah_baris", "", "", nama_data, "")
End Sub

Public Sub InsertGrid(dbg As iGrid, RecNo As Long, lbl_row_count As XtremeSuiteControls.Label, nama_data As String, _
    boldFont As Boolean, warnaBack As OLE_COLOR, warnaFont As OLE_COLOR, warningNull As Boolean, btn1 As Variant, _
    btn2 As Variant, sortStr As String, str1 As String)
    
    Dim i As Integer
    Dim field() As String
    Dim str2 As String
    Dim str3 As String
    Dim s As String
    
    Dim Pos2 As Long
    Dim Pos3 As Long
    
    field = Split(str1, ";")
    With dbg
        If RecNo = 1 Then .Clear
        .BeginUpdate
        .AddRow
        For i = 0 To UBound(field) - 1
            str2 = field(i)
            If Mid(str2, 1, 1) <> "[" Then
                Pos2 = InStr(str2, "[")
                Pos3 = InStr(str2, "]")
                
                If Pos2 > 0 Then
                    str3 = Mid(str2, Pos2 + 1, Pos3 - Pos2 - 1)
                    str2 = Mid(str2, 1, Pos2 - 1)
                End If
            End If
            
            .RowTag(RecNo) = RecNo
            .RowHeight(RecNo) = 25
            .CellAlignV(RecNo, i + 1) = igAlignVCenter
            If InStr(str2, ":") > 0 And Len(str2) = 5 Then
                .CellAlignH(RecNo, i + 1) = igAlignHCenter
            ElseIf str3 = "Left" Then
                .CellAlignH(RecNo, i + 1) = igAlignHLeft
            ElseIf str3 = "Right" Then
                .CellAlignH(RecNo, i + 1) = igAlignHRight
            ElseIf str3 = "Center" Then
                .CellAlignH(RecNo, i + 1) = igAlignHCenter
            End If
            
            If str2 = "[0]" Or str2 = "[False]" Then
                .CellValue(RecNo, i + 1) = ""
                .CellCheckState(RecNo, i + 1) = igCheckStateUnchecked
                .CellAlignH(RecNo, i + 1) = igAlignHCenter
            ElseIf str2 = "[-1]" Or str2 = "[1]" Or str2 = "[True]" Then
                .CellValue(RecNo, i + 1) = ""
                .CellCheckState(RecNo, i + 1) = igCheckStateChecked
                .CellAlignH(RecNo, i + 1) = igAlignHCenter
            Else
                If InStr(str2, "ComboBox") > 0 Then
                    s = Mid(str2, Len("ComboBox") + 2, Len(str2) - (Len("ComboBox") + 2))
                    .CellCtrlKey(RecNo, i + 1) = s
                    .CellValue(RecNo, i + 1) = CStr("1")
                ElseIf InStr(str2, "CmbDB") > 0 Then
                    s = Mid(str2, Len("CmbDB") + 2, Len(str2) - (Len("CmbDB") + 2))
                    .CellCtrlKey(RecNo, i + 1) = s
                    .CellValue(RecNo, i + 1) = CStr("1")
                ElseIf InStr(str2, "CmbText") > 0 Then
                    s = Mid(str2, Len("CmbText") + 2, Len(str2) - (Len("CmbText") + 2))
                    .CellCtrlKey(RecNo, i + 1) = s
                    .CellValue(RecNo, i + 1) = CStr("1")
                ElseIf InStr(str2, "Button1") > 0 Then
                    .CellValue(RecNo, i + 1) = btn1
                ElseIf InStr(str2, "Button2") > 0 Then
                    .CellValue(RecNo, i + 1) = btn2
                Else
                    If str2 = "Header" Then
                        .CellValue(RecNo, i + 1) = .ColHeaderText(i + 1)
                    Else
                        .CellValue(RecNo, i + 1) = CStr(str2)
                    End If
                End If
            End If
            .CellFont(RecNo, i + 1).Bold = boldFont
            If str2 = "Header" Then
                .CellForeColor(RecNo, i + 1) = vbBlue
                .CellFont(RecNo, i + 1).Underline = True
            Else
                .CellForeColor(RecNo, i + 1) = warnaFont
            End If
            
            If warnaBack = &H0& Then
                If (Trim(str2) = "") And (warningNull) Then
                    If .ColHeaderForeColor(Trim(str(i + 1))) = vbBlue Then
                        .CellBackColor(RecNo, i + 1) = WarnaDataKosongTidakWajibdiisi
                    Else
                        .CellBackColor(RecNo, i + 1) = WarnaDataKosongWajibdiisi
                    End If
                Else
                    If RecNo Mod 2 = 0 Then
                        .CellBackColor(RecNo, i + 1) = &HEBEBE0
                    End If
                End If
            Else
                .CellBackColor(RecNo, i + 1) = warnaBack
            End If
        Next
        
        If sortStr <> "" Then
            .CellValue(RecNo, .ColCount) = sortStr
        End If
              
        .EndUpdate
    End With
    
    lbl_row_count.Caption = INILanRead("Default", "jumlah_baris", "", CStr(RecNo), "", "")
End Sub

Public Sub CellBackColor(dbg As iGrid, RecRow As Long, RecCol As Long)
    With dbg
        .BeginUpdate
        If RecRow Mod 2 = 0 Then
            .CellBackColor(RecRow, RecCol) = &HEBEBE0
        Else
            .CellBackColor(RecRow, RecCol) = vbWhite
        End If
        .EndUpdate
    End With
End Sub

Public Sub CellBackColorAfterSort(dbg As iGrid)
    Dim RecRow As Long
    Dim RecCol As Long
    
    With dbg
        .BeginUpdate
        For RecRow = 1 To .RowCount
            For RecCol = 1 To .ColCount
                If RecRow Mod 2 = 0 Then
                    .CellBackColor(RecRow, RecCol) = &HEBEBE0
                Else
                    .CellBackColor(RecRow, RecCol) = vbWhite
                End If
            Next RecCol
            DoEvents
        Next RecRow
        .EndUpdate
    End With
End Sub

Public Sub SetWidthCols(frm_name As String, dbg As iGrid, dbgName As String)
    Dim field() As String
    Dim str1 As String
    Dim str2 As String
    Dim Lebar() As Integer
    Dim RecCol As Integer
    Dim i As Integer
    
    str1 = INILanRead(frm_name, dbgName & "_ColWidth", "", "", "", "")
    If str1 = dbgName & "_ColWidth" Then Exit Sub
    
    field = Split(str1, ";")
    ReDim Lebar(UBound(field) - 1)
    For i = 0 To UBound(field) - 1
        str2 = field(i)
        Lebar(i) = Val(str2)
    Next
    
    With dbg
        .BeginUpdate
        For RecCol = 1 To .ColCount
            If RecCol <= UBound(field) Then
                .ColWidth(RecCol) = Lebar(RecCol - 1)
            End If
        Next RecCol
        .EndUpdate
    End With
End Sub

Public Sub CentangPilihan(dbg As iGrid, pilihan As Integer)
    Dim RecRow As Long
    Dim RecCol As Long
    
    With dbg
        .BeginUpdate
        For RecRow = 1 To .RowCount
            For RecCol = 1 To .ColCount
                If pilihan = 1 Then 'Centang semua
                    .CellCheckState(RecRow, RecCol) = igCheckStateChecked
                ElseIf pilihan = 2 Then 'Tidak centang semua
                    .CellCheckState(RecRow, RecCol) = igCheckStateUnchecked
                ElseIf pilihan = 3 Then 'Kebalikan
                    If .CellCheckState(RecRow, RecCol) = igCheckStateChecked Then
                        .CellCheckState(RecRow, RecCol) = igCheckStateUnchecked
                    Else
                        .CellCheckState(RecRow, RecCol) = igCheckStateChecked
                    End If
                End If
            Next RecCol
        Next RecRow
        .EndUpdate
    End With
End Sub

Private Function InsertGridInfo(ByRef grid As iGrid) As String
    On Error GoTo BugError
    
    Dim temp As String
    Dim i As Integer
    Dim J As Integer
    
    J = grid.ColCount
    For i = 1 To J
        If Not IsMissing(grid.ColTag(i)) Then temp = temp & grid.ColTag(i) & ",1," & CStr(i) & ",130;"
    Next
        
    'Insert ke database
    ExecuteSQLite_Data "INSERT INTO t_grid_view (id_grid,id_user,view_value) VALUES('" & grid.Name & "." & grid.Index & "','" & UserID & "','" & temp & "')"
        
    InsertGridInfo = temp
    
    Exit Function
        
BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Function

Private Function GetGridInfo(ByRef grid As iGrid) As String
    On Error GoTo BugError
    
    Dim rs As cRecordset
    Dim vValue As String
    
    Set rs = getRecordSet_Data("SELECT view_value FROM t_grid_view WHERE id_grid = '" & grid.Name & "." & grid.Index & "' AND id_user = '" & UserID & "'", True)
    
    If rs.RecordCount = 0 Then
        vValue = InsertGridInfo(grid)
    Else
        vValue = rs.fields("view_value").value
    End If
    
    GetGridInfo = vValue
    
    Exit Function
        
BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Function

Private Sub SetGridInfo(ByRef grid As iGrid)
    On Error GoTo BugError
    
    Dim i As Integer
    Dim J As Integer
    Dim field() As String
    Dim isiField() As String

    field = Split(grid.Tag, ";")
    J = UBound(field) - 1
    For i = 0 To J
        isiField = Split(field(i), ",")
        grid.ColPos(isiField(0)) = isiField(2)
        grid.ColWidth(isiField(0)) = isiField(3)
        grid.ColVisible(isiField(0)) = isiField(1)
        grid.ContextMenuCustomItems(igContextMenuColHeader).ItemChecked(i + 2) = isiField(1)
    Next
    
    Exit Sub
        
BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Sub

Public Sub GridColCombo(ByRef grid As iGrid, ComboName As String, ComboDataType As String)
    On Error GoTo BugError
    
    Dim rs As cRecordset
    strSQL = "SELECT value_combo, text_combo FROM t_combo_text WHERE name_combo = '" & LCase(ComboName) & "'"
    Set rs = getLanguageRecordSet(strSQL, True)
    grid.Combos.Add ComboName
    ComboDataType = LCase(ComboDataType)
    
    If ComboDataType = "1" Then
        Do While Not rs.EOF
            grid.Combos(ComboName).AddItem rs.fields("text_combo").value, CInt(rs.fields("value_combo").value)
            rs.MoveNext
        Loop
    ElseIf ComboDataType = "3" Then
        Do While Not rs.EOF
            grid.Combos(ComboName).AddItem rs.fields("text_combo").value, CStr(rs.fields("value_combo").value)
            rs.MoveNext
        Loop
    ElseIf ComboDataType = "byte" Then
        Do While Not rs.EOF
            grid.Combos(ComboName).AddItem rs.fields("text_combo").value, CByte(rs.fields("value_combo").value)
            rs.MoveNext
        Loop
    End If
    
    Exit Sub
        
BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Sub

Public Sub GridColComboTable(ByRef grid As iGrid, ComboName As String, ItemText As String, ItemValue As String, ItemTable As String, ItemWhere As String)
    On Error GoTo BugError
    
    Dim rs As cRecordset
    
    If ItemWhere <> "" Then
        strSQL = "SELECT " & ItemText & "," & ItemValue & " FROM " & ItemTable & " WHERE " & ItemWhere & " = '" & LCase(ComboName) & "' ORDER BY " & ItemValue
    Else
        strSQL = "SELECT " & ItemText & "," & ItemValue & " FROM " & ItemTable & " ORDER BY " & ItemValue
    End If
    
    Set rs = getRecordSet_Data(strSQL, True)
    
    grid.Combos.Add ComboName
    
    Do While Not rs.EOF
        grid.Combos(ComboName).AddItem rs.fields(ItemText).value, CInt(rs.fields(ItemValue).value)
        rs.MoveNext
    Loop
    
    Exit Sub
        
BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Sub

Public Sub loadDataEmployeeToGrid(ByRef grid As iGrid, IDField As String, rs As cRecordset, ColumnsTitle As String, _
    Optional GridEditable As Boolean = False, Optional UseSort As Boolean = True)
    
    On Error GoTo BugError
    
    Dim i As Integer
    Dim J As Integer
    Dim k As Integer
    
    grid.Clear
    grid.BeginUpdate
    grid.Editable = GridEditable
    
    J = rs.fields.count - 1
    
    If grid.ColCount = 0 Then
        Dim Title() As String
        Title = Split(ColumnsTitle, ",")
    
        k = 1
        grid.ContextMenuCustomItems(igContextMenuColHeader).Add INILanRead("General", "ShowAllColumnsOnGrid", "Tampilkan Semua")
        For i = 0 To J - 1
            grid.AddCol rs.fields(k).Name, Title(i)
            grid.ColTag(rs.fields(k).Name) = rs.fields(k).Name
            grid.ContextMenuCustomItems(igContextMenuColHeader).Add Title(i), True, 1
            grid.ColHeaderAlignH(k) = igAlignHCenter
            grid.ColHeaderAlignV(k) = igAlignVCenter
            k = k + 1
        Next
        grid.Tag = GetGridInfo(grid)
        SetGridInfo grid
    End If
    
    Dim l As Integer
    Dim m As Integer
    
    If rs.RecordCount > 0 Then 'And grid.Combos.Count = 0 Then
        l = rs.fields.count - 1
        For m = 0 To l
            Select Case LCase(rs.fields(m).Name)
            Case "gender", "status_employee", "marital", "last_education", "religion", "type_blood", "qty_children", "dayname", "comm_type", _
                "component_type", "reimbursement", "calculation", "use_formula", "active", "id_ta_status"
                GridColCombo grid, rs.fields(m).Name, rs.fields(m).ColumnType
                grid.ColDefaultCell(m).eType = igCellCombo
                grid.ColDefaultCell(m).sCtrlKey = rs.fields(m).Name
            Case "id_office"
                GridColComboTable grid, rs.fields(m).Name, "name_office", "id_office", "t_office", ""
                grid.ColDefaultCell(m).eType = igCellCombo
                grid.ColDefaultCell(m).sCtrlKey = rs.fields(m).Name
            Case "id_position"
                GridColComboTable grid, rs.fields(m).Name, "name_position", "id_position", "t_position", ""
                grid.ColDefaultCell(m).eType = igCellCombo
                grid.ColDefaultCell(m).sCtrlKey = rs.fields(m).Name
            Case "id_schedule"
                GridColComboTable grid, rs.fields(m).Name, "name_schedule", "id_schedule", "t_schedule", ""
                grid.ColDefaultCell(m).eType = igCellCombo
                grid.ColDefaultCell(m).sCtrlKey = rs.fields(m).Name
            End Select
        Next
    End If
    
    k = 1
    If rs.RecordCount > 0 Then
        Do While Not rs.EOF
            grid.AddRow
            grid.RowTag(k) = rs.fields(IDField).value
            For i = 1 To J
                Select Case LCase(rs.fields(i).Name)
                Case "pilih"
                    grid.CellCheckVisible(k, i) = True
                Case "nominal", "nominaltotal"
                    grid.CellFmtString(k, i) = "Rp #,##0;;"""""
                Case "start_date", "scan_date", "ta_date", "from_date", "to_date", "date_holiday", "scandate"
                    grid.CellFmtString(k, i) = DateFormat
                Case "scantime"
                    grid.CellFmtString(k, i) = TimeFormat
                Case "date_log"
                    grid.CellFmtString(k, i) = DateTimeFormat
                End Select
                
                grid.CellValue(k, grid.ColTag(i)) = rs.fields(i).value
                grid.RowHeight(k) = RowHeightGrid
                grid.CellAlignV(k, i) = igAlignVCenter
            Next
            k = k + 1
            rs.MoveNext
        Loop
    End If
    
    Dim SortCol As Integer
    If UseSort Then
        SortCol = GetGridSortColumn(grid.Name, grid.Index)
        grid.Sort SortCol
    End If
    
    With grid.Header
        .UseXPStyles = False
        .Flat = True
        .Height = HeaderHeightGrid
        '.Font.Size = 10
        .BackColor = GRID_HEADER_BACKCOLOR
        .HotTrackForeColor = GRID_HEADER_FORECOLOR
        .ForeColor = vbWhite
        'sorting disabled
        .Buttons = False
        .SortInfoStyle = igSortInfoNone
    End With
    
    grid.BackColorEvenRows = GRID_EVENT_ROWS_BACKCOLOR
    
    grid.EndUpdate
    
    Exit Sub
        
BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Sub

Private Function GetGridSortColumn(gridname As String, gridindex As String) As String
    Dim rs As cRecordset
    Dim vValue As String
    
    Set rs = getRecordSet_Data("SELECT sort_column FROM t_grid_view WHERE id_grid = '" & gridname & "." & gridindex & "' AND id_user = '" & UserID & "'", True)
    
    If rs.RecordCount = 1 Then GetGridSortColumn = rs.fields("sort_column").value
End Function

Public Function checkDBFileExits(DBFile As String, DBName As String) As Boolean
    If Dir$(DBFile) = "" Then
        MsgBoxLanguageFile "Err1", DBName
        End
    Else
        checkDBFileExits = True
    End If
End Function

Public Function getLanguageRecordSet(strSQL As String, Optional ReadOnly As Boolean = True) As cRecordset
    On Error GoTo Err1
    If checkDBFileExits(DB_LANGUANGE, DB_LANGUANGE_NAME) Then
        Dim Cnn As cConnection
        Dim rs As cRecordset
    
        Set Cnn = New_c.connection
        Cnn.OpenDB DB_LANGUANGE, PassDB
        
        Set rs = Cnn.OpenRecordset(strSQL, ReadOnly)
        Set getLanguageRecordSet = rs
        Set Cnn = Nothing
    End If
    
    Exit Function
Err1:
    'If DevelopmentMode Then MsgBox strSQL
    Set rs = Nothing
    Err.Clear
    Resume Next
End Function
