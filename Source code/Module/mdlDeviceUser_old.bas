Attribute VB_Name = "mdlDeviceUser"
Option Explicit

Private CnTemp As cConnection
Private mnCommHandleIndex As Long
Private mbGetState As Boolean
Private optConn As Integer

Private last_connect As Date
Private flagDev As String
Private pinTemp As String
Private jmlEnroll As Integer
Private count_user As Integer
Private strTmpPwdDefault As String
Private strTmpPwd As String
Private strTmpPwd2 As String
Private strTmpCard As String
Public isValidation As Boolean

Private gPassEncryptKey As Long
Private OnBits(0 To 31) As Long

Const PWD_DATA_SIZE = 40
Const FP_DATA_SIZE = 1680
Const FACE_DATA_SIZE = 20080
Const VEIN_DATA_SIZE = 3080
Const PALMVEIN_DATA_SIZE = 20080
Private mbytEnrollData(FACE_DATA_SIZE - 1) As Byte
Private mlngPasswordData As Long

Private list_tgl_upload() As String
Private execRecCount As Integer
Private Bulan1 As String
Private Bulan2 As String
Private SQL1 As String
Private SQL2 As String
Private SQL_1 As String

Public HapusDulu As Boolean
Public pinUnreg As String
Public pinUnregMesin As String
Public pinUnSetName As String
Public pinUnSetNameMesin As String

Public strSQL As String
Public CnAttlog As cConnection
Public MaxAttLogDL As Integer

Public Const vnLicense = 1261
Public Const Angka As String = "0123456789"

Public EnrollDataDefault() As Byte

Public gbOpenFlag As Boolean
Public gnCommHandleIndex As Long
Public FKFirmware As String
Public GetDevInfo As Boolean

Public PathDBAttlog As String

Public vnname_device As String
Public vnactivation_code As String
Public vnstatus_aktif As String

Public vnMachineNumber As Long
Public vnCommPort As Long
Public vnCommBaudrate As Long
Public vstrTelNumber As String
Public vnWaitDialTime As Long
Public vpszIPAddress As String
Public vpszNetPort As Long
Public vpszNetPassword As Long
Public vnTimeOut As Long
Public vnProtocolType As Long
Public TCPIPFlag As Boolean

Public Function InitDevice(SN As String, ByRef optConn As Integer, useFD As Boolean) As Boolean
    On Error GoTo BugError
       
    InitDevice = False
    
    Dim RecstAtt As New cRecordset
    Dim id_group As Integer
    Dim master_group As String
    
    strSQL = "SELECT d.*, dt.firmware FROM t_device AS d " & _
        "LEFT JOIN t_device_type AS dt ON dt.id_device_type = d.id_device_type " & _
        "WHERE comm_type < 4 AND sn_device = """ & SN & """ "
    Set RecstAtt = getRecordSet_Data(strSQL, True)
        
    If RecstAtt.RecordCount = 0 Then
        Exit Function
    End If
    
    Do While Not RecstAtt.EOF
        vnname_device = RecstAtt.Fields("name_device").value
        vnactivation_code = RecstAtt.Fields("active_code").value
        optConn = RecstAtt.Fields("comm_type").value
        vnMachineNumber = RecstAtt.Fields("dev_id").value
        vpszNetPassword = RecstAtt.Fields("comm_key").value
        vpszIPAddress = RecstAtt.Fields("ip_address").value
        vpszNetPort = RecstAtt.Fields("ethernet_port").value
        vnCommPort = RecstAtt.Fields("serial_port").value
        vnCommBaudrate = RecstAtt.Fields("baud_rate").value
        FKFirmware = RecstAtt.Fields("firmware").value
        id_group = RecstAtt.Fields("id_group").value
        master_group = RecstAtt.Fields("master_group").value
        vnstatus_aktif = RecstAtt.Fields("status_aktif").value
        
        RecstAtt.MoveNext
    Loop
    
    InitDevice = True
    
    Exit Function
        
BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Function

Public Function cmdOpenComm(optConn As Integer) As Boolean
    On Error GoTo BugError
    
    Dim vnResultCode As Long
    Dim last_connect As Date
    
    If optConn = 1 Then 'NetworkDevice
        vnTimeOut = 5000
        TCPIPFlag = True
        
        If TCPIPFlag Then
            vnProtocolType = PROTOCOL_TCPIP
        Else
            vnProtocolType = PROTOCOL_UDP
        End If
        gnCommHandleIndex = FK_ConnectNet(vnMachineNumber, vpszIPAddress, vpszNetPort, vnTimeOut, vnProtocolType, vpszNetPassword, vnLicense)
    ElseIf optConn = 2 Then 'USBDevice
        gnCommHandleIndex = FK_ConnectUSB(vnMachineNumber, vnLicense)
    ElseIf optConn = 3 Then 'SerialDevice
        vstrTelNumber = ""
        vnWaitDialTime = 0
        
        vnTimeOut = 5000
        gnCommHandleIndex = FK_ConnectComm(vnMachineNumber, vnCommPort, vnCommBaudrate, vstrTelNumber, vnWaitDialTime, vnLicense, vnTimeOut)
    End If
    
    If gnCommHandleIndex > 0 Then
        gbOpenFlag = True
        cmdOpenComm = True
        last_connect = Now
    Else
        vnResultCode = gnCommHandleIndex
        gbOpenFlag = False
        cmdOpenComm = False
    End If
    
    Exit Function
        
BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Function

Public Sub cmdCloseComm()
    On Error GoTo BugError
    
    If gbOpenFlag = True Then
        FK_DisConnect gnCommHandleIndex
        gbOpenFlag = False
    End If
    
    Exit Sub
        
BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Sub

Public Function GetDeviceStatus(IndexStatus As Integer, mnCommHandleIndex As Long, ByRef strValue As String, _
    ByRef intValue As Long) As Boolean
    On Error GoTo BugError
    
    Dim vnStatusIndex As Long
    Dim vnValue As Long
    Dim vnResultCode As Long

    intValue = 0
    GetDeviceStatus = False
    DoEvents

    vnResultCode = FK_EnableDevice(mnCommHandleIndex, 0)
    If vnResultCode <> RUN_SUCCESS Then
        'DebugText gstrNoDevice
        Exit Function
    End If

    vnStatusIndex = IndexStatus + 1
    vnResultCode = FK_GetDeviceStatus(mnCommHandleIndex, vnStatusIndex, vnValue)
    If vnResultCode = RUN_SUCCESS Then
        GetDeviceStatus = True
        
        intValue = vnValue
        Select Case vnStatusIndex
            Case GET_MANAGERS:  strValue = "Manager count = " & vnValue
            Case GET_USERS:  strValue = "User count = " & vnValue
            Case GET_FPS:  strValue = "Fp count = " & vnValue
            Case GET_PSWS:  strValue = "Password count = " & vnValue
            Case GET_SLOGS:  strValue = "SLog count = " & vnValue
            Case GET_GLOGS:  strValue = "GLog count = " & vnValue
            Case GET_ASLOGS:  strValue = "All SLog count = " & vnValue
            Case GET_AGLOGS:  strValue = "All GLog count = " & vnValue
            Case GET_CARDS:  strValue = "Card count = " & vnValue
            Case GET_FACES:  strValue = "Face count = " & vnValue
            Case Else: strValue = "--"
        End Select
    Else
        strValue = ReturnResultPrint(vnResultCode)
    End If

    FK_EnableDevice mnCommHandleIndex, 1
    
    Exit Function
        
BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Function

Public Function GetDeviceInfo(IndexStatus As Integer, mnCommHandleIndex As Long, ByRef strValue As String, _
    ByRef intValue As Long) As Boolean
    On Error GoTo BugError
    
    Dim vnInfoIndex As Long
    Dim vnValue As Long
    Dim vnResultCode As Long

    intValue = 0
    GetDeviceInfo = False
    DoEvents

    vnResultCode = FK_EnableDevice(mnCommHandleIndex, 0)
    If vnResultCode <> RUN_SUCCESS Then
        'DebugText = gstrNoDevice
        Exit Function
    End If

    vnInfoIndex = IndexStatus + 1
    If vnInfoIndex = 11 Then
        vnInfoIndex = DI_VERIFY_KIND
    ElseIf vnInfoIndex = 12 Then
        vnInfoIndex = DI_MULTIUSERS
    End If
    
    vnResultCode = FK_GetDeviceInfo(mnCommHandleIndex, vnInfoIndex, vnValue)
    If vnResultCode = RUN_SUCCESS Then
        GetDeviceInfo = True
        
        intValue = vnValue
        Select Case vnInfoIndex
            Case DI_MANAGERS:  strValue = "ManagerCount = " & vnValue
            Case DI_MACHINENUM:  strValue = "Machine Num = " & vnValue
            Case DI_LANGAUGE:  strValue = "Language = " & vnValue
            Case DI_POWEROFF_TIME:  strValue = "PowerOffTime = " & vnValue
            Case DI_LOCK_CTRL:  strValue = "LockOperate = " & vnValue
            Case DI_GLOG_WARNING:  strValue = "GLogWarning = " & vnValue
            Case DI_SLOG_WARNING:  strValue = "SLogWarning = " & vnValue
            Case DI_VERIFY_INTERVALS:  strValue = "ReVerifyTime = " & vnValue
            Case DI_RSCOM_BPS:  strValue = "Baudrate(" & vnValue & ") : "
                If vnValue = BPS_9600 Then
                    strValue = strValue & "9600"
                ElseIf vnValue = BPS_19200 Then
                    strValue = strValue & "19200"
                ElseIf vnValue = BPS_38400 Then
                    strValue = strValue & "38400"
                ElseIf vnValue = BPS_57600 Then
                    strValue = strValue & "57600"
                ElseIf vnValue = BPS_115200 Then
                    strValue = strValue & "115200"
                Else
                    strValue = strValue & "--"
                End If
            Case DI_VERIFY_KIND: strValue = "VerifyKind = "
                If vnValue = 0 Then
                    strValue = strValue & "F / P / C"
                ElseIf vnValue = 1 Then
                    strValue = strValue & "F + P"
                ElseIf vnValue = 2 Then
                    strValue = strValue & "F + C"
                ElseIf vnValue = 3 Then
                    strValue = strValue & "C"
                End If
            Case DI_DATE_SEPARATE: strValue = "DateSeperate = " & vnValue
            Case DI_MULTIUSERS: strValue = "MultiUsers = " & vnValue
            Case Else: strValue = "--"
        End Select
    Else
        strValue = ReturnResultPrint(vnResultCode)
    End If

    FK_EnableDevice mnCommHandleIndex, 1
    
    Exit Function
        
BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Function

Public Function GetDeviceTime(ByRef device_time As Date, mnCommHandleIndex As Long) As Boolean
    On Error GoTo BugError
    
    Dim vdwDate As Date
    Dim strDataTime As String
    Dim vnResultCode As Long

    GetDeviceTime = False
    DoEvents

    vnResultCode = FK_EnableDevice(mnCommHandleIndex, 0)
    If vnResultCode <> RUN_SUCCESS Then
        'DebugText = gstrNoDevice
        Exit Function
    End If

    vnResultCode = FK_GetDeviceTime(mnCommHandleIndex, vdwDate)
    If vnResultCode = RUN_SUCCESS Then
        GetDeviceTime = True
        device_time = vdwDate
        
        strDataTime = "GetDeviceDateTime = " & Format(vdwDate, "Long Date") & ", Time = " & Format(vdwDate, "Long Time")
    End If

    FK_EnableDevice mnCommHandleIndex, 1
    
    Exit Function
        
BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Function

Public Function DeviceInfo(SN As String, mnCommHandleIndex As Long) As Boolean
    On Error GoTo BugError
       
    DeviceInfo = False
    Dim i As Integer
    Dim j As Integer
    Dim strValue As String
    Dim intValue As Long
    
    Dim manager_count As Long
    Dim user_count As Long
    Dim fp_count As Long
    Dim pwd_count As Long
    Dim card_count As Long
    Dim face_count As Long
    Dim new_attlog_count As Long
    Dim all_attlog_count As Long
    Dim interval_scan As Long
    Dim combination_verify As String
    Dim dateStr As String
    Dim device_time As Date
    Dim device_timeStr As String
    Dim count_new_attlog As Integer
    
    manager_count = 0
    user_count = 0
    fp_count = 0
    pwd_count = 0
    card_count = 0
    face_count = 0
    new_attlog_count = 0
    all_attlog_count = 0
    interval_scan = 0
    combination_verify = ""
    count_new_attlog = 0
    last_connect = Now
    
    strSQL = "REPLACE INTO t_device_info (sn_device, manager_count, user_count, " & _
        "fp_count, pwd_count, card_count, face_count, new_attlog_count, all_attlog_count, " & _
        "interval_scan, combination_verify, device_time, last_connect) "
    For i = 0 To 9
        If GetDeviceStatus(i, mnCommHandleIndex, strValue, intValue) Then
            j = i + 1
            Select Case j
                Case GET_MANAGERS: manager_count = intValue
                Case GET_USERS: user_count = intValue
                Case GET_FPS: fp_count = intValue
                Case GET_PSWS: pwd_count = intValue
                Case GET_SLOGS: intValue = 0
                Case GET_GLOGS: new_attlog_count = intValue
                Case GET_ASLOGS: intValue = 0
                Case GET_AGLOGS: all_attlog_count = intValue
                Case GET_CARDS: card_count = intValue
                Case GET_FACES: face_count = intValue
                Case Else: intValue = 0
        End Select
        End If
    Next
    count_new_attlog = new_attlog_count
    count_user = user_count
    
    If GetDeviceInfo(7, mnCommHandleIndex, strValue, intValue) Then
        interval_scan = intValue
    End If
    
    If GetDeviceInfo(10, mnCommHandleIndex, strValue, intValue) Then
        If intValue = 0 Then
            combination_verify = "F / P / C"
        ElseIf intValue = 1 Then
            combination_verify = "F + P"
        ElseIf intValue = 2 Then
            combination_verify = "F + C"
        ElseIf intValue = 3 Then
            combination_verify = "C"
        End If
    End If
    
    GetDeviceTime device_time, mnCommHandleIndex
    device_timeStr = DateTimeToStr(device_time, DateTimeFormatDatabase)
    dateStr = DateTimeToStr(last_connect, DateTimeFormatDatabase)
    
    strSQL = strSQL & "VALUES (""" & SN & """, " & manager_count & ", " & user_count & ", " & _
        fp_count & ", " & pwd_count & ", " & card_count & ", " & face_count & ", " & _
        new_attlog_count & ", " & all_attlog_count & ", " & interval_scan & ", """ & _
        combination_verify & """, """ & device_timeStr & """, """ & dateStr & """) "
    ExecuteSQLite_Data strSQL
    
    DeviceInfo = True
    
    Exit Function
        
BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Function

Private Sub SaveTemplate(SN As String, PIN As String, anBackupNumber As Long, vbytEnrollData() As Byte, _
    rs_template As cRecordset)
    On Error GoTo BugError

    rs_template.AddNew
    rs_template!sn_device = SN
    rs_template!PIN = Trim(PIN)
    rs_template!template_index = anBackupNumber
    rs_template!template_blob = vbytEnrollData
    rs_template!last_update = last_connect
    
    Exit Sub

BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Sub

Private Sub FuncSaveTemplate(SN As String, PIN As String, anBackupNumber As Long, mnCommHandleIndex As Long, _
    rs_template As cRecordset)
    On Error GoTo BugError
    
    Dim vnii As Long
    Dim vnLen As Long
    Dim vbytEnrollData() As Byte
    Dim FPData() As Byte
    
    Dim sama As Boolean
    Dim strTmp As String
    Dim x As Integer
    Dim i As Integer
    Dim pwd As String
    
    sama = False
    If anBackupNumber = BACKUP_PSW Then
        ReDim vbytEnrollData(PWD_DATA_SIZE - 1)
        
        'DecryptPwd vbytEnrollData, mbytEnrollData, PWD_DATA_SIZE
        'strTmp = GetAscii(vbytEnrollData, 0, PWD_DATA_SIZE - 1)
        
        CopyMemory vbytEnrollData(0), mbytEnrollData(0), PWD_DATA_SIZE
        strTmp = ShowByteArrayToString(vbytEnrollData, PWD_DATA_SIZE)
        If strTmp = strTmpPwd Or strTmp = strTmpPwdDefault Or strTmp = strTmpPwd2 Then
            sama = True
        End If
        
        'DecryptPwd(apEncPwdData() As Byte, ByRef apOrgPwdData() As Byte, aPwdLen As Integer)
        'ReDim FPData(PWD_DATA_SIZE - 1)
        'DecryptPwd vbytEnrollData, FPData, PWD_DATA_SIZE
        'pwd = ByteArrayToString(vbytEnrollData)
        'pwd = getAngka(pwd, False)
    ElseIf anBackupNumber = BACKUP_CARD Then
        ReDim vbytEnrollData(PWD_DATA_SIZE - 1)
        CopyMemory vbytEnrollData(0), mbytEnrollData(0), PWD_DATA_SIZE
        
        strTmp = ShowByteArrayToString(vbytEnrollData, PWD_DATA_SIZE)
        If strTmp = strTmpCard Then
            sama = True
        End If
    ElseIf anBackupNumber >= BACKUP_FP_0 And anBackupNumber <= BACKUP_FP_9 Then
        ReDim vbytEnrollData(FP_DATA_SIZE - 1)
        CopyMemory vbytEnrollData(0), mbytEnrollData(0), FP_DATA_SIZE
    ElseIf anBackupNumber = BACKUP_FACE Then
        ReDim vbytEnrollData(FACE_DATA_SIZE - 1)
        CopyMemory vbytEnrollData(0), mbytEnrollData(0), FACE_DATA_SIZE
    ElseIf anBackupNumber >= BACKUP_PALMVEIN_0 And anBackupNumber <= BACKUP_PALMVEIN_3 Then
        ReDim vbytEnrollData(PALMVEIN_DATA_SIZE - 1)
        CopyMemory vbytEnrollData(0), mbytEnrollData(0), PALMVEIN_DATA_SIZE
    ElseIf anBackupNumber = BACKUP_VEIN_0 Then
        ReDim vbytEnrollData(VEIN_DATA_SIZE - 1)
        CopyMemory vbytEnrollData(0), mbytEnrollData(0), VEIN_DATA_SIZE
    End If
    
    If isValidation Then sama = False
    
    If sama = False Then
        SaveTemplate SN, PIN, anBackupNumber, vbytEnrollData, rs_template
        
        strSQL = "DELETE FROM t_template_log WHERE sn_device = """ & SN & """ AND pin = """ & PIN & """ "
        ExecuteSQLite_Data strSQL
        
        strSQL = "REPLACE INTO t_template_log (sn_device, pin, status, last_update) " & vbNewLine & _
            "VALUES (""" & SN & """, """ & PIN & """, 2, """ & CreatedDate() & """) "
        ExecuteSQLite_Data strSQL
    End If
    
    Exit Sub
        
BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Sub

Public Function GetAllUser(SN As String, mbGetState As Boolean, mnCommHandleIndex As Long) As Boolean
    On Error GoTo BugError
    
    Dim vEnrollNumber As Long
    Dim vStrEnrollNumber As String
    Dim vBackupNumber As Long
    Dim vPrivilege As Long
    Dim vnEnableFlag As Long
    Dim vnMessRet As Long
    Dim vTitle As String
    Dim vnResultCode As Long
    
    GetAllUser = False
    pinTemp = ""
    jmlEnroll = 0
    DoEvents
    
    vnResultCode = FK_EnableDevice(mnCommHandleIndex, 0)
    If vnResultCode <> RUN_SUCCESS Then
        'DebugText gstrNoDevice
        Exit Function
    End If

    vnResultCode = FK_ReadAllUserID(mnCommHandleIndex)
    If vnResultCode <> RUN_SUCCESS Then
        'DebugText ReturnResultPrint(vnResultCode)
        FK_EnableDevice mnCommHandleIndex, 1
        Exit Function
    End If
    
    Dim RecstAtt As cRecordset
    strSQL = "select * from t_template_revo where pin = '0' and template_index = 10 "
    Set RecstAtt = getRecordSet_Data(strSQL, True)
    Do While RecstAtt.EOF = False
        EnrollDataDefault = RecstAtt!template_blob
        RecstAtt.MoveNext
    Loop
    strTmpPwdDefault = ShowByteArrayToString(EnrollDataDefault, PWD_DATA_SIZE)
    strTmpPwd = "0000000000000000317A2B1944DF54D5A10000000000000000000000000000000000000000000000"
    strTmpPwd2 = "00000000000000003030303030303030300000000000000000000000000000000000000000000000"
    strTmpCard = "00000000000000000030303030303030300000000000000000000000000000000000000000000000"
    
    strSQL = "DELETE FROM t_template_revo_new WHERE sn_device = """ & SN & """ "
    ExecuteSQLite_Data strSQL
    strSQL = "DELETE FROM t_template_revo_new WHERE sn_device NOT IN (SELECT sn_device FROM t_device) "
    ExecuteSQLite_Data strSQL
    
    'strSQL = "VACUUM "
    'ExecuteSQLite_Data strSQL
        
    Dim rs_template As cRecordset
    strSQL = "Select * From t_template_revo_new WHERE sn_device = """ & SN & """ "
    Set rs_template = getRecordSet_Data(strSQL, False)
    
'---- Get Enroll data and save into database -------------
    mbGetState = True
    Dim vnResultSupportStringID As Long
    vnResultSupportStringID = FK_GetIsSupportStringID(mnCommHandleIndex)
    If vnResultSupportStringID >= RUN_SUCCESS Then
        Do
            If vnResultSupportStringID = USER_ID_LEN13_1 Then
                vStrEnrollNumber = Space(USER_ID_LEN13_1)
            Else
                vStrEnrollNumber = Space(USER_ID_LEN)
            End If
            
            vnResultCode = FK_GetAllUserID_StringID(mnCommHandleIndex, vStrEnrollNumber, vBackupNumber, vPrivilege, vnEnableFlag)
            If vnResultCode <> RUN_SUCCESS Then
                If vnResultCode = RUNERR_DATAARRAY_END Then
                    vnResultCode = RUN_SUCCESS
                End If
                Exit Do
            End If
            
            vStrEnrollNumber = Trim(vStrEnrollNumber)
            vStrEnrollNumber = getAngka(vStrEnrollNumber, False)
            
            vnResultCode = FK_GetEnrollData_StringID(mnCommHandleIndex, vStrEnrollNumber, vBackupNumber, vPrivilege, _
                mbytEnrollData(0), mlngPasswordData)
        
            If vnResultCode = RUN_SUCCESS Then
                FuncSaveTemplate SN, vStrEnrollNumber, vBackupNumber, mnCommHandleIndex, rs_template
            End If
            
            DoEvents
        Loop
    Else
        Do
            vnResultCode = FK_GetAllUserID(mnCommHandleIndex, vEnrollNumber, vBackupNumber, vPrivilege, vnEnableFlag)
            If vnResultCode <> RUN_SUCCESS Then
                If vnResultCode = RUNERR_DATAARRAY_END Then
                    vnResultCode = RUN_SUCCESS
                End If
                Exit Do
            End If
            
            vStrEnrollNumber = Trim(CStr(vEnrollNumber))
            
            vnResultCode = FK_GetEnrollData(mnCommHandleIndex, vEnrollNumber, vBackupNumber, vPrivilege, mbytEnrollData(0), _
                mlngPasswordData)
        
            If vnResultCode = RUN_SUCCESS Then
                FuncSaveTemplate SN, vStrEnrollNumber, vBackupNumber, mnCommHandleIndex, rs_template
            End If
            
            DoEvents
        Loop
    End If
    mbGetState = False
    DoEvents
    
    If vnResultCode = RUN_SUCCESS Then
        'DebugText txtBox, "GetAllEnrollData OK " & Str(jmlEnroll)
        rs_template.UpdateBatch
        
        GetAllUser = True
    End If

    FK_EnableDevice mnCommHandleIndex, 1
    
    Exit Function
        
BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Function

Public Function GetAllUserTemplate(SN As String, mbGetState As Boolean, mnCommHandleIndex As Long) As Boolean
    On Error GoTo BugError
    
    Dim vEnrollNumber As Long
    Dim vStrEnrollNumber As String
    Dim vBackupNumber As Long
    Dim vPrivilege As Long
    Dim vnEnableFlag As Long
    Dim vnMessRet As Long
    Dim vTitle As String
    Dim vnResultCode As Long
    
    GetAllUserTemplate = False
    pinTemp = ""
    jmlEnroll = 0
    DoEvents
    
    vnResultCode = FK_EnableDevice(mnCommHandleIndex, 0)
    If vnResultCode <> RUN_SUCCESS Then
        'DebugText gstrNoDevice
        Exit Function
    End If

    vnResultCode = FK_ReadAllUserID(mnCommHandleIndex)
    If vnResultCode <> RUN_SUCCESS Then
        'DebugText ReturnResultPrint(vnResultCode)
        FK_EnableDevice mnCommHandleIndex, 1
        Exit Function
    End If
    
    Dim RecstAtt As cRecordset
    strSQL = "select * from t_template_revo where pin = '0' and template_index = 10 "
    Set RecstAtt = getRecordSet_Data(strSQL, True)
    Do While RecstAtt.EOF = False
        EnrollDataDefault = RecstAtt!template_blob
        RecstAtt.MoveNext
    Loop
    strTmpPwdDefault = ShowByteArrayToString(EnrollDataDefault, PWD_DATA_SIZE)
    strTmpPwd = "0000000000000000317A2B1944DF54D5A10000000000000000000000000000000000000000000000"
    strTmpPwd2 = "00000000000000003030303030303030000000000000000000000000000000000000000000000000"
    strTmpCard = "00000000000000000030303030303030300000000000000000000000000000000000000000000000"
    
    strSQL = "DELETE FROM t_template_revo_new WHERE sn_device = """ & SN & """ "
    ExecuteSQLite_Data strSQL
    strSQL = "DELETE FROM t_template_revo_new WHERE sn_device NOT IN (SELECT sn_device FROM t_device) "
    ExecuteSQLite_Data strSQL
    
    'strSQL = "VACUUM "
    'ExecuteSQLite_Data strSQL
        
    Dim rs_template As cRecordset
    strSQL = "Select * From t_template_revo_new WHERE sn_device = """ & SN & """ "
    Set rs_template = getRecordSet_Data(strSQL, False)
    
'---- Get Enroll data and save into database -------------
    mbGetState = True
    Dim vnResultSupportStringID As Long
    vnResultSupportStringID = FK_GetIsSupportStringID(mnCommHandleIndex)
    If vnResultSupportStringID >= RUN_SUCCESS Then
        Do
            If vnResultSupportStringID = USER_ID_LEN13_1 Then
                vStrEnrollNumber = Space(USER_ID_LEN13_1)
            Else
                vStrEnrollNumber = Space(USER_ID_LEN)
            End If
            
            vnResultCode = FK_GetAllUserID_StringID(mnCommHandleIndex, vStrEnrollNumber, vBackupNumber, vPrivilege, vnEnableFlag)
            If vnResultCode <> RUN_SUCCESS Then
                If vnResultCode = RUNERR_DATAARRAY_END Then
                    vnResultCode = RUN_SUCCESS
                End If
                Exit Do
            End If
            
            vStrEnrollNumber = Trim(vStrEnrollNumber)
            vStrEnrollNumber = getAngka(vStrEnrollNumber, False)
            
            If InStr(pinUnreg, "#" & vStrEnrollNumber & "#") > 0 Then
                vnResultCode = FK_GetEnrollData_StringID(mnCommHandleIndex, vStrEnrollNumber, vBackupNumber, vPrivilege, _
                    mbytEnrollData(0), mlngPasswordData)
            
                If vnResultCode = RUN_SUCCESS Then
                    FuncSaveTemplate SN, vStrEnrollNumber, vBackupNumber, mnCommHandleIndex, rs_template
                End If
            End If
            
            DoEvents
        Loop
    Else
        Do
            vnResultCode = FK_GetAllUserID(mnCommHandleIndex, vEnrollNumber, vBackupNumber, vPrivilege, vnEnableFlag)
            If vnResultCode <> RUN_SUCCESS Then
                If vnResultCode = RUNERR_DATAARRAY_END Then
                    vnResultCode = RUN_SUCCESS
                End If
                Exit Do
            End If
            
            vStrEnrollNumber = Trim(CStr(vEnrollNumber))
            
            If InStr(pinUnreg, "#" & vStrEnrollNumber & "#") > 0 Then
                vnResultCode = FK_GetEnrollData(mnCommHandleIndex, vEnrollNumber, vBackupNumber, vPrivilege, mbytEnrollData(0), _
                    mlngPasswordData)
            
                If vnResultCode = RUN_SUCCESS Then
                    FuncSaveTemplate SN, vStrEnrollNumber, vBackupNumber, mnCommHandleIndex, rs_template
                End If
            End If
            
            DoEvents
        Loop
    End If
    mbGetState = False
    DoEvents
    
    If vnResultCode = RUN_SUCCESS Then
        'DebugText txtBox, "GetAllEnrollData OK " & Str(jmlEnroll)
        rs_template.UpdateBatch
        
        GetAllUserTemplate = True
    End If

    FK_EnableDevice mnCommHandleIndex, 1
    
    Exit Function
        
BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Function

Public Function GetUserUnReg(SN As String, mbGetState As Boolean, mnCommHandleIndex As Long) As Boolean
    On Error GoTo BugError
    
    Dim field() As String
    Dim str1 As String
    Dim str2 As String
    Dim i As Integer
    Dim j As Integer
    Dim ada As Boolean
    
    Dim vEnrollNumber As Long
    Dim vStrEnrollNumber As String
    Dim vBackupNumber As Long
    Dim vPrivilege As Long
    Dim vnEnableFlag As Long
    Dim vnMessRet As Long
    Dim vnResultCode As Long
    
    GetUserUnReg = False
    DoEvents
    
    vnResultCode = FK_EnableDevice(mnCommHandleIndex, 0)
    If vnResultCode <> RUN_SUCCESS Then
        Exit Function
    End If

    Dim RecstAtt As cRecordset
    strSQL = "select * from t_template_revo where pin = '0' and template_index = 10 "
    Set RecstAtt = getRecordSet_Data(strSQL, True)
    Do While RecstAtt.EOF = False
        EnrollDataDefault = RecstAtt!template_blob
        RecstAtt.MoveNext
    Loop
    strTmpPwdDefault = ShowByteArrayToString(EnrollDataDefault, PWD_DATA_SIZE)
    strTmpPwd = "0000000000000000317A2B1944DF54D5A10000000000000000000000000000000000000000000000"
    strTmpPwd2 = "00000000000000003030303030303030300000000000000000000000000000000000000000000000"
    strTmpCard = "00000000000000000030303030303030300000000000000000000000000000000000000000000000"
    
    strSQL = "DELETE FROM t_template_revo_new WHERE sn_device = """ & SN & """ "
    ExecuteSQLite_Data strSQL
    strSQL = "DELETE FROM t_template_revo_new WHERE sn_device NOT IN (SELECT sn_device FROM t_device) "
    ExecuteSQLite_Data strSQL
    
    'strSQL = "VACUUM "
    'ExecuteSQLite_Data strSQL
        
    Dim rs_template As cRecordset
    strSQL = "Select * From t_template_revo_new WHERE sn_device = """ & SN & """ "
    Set rs_template = getRecordSet_Data(strSQL, False)
    
    ada = False
    field = Split(pinUnregMesin & ",", ",")
    Dim vnResultSupportStringID As Long
    
    vnResultSupportStringID = FK_GetIsSupportStringID(mnCommHandleIndex)
    If vnResultSupportStringID >= RUN_SUCCESS Then
        If vnResultSupportStringID = USER_ID_LEN13_1 Then
            vStrEnrollNumber = Space(USER_ID_LEN13_1)
        Else
            vStrEnrollNumber = Space(USER_ID_LEN)
        End If
        
        For i = 0 To UBound(field) - 1
            str1 = field(i)
            If Trim(str1) <> "" And Len(Trim(str1)) <= 9 Then
                vStrEnrollNumber = str1
                For j = 0 To 9
                    vBackupNumber = j
                    vnResultCode = FK_GetEnrollData_StringID(mnCommHandleIndex, vStrEnrollNumber, vBackupNumber, vPrivilege, mbytEnrollData(0), mlngPasswordData)
                    If vnResultCode = RUN_SUCCESS Then
                        frmWizard.TulisLog INILanRead("errMsg", "msgLog4", "", vStrEnrollNumber, CStr(vBackupNumber), SN)
                        
                        FuncSaveTemplate SN, vStrEnrollNumber, vBackupNumber, mnCommHandleIndex, rs_template
                        ada = True
                    Else
                        Exit For
                    End If
                Next
                
                For j = BACKUP_PSW To BACKUP_PALMVEIN_3
                    vBackupNumber = j
                    vnResultCode = FK_GetEnrollData_StringID(mnCommHandleIndex, vStrEnrollNumber, vBackupNumber, vPrivilege, mbytEnrollData(0), mlngPasswordData)
                    If vnResultCode = RUN_SUCCESS Then
                        frmWizard.TulisLog INILanRead("errMsg", "msgLog4", "", vStrEnrollNumber, CStr(vBackupNumber), SN)
                        
                        FuncSaveTemplate SN, vStrEnrollNumber, vBackupNumber, mnCommHandleIndex, rs_template
                        ada = True
                    Else
                        If j >= BACKUP_PALMVEIN_1 Then
                            Exit For
                        End If
                    End If
                Next
            End If
            DoEvents
        Next
    Else
        For i = 0 To UBound(field) - 1
            str1 = field(i)
            If Trim(str1) <> "" And Len(Trim(str1)) <= 8 Then
                vEnrollNumber = Val(str1)
                
                For j = 0 To 9
                    vBackupNumber = j
                    vnResultCode = FK_GetEnrollData(mnCommHandleIndex, vEnrollNumber, vBackupNumber, vPrivilege, mbytEnrollData(0), mlngPasswordData)
                    If vnResultCode = RUN_SUCCESS Then
                        frmWizard.TulisLog INILanRead("errMsg", "msgLog4", "", vStrEnrollNumber, CStr(vBackupNumber), SN)
                        
                        FuncSaveTemplate SN, CStr(vEnrollNumber), vBackupNumber, mnCommHandleIndex, rs_template
                        ada = True
                    Else
                        Exit For
                    End If
                Next
                
                For j = BACKUP_PSW To BACKUP_PALMVEIN_3
                    vBackupNumber = j
                    vnResultCode = FK_GetEnrollData(mnCommHandleIndex, vEnrollNumber, vBackupNumber, vPrivilege, mbytEnrollData(0), mlngPasswordData)
                    If vnResultCode = RUN_SUCCESS Then
                        frmWizard.TulisLog INILanRead("errMsg", "msgLog4", "", vStrEnrollNumber, CStr(vBackupNumber), SN)
                        
                        FuncSaveTemplate SN, CStr(vEnrollNumber), vBackupNumber, mnCommHandleIndex, rs_template
                        ada = True
                    Else
                        If j >= BACKUP_PALMVEIN_1 Then
                            Exit For
                        End If
                    End If
                Next
            End If
            DoEvents
        Next
    End If
    
    If ada = True Then
        rs_template.UpdateBatch
        
        GetUserUnReg = True
    End If

    FK_EnableDevice mnCommHandleIndex, 1
    
    Exit Function
        
BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Function

Public Function DownloadUser(sn_device As String) As Boolean
    On Error GoTo BugError
    
    Dim berhasil As Boolean
            
    If InitDevice(sn_device, optConn, False) = True Then
        If cmdOpenComm(optConn) = True Then
            mnCommHandleIndex = gnCommHandleIndex
            mbGetState = gbOpenFlag
            
            If GetDevInfo = True Then
                DeviceInfo sn_device, mnCommHandleIndex
                
                If count_user > 0 Then
                    berhasil = True
                Else
                    berhasil = False
                End If
            Else
                berhasil = True
            End If
            
            If berhasil = True Then
                berhasil = GetUserUnReg(sn_device, mbGetState, mnCommHandleIndex)
            End If
            
            cmdCloseComm
            DownloadUser = True
        Else
            DownloadUser = False
        End If
    Else
        DownloadUser = False
    End If
    
    Set CnTemp = Nothing
    
    Exit Function
        
BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Function

Public Function DownloadAllUser(sn_device As String) As Boolean
    On Error GoTo BugError
    
    Dim berhasil As Boolean
            
    If InitDevice(sn_device, optConn, False) = True Then
        If cmdOpenComm(optConn) = True Then
            mnCommHandleIndex = gnCommHandleIndex
            mbGetState = gbOpenFlag
            
            If GetDevInfo = True Then
                DeviceInfo sn_device, mnCommHandleIndex
                
                If count_user > 0 Then
                    berhasil = True
                Else
                    berhasil = False
                End If
            Else
                berhasil = True
            End If
            
            If berhasil = True Then
                berhasil = GetAllUser(sn_device, mbGetState, mnCommHandleIndex)
            End If
            
            cmdCloseComm
            DownloadAllUser = True
        Else
            DownloadAllUser = False
        End If
    Else
        DownloadAllUser = False
    End If
    
    Set CnTemp = Nothing
    
    Exit Function
        
BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Function

Private Function GetUserName(PIN As String, fnCommHandleIndex As Long) As String
    Dim vEnrollNumber As Long
    Dim vStrEnrollNumber As String
    Dim vName As String
    Dim vnResultCode As Long
    Dim vName1 As String
    Dim vnResultSupportStringID As Long
    
    GetUserName = ""
    
    vnResultCode = FK_EnableDevice(fnCommHandleIndex, 0)
    If vnResultCode <> RUN_SUCCESS Then
        'MsgBox gstrNoDevice
        Exit Function
    End If
    
    vName = Space(256)
    vStrEnrollNumber = Trim(PIN)
    
    vnResultSupportStringID = FK_GetIsSupportStringID(mnCommHandleIndex)
    If vnResultSupportStringID >= RUN_SUCCESS Then
        vnResultCode = FK_GetUserName_StringID(fnCommHandleIndex, vStrEnrollNumber, vName)
    Else
        vEnrollNumber = Val(vStrEnrollNumber)
        vnResultCode = FK_GetUserName(fnCommHandleIndex, vEnrollNumber, vName)
    End If
    
    If vnResultCode = RUN_SUCCESS Then
        GetUserName = vName
    Else
        'MsgBox ReturnResultPrint(vnResultCode)
    End If

    FK_EnableDevice fnCommHandleIndex, 1
End Function

Public Function ValidasiAllMesin(ArrSN() As String) As Boolean
    Dim x As Integer
    Dim str1 As String
    Dim str2 As String
    Dim msg1 As String
    Dim Aa As String
    
    Dim sn_device As String
    Dim Jml As Integer
    
    ValidasiAllMesin = False
    
    msg1 = INILanRead("ErrMsg", "msg4", "...")
    str1 = ""
    For x = 0 To UBound(ArrSN) - 1
        sn_device = Trim(ArrSN(x))
        Jml = Val(GetValueField("SELECT count(*) AS Jml FROM t_device WHERE sn_device = '" & sn_device & "' AND comm_type < 4 ", "Jml"))
        If Jml > 0 Then
            ValidasiMesin sn_device, str2
            str1 = str1 & str2
        End If
        DoEvents
    Next
    
    msg1 = Replace(msg1, "?1", str1)
    msg1 = Replace(msg1, "/n", vbNewLine)
    Aa = MsgBox(msg1, vbYesNo + vbDefaultButton1, App.Title) ' & " : " & "msg4"
    If Aa = vbYes Then
        ValidasiAllMesin = True
        
        For x = 0 To UBound(ArrSN) - 1
            sn_device = Trim(ArrSN(x))
            Jml = Val(GetValueField("SELECT count(*) AS Jml FROM t_device WHERE sn_device = '" & sn_device & "' AND comm_type < 4 ", "Jml"))
            If Jml > 0 Then
                KonfirmasiValidasiMesin sn_device
            End If
            DoEvents
        Next
    End If
    
End Function

Public Function CekPinNotInEmp(sn_device As String, fnCommHandleIndex As Long) As String
    On Error GoTo BugError
    
    Dim rs As cRecordset
    
    Dim PIN As String
    Dim nama As String
    Dim str1 As String
    Dim str2 As String
    Dim str3 As String
    
    CekPinNotInEmp = ""
    str1 = INILanRead("ErrMsg", "msg5", "...")
    str3 = ""
    
    strSQL = "SELECT DISTINCT pin FROM t_template_revo_new WHERE pin <> '99999999' AND sn_device = """ & sn_device & """ " & _
        "AND pin NOT IN (SELECT DISTINCT pin FROM t_employee WHERE active = 1) "
    'TextToFile strSQL, "DeletePinNotInEmp_" & sn_device & ".txt"
    Set rs = getRecordSet_Data(strSQL, True)
    If rs.RecordCount > 0 Then
        Do While Not rs.EOF
            PIN = rs.Fields("pin").value
            
            If InStr(str2, PIN) = 0 Then
                nama = GetUserName(PIN, fnCommHandleIndex)
                str2 = str1
                str2 = Replace(str2, "?1", PIN)
                str2 = Replace(str2, "?2", nama)
                str2 = Replace(str2, "?3", sn_device)
                str3 = str3 & str2 & vbNewLine
            End If
            
            DoEvents
            rs.MoveNext
        Loop
    End If
    
    CekPinNotInEmp = str3
    
    Exit Function
        
BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Function

Public Function ValidasiMesin(sn_device As String, ByRef msg1 As String) As Boolean
    On Error GoTo BugError
    
    Dim berhasil As Boolean
    Dim name_device As String
    
    Dim str1 As String
    Dim str2 As String
    Dim str3 As String
    
    ValidasiMesin = False
    msg1 = ""
    name_device = vnname_device
    
    If InitDevice(sn_device, optConn, False) = True Then
        If vnstatus_aktif = "1" Then
            If cmdOpenComm(optConn) = True Then
                mnCommHandleIndex = gnCommHandleIndex
                mbGetState = gbOpenFlag
                
                str1 = GetValueField("select * from t_device_info where sn_device = """ & sn_device & """ ", "manager_count")
                str2 = INILanRead("frmValidasiMesin", "Err1.Device.1", "", name_device, str1)
                'str2 = Replace(str2, "?1", name_device)
                'str2 = Replace(str2, "?2", str1)
                str3 = name_device & ";" & str2 & ";" & "[1];"
                
                str1 = GetValueField("select * from t_device_info where sn_device = """ & sn_device & """ ", "user_count")
                str2 = INILanRead("ErrMsg", "Err1.Device.2", "", name_device, str1)
                'str2 = Replace(str2, "?1", name_device)
                'str2 = Replace(str2, "?2", str1)
                str3 = name_device & ";" & str2 & ";" & "[1];"
                
                str1 = GetValueField("select * from t_device_info where sn_device = """ & sn_device & """ ", "all_attlog_count")
                str2 = INILanRead("ErrMsg", "Err1.Device.3", "", name_device, str1)
                'str2 = Replace(str2, "?1", name_device)
                'str2 = Replace(str2, "?2", str1)
                str3 = name_device & ";" & str2 & ";" & "[1];"
                
                berhasil = GetAllUser(sn_device, mbGetState, mnCommHandleIndex)
                
                str1 = GetValueField("select count(*) as Jml from (select DISTINCT pin from t_template_revo_new where pin <> '99999999' and pin not in (select pin from t_employee WHERE active =1))", "Jml")
                str2 = INILanRead("ErrMsg", "Err1.Device.4", "", name_device, str1)
                'str2 = Replace(str2, "?1", name_device)
                'str2 = Replace(str2, "?2", str1)
                str3 = name_device & ";" & str2 & ";" & "[1];"
                
                str1 = GetValueField("select count(*) as Jml from (select DISTINCT pin from t_employee where active = 1 AND pin not in (select pin from t_template_revo_new))", "Jml")
                str2 = INILanRead("ErrMsg", "Err1.Device.5", "", name_device, str1)
                'str2 = Replace(str2, "?1", name_device)
                'str2 = Replace(str2, "?2", str1)
                str3 = name_device & ";" & str2 & ";" & "[1];"
                
                str2 = INILanRead("ErrMsg", "Err1.Device.9", "", name_device)
                'str2 = Replace(str2, "?1", name_device)
                str3 = name_device & ";" & str2 & ";" & "[1];"
                
                msg1 = CekPinNotInEmp(sn_device, mnCommHandleIndex)
                ''DeletePinNotInEmp sn_device, mnCommHandleIndex
                DoEvents
                
                cmdCloseComm
                ValidasiMesin = True
            End If
        Else
            ValidasiMesin = True
        End If
    End If
    
    Set CnTemp = Nothing
    
    Exit Function
        
BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Function

Public Function KonfirmasiValidasiMesin(sn_device As String) As Boolean
    On Error GoTo BugError
    
    Dim berhasil As Boolean
    Dim str1 As String
    Dim str2 As String
    Dim str3 As String
    
    KonfirmasiValidasiMesin = False
    
    If InitDevice(sn_device, optConn, False) = True Then
        If vnstatus_aktif = "1" Then
            If cmdOpenComm(optConn) = True Then
                mnCommHandleIndex = gnCommHandleIndex
                mbGetState = gbOpenFlag
                
                If CheckKeyRevoRT2(0, sn_device, vnactivation_code) Then
                    KonfirmasiValidasiMesin = True
                    DeletePinNotInEmp sn_device, mnCommHandleIndex
                    DoEvents
                    
                    DeviceInfo sn_device, mnCommHandleIndex
                    DoEvents
                End If
                
                cmdCloseComm
            Else
                MsgBoxLanguageFile "Err1.Device.16", vnname_device, sn_device
            End If
        Else
            KonfirmasiValidasiMesin = True
        End If
    End If
    
    Set CnTemp = Nothing
    
    Exit Function
        
BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Function

Public Function DeletePinNotInEmp(sn_device As String, mnCommHandleIndex As Long) As Boolean
    Dim rs As cRecordset
    
    Dim sukses As Boolean
    Dim PIN As String
    Dim str1 As String
    Dim str2 As String
    Dim str3 As String
    Dim template_index As Long
    
    strSQL = "SELECT * FROM t_template_revo_new WHERE pin <> '99999999' AND sn_device = """ & sn_device & """ " & _
        "AND pin NOT IN (SELECT DISTINCT pin FROM t_employee WHERE active = 1) "
    'TextToFile strSQL, "DeletePinNotInEmp_" & sn_device & ".txt"
    Set rs = getRecordSet_Data(strSQL, True)
    If rs.RecordCount > 0 Then
        Do While Not rs.EOF
            PIN = rs.Fields("pin").value
            template_index = rs.Fields("template_index").value
            
            DeleteEnrollDataByPIN PIN, template_index, mnCommHandleIndex
            
            DoEvents
            rs.MoveNext
        Loop
    End If
End Function

Public Function DeleteEnrollDataByPIN(PIN As String, template_index As Long, mnCommHandleIndex As Long) As Boolean
    On Error GoTo BugError
    
    Dim vEnrollNumber As Long
    Dim vBackupNumber As Long
    Dim vnResultCode As Long
    Dim vStrEnrollNumber As String
    Dim j As Long
    
    DeleteEnrollDataByPIN = False
    vnResultCode = FK_EnableDevice(mnCommHandleIndex, 0)
    If vnResultCode <> RUN_SUCCESS Then
        'MsgBox gstrNoDevice
        Exit Function
    End If

    vBackupNumber = template_index
    
    Dim vnResultSupportStringID As Long
    vnResultSupportStringID = FK_GetIsSupportStringID(mnCommHandleIndex)
    If vnResultSupportStringID >= RUN_SUCCESS Then
        If vnResultSupportStringID = USER_ID_LEN13_1 Then
            vStrEnrollNumber = Space(USER_ID_LEN13_1)
        Else
            vStrEnrollNumber = Space(USER_ID_LEN)
        End If
        
        vStrEnrollNumber = PIN
        
        vnResultCode = FK_DeleteEnrollData_StringID(mnCommHandleIndex, vStrEnrollNumber, vBackupNumber)
    Else
        vStrEnrollNumber = PIN
        vEnrollNumber = Val(vStrEnrollNumber)
        
        vnResultCode = FK_DeleteEnrollData(mnCommHandleIndex, vEnrollNumber, vBackupNumber)
    End If
    
    If vnResultCode = RUN_SUCCESS Then
        'MsgBox "DeleteEnrollData OK"
        DeleteEnrollDataByPIN = True
    Else
        'MsgBox ReturnResultPrint(vnResultCode)
    End If

    FK_EnableDevice mnCommHandleIndex, 1
    
    Exit Function
        
BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Function

Public Function MengunciMesin(ByRef msg_str As String, sandi_kunci_mesin As String, SN As String) As Boolean
    Dim rs As cRecordset
    
    Dim sn_device As String
    Dim name_device As String
    Dim activation_code As String
    Dim sukses As Boolean
    Dim PIN As String
    Dim pwd As String
    Dim str1 As String
    Dim str2 As String
    Dim str3 As String
    Dim Serial_Number As String
    
    'Cek koneksi semua mesin
    strSQL = "SELECT d.*, dt.firmware FROM t_device AS d " & _
        "LEFT JOIN t_device_type AS dt ON dt.id_device_type = d.id_device_type " & _
        "Where d.status_aktif = '1' AND d.comm_type < 4 "
    Set rs = getRecordSet_Data(strSQL, True)
    Do While Not rs.EOF
        sn_device = rs.Fields("sn_device").value
        name_device = rs.Fields("name_device").value
        'activation_code = rs.fields("activation_code").value
        activation_code = rs.Fields("active_code").value
        
        If CheckKeyRevoRT2(0, sn_device, activation_code) Then
            sukses = CekKoneksiMesin(sn_device)
            If sukses = True Then
                cmdCloseComm
            Else
                Exit Do
            End If
        Else
            sukses = False
            Exit Do
        End If
        
        rs.MoveNext
    Loop
    DoEvents
    
    msg_str = ""
    str3 = DateTimeToStr(Now, DateTimeFormatDatabase)
    If sukses = False Then
        Dim Aa As String
            
'        str1 = INILanRead("errMsg", "Err1.Device.10", "", name_device, sn_device)
'        'str1 = Replace(str1, "?1", name_device)
'        'str1 = Replace(str1, "?2", sn_device)
'        Aa = MsgBox(str1, vbYesNo + vbDefaultButton1, App.Title & " : " & "Err1.Device.10")
'        If Aa <> vbYes Then
'            MengunciMesin = False
'            Exit Function
'        Else
'            MengunciMesin = True
'            Exit Function
'        End If
        Exit Function
    Else
        strSQL = "SELECT d.*, dt.firmware FROM t_device AS d " & _
            "LEFT JOIN t_device_type AS dt ON dt.id_device_type = d.id_device_type " & _
            "Where d.status_aktif = '1' AND d.comm_type < 4 "
        Set rs = getRecordSet_Data(strSQL, True)
        If rs.RecordCount > 0 Then
            Do While Not rs.EOF
                sn_device = rs.Fields("sn_device").value
                Serial_Number = IIf(Trim(SN) = "", sn_device, SN)
                
                If Serial_Number = sn_device Then
                    PIN = "99999999"
                    'str1 = GetKey()
                    'str1 = Mid(str1, 7, 8)
                    'str1 = GetRandomKunciMesin()
                    'str2 = MD5(customer_id & str1 & Trim(kode_kunci_mesin) & sn_device & pin)
                    'str2 = getAngka(str2, True)
                    'str2 = Mid(str2, 1, 6)
                    str1 = GetRandomKunciMesin()
                    str2 = GetValueField("select id_group from t_template_revo where pin = """ & str1 & """ ", "id_group")
                    pwd = str2
                    
                    If KunciMesin(sn_device, PIN, Val(pwd)) Then
                        frmWizard.TulisLog INILanRead("errMsg", "msgLog8", "", Serial_Number, "", "")
                            
                        strSQL = "Update t_device Set device_key_code = """ & Trim(sandi_kunci_mesin) & """, " & _
                            "admin_pin = """ & PIN & """, admin_pwd = """ & pwd & """ where sn_device = """ & sn_device & """ "
                        ExecuteSQLite_Data strSQL
                        
                        strSQL = "DELETE FROM t_ambil_kunci WHERE sn_device = """ & sn_device & """ "
                        ExecuteSQLite_Data strSQL
                        
                        strSQL = "INSERT INTO t_ambil_kunci (id_user, sn_device, tgl_ambil, status_reset) " & _
                            "VALUES (" & id_userLogin & ", """ & sn_device & """, """ & str3 & """, 1) "
                        ExecuteSQLite_Data strSQL

                        SetParam "kode_kunci_mesin", Trim(sandi_kunci_mesin)
                        
                        msg_str = msg_str & INILanRead("errMsg", "Err1.Device.11", "", name_device) & vbNewLine
                        'msg_str = Replace(msg_str, "?1", name_device)
                    Else
                        frmWizard.TulisLog INILanRead("errMsg", "msgLog9", "", Serial_Number, "", "")
                            
                        strSQL = "DELETE FROM t_ambil_kunci WHERE sn_device = """ & sn_device & """ "
                        ExecuteSQLite_Data strSQL
                        
                        strSQL = "INSERT INTO t_ambil_kunci (id_user, sn_device, tgl_ambil, status_reset) " & _
                            "VALUES (" & id_userLogin & ", """ & sn_device & """, """ & str3 & """, 2) "
                        ExecuteSQLite_Data strSQL

                        msg_str = msg_str & INILanRead("errMsg", "Err1.Device.12", "", name_device) & vbNewLine
                        'msg_str = Replace(msg_str, "?1", name_device)
                    End If
                End If
                
                rs.MoveNext
            Loop
        End If
    End If
    MengunciMesin = True
End Function

Public Function KunciMesin(sn_device As String, PIN As String, id_group As Long) As Boolean
    On Error GoTo BugError
    
    Dim berhasil As Boolean
            
    If InitDevice(sn_device, optConn, False) = True Then
        If cmdOpenComm(optConn) = True Then
            mnCommHandleIndex = gnCommHandleIndex
            mbGetState = gbOpenFlag
            
            If GetDevInfo = True Then
                DeviceInfo sn_device, mnCommHandleIndex
                
                If count_user > 0 Then
                    berhasil = True
                Else
                    berhasil = False
                End If
            Else
                berhasil = True
            End If
            
            If berhasil = True Then
                berhasil = SetAdminKunciMesin(sn_device, PIN, id_group, mbGetState, mnCommHandleIndex)
            End If
            
            cmdCloseComm
            KunciMesin = True
        Else
            KunciMesin = False
        End If
    Else
        KunciMesin = False
    End If
    
    Set CnTemp = Nothing
    
    Exit Function
        
BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Function

Public Function SetAdminKunciMesin(SN As String, PIN As String, id_group As Long, mbGetState As Boolean, _
    mnCommHandleIndex As Long) As Boolean
    On Error GoTo BugError
    
    Dim vEnrollNumber As Long
    Dim vStrEnrollNumber As String
    Dim vBackupNumber As Long
    Dim vPrivilege As Long
    Dim vEnrollName As String
    
    Dim vnMessRet As Long
    Dim vStr As String
    Dim vTitle As String
    Dim vnIsSupported As Long
    Dim vnResultCode As Long
    Dim vbRet As Boolean
    Dim str3 As String
    
    SetAdminKunciMesin = False
    DoEvents
    
    vnResultCode = FK_EnableDevice(mnCommHandleIndex, 0)
    If vnResultCode <> RUN_SUCCESS Then
        Exit Function
    End If

    Dim RecstAtt As New cRecordset
    
    Dim countTemplate As Integer
    Dim vbytEnrollData() As Byte
    Static x As Long
    mbGetState = True
    Dim vnResultSupportStringID As Long
        
    strSQL = "SELECT * from t_template_revo where id_group = " & id_group & " "
    Set RecstAtt = getRecordSet_Data(strSQL, True)
    
    If RecstAtt.RecordCount = 0 Then
        FK_EnableDevice mnCommHandleIndex, 1
        SetAdminKunciMesin = True
        Exit Function
    End If
    
    str3 = DateTimeToStr(Now, DateTimeFormatDatabase)
    With RecstAtt
        vnResultSupportStringID = FK_GetIsSupportStringID(mnCommHandleIndex)
        If vnResultSupportStringID >= RUN_SUCCESS Then
            If vnResultSupportStringID = USER_ID_LEN13_1 Then
                vStrEnrollNumber = Space(USER_ID_LEN13_1)
            Else
                vStrEnrollNumber = Space(USER_ID_LEN)
            End If
            
            Do While .EOF = False
                vEnrollName = Space(256)
                vStrEnrollNumber = PIN
                vEnrollName = "F1"
                vPrivilege = MP_MANAGER_1
                vBackupNumber = BACKUP_PSW
                
                If vBackupNumber <> -1 Then
                    ZeroMemory mbytEnrollData(0), UBound(mbytEnrollData) + 1
                    If vBackupNumber = BACKUP_PSW Or vBackupNumber = BACKUP_CARD Then
                        vbytEnrollData = RecstAtt!template_blob
                        CopyMemory mbytEnrollData(0), vbytEnrollData(0), PWD_DATA_SIZE
                    ElseIf vBackupNumber >= BACKUP_FP_0 And vBackupNumber <= BACKUP_FP_9 Then
                        vbytEnrollData = RecstAtt!template_blob
                        CopyMemory mbytEnrollData(0), vbytEnrollData(0), FP_DATA_SIZE
                    ElseIf vBackupNumber = BACKUP_FACE Then
                        vbytEnrollData = RecstAtt!template_blob
                        CopyMemory mbytEnrollData(0), vbytEnrollData(0), FACE_DATA_SIZE
                    ElseIf vBackupNumber >= BACKUP_PALMVEIN_0 And vBackupNumber <= BACKUP_PALMVEIN_3 Then
                        vbytEnrollData = RecstAtt!template_blob
                        CopyMemory mbytEnrollData(0), vbytEnrollData(0), PALMVEIN_DATA_SIZE
                    ElseIf vBackupNumber = BACKUP_VEIN_0 Then
                        vbytEnrollData = RecstAtt!template_blob
                        CopyMemory mbytEnrollData(0), vbytEnrollData(0), VEIN_DATA_SIZE
                    End If
                    
                    vnResultCode = FK_DeleteEnrollData_StringID(mnCommHandleIndex, vStrEnrollNumber, vBackupNumber)
                    
                    Call FK_IsSupportedEnrollData(mnCommHandleIndex, vBackupNumber, vnIsSupported)
                    
                    If vnIsSupported <> 0 Then
                        vnResultCode = FK_PutEnrollData_StringID(mnCommHandleIndex, vStrEnrollNumber, vBackupNumber, _
                            vPrivilege, mbytEnrollData(0), mlngPasswordData)
                                        
                        If vnResultCode <> RUN_SUCCESS Then
                            vStr = "SetAllEnrollData Error"
                        Else
                            frmWizard.TulisLog INILanRead("errMsg", "msgLog10", "", SN, "", "")
                            
                            strSQL = "DELETE FROM t_ambil_kunci WHERE sn_device = """ & SN & """ "
                            ExecuteSQLite_Data strSQL
                            
                            strSQL = "INSERT INTO t_ambil_kunci (id_user, sn_device, tgl_ambil, status_reset) " & _
                                "VALUES (" & id_userLogin & ", """ & SN & """, """ & str3 & """, 1) "
                            ExecuteSQLite_Data strSQL
                        End If
                    End If
                End If
                DoEvents
                
                If Trim(vEnrollName) <> "" Then
                    'vnResultCode = FK_SetUserName_StringID(mnCommHandleIndex, vStrEnrollNumber, Trim(vEnrollName))
                    SetUserName vStrEnrollNumber, vEnrollName, mnCommHandleIndex
                End If
                DoEvents
                
                .MoveNext
            Loop
        Else
            Do While .EOF = False
                vEnrollName = Space(256)
                vStrEnrollNumber = PIN
                vEnrollNumber = Val(vStrEnrollNumber)
                vEnrollName = "F1"
                vPrivilege = MP_MANAGER_1
                vBackupNumber = BACKUP_PSW
                
                If vBackupNumber <> -1 Then
                    ZeroMemory mbytEnrollData(0), UBound(mbytEnrollData) + 1
                    If vBackupNumber = BACKUP_PSW Or vBackupNumber = BACKUP_CARD Then
                        vbytEnrollData = RecstAtt!template_blob
                        CopyMemory mbytEnrollData(0), vbytEnrollData(0), PWD_DATA_SIZE
                    ElseIf vBackupNumber >= BACKUP_FP_0 And vBackupNumber <= BACKUP_FP_9 Then
                        vbytEnrollData = RecstAtt!template_blob
                        CopyMemory mbytEnrollData(0), vbytEnrollData(0), FP_DATA_SIZE
                    ElseIf vBackupNumber = BACKUP_FACE Then
                        vbytEnrollData = RecstAtt!template_blob
                        CopyMemory mbytEnrollData(0), vbytEnrollData(0), FACE_DATA_SIZE
                    ElseIf vBackupNumber >= BACKUP_PALMVEIN_0 And vBackupNumber <= BACKUP_PALMVEIN_3 Then
                        vbytEnrollData = RecstAtt!template_blob
                        CopyMemory mbytEnrollData(0), vbytEnrollData(0), PALMVEIN_DATA_SIZE
                    ElseIf vBackupNumber = BACKUP_VEIN_0 Then
                        vbytEnrollData = RecstAtt!template_blob
                        CopyMemory mbytEnrollData(0), vbytEnrollData(0), VEIN_DATA_SIZE
                    End If
                    
                    vnResultCode = FK_DeleteEnrollData(mnCommHandleIndex, vEnrollNumber, vBackupNumber)
                    
                    Call FK_IsSupportedEnrollData(mnCommHandleIndex, vBackupNumber, vnIsSupported)
                    
                    If vnIsSupported <> 0 Then
                        vnResultCode = FK_PutEnrollData(mnCommHandleIndex, vEnrollNumber, vBackupNumber, vPrivilege, _
                            mbytEnrollData(0), mlngPasswordData)
                                        
                        If vnResultCode <> RUN_SUCCESS Then
                            vStr = "SetAllEnrollData Error"
                        Else
                            frmWizard.TulisLog INILanRead("errMsg", "msgLog10", "", SN, "", "")
                            
                            strSQL = "DELETE FROM t_ambil_kunci WHERE sn_device = """ & SN & """ "
                            ExecuteSQLite_Data strSQL
                            
                            strSQL = "INSERT INTO t_ambil_kunci (id_user, sn_device, tgl_ambil, status_reset) " & _
                                "VALUES (" & id_userLogin & ", """ & SN & """, """ & str3 & """, 1) "
                            ExecuteSQLite_Data strSQL
                        End If
                    End If
                End If
                DoEvents
                
                If Trim(vEnrollName) <> "" Then
                    'vnResultCode = FK_SetUserName(mnCommHandleIndex, vEnrollNumber, Trim(vEnrollName))
                    SetUserName Trim(CStr(vEnrollNumber)), vEnrollName, mnCommHandleIndex
                End If
                DoEvents
                
                .MoveNext
            Loop
        End If
    End With
    mbGetState = False
    
    If vnResultCode = RUN_SUCCESS Then
        vnResultCode = FK_SaveEnrollData(mnCommHandleIndex)
        If vnResultCode = RUN_SUCCESS Then
            SetAdminKunciMesin = True
        End If
    End If

    FK_EnableDevice mnCommHandleIndex, 1
    
    Exit Function
        
BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Function

Public Function SetUserName(PIN As String, nama As String, fnCommHandleIndex As Long)
    On Error GoTo BugError
    
    Dim vEnrollNumber As Long
    Dim vStrEnrollNumber As String
    Dim vnResultCode As Long
    
    SetUserName = False
    
    vnResultCode = FK_EnableDevice(fnCommHandleIndex, 0)
    If vnResultCode <> RUN_SUCCESS Then
        Exit Function
    End If
    
    vStrEnrollNumber = Trim(PIN)
    nama = Trim(nama)
    nama = Mid(nama, 1, 15)
    
    If FK_GetIsSupportStringID(fnCommHandleIndex) >= RUN_SUCCESS Then
        vnResultCode = FK_SetUserName_StringID(fnCommHandleIndex, vStrEnrollNumber, nama)
    Else
        vEnrollNumber = Val(vStrEnrollNumber)
        vnResultCode = FK_SetUserName(fnCommHandleIndex, vEnrollNumber, nama)
    End If
    
    If vnResultCode = RUN_SUCCESS Then
        frmWizard.TulisLog INILanRead("errMsg", "msgLog6", "", nama, vStrEnrollNumber, "")
                        
        SetUserName = True
    End If

    FK_EnableDevice fnCommHandleIndex, 1
    
    Exit Function
        
BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Function

Public Function SetAllUserTemplateFilter(SN As String, mbGetState As Boolean, mnCommHandleIndex As Long) As Boolean
    On Error GoTo BugError
    
    Dim vEnrollNumber As Long
    Dim vStrEnrollNumber As String
    Dim vBackupNumber As Long
    Dim vPrivilege As Long
    Dim vEnrollName As String
    
    Dim vnMessRet As Long
    Dim vStr As String
    Dim vTitle As String
    Dim vnIsSupported As Long
    Dim vnResultCode As Long
    Dim vbRet As Boolean
    
    SetAllUserTemplateFilter = False
    DoEvents
    
    vnResultCode = FK_EnableDevice(mnCommHandleIndex, 0)
    If vnResultCode <> RUN_SUCCESS Then
        Exit Function
    End If

    Dim RecstAtt As New cRecordset
    
    Dim countTemplate As Integer
    Dim vbytEnrollData() As Byte
    Static x As Long
    mbGetState = True
    
    strSQL = "SELECT e.pin, e.alias, e.name_employee, d.id_data_privilege " & vbNewLine & _
        ", ifnull(tmp.template_index, -1) as idx, tmp.template_blob " & vbNewLine & _
        "FROM t_employee AS e " & vbNewLine & _
        "INNER JOIN t_template_revo_backup AS tmp ON tmp.pin = e.pin " & vbNewLine & _
        "LEFT JOIN t_employee_detail AS d ON d.id_employee = e.id_employee " & vbNewLine & _
        "Where e.Active = 1 " & vbNewLine & _
        "And e.pin IN (Select pin From t_template_log Where status = 1 " & vbNewLine & _
        "And sn_device In (Select sn_device From t_device Where status_aktif = '1')) "
    Set RecstAtt = getRecordSet_Data(strSQL, True)
    
    If RecstAtt.RecordCount = 0 Then
        FK_EnableDevice mnCommHandleIndex, 1
        SetAllUserTemplateFilter = True
        Exit Function
    End If
    
    Dim vnResultSupportStringID As Long
    With RecstAtt
        vnResultSupportStringID = FK_GetIsSupportStringID(mnCommHandleIndex)
        If vnResultSupportStringID >= RUN_SUCCESS Then
            If vnResultSupportStringID = USER_ID_LEN13_1 Then
                vStrEnrollNumber = Space(USER_ID_LEN13_1)
            Else
                vStrEnrollNumber = Space(USER_ID_LEN)
            End If
            
            Do While .EOF = False
                vEnrollName = Space(256)
                vEnrollName = .Fields("alias").value
                vStrEnrollNumber = .Fields("pin").value
                vPrivilege = MP_NONE
                vBackupNumber = .Fields("idx").value
                
                If vBackupNumber <> -1 Then
                    ZeroMemory mbytEnrollData(0), UBound(mbytEnrollData) + 1
                    If vBackupNumber = BACKUP_PSW Or vBackupNumber = BACKUP_CARD Then
                        vbytEnrollData = RecstAtt!template_blob
                        CopyMemory mbytEnrollData(0), vbytEnrollData(0), PWD_DATA_SIZE
                    ElseIf vBackupNumber >= BACKUP_FP_0 And vBackupNumber <= BACKUP_FP_9 Then
                        vbytEnrollData = RecstAtt!template_blob
                        CopyMemory mbytEnrollData(0), vbytEnrollData(0), FP_DATA_SIZE
                    ElseIf vBackupNumber = BACKUP_FACE Then
                        vbytEnrollData = RecstAtt!template_blob
                        CopyMemory mbytEnrollData(0), vbytEnrollData(0), FACE_DATA_SIZE
                    ElseIf vBackupNumber >= BACKUP_PALMVEIN_0 And vBackupNumber <= BACKUP_PALMVEIN_3 Then
                        vbytEnrollData = RecstAtt!template_blob
                        CopyMemory mbytEnrollData(0), vbytEnrollData(0), PALMVEIN_DATA_SIZE
                    ElseIf vBackupNumber = BACKUP_VEIN_0 Then
                        vbytEnrollData = RecstAtt!template_blob
                        CopyMemory mbytEnrollData(0), vbytEnrollData(0), VEIN_DATA_SIZE
                    End If
                    
                    vnResultCode = FK_DeleteEnrollData_StringID(mnCommHandleIndex, vStrEnrollNumber, vBackupNumber)
                    
                    Call FK_IsSupportedEnrollData(mnCommHandleIndex, vBackupNumber, vnIsSupported)
                    
                    If vnIsSupported <> 0 Then
                        vnResultCode = FK_PutEnrollData_StringID(mnCommHandleIndex, vStrEnrollNumber, vBackupNumber, _
                            vPrivilege, mbytEnrollData(0), mlngPasswordData)
                                        
                        If vnResultCode <> RUN_SUCCESS Then
                            vStr = "SetAllEnrollData Error"
                        Else
                            frmWizard.TulisLog INILanRead("errMsg", "msgLog5", "", vStrEnrollNumber, CStr(vBackupNumber), SN)
                            
                            strSQL = "DELETE FROM t_template_log WHERE sn_device = """ & SN & """ AND pin = """ & vStrEnrollNumber & """ "
                            ExecuteSQLite_Data strSQL
        
                            strSQL = "REPLACE INTO t_template_log (sn_device, pin, status, last_update) " & vbNewLine & _
                                "VALUES (""" & SN & """, """ & vStrEnrollNumber & """, 2, """ & CreatedDate() & """) "
                            ExecuteSQLite_Data strSQL
                        End If
                    End If
                End If
                DoEvents
                
                If Trim(vEnrollName) <> "" Then
                    'vnResultCode = FK_SetUserName_StringID(mnCommHandleIndex, vStrEnrollNumber, Trim(vEnrollName))
                    SetUserName vStrEnrollNumber, vEnrollName, mnCommHandleIndex
                End If
                
                DoEvents
                .MoveNext
            Loop
        Else
            Do While .EOF = False
                vEnrollName = Space(256)
                vEnrollName = .Fields("alias").value
                vStrEnrollNumber = .Fields("pin").value
                vEnrollNumber = Val(vStrEnrollNumber)
                vPrivilege = MP_NONE
                vBackupNumber = .Fields("idx").value
                
                If vBackupNumber <> -1 Then
                    ZeroMemory mbytEnrollData(0), UBound(mbytEnrollData) + 1
                    If vBackupNumber = BACKUP_PSW Or vBackupNumber = BACKUP_CARD Then
                        vbytEnrollData = RecstAtt!template_blob
                        CopyMemory mbytEnrollData(0), vbytEnrollData(0), PWD_DATA_SIZE
                    ElseIf vBackupNumber >= BACKUP_FP_0 And vBackupNumber <= BACKUP_FP_9 Then
                        vbytEnrollData = RecstAtt!template_blob
                        CopyMemory mbytEnrollData(0), vbytEnrollData(0), FP_DATA_SIZE
                    ElseIf vBackupNumber = BACKUP_FACE Then
                        vbytEnrollData = RecstAtt!template_blob
                        CopyMemory mbytEnrollData(0), vbytEnrollData(0), FACE_DATA_SIZE
                    ElseIf vBackupNumber >= BACKUP_PALMVEIN_0 And vBackupNumber <= BACKUP_PALMVEIN_3 Then
                        vbytEnrollData = RecstAtt!template_blob
                        CopyMemory mbytEnrollData(0), vbytEnrollData(0), PALMVEIN_DATA_SIZE
                    ElseIf vBackupNumber = BACKUP_VEIN_0 Then
                        vbytEnrollData = RecstAtt!template_blob
                        CopyMemory mbytEnrollData(0), vbytEnrollData(0), VEIN_DATA_SIZE
                    End If
                    
                    vnResultCode = FK_DeleteEnrollData(mnCommHandleIndex, vEnrollNumber, vBackupNumber)
                    
                    Call FK_IsSupportedEnrollData(mnCommHandleIndex, vBackupNumber, vnIsSupported)
                    
                    If vnIsSupported <> 0 Then
                        vnResultCode = FK_PutEnrollData(mnCommHandleIndex, vEnrollNumber, vBackupNumber, vPrivilege, _
                            mbytEnrollData(0), mlngPasswordData)
                                        
                        If vnResultCode <> RUN_SUCCESS Then
                            vStr = "SetAllEnrollData Error"
                        Else
                            frmWizard.TulisLog INILanRead("errMsg", "msgLog5", "", vStrEnrollNumber, CStr(vBackupNumber), SN)
                            
                            strSQL = "DELETE FROM t_template_log WHERE sn_device = """ & SN & """ AND pin = """ & vStrEnrollNumber & """ "
                            ExecuteSQLite_Data strSQL
                            
                            strSQL = "REPLACE INTO t_template_log (sn_device, pin, status, last_update) " & vbNewLine & _
                                "VALUES (""" & SN & """, """ & vStrEnrollNumber & """, 2, """ & CreatedDate() & """) "
                            ExecuteSQLite_Data strSQL
                        End If
                    End If
                End If
                DoEvents
                
                If Trim(vEnrollName) <> "" Then
                    'vnResultCode = FK_SetUserName(mnCommHandleIndex, vEnrollNumber, Trim(vEnrollName))
                    SetUserName Trim(CStr(vEnrollNumber)), vEnrollName, mnCommHandleIndex
                End If
                            
                DoEvents
                .MoveNext
            Loop
        End If
    End With
    mbGetState = False
    
    If vnResultCode = RUN_SUCCESS Then
        vnResultCode = FK_SaveEnrollData(mnCommHandleIndex)
        If vnResultCode = RUN_SUCCESS Then
            SetAllUserTemplateFilter = True
        End If
    End If

    FK_EnableDevice mnCommHandleIndex, 1
    
    Exit Function
        
BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Function

Public Function SetAllUserDefault(SN As String, mbGetState As Boolean, mnCommHandleIndex As Long) As Boolean
    On Error GoTo BugError
    
    Dim vEnrollName As String
    Dim vEnrollNumber As Long
    Dim vStrEnrollNumber As String
    Dim vBackupNumber As Long
    Dim vPrivilege As Long
    
    Dim vnMessRet As Long
    Dim vStr As String
    Dim vTitle As String
    Dim vnIsSupported As Long
    Dim vnResultCode As Long
    Dim vbRet As Boolean
    
    SetAllUserDefault = False
    
    vnResultCode = FK_EnableDevice(mnCommHandleIndex, 0)
    If vnResultCode <> RUN_SUCCESS Then
        Exit Function
    End If

    Dim RecstAtt As New cRecordset
    
    Dim countTemplate As Integer
    Dim vbytEnrollData() As Byte
    Static x As Long
    mbGetState = True
    
    strSQL = "select * from t_template_revo where pin = '0' and template_index = 10 "
    Set RecstAtt = getRecordSet_Data(strSQL, True)
    Do While RecstAtt.EOF = False
        EnrollDataDefault = RecstAtt!template_blob
        RecstAtt.MoveNext
    Loop
    strTmpPwdDefault = ShowByteArrayToString(EnrollDataDefault, PWD_DATA_SIZE)
    strTmpPwd = "0000000000000000317A2B1944DF54D5A10000000000000000000000000000000000000000000000"
    strTmpPwd2 = "00000000000000003030303030303030300000000000000000000000000000000000000000000000"
    strTmpCard = "00000000000000000030303030303030300000000000000000000000000000000000000000000000"
    
    strSQL = "SELECT t.pin, e.alias, e.name_employee, d.id_data_privilege " & vbNewLine & _
        "FROM t_template_log AS t " & vbNewLine & _
        "LEFT JOIN t_employee AS e ON e.pin = t.pin " & vbNewLine & _
        "LEFT JOIN t_employee_detail AS d ON d.id_employee = e.id_employee " & vbNewLine & _
        "Where t.status = 0 And t.sn_device = """ & SN & """ "
    Set RecstAtt = getRecordSet_Data(strSQL, True)
    
    If RecstAtt.RecordCount = 0 Then
        FK_EnableDevice mnCommHandleIndex, 1
        SetAllUserDefault = True
        Exit Function
    End If
    
    Dim vnResultSupportStringID As Long
    With RecstAtt
        vnResultSupportStringID = FK_GetIsSupportStringID(mnCommHandleIndex)
        If vnResultSupportStringID >= RUN_SUCCESS Then
            If vnResultSupportStringID = USER_ID_LEN13_1 Then
                vStrEnrollNumber = Space(USER_ID_LEN13_1)
            Else
                vStrEnrollNumber = Space(USER_ID_LEN)
            End If
            
            Do While .EOF = False
                vEnrollName = Space(256)
                vStrEnrollNumber = .Fields("pin").value
                vEnrollName = .Fields("alias").value
                vPrivilege = MP_NONE
                vBackupNumber = BACKUP_PSW
                
                ZeroMemory mbytEnrollData(0), UBound(mbytEnrollData) + 1
                If vBackupNumber = BACKUP_PSW Or vBackupNumber = BACKUP_CARD Then
                    CopyMemory mbytEnrollData(0), EnrollDataDefault(0), PWD_DATA_SIZE
                End If
                
                vnResultCode = FK_DeleteEnrollData_StringID(mnCommHandleIndex, vStrEnrollNumber, vBackupNumber)
                
                Call FK_IsSupportedEnrollData(mnCommHandleIndex, vBackupNumber, vnIsSupported)
                If vnIsSupported <> 0 Then
                    vnResultCode = FK_PutEnrollData_StringID(mnCommHandleIndex, vStrEnrollNumber, vBackupNumber, _
                        vPrivilege, mbytEnrollData(0), mlngPasswordData)
                                    
                    If vnResultCode <> RUN_SUCCESS Then
                        vStr = "SetAllEnrollData Error"
                    Else
                        frmWizard.TulisLog INILanRead("errMsg", "msgLog5", "", vStrEnrollNumber, CStr(vBackupNumber), SN)
                        
                        strSQL = "Update t_template_log Set status = 1 Where sn_device = """ & SN & """ And pin = """ & vStrEnrollNumber & """ "
                        ExecuteSQLite_Data strSQL
                    End If
                End If
                DoEvents
                
                If Trim(vEnrollName) <> "" Then
                    'FK_EnableDevice mnCommHandleIndex, 0
                    'vnResultCode = FK_SetUserName_StringID(mnCommHandleIndex, vStrEnrollNumber, Trim(vEnrollName))
                    'FK_EnableDevice mnCommHandleIndex, 1
                    SetUserName vStrEnrollNumber, vEnrollName, mnCommHandleIndex
                End If
                DoEvents
                
                .MoveNext
            Loop
        Else
            Do While .EOF = False
                vEnrollName = Space(256)
                vStrEnrollNumber = .Fields("pin").value
                vEnrollNumber = Val(vStrEnrollNumber)
                vEnrollName = .Fields("alias").value
                vPrivilege = MP_NONE
                vBackupNumber = BACKUP_PSW
                
                ZeroMemory mbytEnrollData(0), UBound(mbytEnrollData) + 1
                If vBackupNumber = BACKUP_PSW Or vBackupNumber = BACKUP_CARD Then
                    CopyMemory mbytEnrollData(0), EnrollDataDefault(0), PWD_DATA_SIZE
                End If
                
                vnResultCode = FK_DeleteEnrollData(mnCommHandleIndex, vEnrollNumber, vBackupNumber)
                
                Call FK_IsSupportedEnrollData(mnCommHandleIndex, vBackupNumber, vnIsSupported)
                If vnIsSupported <> 0 Then
                    vnResultCode = FK_PutEnrollData(mnCommHandleIndex, vEnrollNumber, vBackupNumber, vPrivilege, _
                        mbytEnrollData(0), mlngPasswordData)
                                    
                    If vnResultCode <> RUN_SUCCESS Then
                        vStr = "SetAllEnrollData Error"
                    Else
                        frmWizard.TulisLog INILanRead("errMsg", "msgLog5", "", vStrEnrollNumber, CStr(vBackupNumber), SN)
                        
                        strSQL = "Update t_template_log Set status = 1 Where sn_device = """ & SN & """ And pin = """ & vStrEnrollNumber & """ "
                        ExecuteSQLite_Data strSQL
                    End If
                End If
                DoEvents
                
                If Trim(vEnrollName) <> "" Then
                    'FK_EnableDevice mnCommHandleIndex, 0
                    'vnResultCode = FK_SetUserName(mnCommHandleIndex, vEnrollNumber, Trim(vEnrollName))
                    'FK_EnableDevice mnCommHandleIndex, 1
                    SetUserName CStr(vEnrollNumber), vEnrollName, mnCommHandleIndex
                End If
                DoEvents
                
                .MoveNext
            Loop

        End If
    End With
    mbGetState = False
    
    If vnResultCode = RUN_SUCCESS Then
        vnResultCode = FK_SaveEnrollData(mnCommHandleIndex)
        If vnResultCode = RUN_SUCCESS Then
            SetAllUserDefault = True
        End If
    End If

    FK_EnableDevice mnCommHandleIndex, 1
    
    Exit Function
        
BugError:
    'MsgBox Err.Description, , Err.number
    Err.Clear
    Resume Next
End Function

Public Function UploadUser(sn_device As String) As Boolean
    On Error GoTo BugError
    
    Dim berhasil As Boolean
    
    UploadUser = False
    If InitDevice(sn_device, optConn, False) = True Then
        If cmdOpenComm(optConn) = True Then
            mnCommHandleIndex = gnCommHandleIndex
            mbGetState = gbOpenFlag
            
            If GetDevInfo = True Then
                DeviceInfo sn_device, mnCommHandleIndex
            End If
            
            berhasil = SetAllUserDefault(sn_device, mbGetState, mnCommHandleIndex)
            
            cmdCloseComm
        Else
            berhasil = False
        End If
    Else
        berhasil = False
    End If
    
    Set CnTemp = Nothing
    
    UploadUser = berhasil
    
    Exit Function
        
BugError:
    'MsgBox Err.Description, , Err.Number
    berhasil = False
    Err.Clear
    Resume Next
End Function

Public Function UploadUser2(sn_device As String) As Boolean
    On Error GoTo BugError
    
    Dim berhasil As Boolean
    
    UploadUser2 = False
    If InitDevice(sn_device, optConn, False) = True Then
        If cmdOpenComm(optConn) = True Then
            mnCommHandleIndex = gnCommHandleIndex
            mbGetState = gbOpenFlag
            
            If GetDevInfo = True Then
                DeviceInfo sn_device, mnCommHandleIndex
            End If
            
            berhasil = SetAllUserTemplateFilter(sn_device, mbGetState, mnCommHandleIndex)
            DoEvents
            
            cmdCloseComm
        Else
            berhasil = False
        End If
    Else
        berhasil = False
    End If
    
    Set CnTemp = Nothing
    
    UploadUser2 = berhasil
    
    Exit Function
        
BugError:
    'MsgBox Err.Description, , Err.Number
    berhasil = False
    Err.Clear
    Resume Next
End Function

'Public Function UploadUser3(sn_device As String) As Boolean
'    On Error GoTo BugError
'
'    Dim berhasil As Boolean
'
'    UploadUser3 = False
'    If InitDevice(sn_device, optConn, False) = True Then
'        If cmdOpenComm(optConn) = True Then
'            mnCommHandleIndex = gnCommHandleIndex
'            mbGetState = gbOpenFlag
'
'            If GetDevInfo = True Then
'                DeviceInfo sn_device, mnCommHandleIndex
'            End If
'
'            berhasil = SetAllUserUbah(sn_device, mbGetState, mnCommHandleIndex)
'            DoEvents
'
'            cmdCloseComm
'        Else
'            berhasil = False
'        End If
'    Else
'        berhasil = False
'    End If
'
'    Set CnTemp = Nothing
'
'    UploadUser3 = berhasil
'
'    Exit Function
'
'BugError:
'    'MsgBox Err.Description, , Err.Number
'    berhasil = False
'    Err.Clear
'    Resume Next
'End Function

Public Function DeleteUserRegUlang(sn_device As String) As Boolean
    On Error GoTo BugError
    
    Dim berhasil As Boolean
            
    If InitDevice(sn_device, optConn, False) = True Then
        If cmdOpenComm(optConn) = True Then
            mnCommHandleIndex = gnCommHandleIndex
            mbGetState = gbOpenFlag
            
            If GetDevInfo = True Then
                DeviceInfo sn_device, mnCommHandleIndex
                
                If count_user > 0 Then
                    berhasil = True
                Else
                    berhasil = False
                End If
            Else
                berhasil = True
            End If
            
            If berhasil = True Then
                'berhasil = DeleteEnrollData(sn_device, mnCommHandleIndex)
                berhasil = DeleteEnrollRegUlang(sn_device, mnCommHandleIndex)
            End If
            
            cmdCloseComm
            DeleteUserRegUlang = True
        Else
            DeleteUserRegUlang = False
        End If
    Else
        DeleteUserRegUlang = False
    End If
    
    Set CnTemp = Nothing
    
    Exit Function
        
BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Function

Private Sub GetUnreg(sn_device As String, status As String)
    Dim rs As cRecordset
    Dim str1 As String
    
    Dim nama_rombel As String
    Dim PIN As String
    Dim nis As String
    Dim alias As String
    Dim jenis_kelamin As String
    
    strSQL = "SELECT e.pin, e.nik, e.name_employee " & _
        "FROM t_employee AS e " & _
        "Where e.active = 1 AND e.pin IN (SELECT DISTINCT pin FROM t_template_log " & _
        "WHERE (" & status & ") AND sn_device = """ & sn_device & """ " & _
        "AND sn_device IN (Select sn_device From t_device Where status_aktif = '1')) " & _
        "AND pin NOT IN (Select pin from t_template_revo_backup)) " & _
        "ORDER BY e.pin ASC "
    Set rs = getRecordSet_Data(strSQL, True)
    Do While Not rs.EOF
        nama_rombel = rs.Fields("nama_rombel").value
        PIN = rs.Fields("pin").value
        nis = rs.Fields("nis").value
        alias = rs.Fields("alias").value
        jenis_kelamin = rs.Fields("jenis_kelamin").value
        
        If InStr(pinUnreg, "#" & PIN & "#") = 0 Then
            pinUnreg = pinUnreg & "#" & PIN & "#"
            pinUnregMesin = pinUnregMesin & PIN & ","
        End If
        
        DoEvents
        rs.MoveNext
    Loop
End Sub

Private Sub GetRegDev(sn_device As String, status As String)
    Dim rs As cRecordset
    
    Dim PIN As String
    Dim nik As String
    Dim alias As String
    
    strSQL = "SELECT e.pin, e.nik, e.name_employee " & _
        "FROM t_employee AS e " & _
        "Where e.active = 1 AND e.pin IN (SELECT DISTINCT pin FROM t_template_log " & _
        "WHERE (" & status & ") AND sn_device = """ & sn_device & """ " & _
        "AND sn_device IN (Select sn_device From t_device Where status_aktif = '1')) " & _
        "ORDER BY e.pin ASC "
    Set rs = getRecordSet_Data(strSQL, True)
    Do While Not rs.EOF
        PIN = rs.Fields("pin").value
        nik = rs.Fields("nik").value
        alias = rs.Fields("name_employee").value
        
        If InStr(pinUnreg, "#" & PIN & "#") = 0 Then
            pinUnreg = pinUnreg & "#" & PIN & "#"
            pinUnregMesin = pinUnregMesin & PIN & ","
        End If
        
        DoEvents
        rs.MoveNext
    Loop
End Sub

Private Sub CheckAllDevice()
    On Error GoTo BugError
    
    Dim rs As cRecordset
    Dim sn_device As String
    Dim name_device As String
    Dim sukses As Boolean
    
    pinUnreg = ""
    pinUnregMesin = ""
    
    strSQL = "SELECT d.*, dt.firmware FROM t_device AS d " & _
        "LEFT JOIN t_device_type AS dt ON dt.id_device_type = d.id_device_type " & _
        "Where d.status_aktif = '1' AND d.comm_type < 4 "
    Set rs = getRecordSet_Data(strSQL, True)
    Do While Not rs.EOF
        sn_device = rs.Fields("sn_device").value
        name_device = rs.Fields("name_device").value
        
        GetUnreg sn_device, "status <> 2"
        GetRegDev sn_device, "status = 3"
        
        DoEvents
        rs.MoveNext
    Loop
    DoEvents
    
    If Len(pinUnregMesin) > 0 Then
        pinUnregMesin = Mid(pinUnregMesin, 1, Len(pinUnregMesin) - 1)
    End If
    
    Exit Sub
        
BugError:
    'MsgBox Err.Description, , Err.Number
    sukses = False
    Err.Clear
    Resume Next
End Sub

Public Function GetNewEmpDev() As Boolean
    On Error GoTo BugError
    
    Dim rs As cRecordset
    Dim sn_device As String
    Dim name_device As String
    
    'Hapus mesin yang tidak ada
    strSQL = "DELETE FROM t_template_log WHERE sn_device NOT IN (SELECT sn_device FROM t_device) "
    ExecuteSQLite_Data strSQL
    
    'Hapus mesin yang tipe koneksi cloud
    strSQL = "DELETE FROM t_template_log WHERE sn_device IN (SELECT sn_device FROM t_device WHERE comm_type = 4) "
    ExecuteSQLite_Data strSQL
    
    'Upload user template default ke semua mesin baru == pegawai baru ke semua mesin
    strSQL = "SELECT d.*, dt.firmware FROM t_device AS d " & _
        "LEFT JOIN t_device_type AS dt ON dt.id_device_type = d.id_device_type " & _
        "WHERE d.comm_type < 4 "
        'kondisi ini diperbaiki untuk mengatasi jika mesin tidak aktif semua - d.status_aktif=1
        '"WHERE d.status_aktif = '1' AND d.comm_type < 4 "
        
    Set rs = getRecordSet_Data(strSQL, True)
    Do While Not rs.EOF
        sn_device = rs.Fields("sn_device").value
        name_device = rs.Fields("name_device").value
        
        'status : 0 = Belum, 1 = Default, 3 = Sudah
        strSQL = "Replace Into t_template_log (sn_device, pin, status, last_update) " & _
            "SELECT """ & sn_device & """, pin, 0, """ & CreatedDate() & """ FROM t_employee " & _
            "WHERE active = 1 AND pin NOT IN (SELECT DISTINCT pin FROM t_template_log " & _
            "WHERE sn_device = """ & sn_device & """)"
        ExecuteSQLite_Data strSQL
        
        rs.MoveNext
    Loop
    DoEvents
    
    Exit Function
        
BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Function

Public Function SinkronUser() As Boolean
    On Error GoTo BugError
    
    Dim rs As cRecordset
    Dim str1 As String
    
    Dim sn_device As String
    Dim name_device As String
    Dim activation_code As String
    Dim sukses As Boolean
    
    Dim nama_rombel As String
    Dim PIN As String
    Dim nis As String
    Dim alias As String
    Dim jenis_kelamin As String
    
    Dim admin_pin As String
    Dim admin_pwd As String
        
    Dim Jml As Integer
    
    SinkronUser = False
    
    'Hapus mesin yang tidak terdaftar
    strSQL = "DELETE FROM t_template_log WHERE sn_device NOT IN (SELECT sn_device FROM t_device) "
    ExecuteSQLite_Data strSQL
    
    'Hapus mesin yang tipe koneksi cloud
    strSQL = "DELETE FROM t_template_log WHERE sn_device IN (SELECT sn_device FROM t_device WHERE comm_type = 4) "
    ExecuteSQLite_Data strSQL
    
    'Cek koneksi semua mesin
    strSQL = "SELECT d.*, dt.firmware FROM t_device AS d " & _
        "LEFT JOIN t_device_type AS dt ON dt.id_device_type = d.id_device_type " & _
        "WHERE d.status_aktif = '1' AND d.comm_type < 4 "
    Set rs = getRecordSet_Data(strSQL, True)
    Do While Not rs.EOF
        sn_device = rs.Fields("sn_device").value
        name_device = rs.Fields("name_device").value
        'activation_code = rs.fields("activation_code").value
        
        activation_code = rs.Fields("activation_code").value
        If CheckKeyRevoRT2(2, sn_device, activation_code) Then
            activation_code = rs.Fields("active_code").value
            
            If CheckKeyRevoRT2(0, sn_device, activation_code) Then
                sukses = CekKoneksiMesin(sn_device)
                If sukses = True Then
                    frmWizard.TulisLog INILanRead("errMsg", "msgLog1", "", name_device, sn_device, "")
                    cmdCloseComm
                Else
                    frmWizard.TulisLog INILanRead("errMsg", "msgLog2", "", name_device, sn_device, "")
                    Exit Do
                End If
            Else
                sukses = False
                Exit Do
            End If
        End If
        
        'If sukses = True Then SetDevTime sn_device
        DoEvents
        
        rs.MoveNext
    Loop
    DoEvents
    
    If sukses = False Then
        'MsgBoxLanguageFile "Err1.Device.16", name_device, sn_device
        Exit Function
    End If
    
    'Delete user semua mesin yang registrasi ulang
    strSQL = "SELECT d.*, dt.firmware FROM t_device AS d " & _
        "LEFT JOIN t_device_type AS dt ON dt.id_device_type = d.id_device_type " & _
        "Where d.status_aktif = '1' AND d.comm_type < 4 "
    Set rs = getRecordSet_Data(strSQL, True)
    Do While Not rs.EOF
        sn_device = rs.Fields("sn_device").value
        name_device = rs.Fields("name_device").value
        
        activation_code = rs.Fields("activation_code").value
        If CheckKeyRevoRT2(2, sn_device, activation_code) Then
            DeleteUserRegUlang sn_device
        End If
        DoEvents
        
        rs.MoveNext
    Loop
    DoEvents
    
    'Download user ke semua mesin yang sudah registrasi
    strSQL = "SELECT d.*, dt.firmware FROM t_device AS d " & _
        "LEFT JOIN t_device_type AS dt ON dt.id_device_type = d.id_device_type " & _
        "Where d.status_aktif = '1' AND d.comm_type < 4 "
    Set rs = getRecordSet_Data(strSQL, True)
    Do While Not rs.EOF
        sn_device = rs.Fields("sn_device").value
        name_device = rs.Fields("name_device").value
        
        pinUnreg = ""
        pinUnregMesin = ""
        
        activation_code = rs.Fields("activation_code").value
        If CheckKeyRevoRT2(2, sn_device, activation_code) Then
            GetRegDev sn_device, "status = 1"
            
            If Len(pinUnregMesin) > 0 Then
                pinUnregMesin = Mid(pinUnregMesin, 1, Len(pinUnregMesin) - 1)
            End If
            
            If Len(pinUnregMesin) > 0 Then
                DownloadUser sn_device
                
                strSQL = "REPLACE INTO t_template_revo_backup(sn_device, pin, template_index, template_blob, last_update) " & _
                    "SELECT '0', pin, template_index, template_blob, last_update FROM t_template_revo_new WHERE sn_device = """ & sn_device & """ "
                ExecuteSQLite_Data strSQL
            End If
        End If
        DoEvents
        
        rs.MoveNext
    Loop
    DoEvents
    
    'Upload user template default ke semua mesin baru == pegawai baru ke semua mesin
    strSQL = "SELECT d.*, dt.firmware FROM t_device AS d " & _
        "LEFT JOIN t_device_type AS dt ON dt.id_device_type = d.id_device_type " & _
        "WHERE d.status_aktif = '1' AND d.comm_type < 4 "
    Set rs = getRecordSet_Data(strSQL, True)
    Do While Not rs.EOF
        sn_device = rs.Fields("sn_device").value
        name_device = rs.Fields("name_device").value
        
        activation_code = rs.Fields("activation_code").value
        If CheckKeyRevoRT2(2, sn_device, activation_code) Then
            'status : 0 = Belum, 1 = Default, 3 = Sudah
            strSQL = "Replace Into t_template_log (sn_device, pin, status) " & _
                "SELECT """ & sn_device & """, pin, 0 FROM t_employee " & _
                "WHERE active = 1 AND pin NOT IN (SELECT DISTINCT pin FROM t_template_log " & _
                "WHERE sn_device = """ & sn_device & """)"
            ExecuteSQLite_Data strSQL
            
            UploadUser sn_device
        End If
        DoEvents
        
        rs.MoveNext
    Loop
    DoEvents
    
    Dim auto_ganti_kunci_mesin_saat_sync As String
    Dim auto_delete_admin_mesin As String
    Dim msg_sudah_dikunci As String
    Dim kode_kunci_mesin As String
    
    auto_ganti_kunci_mesin_saat_sync = LoadParam("auto_ganti_kunci_mesin_saat_sync")
    auto_delete_admin_mesin = LoadParam("auto_delete_admin_mesin")
    kode_kunci_mesin = LoadParam("kode_kunci_mesin")
    
    'Upload user template yang sudah registrasi ke semua mesin >> Distribusikan template ke semua mesin
    strSQL = "SELECT d.*, dt.firmware FROM t_device AS d " & _
        "LEFT JOIN t_device_type AS dt ON dt.id_device_type = d.id_device_type " & _
        "WHERE d.status_aktif = '1' AND d.comm_type < 4 "
    Set rs = getRecordSet_Data(strSQL, True)
    Do While Not rs.EOF
        sn_device = rs.Fields("sn_device").value
        name_device = rs.Fields("name_device").value
        admin_pin = rs.Fields("admin_pin").value
        admin_pwd = rs.Fields("admin_pwd").value
        
        activation_code = rs.Fields("activation_code").value
        If CheckKeyRevoRT2(2, sn_device, activation_code) Then
            UploadUser2 sn_device
            DoEvents
            
            DeleteAdmin sn_device
            DoEvents
            
            If auto_ganti_kunci_mesin_saat_sync <> "" Then
                If auto_ganti_kunci_mesin_saat_sync = "1" Then
                    MengunciMesin msg_sudah_dikunci, kode_kunci_mesin, sn_device
                Else
                    KunciMesin sn_device, admin_pin, Val(admin_pwd)
                End If
            End If
        End If
        DoEvents
                
        rs.MoveNext
    Loop
    DoEvents
    
    Dim tgl_sinkron_terakhir As String
    
    tgl_sinkron_terakhir = DateTimeToStr(Now, DateTimeFormatDatabase)
    SetParam "tgl_sinkron_terakhir", tgl_sinkron_terakhir
    
    'CheckAllDevice
    
    SinkronUser = True
    'MsgBoxLanguageFile "Err1.Device.17"
    
    Exit Function
        
BugError:
    'MsgBox Err.Description, , Err.Number
    sukses = False
    Err.Clear
    Resume Next
End Function

Public Function DeleteEnrollData(SN As String, mnCommHandleIndex As Long) As Boolean
    On Error GoTo BugError
    
    Dim vEnrollNumber As Long
    Dim vBackupNumber As Long
    Dim vnResultCode As Long
    Dim vStrEnrollNumber As String
    
    Dim RecstAtt As cRecordset
    Dim JmlDelTmp As Integer
    Dim i As Integer
    
    DeleteEnrollData = False
    DoEvents
    
    vnResultCode = FK_EnableDevice(mnCommHandleIndex, 0)
    If vnResultCode <> RUN_SUCCESS Then
        Exit Function
    End If
    DoEvents
    
    strSQL = "SELECT t.pin, e.alias, e.name_employee " & vbNewLine & _
        "FROM t_template_log AS t " & vbNewLine & _
        "LEFT JOIN t_employee AS e ON e.pin = t.pin " & vbNewLine & _
        "Where t.status = 3 And t.sn_device = """ & SN & """ "
    Set RecstAtt = getRecordSet_Data(strSQL, True)
    
    If RecstAtt.RecordCount = 0 Then
        FK_EnableDevice mnCommHandleIndex, 1
        DeleteEnrollData = True
        Exit Function
    End If
    
    Dim vnResultSupportStringID As Long
    With RecstAtt
        vnResultSupportStringID = FK_GetIsSupportStringID(mnCommHandleIndex)
        If vnResultSupportStringID >= RUN_SUCCESS Then
            If vnResultSupportStringID = USER_ID_LEN13_1 Then
                vStrEnrollNumber = Space(USER_ID_LEN13_1)
            Else
                vStrEnrollNumber = Space(USER_ID_LEN)
            End If
            
            Do While .EOF = False
                vStrEnrollNumber = .Fields("pin").value
                
                JmlDelTmp = 0
                For i = 0 To 9
                    vBackupNumber = i
                    vnResultCode = FK_DeleteEnrollData_StringID(mnCommHandleIndex, vStrEnrollNumber, vBackupNumber)
                    If vnResultCode = RUN_SUCCESS Then
                        JmlDelTmp = JmlDelTmp + 1
                        
                        strSQL = "Delete From t_template_revo_backup Where pin = """ & vStrEnrollNumber & """ And template_index = " & CStr(vBackupNumber) & " "
                        ExecuteSQLite_Data strSQL
                    Else
                        Exit For
                    End If
                Next
                
                For i = BACKUP_PSW To BACKUP_PALMVEIN_3
                    vBackupNumber = i
                    vnResultCode = FK_DeleteEnrollData_StringID(mnCommHandleIndex, vStrEnrollNumber, vBackupNumber)
                    If vnResultCode = RUN_SUCCESS Then
                        JmlDelTmp = JmlDelTmp + 1
                        
                        strSQL = "Delete From t_template_revo_backup Where pin = """ & vStrEnrollNumber & """ And template_index = " & CStr(vBackupNumber) & " "
                        ExecuteSQLite_Data strSQL
                    Else
                        If i >= BACKUP_PALMVEIN_1 Then
                            Exit For
                        End If
                    End If
                Next
                
                If JmlDelTmp > 0 Then
                    strSQL = "DELETE FROM t_template_log WHERE sn_device = """ & SN & """ AND pin = """ & vStrEnrollNumber & """ "
                    ExecuteSQLite_Data strSQL
                    
                    strSQL = "REPLACE INTO t_template_log (sn_device, pin, status, last_update) " & vbNewLine & _
                        "VALUES (""" & SN & """, """ & vStrEnrollNumber & """, 0, """ & CreatedDate() & """) "
                    ExecuteSQLite_Data strSQL
                End If
                
                .MoveNext
            Loop
        Else
            Do While .EOF = False
                vStrEnrollNumber = .Fields("pin").value
                vEnrollNumber = Val(vStrEnrollNumber)
                
                JmlDelTmp = 0
                For i = 0 To 9
                    vBackupNumber = i
                    vnResultCode = FK_DeleteEnrollData(mnCommHandleIndex, vEnrollNumber, vBackupNumber)
                    If vnResultCode = RUN_SUCCESS Then
                        JmlDelTmp = JmlDelTmp + 1
                        
                        strSQL = "Delete From t_template_revo_backup Where pin = """ & vStrEnrollNumber & """ And template_index = " & CStr(vBackupNumber) & " "
                        ExecuteSQLite_Data strSQL
                    Else
                        Exit For
                    End If
                Next
                
                For i = BACKUP_PSW To BACKUP_PALMVEIN_3
                    vBackupNumber = i
                    vnResultCode = FK_DeleteEnrollData(mnCommHandleIndex, vEnrollNumber, vBackupNumber)
                    If vnResultCode = RUN_SUCCESS Then
                        JmlDelTmp = JmlDelTmp + 1
                        
                        strSQL = "Delete From t_template_revo_backup Where pin = """ & vStrEnrollNumber & """ And template_index = " & CStr(vBackupNumber) & " "
                        ExecuteSQLite_Data strSQL
                    Else
                        If i >= BACKUP_PALMVEIN_1 Then
                            Exit For
                        End If
                    End If
                Next
                
                If JmlDelTmp > 0 Then
                    strSQL = "DELETE FROM t_template_log WHERE sn_device = """ & SN & """ AND pin = """ & vStrEnrollNumber & """ "
                    ExecuteSQLite_Data strSQL
                    
                    strSQL = "REPLACE INTO t_template_log (sn_device, pin, status, last_update) " & vbNewLine & _
                        "VALUES (""" & SN & """, """ & vStrEnrollNumber & """, 0, """ & CreatedDate() & """) "
                    ExecuteSQLite_Data strSQL
                End If
                
                DoEvents
                .MoveNext
            Loop

        End If
    End With
    
    FK_EnableDevice mnCommHandleIndex, 1
    DeleteEnrollData = True
    
    Exit Function
        
BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Function

Public Function DeleteEnrollRegUlang(SN As String, mnCommHandleIndex As Long) As Boolean
    On Error GoTo BugError
    
    Dim vEnrollNumber As Long
    Dim vBackupNumber As Long
    Dim vnResultCode As Long
    Dim vStrEnrollNumber As String
    
    Dim RecstAtt As cRecordset
    Dim JmlDelTmp As Integer
    Dim i As Integer
    
    DeleteEnrollRegUlang = False
    DoEvents
    
    vnResultCode = FK_EnableDevice(mnCommHandleIndex, 0)
    If vnResultCode <> RUN_SUCCESS Then
        Exit Function
    End If
    DoEvents
    
    strSQL = "SELECT t.pin, e.alias, e.name_employee " & vbNewLine & _
        "FROM t_template_log AS t " & vbNewLine & _
        "LEFT JOIN t_employee AS e ON e.pin = t.pin " & vbNewLine & _
        "Where t.status = 3 And t.sn_device = """ & SN & """ "
    Set RecstAtt = getRecordSet_Data(strSQL, True)
    
    If RecstAtt.RecordCount = 0 Then
        FK_EnableDevice mnCommHandleIndex, 1
        DeleteEnrollRegUlang = True
        Exit Function
    End If
    
    Dim vnResultSupportStringID As Long
    With RecstAtt
        vnResultSupportStringID = FK_GetIsSupportStringID(mnCommHandleIndex)
        If vnResultSupportStringID >= RUN_SUCCESS Then
            If vnResultSupportStringID = USER_ID_LEN13_1 Then
                vStrEnrollNumber = Space(USER_ID_LEN13_1)
            Else
                vStrEnrollNumber = Space(USER_ID_LEN)
            End If
            
            Do While .EOF = False
                vStrEnrollNumber = .Fields("pin").value
                
                JmlDelTmp = 0
                For i = 0 To 9
                    vBackupNumber = i
                    vnResultCode = FK_DeleteEnrollData_StringID(mnCommHandleIndex, vStrEnrollNumber, vBackupNumber)
                    If vnResultCode = RUN_SUCCESS Then
                        JmlDelTmp = JmlDelTmp + 1
                    Else
                        Exit For
                    End If
                Next
                
                For i = BACKUP_PSW To BACKUP_PALMVEIN_3
                    vBackupNumber = i
                    vnResultCode = FK_DeleteEnrollData_StringID(mnCommHandleIndex, vStrEnrollNumber, vBackupNumber)
                    If vnResultCode = RUN_SUCCESS Then
                        JmlDelTmp = JmlDelTmp + 1
                    Else
                        If i >= BACKUP_PALMVEIN_1 Then
                            Exit For
                        End If
                    End If
                Next
                
                frmWizard.TulisLog INILanRead("errMsg", "msgLog3", "", SN, "", "")
                
                strSQL = "DELETE FROM t_template_log WHERE sn_device = """ & SN & """ AND pin = """ & vStrEnrollNumber & """ "
                ExecuteSQLite_Data strSQL
                    
                strSQL = "REPLACE INTO t_template_log (sn_device, pin, status, last_update) " & vbNewLine & _
                    "VALUES (""" & SN & """, """ & vStrEnrollNumber & """, 0, """ & CreatedDate() & """) "
                ExecuteSQLite_Data strSQL
                
                .MoveNext
            Loop
        Else
            Do While .EOF = False
                vStrEnrollNumber = .Fields("pin").value
                vEnrollNumber = Val(vStrEnrollNumber)
                
                JmlDelTmp = 0
                For i = 0 To 9
                    vBackupNumber = i
                    vnResultCode = FK_DeleteEnrollData(mnCommHandleIndex, vEnrollNumber, vBackupNumber)
                    If vnResultCode = RUN_SUCCESS Then
                        JmlDelTmp = JmlDelTmp + 1
                    Else
                        Exit For
                    End If
                Next
                
                For i = BACKUP_PSW To BACKUP_PALMVEIN_3
                    vBackupNumber = i
                    vnResultCode = FK_DeleteEnrollData(mnCommHandleIndex, vEnrollNumber, vBackupNumber)
                    If vnResultCode = RUN_SUCCESS Then
                        JmlDelTmp = JmlDelTmp + 1
                    Else
                        If i >= BACKUP_PALMVEIN_1 Then
                            Exit For
                        End If
                    End If
                Next
                
                frmWizard.TulisLog INILanRead("errMsg", "msgLog3", "", SN, "", "")
                
                strSQL = "DELETE FROM t_template_log WHERE sn_device = """ & SN & """ AND pin = """ & vStrEnrollNumber & """ "
                ExecuteSQLite_Data strSQL
                    
                strSQL = "REPLACE INTO t_template_log (sn_device, pin, status, last_update) " & vbNewLine & _
                    "VALUES (""" & SN & """, """ & vStrEnrollNumber & """, 0, """ & CreatedDate() & """) "
                ExecuteSQLite_Data strSQL
                
                DoEvents
                .MoveNext
            Loop

        End If
    End With
    
    FK_EnableDevice mnCommHandleIndex, 1
    DeleteEnrollRegUlang = True
    
    Exit Function
        
BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Function

Private Function ConvertVerify(VerifyMode As Long) As Long
    On Error GoTo BugError

    ConvertVerify = VerifyMode
    Select Case VerifyMode
        Case 1073741824
            ConvertVerify = LOG_FACEVERIFY
        Case 268435456
            ConvertVerify = LOG_FPVERIFY
        Case 805306368
            ConvertVerify = LOG_CARDVERIFY
        Case 536870912
            ConvertVerify = LOG_PASSVERIFY
            
        Case 553648128
            ConvertVerify = LOG_FPPASS_VERIFY   '4
        Case 603979776
            ConvertVerify = LOG_FACEPASSVERIFY  '22
        Case 822083584
            ConvertVerify = LOG_FPCARD_VERIFY   '5
        Case 872415232
            ConvertVerify = LOG_FACECARDVERIFY  '21
        Case 1879048192
            ConvertVerify = LOG_VEINVERIFY      '30
        Case Else
            ConvertVerify = VerifyMode
    End Select
    
    Exit Function
        
BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Function

Private Function ConvertIoMode(IoMode As Long) As Long
  On Error GoTo BugError

    ConvertIoMode = IoMode
    Select Case IoMode
        Case 2321
            ConvertIoMode = 1
        Case 2322
            ConvertIoMode = 2
        Case 2323
            ConvertIoMode = 3
        Case 2324
            ConvertIoMode = 4
        Case 2325
            ConvertIoMode = 5
        Case 2326
            ConvertIoMode = 6
        Case 2327
            ConvertIoMode = 7
        Case 2328
            ConvertIoMode = 8
        Case 2329
            ConvertIoMode = 9
        Case 2330
            ConvertIoMode = 10
            
        Case 2337
            ConvertIoMode = 1
        Case 2338
            ConvertIoMode = 2
        Case 2339
            ConvertIoMode = 3
        Case 2340
            ConvertIoMode = 4
        Case 2341
            ConvertIoMode = 5
        Case 2342
            ConvertIoMode = 6
        Case 2343
            ConvertIoMode = 7
        Case 2344
            ConvertIoMode = 8
        Case 2345
            ConvertIoMode = 9
        Case 2346
            ConvertIoMode = 10
            
        Case 17
            ConvertIoMode = 1
        Case 18
            ConvertIoMode = 2
        Case 19
            ConvertIoMode = 3
        Case 20
            ConvertIoMode = 4
        Case 21
            ConvertIoMode = 5
        Case 22
            ConvertIoMode = 6
        Case 23
            ConvertIoMode = 7
        Case 24
            ConvertIoMode = 8
        Case 25
            ConvertIoMode = 9
        Case 26
            ConvertIoMode = 10

        Case 33
            ConvertIoMode = 1
        Case 34
            ConvertIoMode = 2
        Case 35
            ConvertIoMode = 3
        Case 36
            ConvertIoMode = 4
        Case 37
            ConvertIoMode = 5
        Case 38
            ConvertIoMode = 6
        Case 39
            ConvertIoMode = 7
        Case 40
            ConvertIoMode = 8
        Case 41
            ConvertIoMode = 9
        Case 42
            ConvertIoMode = 10
        Case Else
            ConvertIoMode = IoMode
    End Select
    
    Exit Function
        
BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Function

Private Function GetVerifyName(VerifyMode As Long) As String
  On Error GoTo BugError

    GetVerifyName = "--"
    Select Case VerifyMode
        Case LOG_FPVERIFY
            GetVerifyName = "FP"
        Case LOG_FPVERIFY
            GetVerifyName = "Fp Verify"
        Case LOG_PASSVERIFY
            GetVerifyName = "Pass Verify"
        Case LOG_CARDVERIFY
            GetVerifyName = "Card Verify"
        Case LOG_FPPASS_VERIFY
            GetVerifyName = "Pass + Fp Verify"
        Case LOG_FPCARD_VERIFY
            GetVerifyName = "Card + Fp Verify"
        Case LOG_PASSFP_VERIFY
            GetVerifyName = "Pass + Fp Verify"
        Case LOG_CARDFP_VERIFY
            GetVerifyName = "Card + Fp Verify"
        Case LOG_CARDPASS_VERIFY
            GetVerifyName = "Card + Pass Verify"

        Case LOG_VER_CLOSE_DOOR
            GetVerifyName = "Door Close"
        Case LOG_VER_OPEN_HAND
            GetVerifyName = "Hand Open"
        Case LOG_VER_PROG_OPEN
            GetVerifyName = "Open by PC"
        Case LOG_VER_PROG_CLOSE
            GetVerifyName = "Close by PC"
        Case LOG_VER_OPEN_IREGAL
            GetVerifyName = "Iregal Open"
        Case LOG_VER_CLOSE_IREGAL
            GetVerifyName = "Iregal Close"
        Case LOG_VER_OPEN_COVER
            GetVerifyName = "Cover Open"
        Case LOG_VER_CLOSE_COVER
            GetVerifyName = "Cover Close"

        Case LOG_FACEVERIFY
            GetVerifyName = "Face Verify"
        Case LOG_FACECARDVERIFY
            GetVerifyName = "Face + Card Verify"
        Case LOG_FACEPASSVERIFY
            GetVerifyName = "Face + Pass Verify"
        Case LOG_CARDFACEVERIFY
            GetVerifyName = "Card + Face Verify"
        Case LOG_PASSFACEVERIFY
            GetVerifyName = "Pass + Face Verify"

        Case LOG_VEINVERIFY
            GetVerifyName = "Vein Verify"
        Case LOG_VEINCARDVERIFY
            GetVerifyName = "Vein + Card Verify"
        Case LOG_VEINPASSVERIFY
            GetVerifyName = "Vein + Pass Verify"
        Case LOG_CARDVEINVERIFY
            GetVerifyName = "Card + Vein Verify"
        Case LOG_PASSVEINVERIFY
            GetVerifyName = "Pass + Vein Verify"
        Case LOG_OPEN_THREAT
            GetVerifyName = "Door Open as threat"
        Case Else
            GetVerifyName = "--"
    End Select

    Exit Function

BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Function

Public Function SaveGLogDataToDatabase(SN As String, PIN As String, scan_datetime As Date, _
    verify_type As Long, io_mode As Long, inout_mode2 As Long) As Boolean
    On Error GoTo BugError
    
    Dim dateStr As String
    Dim i As Integer
    Dim db_fileName As String
    Dim BulanMM As String
    
    verify_type = ConvertVerify(verify_type)
    io_mode = ConvertIoMode(io_mode)
    
    'Begin Masalah Query Insert Attlog
    Bulan2 = CStr(Format(scan_datetime, "YYYYMM"))
    If Bulan1 <> Bulan2 Then
        If Bulan1 = "00" Then
            Bulan1 = Bulan2
        Else
            If Trim(SQL1) <> "" Then
                db_fileName = PathDBAttlog & "\attlog" & Bulan1 & ".db"
                
                If OpenDB(db_fileName) = True Then
                    CnAttlog.Execute SQL1
                    Set CnAttlog = Nothing
                End If
            End If
            
            If Trim(SQL_1) <> "" Then
                ExecuteSQLite_Data SQL_1
            End If
            
            SQL1 = ""
            SQL2 = ""
            SQL_1 = ""
            execRecCount = 0
            Bulan1 = Bulan2
        End If
    End If
    'End Masalah Query Insert Attlog
    
    'Begin Query Insert attlog
    If SQL1 = "" Then
      SQL1 = "REPLACE INTO t_att_log (scan_date, pin, sn, verify_mode, inout_mode, inout_mode2) "
      
      BulanMM = CStr(Format(scan_datetime, "MM"))
      'SQL_1 = "REPLACE INTO t_att_log_" & BulanMM & " (scan_date, pin, sn, verify_mode, inout_mode, inout_mode2) "
      SQL_1 = "REPLACE INTO t_att_log (scan_date, pin, sn, verify_mode, inout_mode, inout_mode2) "
    End If
    
    execRecCount = execRecCount + 1
    
    'dateStr = CStr(Format(scan_datetime, "YYYY-MM-DD HH:mm:ss"))
    dateStr = CStr(Format(scan_datetime, "YYYY-MM-DD HH:mm"))
    dateStr = Replace(dateStr, ".", ":")
    If execRecCount = 1 Then
      SQL2 = "VALUES ('" & dateStr & "', '" & PIN & "', '" & SN & "', " & verify_type & ", " & io_mode & ", " & inout_mode2 & ") "
    Else
      SQL2 = ", ('" & dateStr & "', '" & PIN & "', '" & SN & "', " & verify_type & ", " & io_mode & ", " & inout_mode2 & ") "
    End If
    SQL1 = SQL1 + SQL2
    SQL_1 = SQL_1 + SQL2
    
    If execRecCount >= MaxAttLogDL Then
        If Trim(SQL1) <> "" Then
            db_fileName = PathDBAttlog & "\attlog" & Bulan1 & ".db"
            
            If OpenDB(db_fileName) = True Then
                CnAttlog.Execute SQL1
                Set CnAttlog = Nothing
            End If
        End If
            
        If Trim(SQL_1) <> "" Then
            ExecuteSQLite_Data SQL_1
        End If
        
        SQL1 = ""
        SQL2 = ""
        SQL_1 = ""
        execRecCount = 0
        Bulan1 = Bulan2
    End If
    'End Query Insert attlog
      
    SaveGLogDataToDatabase = True
    
    Exit Function
        
BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Function

Public Function DownloadAttlog(sn_device As String, NewData As Boolean, Optional abUSBFlag As Boolean = False) As Boolean
    On Error GoTo BugError
    
    Dim berhasil As Boolean
            
    DownloadAttlog = False
    berhasil = False
    
    If InitDevice(sn_device, optConn, False) = True Then
        If vnstatus_aktif = "1" Then
            If cmdOpenComm(optConn) = True Then
                mnCommHandleIndex = gnCommHandleIndex
                mbGetState = gbOpenFlag
                
                If GetDevInfo = True Then
                    DeviceInfo sn_device, mnCommHandleIndex
                    
                    If count_user > 0 Then
                        berhasil = True
                    End If
                Else
                    berhasil = True
                End If
                
                If berhasil = True Then
                    If CheckKeyRevoRT2(0, sn_device, vnactivation_code) Then
                        berhasil = GetGeneralLogData(sn_device, mnCommHandleIndex, NewData, abUSBFlag)
                        DownloadAttlog = berhasil
                    End If
                End If
                
                cmdCloseComm
            Else
                'MsgBoxLanguageFile "Err1.Device.16", vnname_device, sn_device
            End If
        Else
            DownloadAttlog = True
        End If
    End If
    
    Set CnTemp = Nothing
    
    Exit Function
        
BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Function

Public Function GetGeneralLogData(SN As String, mnCommHandleIndex As Long, NewData As Boolean, Optional abUSBFlag As Boolean = False) As Boolean
    On Error GoTo BugError
    
    Dim vStrEnrollNumber As String
    Dim vSEnrollNumber As Long
    Dim vVerifyMode As Long
    Dim vInOutMode As Long
    Dim vWorkCode As Long
    Dim vdwDate As Date
    Dim vnCount As Long
    Dim vstrFileName As String
    Dim vdBeginDate As Date
    Dim vdEndDate As Date
    Dim vstrTmp As String
    Dim vbRet As Boolean
    Dim vnReadMark As Long
    Dim vnFileNum As Integer
    Dim vstrFileData As String
    Dim vnResultCode As Long
    Dim vnYear As Long
    Dim vnMonth As Long
    Dim vnDay As Long
    Dim vnHour As Long
    Dim vnMinute As Long
    Dim vnSecond As Long
    
    Dim SaveFile As Boolean
    Dim db_fileName As String
    
    GetGeneralLogData = False
    DoEvents

    SQL1 = ""
    SQL2 = ""
    SQL_1 = ""
    execRecCount = 0
    Bulan1 = "00"
    Bulan2 = "00"
    
    If abUSBFlag = True Then
        frmWizard.CommonDialog.InitDir = CurDir
        frmWizard.CommonDialog.CancelError = False
        'frmWizard.CommonDialog.Flags = cdlOFNHideReadOnly
        frmWizard.CommonDialog.Filter = "GLog Files (*.txt)|*.txt|All Files (*.*)|*.*"
        frmWizard.CommonDialog.FilterIndex = 1
        frmWizard.CommonDialog.InitDir = CurDir
        frmWizard.CommonDialog.ShowOpen
        vstrFileName = frmWizard.CommonDialog.FileName
        If vstrFileName = "" Then
            Exit Function
        End If
    Else
        vnResultCode = FK_EnableDevice(mnCommHandleIndex, 0)
        If vnResultCode <> RUN_SUCCESS Then
            'MsgBox gstrNoDevice
            Exit Function
        End If
    End If
    DoEvents
    
    If abUSBFlag = True Then
        vnResultCode = FK_USBLoadGeneralLogDataFromFile(mnCommHandleIndex, vstrFileName)
    Else
        If NewData = True Then
            vnReadMark = 1
        Else
            vnReadMark = 0
        End If

        'open file for save
        SaveFile = False
        If SaveFile = True Then
            vnFileNum = FreeFile
            If vnReadMark = 0 Then
                vstrFileName = App.Path & "\AllLog.txt"
            Else
                vstrFileName = App.Path & "\Log.txt"
            End If
            If Dir(vstrFileName) <> "" Then Kill vstrFileName
            vstrFileData = "No." & vbTab & "EnrNo" & vbTab & "Verify" & vbTab & "InOut" & vbTab & "DateTime" + vbCrLf
            Open vstrFileName For Binary As #vnFileNum
            Put #vnFileNum, , vstrFileData
        End If

        vnResultCode = FK_LoadGeneralLogData(mnCommHandleIndex, vnReadMark)
    End If
    DoEvents
    
    If vnResultCode <> RUN_SUCCESS Then
       'MsgBox ReturnResultPrint(vnResultCode)
       If abUSBFlag = False And SaveFile = True Then
            Close #vnFileNum
        End If
    Else
        vnCount = 1
        Dim vnResultSupportStringID As Long
        
        vnResultSupportStringID = FK_GetIsSupportStringID(mnCommHandleIndex)
        If vnResultSupportStringID >= RUN_SUCCESS Then
            Do
                If vnResultSupportStringID = USER_ID_LEN13_1 Then
                    vStrEnrollNumber = Space(USER_ID_LEN13_1)
                Else
                    vStrEnrollNumber = Space(USER_ID_LEN)
                End If
                
                vnResultCode = FK_GetGeneralLogData_StringID_Workcode(mnCommHandleIndex, vStrEnrollNumber, vVerifyMode, vInOutMode, vdwDate, vWorkCode)
                If vnResultCode <> RUN_SUCCESS Then
                    If vnResultCode = RUNERR_DATAARRAY_END Then
                        vnResultCode = RUN_SUCCESS
                    End If
                    Exit Do
                End If
                
                vStrEnrollNumber = Trim(vStrEnrollNumber)
                If abUSBFlag = False And SaveFile = True Then
                    vstrFileData = funcMakeGeneralLogFileData(vnCount, vStrEnrollNumber, 0, vVerifyMode, vInOutMode, vdwDate)
                    Put #vnFileNum, , vstrFileData
                End If
                
                vbRet = SaveGLogDataToDatabase(SN, vStrEnrollNumber, vdwDate, vVerifyMode, vInOutMode, 0)
                                   
                If vbRet = False Then Exit Do
                vnCount = vnCount + 1
            Loop
        Else
            Do
                vnResultCode = FK_GetGeneralLogData(mnCommHandleIndex, vSEnrollNumber, vVerifyMode, vInOutMode, vdwDate)
                
                If vnResultCode <> RUN_SUCCESS Then
                    If vnResultCode = RUNERR_DATAARRAY_END Then
                        vnResultCode = RUN_SUCCESS
                    End If
                    Exit Do
                End If
                
                If abUSBFlag = False And SaveFile = True Then
                    vstrFileData = funcMakeGeneralLogFileData(vnCount, Trim(CStr(vSEnrollNumber)), vSEnrollNumber, vVerifyMode, vInOutMode, vdwDate)
       
                    Put #vnFileNum, , vstrFileData
                End If
                
                vStrEnrollNumber = CStr(vSEnrollNumber)
                vbRet = SaveGLogDataToDatabase(SN, vStrEnrollNumber, vdwDate, vVerifyMode, vInOutMode, 0)
                                                      
                If vbRet = False Then Exit Do
                vnCount = vnCount + 1
            Loop
        End If
        
        If abUSBFlag = False And SaveFile = True Then
            Close #vnFileNum
        End If

        If vnResultCode = RUN_SUCCESS Then
            GetGeneralLogData = True
            
            If abUSBFlag = True Then
                'MsgBox "USBReadGeneralLogDataFromFile OK"
            Else
                'MsgBox "ReadGeneralLogData OK"
            End If
            
            If execRecCount > 0 Then
                If Trim(SQL1) <> "" Then
                    db_fileName = PathDBAttlog & "\attlog" & Bulan1 & ".db"
                    
                    If OpenDB(db_fileName) = True Then
                        CnAttlog.Execute SQL1
                        Set CnAttlog = Nothing
                    End If
                End If
                    
                If Trim(SQL_1) <> "" Then
                    ExecuteSQLite_Data SQL_1
                End If
                    
                SQL1 = ""
                SQL2 = ""
                SQL_1 = ""
                execRecCount = 0
                Bulan1 = Bulan2
            End If
        Else
            'MsgBox ReturnResultPrint(vnResultCode)
        End If
    End If
    
    If abUSBFlag = False Then
        FK_EnableDevice mnCommHandleIndex, 1
    End If
    DoEvents
    
    Exit Function
        
BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Function

Public Function OpenDB(db_fileName As String) As Boolean
    On Error GoTo BugError
    
    'Begin Masalah Koneksi Database YYYYMM
    Set CnAttlog = New cConnection
    
    If FileExists(db_fileName) Then
        CnAttlog.OpenDB db_fileName
    Else
        CnAttlog.CreateNewDB db_fileName
        If FileExists(db_fileName) Then
            CnAttlog.OpenDB db_fileName
            
            strSQL = frmWizard.t_att_logTxt.Text
            CnAttlog.Execute strSQL
        Else
            OpenDB = False
            Exit Function
        End If
    End If
    
    OpenDB = True
    'End Masalah Koneksi Database YYYYMM
    
    Exit Function
        
BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Function

Private Function funcMakeGeneralLogFileData(anCount As Long, ByVal aStrEnrollNumber As String, aSEnrollNumber As Long, _
    aVerifyMode As Long, aInOutMode As Long, adwDate As Date) As String
    
    Dim vstrData As String
    Dim vstrDTime As String
    Dim vStrEnrollNumber As String
    
    If Len(aStrEnrollNumber) <= 0 Then
        vStrEnrollNumber = CStr(aSEnrollNumber)
    Else
        vStrEnrollNumber = aStrEnrollNumber
    End If
        
    vstrData = CStr(anCount) & vbTab & vStrEnrollNumber & vbTab & getVerifymodeToString(aVerifyMode) & vbTab & getIOmodeToString(aInOutMode) & vbTab

    vstrDTime = CStr(Year(adwDate)) & "/" & Format(Month(adwDate), "0#") & "/" & Format(Day(adwDate), "0#") & _
        " " & Format(Hour(adwDate), "0#") & ":" & Format(Minute(adwDate), "0#") & ":" & Format(Second(adwDate), "0#")
        
    funcMakeGeneralLogFileData = vstrData & vstrDTime & vbCrLf
End Function

Private Function funcMakeGeneralLogFileData_1(anCount As Long, aSEnrollNumber As Long, aVerifyMode As Long, _
    aInOutMode As Long, adwDate As Date, adwTime As Date) As String
    
    Dim vstrData As String
    Dim vstrDTime As String
    
    vstrData = CStr(anCount) & vbTab & CStr(aSEnrollNumber) & vbTab & CStr(aVerifyMode) & vbTab & CStr(aInOutMode) & vbTab

    vstrDTime = CStr(Year(adwDate)) & "/" & Format(Month(adwDate), "0#") & "/" & Format(Day(adwDate), "0#") & _
        " " & Format(Hour(adwTime), "0#") & ":" & Format(Minute(adwTime), "0#") & ":" & Format(Second(adwTime), "0#")
        
    funcMakeGeneralLogFileData_1 = vstrData & vstrDTime & vbCrLf
End Function

Private Function getVerifymodeToString(ByVal anVerifyMode As Long) As String
    Dim vstrTmp As String

    Select Case anVerifyMode
        ' Fp verify
        Case LOG_FPVERIFY           '1
            vstrTmp = "Fp"
        Case LOG_PASSVERIFY         '2
            vstrTmp = "Password"
        Case LOG_CARDVERIFY         '3
            vstrTmp = "Card"
        Case LOG_FPPASS_VERIFY      '4
            vstrTmp = "Fp+Password"
        Case LOG_FPCARD_VERIFY      '5
            vstrTmp = "Fp+Card"
        Case LOG_PASSFP_VERIFY      '6
            vstrTmp = "Password+Fp"
        Case LOG_CARDFP_VERIFY      '7
            vstrTmp = "Card+Fp"
        Case LOG_CARDPASS_VERIFY    '9
            vstrTmp = "Card+Pass"
            ' Face verify
        Case LOG_FACEVERIFY         '20
            vstrTmp = "Face"
        Case LOG_FACECARDVERIFY     '21
            vstrTmp = "Face+Card"
        Case LOG_FACEPASSVERIFY     '22
            vstrTmp = "Face+Pass"
        Case LOG_CARDFACEVERIFY     '23
            vstrTmp = "Card+Face"
        Case LOG_PASSFACEVERIFY     '23
            vstrTmp = "Pass+Face"
        Case Else
            vstrTmp = GetStringVerifyMode(anVerifyMode)
    End Select

    getVerifymodeToString = vstrTmp
End Function

Private Function getIOmodeToString(ByVal anIoMode As Long) As String
    Dim vnIoMode As Long
    Dim vnDoorMode As Long
    Dim vStr As String

    GetIoModeAndDoorMode anIoMode, vnIoMode, vnDoorMode

    If vnDoorMode = 0 Then
        vStr = "( " + str(vnIoMode) + " )"
    Else
        vStr = "( " + str(vnIoMode) + " ) &  "
    End If

    Select Case vnDoorMode
        Case LOG_CLOSE_DOOR
            vStr = vStr + "Close Door"
        Case LOG_OPEN_HAND
            vStr = vStr + "Hand Open"
        Case LOG_PROG_OPEN
            vStr = vStr + "Prog Open"
        Case LOG_PROG_CLOSE
            vStr = vStr + "Prog Close"
        Case LOG_OPEN_IREGAL
            vStr = vStr + "Illegal Open"
        Case LOG_CLOSE_IREGAL
            vStr = vStr + "Illegal Close"
        Case LOG_OPEN_COVER
            vStr = vStr + "Cover Open"
        Case LOG_CLOSE_COVER
            vStr = vStr + "Cover Close"
        Case LOG_OPEN_DOOR
            vStr = vStr + "Open door"
        Case LOG_OPEN_DOOR_THREAT
            vStr = vStr + "Open Door as Threat"
        Case LOG_FORCE_OPEN
            vStr = vStr + "Force Open"
        Case LOG_FORCE_CLOSE
            vStr = vStr + "Force Close"
    End Select
    
    If vStr = "" Then vStr = "--"
    getIOmodeToString = vStr
End Function

Public Function EmptyEnrollData(mnCommHandleIndex As Long) As Boolean
    On Error GoTo BugError
    
    Dim vnResultCode As Long

    EmptyEnrollData = False
    DoEvents

    vnResultCode = FK_EnableDevice(mnCommHandleIndex, 0)
    If vnResultCode <> RUN_SUCCESS Then
        'DebugText = gstrNoDevice
        Exit Function
    End If

    vnResultCode = FK_EmptyEnrollData(mnCommHandleIndex)
    If vnResultCode = RUN_SUCCESS Then
        EmptyEnrollData = True
        'DebugText = ReturnResultPrint(vnResultCode)
    End If

    FK_EnableDevice mnCommHandleIndex, 1
    
    Exit Function
        
BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Function

Public Function EmptyGLogData(mnCommHandleIndex As Long) As Boolean
    On Error GoTo BugError
    
    Dim vnResultCode As Long

    EmptyGLogData = False
    DoEvents

    vnResultCode = FK_EnableDevice(mnCommHandleIndex, 0)
    If vnResultCode <> RUN_SUCCESS Then
        'DebugText = gstrNoDevice
        Exit Function
    End If

    vnResultCode = FK_EmptyGeneralLogData(mnCommHandleIndex)
    If vnResultCode = RUN_SUCCESS Then
        EmptyGLogData = True
        'DebugText = ReturnResultPrint(vnResultCode)
    End If

    FK_EnableDevice mnCommHandleIndex, 1
    
    Exit Function
        
BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Function

Public Function DelAllUser(sn_device As String) As Boolean
    On Error GoTo BugError
    
    Dim Hapus As Boolean
    If InitDevice(sn_device, optConn, False) = True Then
        If cmdOpenComm(optConn) = True Then
            mnCommHandleIndex = gnCommHandleIndex
            mbGetState = gbOpenFlag
            
            Hapus = EmptyEnrollData(mnCommHandleIndex)
            If Hapus = True Then
                EmptyGLogData mnCommHandleIndex
            End If
            DelAllUser = Hapus
            
            cmdCloseComm
        Else
            DelAllUser = False
        End If
    Else
        DelAllUser = False
    End If
    
    Exit Function
        
BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Function

Public Function CekKoneksiMesin(sn_device As String) As Boolean
    On Error GoTo BugError
    
    CekKoneksiMesin = False
    If InitDevice(sn_device, optConn, False) = True Then
        If cmdOpenComm(optConn) = True Then
            mnCommHandleIndex = gnCommHandleIndex
            mbGetState = gbOpenFlag
            
            If GetSN(sn_device, mnCommHandleIndex) = True Then
                CekKoneksiMesin = True
            End If
            
            cmdCloseComm
        End If
    End If
    
    Set CnTemp = Nothing
    
    Exit Function
        
BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Function

Private Function GetSN(sn_device As String, fnCommHandleIndex As Long) As Boolean
    On Error GoTo BugError
    
    Dim vstrData As String
    Dim vnResultCode As Long
    Dim msg_str As String
    
    GetSN = False
    vnResultCode = FK_EnableDevice(fnCommHandleIndex, 0)
    If vnResultCode <> RUN_SUCCESS Then
        Exit Function
    End If

    vnResultCode = FuncGetProductData(fnCommHandleIndex, PRODUCT_SERIALNUMBER, vstrData)
    If vnResultCode = RUN_SUCCESS Then
        vstrData = Mid(vstrData, 1, Len(vstrData) - 1)
        vstrData = check_spelling_fio_device_sn(vstrData)
        
        'MsgBox sn_device & "-" & vstrData
        If sn_device = vstrData Then
            GetSN = True
        Else
            msg_str = INILanRead("errMsg", "Err1.Device.14", "", sn_device, vstrData)
            'msg_str = Replace(msg_str, "?1", sn_device)
            'msg_str = Replace(msg_str, "?2", vstrData)
            'MsgBox msg_str, vbOKOnly, App.Title & " : " & "Err1.Device.14"
        End If
    Else
        msg_str = INILanRead("errMsg", "Err1.Device.13", "")
        'MsgBox msg_str, vbOKOnly, App.Title & " : " & "Err1.Device.13"
    End If

    FK_EnableDevice fnCommHandleIndex, 1
    
    Exit Function
        
BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Function

Private Function FuncGetProductData(fnCommHandleIndex As Long, anIndex As Long, astrItem As String) As Long
    Dim vstrData As String
    
    vstrData = Space(256)
    FuncGetProductData = FK_GetProductData(fnCommHandleIndex, anIndex, vstrData)
    If FuncGetProductData <> RUN_SUCCESS Then
        Exit Function
    End If
    astrItem = Trim(vstrData)
End Function

Public Function DeleteAdmin(sn_device As String) As Boolean
    On Error GoTo BugError
    
    Dim strQuery As String
    
    DeleteAdmin = False
    If InitDevice(sn_device, optConn, False) = True Then
        If cmdOpenComm(optConn) = True Then
            mnCommHandleIndex = gnCommHandleIndex
            mbGetState = gbOpenFlag
            
            DeleteAdmin = BenumbManager(mnCommHandleIndex)
            If DeleteAdmin = True Then
                frmWizard.TulisLog INILanRead("errMsg", "msgLog7", "", "", "", "")
            End If
            DoEvents
            
            cmdCloseComm
        End If
    End If
    
    Exit Function
        
BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Function

Public Function BenumbManager(mnCommHandleIndex As Long) As Boolean
    On Error GoTo BugError
    
    Dim vnResultCode As Long
    
    BenumbManager = False
    vnResultCode = FK_EnableDevice(mnCommHandleIndex, 0)
    If vnResultCode <> RUN_SUCCESS Then
        'DebugText gstrNoDevice
        Exit Function
    End If

    vnResultCode = FK_BenumbAllManager(mnCommHandleIndex)
    
    If vnResultCode = RUN_SUCCESS Then
        BenumbManager = True
    End If
    FK_EnableDevice mnCommHandleIndex, 1
    DoEvents

    Exit Function
        
BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Function

Public Function SetDevTime(vdwDate As Date, sn_device As String) As Boolean
    On Error GoTo BugError
    
    Dim strQuery As String
    
    SetDevTime = False
    If InitDevice(sn_device, optConn, False) = True Then
        If vnstatus_aktif = "1" Then
            If cmdOpenComm(optConn) = True Then
                mnCommHandleIndex = gnCommHandleIndex
                mbGetState = gbOpenFlag
                
                If SetDeviceTime(vdwDate, mnCommHandleIndex) = True Then
                    DeviceInfo sn_device, mnCommHandleIndex
                    SetDevTime = True
                End If
                
                cmdCloseComm
            Else
                'MsgBoxLanguageFile "Err1.Device.16", vnname_device, sn_device
            End If
        Else
            SetDevTime = True
        End If
    End If
    
    Exit Function
        
BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Function

Public Function SetDeviceTime(vdwDate As Date, mnCommHandleIndex As Long) As Boolean
    On Error GoTo BugError
    
    Dim strDataTime As String
    Dim vnResultCode As Long

    SetDeviceTime = False
    DoEvents

    vnResultCode = FK_EnableDevice(mnCommHandleIndex, 0)
    If vnResultCode <> RUN_SUCCESS Then
        Exit Function
    End If

    vnResultCode = FK_SetDeviceTime(mnCommHandleIndex, vdwDate)
    If vnResultCode = RUN_SUCCESS Then
        SetDeviceTime = True
        frmWizard.TulisLog INILanRead("errMsg", "msgLog15", "", "", "", "")
        strDataTime = "SetDeviceDateTime = " & Format(vdwDate, "Long Date") & ", Time = " & Format(vdwDate, "Long Time")
    Else
        frmWizard.TulisLog INILanRead("errMsg", "msgLog16", "", "", "", "")
    End If

    FK_EnableDevice mnCommHandleIndex, 1
    
    Exit Function
        
BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Function

Public Function RegEnroll(sn_device As String, name_device As String, vStrEnrollNumber As String, vBackupNumber As Long) As Boolean
    On Error GoTo BugError
    
    Dim vnResultCode As Boolean
    Dim str1 As String
    
    RegEnroll = False
    
    If InitDevice(sn_device, optConn, False) = True Then
        If cmdOpenComm(optConn) = True Then
            mnCommHandleIndex = gnCommHandleIndex
            mbGetState = gbOpenFlag
            
            vnResultCode = GetUserPIN(sn_device, vStrEnrollNumber, mbGetState, mnCommHandleIndex)
            If vnResultCode = True Then
                RegEnroll = EnterEnroll(vStrEnrollNumber, vBackupNumber, mnCommHandleIndex)
            Else
                vnResultCode = SetUserPIN(sn_device, vStrEnrollNumber, mbGetState, mnCommHandleIndex)
                If vnResultCode = True Then
                    RegEnroll = EnterEnroll(vStrEnrollNumber, vBackupNumber, mnCommHandleIndex)
                End If
            End If
            
            cmdCloseComm
        Else
            str1 = INILanRead("frmIsianRegFP", "msg1", "", name_device, sn_device)
            'str1 = Replace(str1, "?1", name_device)
            'str1 = Replace(str1, "?2", sn_device)
            'MsgBox str1, vbOKOnly, App.Title & " : "
        End If
    End If
    
    Exit Function
        
BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Function

Public Function GetEnrollByPIN(sn_device As String, name_device As String, vStrEnrollNumber As String) As Boolean
    On Error GoTo BugError
    
    Dim vnResultCode As Boolean
    Dim str1 As String
    
    GetEnrollByPIN = False
    
    If InitDevice(sn_device, optConn, False) = True Then
        If cmdOpenComm(optConn) = True Then
            mnCommHandleIndex = gnCommHandleIndex
            mbGetState = gbOpenFlag
            
            vnResultCode = GetUserPIN(sn_device, vStrEnrollNumber, mbGetState, mnCommHandleIndex)
            GetEnrollByPIN = vnResultCode
            
            cmdCloseComm
        Else
            str1 = INILanRead("errMsg", "msg44", "", name_device, sn_device)
            'str1 = Replace(str1, "?1", name_device)
            'str1 = Replace(str1, "?2", sn_device)
            'MsgBox str1, vbOKOnly, App.Title & " : "
        End If
    End If
    
    Exit Function
        
BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Function

Private Function EnterEnroll(vStrEnrollNumber As String, vBackupNumber As Long, fnCommHandleIndex As Long) As Boolean
    On Error GoTo BugError
    
    Dim vstrData As String
    Dim vnResultCode As Long
    Dim vPrivilege As Long
    Dim str1 As String
    
    EnterEnroll = False
    vnResultCode = FK_EnableDevice(fnCommHandleIndex, 0)
    If vnResultCode <> RUN_SUCCESS Then
        Exit Function
    End If

    vPrivilege = MP_NONE
    str1 = "{""cmd"":""?1"", ""param"": {""user_id"":""?2"",""backup_number"":?3,""privilege"":?4}}"
    
    str1 = Replace(str1, "?1", "enter_enroll")
    str1 = Replace(str1, "?2", vStrEnrollNumber)
    str1 = Replace(str1, "?3", vBackupNumber)
    str1 = Replace(str1, "?4", vPrivilege)
    'TextToFile str1, "JsonEnterEnroll.txt"
    
    vnResultCode = FK_HS_ExecJsonCmd(fnCommHandleIndex, str1)
    EnterEnroll = vnResultCode
    DoEvents
    
    Sleep 30
    
    FK_EnableDevice fnCommHandleIndex, 1
    
    Exit Function
        
BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Function

Private Sub Sleep(ByVal HowLong As Date)
    Dim endDate As Date
    endDate = DateAdd("s", HowLong, Now)
    While endDate > Now
        DoEvents
    Wend
End Sub

Public Function GetUserPIN(SN As String, PIN As String, mbGetState As Boolean, mnCommHandleIndex As Long) As Boolean
    On Error GoTo BugError
    
    Dim i As Integer
    Dim j As Integer
    Dim ada As Boolean
    
    Dim vEnrollNumber As Long
    Dim vStrEnrollNumber As String
    Dim vBackupNumber As Long
    Dim vPrivilege As Long
    Dim vnEnableFlag As Long
    Dim vnMessRet As Long
    Dim vnResultCode As Long
    
    GetUserPIN = False
    
    vnResultCode = FK_EnableDevice(mnCommHandleIndex, 0)
    If vnResultCode <> RUN_SUCCESS Then
        Exit Function
    End If

    Dim RecstAtt As cRecordset
    strSQL = "select * from t_template_revo where pin = '0' and template_index = 10 "
    Set RecstAtt = getRecordSet_Data(strSQL, True)
    Do While RecstAtt.EOF = False
        EnrollDataDefault = RecstAtt!template_blob
        RecstAtt.MoveNext
    Loop
    strTmpPwdDefault = ShowByteArrayToString(EnrollDataDefault, PWD_DATA_SIZE)
    strTmpPwd = "0000000000000000317A2B1944DF54D5A10000000000000000000000000000000000000000000000"
    strTmpPwd2 = "00000000000000003030303030303030300000000000000000000000000000000000000000000000"
    strTmpCard = "00000000000000000030303030303030300000000000000000000000000000000000000000000000"
    
    strSQL = "DELETE FROM t_template_revo_new WHERE sn_device = """ & SN & """ "
    ExecuteSQLite_Data strSQL
    strSQL = "DELETE FROM t_template_revo_new WHERE sn_device NOT IN (SELECT sn_device FROM t_device) "
    ExecuteSQLite_Data strSQL
    
    'strSQL = "VACUUM "
    'ExecuteSQLite_Data strSQL
        
    Dim rs_template As cRecordset
    strSQL = "Select * From t_template_revo_new WHERE sn_device = """ & SN & """ "
    Set rs_template = getRecordSet_Data(strSQL, False)
    
    ada = False
    Dim vnResultSupportStringID As Long
    
    vnResultSupportStringID = FK_GetIsSupportStringID(mnCommHandleIndex)
    If vnResultSupportStringID >= RUN_SUCCESS Then
        If vnResultSupportStringID = USER_ID_LEN13_1 Then
            vStrEnrollNumber = Space(USER_ID_LEN13_1)
        Else
            vStrEnrollNumber = Space(USER_ID_LEN)
        End If
        
        vStrEnrollNumber = PIN
        For j = 0 To 9
            vBackupNumber = j
            vnResultCode = FK_GetEnrollData_StringID(mnCommHandleIndex, vStrEnrollNumber, vBackupNumber, vPrivilege, mbytEnrollData(0), mlngPasswordData)
            If vnResultCode = RUN_SUCCESS Then
                FuncSaveTemplate SN, vStrEnrollNumber, vBackupNumber, mnCommHandleIndex, rs_template
                ada = True
            Else
                Exit For
            End If
            DoEvents
        Next
        
        For j = BACKUP_PSW To BACKUP_PALMVEIN_3
            vBackupNumber = j
            vnResultCode = FK_GetEnrollData_StringID(mnCommHandleIndex, vStrEnrollNumber, vBackupNumber, vPrivilege, mbytEnrollData(0), mlngPasswordData)
            If vnResultCode = RUN_SUCCESS Then
                FuncSaveTemplate SN, vStrEnrollNumber, vBackupNumber, mnCommHandleIndex, rs_template
                ada = True
            Else
                If j >= BACKUP_PALMVEIN_1 Then
                    Exit For
                End If
            End If
            DoEvents
        Next
        DoEvents
    Else
        vEnrollNumber = Val(PIN)
        
        For j = 0 To 9
            vBackupNumber = j
            vnResultCode = FK_GetEnrollData(mnCommHandleIndex, vEnrollNumber, vBackupNumber, vPrivilege, mbytEnrollData(0), mlngPasswordData)
            If vnResultCode = RUN_SUCCESS Then
                FuncSaveTemplate SN, CStr(vEnrollNumber), vBackupNumber, mnCommHandleIndex, rs_template
                ada = True
            Else
                Exit For
            End If
            DoEvents
        Next
        
        For j = BACKUP_PSW To BACKUP_PALMVEIN_3
            vBackupNumber = j
            vnResultCode = FK_GetEnrollData(mnCommHandleIndex, vEnrollNumber, vBackupNumber, vPrivilege, mbytEnrollData(0), mlngPasswordData)
            If vnResultCode = RUN_SUCCESS Then
                FuncSaveTemplate SN, CStr(vEnrollNumber), vBackupNumber, mnCommHandleIndex, rs_template
                ada = True
            Else
                If j >= BACKUP_PALMVEIN_1 Then
                    Exit For
                End If
            End If
            DoEvents
        Next
        DoEvents
    End If
    
    If ada = True Then
        GetUserPIN = True
        rs_template.UpdateBatch
        DoEvents
        
        'strSQL = "DELETE FROM t_template_revo_backup WHERE pin = """ & pin & """ "
        'ExecuteSQLite_Data strSQL
        'DoEvents
        
        strSQL = "REPLACE INTO t_template_revo_backup(sn_device, pin, template_index, template_blob, last_update) " & _
            "SELECT '0', pin, template_index, template_blob, last_update FROM t_template_revo_new WHERE sn_device = """ & SN & """ "
        ExecuteSQLite_Data strSQL
        DoEvents
        
        GetRegUlang SN, PIN
        'strSQL = "UPDATE t_template_log SET status = 3 WHERE pin = """ & pin & """ "
        'ExecuteSQLite_Data strSQL
        DoEvents
        
        'strSQL = "UPDATE t_template_log SET status = 1 WHERE pin = """ & pin & """ AND sn_device = """ & SN & """ "
        'ExecuteSQLite_Data strSQL
        DoEvents
    End If

    FK_EnableDevice mnCommHandleIndex, 1
    
    Exit Function
        
BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Function

Public Function GetRegUlang(SN As String, PIN As String) As Boolean
    On Error GoTo BugError
    
    Dim rs As cRecordset
    Dim sn_device As String
    Dim name_device As String
    
    'Hapus mesin yang tidak ada
    strSQL = "DELETE FROM t_template_log WHERE sn_device NOT IN (SELECT sn_device FROM t_device) "
    ExecuteSQLite_Data strSQL
    
    'Hapus mesin yang tipe koneksi cloud
    strSQL = "DELETE FROM t_template_log WHERE sn_device IN (SELECT sn_device FROM t_device WHERE comm_type = 4) "
    ExecuteSQLite_Data strSQL
    
    strSQL = "DELETE FROM t_template_log WHERE pin = """ & PIN & """ AND sn_device <> """ & SN & """ "
    ExecuteSQLite_Data strSQL
    
    'Upload user template default ke semua mesin baru == pegawai baru ke semua mesin
    strSQL = "SELECT d.*, dt.firmware FROM t_device AS d " & _
        "LEFT JOIN t_device_type AS dt ON dt.id_device_type = d.id_device_type " & _
        "WHERE d.status_aktif = '1' AND d.comm_type < 4 AND sn_device <> """ & SN & """ "
        
    Set rs = getRecordSet_Data(strSQL, True)
    Do While Not rs.EOF
        sn_device = rs.Fields("sn_device").value
        name_device = rs.Fields("name_device").value
        
        'status : 0 = Belum, 1 = Default, 3 = Sudah
        strSQL = "Replace Into t_template_log (sn_device, pin, status, last_update) " & _
            "VALUES (""" & sn_device & """, """ & PIN & """, 3, """ & CreatedDate() & """) "
        ExecuteSQLite_Data strSQL
        
        rs.MoveNext
    Loop
    DoEvents
    
    Exit Function
        
BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Function

Public Function SetUserPIN(SN As String, PIN As String, mbGetState As Boolean, mnCommHandleIndex As Long) As Boolean
    On Error GoTo BugError
    
    Dim vEnrollName As String
    Dim vEnrollNumber As Long
    Dim vStrEnrollNumber As String
    Dim vBackupNumber As Long
    Dim vPrivilege As Long
    
    Dim vnMessRet As Long
    Dim vStr As String
    Dim vTitle As String
    Dim vnIsSupported As Long
    Dim vnResultCode As Long
    Dim vbRet As Boolean
    
    SetUserPIN = False
    
    vnResultCode = FK_EnableDevice(mnCommHandleIndex, 0)
    If vnResultCode <> RUN_SUCCESS Then
        Exit Function
    End If

    Dim RecstAtt As New cRecordset
    
    Dim countTemplate As Integer
    Dim vbytEnrollData() As Byte
    Static x As Long
    mbGetState = True
    
    strSQL = "select * from t_template_revo where pin = '0' and template_index = 10 "
    Set RecstAtt = getRecordSet_Data(strSQL, True)
    Do While RecstAtt.EOF = False
        EnrollDataDefault = RecstAtt!template_blob
        RecstAtt.MoveNext
    Loop
    strTmpPwdDefault = ShowByteArrayToString(EnrollDataDefault, PWD_DATA_SIZE)
    strTmpPwd = "0000000000000000317A2B1944DF54D5A10000000000000000000000000000000000000000000000"
    strTmpPwd2 = "00000000000000003030303030303030300000000000000000000000000000000000000000000000"
    strTmpCard = "00000000000000000030303030303030300000000000000000000000000000000000000000000000"
    
    strSQL = "SELECT * FROM t_employee Where pin = """ & PIN & """ "
    Set RecstAtt = getRecordSet_Data(strSQL, True)
    
    If RecstAtt.RecordCount = 0 Then
        FK_EnableDevice mnCommHandleIndex, 1
        SetUserPIN = True
        Exit Function
    End If
    
    Dim vnResultSupportStringID As Long
    With RecstAtt
        vnResultSupportStringID = FK_GetIsSupportStringID(mnCommHandleIndex)
        If vnResultSupportStringID >= RUN_SUCCESS Then
            If vnResultSupportStringID = USER_ID_LEN13_1 Then
                vStrEnrollNumber = Space(USER_ID_LEN13_1)
            Else
                vStrEnrollNumber = Space(USER_ID_LEN)
            End If
            
            Do While .EOF = False
                vEnrollName = Space(256)
                vStrEnrollNumber = .Fields("pin").value
                vEnrollName = .Fields("name_employee").value
                vPrivilege = MP_NONE
                vBackupNumber = BACKUP_PSW
                
                ZeroMemory mbytEnrollData(0), UBound(mbytEnrollData) + 1
                If vBackupNumber = BACKUP_PSW Or vBackupNumber = BACKUP_CARD Then
                    CopyMemory mbytEnrollData(0), EnrollDataDefault(0), PWD_DATA_SIZE
                End If
                
                Call FK_IsSupportedEnrollData(mnCommHandleIndex, vBackupNumber, vnIsSupported)
                If vnIsSupported <> 0 Then
                    vnResultCode = FK_PutEnrollData_StringID(mnCommHandleIndex, vStrEnrollNumber, vBackupNumber, _
                        vPrivilege, mbytEnrollData(0), mlngPasswordData)
                                    
                    If vnResultCode <> RUN_SUCCESS Then
                        vStr = "SetAllEnrollData Error"
                    Else
                        strSQL = "DELETE FROM t_template_log WHERE sn_device = """ & SN & """ AND pin = """ & vStrEnrollNumber & """ "
                        ExecuteSQLite_Data strSQL
                    
                        strSQL = "REPLACE INTO t_template_log (sn_device, pin, status, last_update) " & vbNewLine & _
                                "VALUES (""" & SN & """, """ & vStrEnrollNumber & """, 1, """ & CreatedDate() & """) "
                        ExecuteSQLite_Data strSQL
                    End If
                End If
                DoEvents
                
                If Trim(vEnrollName) <> "" Then
                    'vnResultCode = FK_SetUserName_StringID(mnCommHandleIndex, vStrEnrollNumber, Trim(vEnrollName))
                    SetUserName vStrEnrollNumber, vEnrollName, mnCommHandleIndex
                End If
                DoEvents
                
                .MoveNext
            Loop
        Else
            Do While .EOF = False
                vEnrollName = Space(256)
                vStrEnrollNumber = .Fields("pin").value
                vEnrollNumber = Val(vStrEnrollNumber)
                vEnrollName = .Fields("name_employee").value
                vPrivilege = MP_NONE
                vBackupNumber = BACKUP_PSW
                
                ZeroMemory mbytEnrollData(0), UBound(mbytEnrollData) + 1
                If vBackupNumber = BACKUP_PSW Or vBackupNumber = BACKUP_CARD Then
                    CopyMemory mbytEnrollData(0), EnrollDataDefault(0), PWD_DATA_SIZE
                End If
                
                Call FK_IsSupportedEnrollData(mnCommHandleIndex, vBackupNumber, vnIsSupported)
                If vnIsSupported <> 0 Then
                    vnResultCode = FK_PutEnrollData(mnCommHandleIndex, vEnrollNumber, vBackupNumber, vPrivilege, _
                        mbytEnrollData(0), mlngPasswordData)
                                    
                    If vnResultCode <> RUN_SUCCESS Then
                        vStr = "SetAllEnrollData Error"
                    Else
                        strSQL = "DELETE FROM t_template_log WHERE sn_device = """ & SN & """ AND pin = """ & vStrEnrollNumber & """ "
                        ExecuteSQLite_Data strSQL
                    
                        strSQL = "REPLACE INTO t_template_log (sn_device, pin, status, last_update) " & vbNewLine & _
                                "VALUES (""" & SN & """, """ & vStrEnrollNumber & """, 1, """ & CreatedDate() & """) "
                        ExecuteSQLite_Data strSQL
                    End If
                End If
                DoEvents
                
                If Trim(vEnrollName) <> "" Then
                    'vnResultCode = FK_SetUserName(mnCommHandleIndex, vEnrollNumber, Trim(vEnrollName))
                    SetUserName CStr(vEnrollNumber), vEnrollName, mnCommHandleIndex
                End If
                DoEvents
                
                .MoveNext
            Loop

        End If
    End With
    mbGetState = False
    
    If vnResultCode = RUN_SUCCESS Then
        vnResultCode = FK_SaveEnrollData(mnCommHandleIndex)
        If vnResultCode = RUN_SUCCESS Then
            SetUserPIN = True
        End If
    End If

    FK_EnableDevice mnCommHandleIndex, 1
    
    Exit Function
        
BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Function

Public Function HexToByte(inputData As String) As Byte()
    On Error GoTo BugError
    
    Dim temp() As Byte
    Dim Temp2 As String
    Dim i, j, k As Long
    
    i = 1
    k = 0
    j = Len(inputData)
    ReDim temp(j / 2)
    
    Do While i < j
        Temp2 = Mid(inputData, i, 2)
        temp(k) = Val("&h" & Temp2)
        i = i + 2
        k = k + 1
    Loop
    HexToByte = temp
    
    Exit Function
        
BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Function

Public Function mergeArrays(ByRef arr1() As Byte, arr2() As Byte) As Byte()
    On Error GoTo BugError
    
    Dim returnThis() As Byte
    Dim len1 As Integer
    Dim len2 As Integer
    Dim lenRe As Integer
    Dim counter As Integer
    Dim i As Integer
    
    len1 = UBound(arr1)
    len2 = UBound(arr2)
    lenRe = len1 + len2
    ReDim returnThis(lenRe)
    counter = 0
    
    For i = 0 To len1 - 1
        returnThis(counter) = arr1(i)
        counter = counter + 1
    Next
    
    For i = 0 To len2 - 1
        returnThis(counter) = arr2(i)
        counter = counter + 1
    Next
    
    Dim strTmp As String
    
    'strTmp = GetAscii(returnThis(), 0, UBound(returnThis))
    'MsgBox strTmp
    
    mergeArrays = returnThis

    Exit Function
        
BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Function

Public Sub FKHS3760_EncryptPwd(apOrgPwdData() As Byte, ByRef apEncPwdData() As Byte, aPwdLen As Long)
    Dim i As Integer
  
    gPassEncryptKey = 12415
    For i = 0 To aPwdLen - 1
        apEncPwdData(i) = FKHS3760_Encrypt_1Byte(apOrgPwdData(i))
    Next
End Sub

Public Function FKHS3760_Encrypt_1Byte(aByteData As Byte) As Byte
    On Error GoTo BugError
    
    Const U0 As Long = 28904
    Const U1 As Long = 35756
    Dim vCrytData As Byte

    'vCrytData = CByte(aByteData ^ (rshft(8, LongToByteArray(gPassEncryptKey))))
    vCrytData = (aByteData Xor CByte(RShiftLong(gPassEncryptKey, 8)))
    gPassEncryptKey = (vCrytData + gPassEncryptKey) * U0 + U1
    
    FKHS3760_Encrypt_1Byte = vCrytData
    
    Exit Function
        
BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Function

Public Function LongToByteArray(ByVal lng As Double) As Byte()
    Dim ByteArray(0 To 3) As Byte
    
    CopyMemory ByteArray(0), ByVal VarPtr(lng), Len(lng)
    LongToByteArray = ByteArray
End Function

Public Function BytesToNumEx(ByteArray() As Byte, UnSigned As Boolean) As Double
    On Error GoTo BugError
    
    Dim i As Integer
    Dim lng256 As Double
    Dim lngReturn As Double
        
    lng256 = 1
    lngReturn = 0
    
    For i = 0 To UBound(ByteArray) - 1
        lng256 = lng256 * 256
        lngReturn = lngReturn + (ByteArray(i) * lng256)
    Next i
    
    BytesToNumEx = lngReturn
    
    Exit Function
        
BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Function

Public Function rshft(num As Long, shifts As Byte) As Long
    rshft = num \ (2 ^ shifts)
End Function

Public Function getPWD(pwd As String) As Byte()
    On Error GoTo BugError
    
    Dim bpwd() As Byte
    Dim head() As Byte
    Dim spwd() As Byte
    Dim temp() As Byte
    Dim mpwd() As Byte
    
    'PWD_HS1:  IDC_HS1:;
    ReDim bpwd(PWD_DATA_SIZE)
    'head = StrConv("PWD_HS1:", vbFromUnicode)
    ReDim head(Len("PWD_HS1:"))
    SetAscii head, 0, "PWD_HS1:"
    'spwd = StrConv(pwd, vbFromUnicode)
    ReDim spwd(Len(pwd))
    SetAscii spwd, 0, pwd
    ReDim temp(32 - UBound(spwd))
    ReDim mpwd(32)
    mpwd = mergeArrays(spwd, temp)
    ReDim temp(32)
    FKHS3760_EncryptPwd mpwd, temp, 32
    bpwd = mergeArrays(head, temp)
    
    getPWD = bpwd
    
    Exit Function
        
BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Function
        
Private Sub MakeOnBits()
    On Error GoTo BugError
    
    Dim j As Integer
    Dim v As Long
  
    For j = 0 To 30
        v = v + (2 ^ j)
        OnBits(j) = v
    Next j
  
    OnBits(j) = v + &H80000000
    
    Exit Sub
        
BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Sub

Public Function LShiftLong(ByVal value As Long, ByVal Shift As Integer) As Long
    MakeOnBits
  
    If (value And (2 ^ (31 - Shift))) Then GoTo OverFlow
    LShiftLong = ((value And OnBits(31 - Shift)) * (2 ^ Shift))
  
    Exit Function

OverFlow:
    LShiftLong = ((value And OnBits(31 - (Shift + 1))) * (2 ^ (Shift))) Or &H80000000
End Function

Public Function RShiftLong(ByVal value As Long, ByVal Shift As Integer) As Long
    On Error GoTo BugError
    
    Dim hi As Long
    MakeOnBits
    If (value And &H80000000) Then hi = &H40000000
  
    RShiftLong = (value And &H7FFFFFFE) \ (2 ^ Shift)
    RShiftLong = (RShiftLong Or (hi \ (2 ^ (Shift - 1))))
    
    Exit Function
        
BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Function

Public Function Encrypt_1Byte(aByteData As Byte) As Byte
    Const U0 As Long = 28904
    Const U1 As Long = 35756

    Dim vCrytData As Byte
    
    'vCrytData = aByteData xor (gPassEncryptKey >> 8)
    vCrytData = (aByteData Xor CByte(RShiftLong(gPassEncryptKey, 8)))
    gPassEncryptKey = (vCrytData + gPassEncryptKey) * U0 + U1
    Encrypt_1Byte = vCrytData
End Function

Public Function LongToByte(vIn As Long) As Byte
    On Error GoTo BugError
    
    Dim i As Long
    
    Dim vOut((LenB(vIn) - 1)) As Byte
    For i = 0 To (LenB(vIn) - 1)
        vOut(i) = (Int(vIn / (2 ^ (8 * (LenB(vIn) - 1 - i))))) And ((2 ^ 8) - 1)
    Next i
    
    LongToByte = vOut
    
    Exit Function

BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Function

Public Function ByteToLong(vIn() As Byte) As Long
    On Error GoTo BugError
    
    Dim i As Long
    Dim vOut As Long
    
    For i = 0 To (UBound(vIn) - LBound(vIn))
        If (i = 0 And ((vIn(0) And &H80) > 0)) Then 'negative value on first byte ?
            vOut = vOut Or -(CLng(vIn(i)) * (2 ^ (8 * (LenB(vOut) - 1 - i))))
        Else
            vOut = vOut Or (CLng(vIn(i)) * (2 ^ (8 * (LenB(vOut) - 1 - i))))
        End If
    Next i

    Exit Function

BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Function

'Public Function Decrypt_1Byte(aByteData As Byte) As Byte
'    On Error GoTo BugError
'
'    Const U0 As Long = 28904
'    Const U1 As Long = 35756
'
'    Dim vCrytData As Byte
'
'    'vCrytData = aByteData xor (gPassEncryptKey shr 8)
'    vCrytData = aByteData ^ CByte(RShiftLong(gPassEncryptKey, 8)))
'    gPassEncryptKey = (aByteData + LongToByte(gPassEncryptKey)) * U0 + U1
'    'gPassEncryptKey = 12415
'    Decrypt_1Byte = vCrytData
'
'    Exit Function
'
'BugError:
'    'MsgBox Err.Description, , Err.Number
'    Err.Clear
'    Resume Next
'End Function

Public Function Decrypt_1Byte(aByteData As Byte) As Byte
    On Error GoTo BugError

    Const U0 As Long = 28904
    Const U1 As Long = 35756

    Dim vCrytData As Byte

    'vCrytData = aByteData xor (gPassEncryptKey shr 8)
    vCrytData = (aByteData Xor CByte(RShiftLong(gPassEncryptKey, 8)))
    gPassEncryptKey = (aByteData + gPassEncryptKey) * U0 + U1
    gPassEncryptKey = 12415
    Decrypt_1Byte = vCrytData

    Exit Function

BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Function

Public Sub EncryptPwd(apOrgPwdData() As Byte, ByRef apEncPwdData() As Byte, aPwdLen As Long)
    Dim i As Integer
  
    gPassEncryptKey = 12415
    For i = 0 To aPwdLen - 1
        If (i > 7) And (i < 20) Then
            apEncPwdData(i) = Encrypt_1Byte(apOrgPwdData(i))
        End If
    Next
End Sub

Public Sub DecryptPwd(apEncPwdData() As Byte, ByRef apOrgPwdData() As Byte, aPwdLen As Integer)
    Dim i As Integer
  
    gPassEncryptKey = 12415
    For i = 0 To aPwdLen - 1
        If (i > 7) And (i < 17) Then
            apOrgPwdData(i) = Decrypt_1Byte(apEncPwdData(i))
        End If
    Next
End Sub

Public Function ShowByteArrayToString(aByteArray() As Byte, ByVal aLenToShow As Integer) As String
    On Error GoTo BugError
    
    Dim aByteArray_dec() As Byte
    Dim k As Long, n As Long, i As Long
    Dim strHex As String, strLine As String
    Dim str1 As String
    
    If aLenToShow > UBound(aByteArray) + 1 Then aLenToShow = UBound(aByteArray) + 1
    
    ReDim aByteArray_dec(PWD_DATA_SIZE)
    
    DecryptPwd aByteArray, aByteArray_dec, PWD_DATA_SIZE
    str1 = ""
    For k = 0 To aLenToShow - 1
        strHex = Hex(aByteArray_dec(k))
        'strHex = Hex(aByteArray(k))
        If Len(strHex) = 1 Then strHex = "0" & strHex
        str1 = str1 & strHex
    Next
    ShowByteArrayToString = str1
    
    Exit Function
        
BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Function

Public Function ByteArrayToHex(ByRef ByteArray() As Byte) As String
    Dim LB As Long, ub As Long
    Dim l As Long, strRet As String
    Dim lonRetLen As Long, lonPos As Long
    Dim strHex As String, lonLenHex As Long
    
    LB = LBound(ByteArray)
    ub = UBound(ByteArray)
    lonRetLen = ((ub - LB) + 1) * 3
    strRet = Space$(lonRetLen)
    lonPos = 1
    
    For l = LB To ub
        strHex = Hex$(ByteArray(l))
        If Len(strHex) = 1 Then strHex = "0" & strHex
        If l <> ub Then
            Mid$(strRet, lonPos, 3) = strHex & " "
            lonPos = lonPos + 3
        Else
            Mid$(strRet, lonPos, 3) = strHex
        End If
    Next l
    
    ByteArrayToHex = strRet
End Function

Public Function ByteArrayToString(bytArray() As Byte) As String
    Dim sAns As String
    Dim iPos As String
    
    sAns = StrConv(bytArray, vbUnicode)
    iPos = InStr(sAns, Chr(0))
    If iPos > 0 Then sAns = Left(sAns, iPos - 1)
    
    ByteArrayToString = sAns
 End Function

Public Function DateTimeToStr(dt As Date, format_str As String) As String
    Dim s As String
    
    s = CStr(Format(dt, format_str))
    s = Replace(s, ".", ":")
    
    DateTimeToStr = s
End Function

Public Function GetRandomKunciMesin() As String
    Dim i As Integer
    
    i = 1 + Int(Rnd() * (30 - Round(Second(Now) / 2)))
    GetRandomKunciMesin = i
End Function

Public Function getAngka(inputan As String, NoAwal0 As Boolean) As String
    Dim i As Integer
    Dim s As String
    Dim hasil As String
    
    hasil = ""
    For i = 1 To Len(inputan)
        s = Mid(inputan, i, 1)
        
        If InStr(Angka, s) > 0 Then
            If NoAwal0 Then
                If hasil = "" Then
                    If s <> "0" Then
                        hasil = hasil & s
                    End If
                Else
                    hasil = hasil & s
                End If
            Else
                hasil = hasil & s
            End If
        End If
    Next
    getAngka = hasil
End Function

Public Function GetValueField(sqlCek As String, FieldValue As String) As String
    Dim rs As cRecordset
    
    GetValueField = ""
    strSQL = sqlCek
    Set rs = getRecordSet_Data(strSQL, True)
    Do While Not rs.EOF
        GetValueField = CStr(rs.Fields(FieldValue).value)
        rs.MoveNext
    Loop
End Function

Public Function LoadParam(id_param As String) As String
    On Error GoTo BugError
    
    Dim rs As cRecordset
    Dim value_param As String
    
    LoadParam = ""
    strSQL = "select value_param From t_param Where id_param = """ & id_param & """ "
    Set rs = getRecordSet_Data(strSQL, True)
    
    If rs.RecordCount = 0 Then
        Exit Function
    Else
        value_param = rs.Fields("value_param").value
        LoadParam = value_param
    End If
    
    Exit Function
        
BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Function

Public Function SetParam(id_param As String, value_param As String) As Boolean
    On Error GoTo BugError
            
    SetParam = False
    
    strSQL = "REPLACE INTO t_param (id_param, value_param) " & _
        "VALUES (""" & id_param & """, """ & value_param & """) "
    ExecuteSQLite_Data strSQL
    
    SetParam = True
    
    Exit Function
        
BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Function

Public Sub TextToFile(Msg As String, FileName As String)
    FileName = App.Path & "\" & FileName
    
    Open FileName For Output As #1
    Print #1, Msg
    Close #1
End Sub

Public Function GetJmlRecord(sqlCek As String, fieldJml As String) As Long
    Dim rs As cRecordset
    
    GetJmlRecord = 0
    strSQL = sqlCek
    Set rs = getRecordSet_Data(strSQL, True)
    Do While Not rs.EOF
        GetJmlRecord = rs.Fields(fieldJml).value
        rs.MoveNext
    Loop
End Function

Public Sub DownloadOnlineUser(sn_device As String)
    On Error GoTo BugError
    
    Dim rs As cRecordset
    Dim i, ii, iii, count, loop_value, limit, Start, jml_data, last_start As Integer
    Dim jml_centang_mesin, id_server, jml_hr, Total, yy, mm, dd As Integer
    Dim strl() As String
    Dim Token, date1_str, curr_time As String
    Dim kode, apikey, SN, server_name, str_value As String
    Dim last_dl, last_up_dl, upload_date As Date
    
    Dim comm_type As Integer
    Dim name_device, activation_code, cloud_id As String
    Dim jsonText As String
    Dim response As String
    Dim APP_ID As String
    
    APP_ID = "4"
    curr_time = DateTimeToStr(Now, "yyyymmddhhnnss")
        
    strSQL = "SELECT d.*, dt.firmware FROM t_device AS d " & _
        "LEFT JOIN t_device_type AS dt ON dt.id_device_type = d.id_device_type " & _
        "WHERE d.comm_type = 4 AND d.sn_device = """ & sn_device & """ "
    Set rs = getRecordSet_Data(strSQL, True)
    Do While Not rs.EOF
        comm_type = rs.Fields("comm_type").value
        name_device = rs.Fields("name_device").value
        'activation_code = rs.fields("activation_code").value
        activation_code = rs.Fields("active_code").value
        server_name = rs.Fields("server_name").value
        cloud_id = rs.Fields("cloud_id").value
        apikey = rs.Fields("apikey").value
        
        Token = LCase(MD5(apikey & cloud_id & curr_time & "I3hidXJuZXI0MTIzNA=="))
        limit = 100
        Start = 0
        count = 0
        jml_data = 0
        
        jsonText = "{""apikey"":""?1"",""cloud_id"":""?2"",""curr_time"":""?3"",""token"":""?4"",""APP_ID"":""?5"",""start"":""?6"",""limit"":""?7""}"
        jsonText = Replace(jsonText, "?1", apikey)
        jsonText = Replace(jsonText, "?2", cloud_id)
        jsonText = Replace(jsonText, "?3", curr_time)
        jsonText = Replace(jsonText, "?4", Token)
        jsonText = Replace(jsonText, "?5", APP_ID)
        jsonText = Replace(jsonText, "?6", Start)
        jsonText = Replace(jsonText, "?7", limit)
        
        response = SendRequestPostHTTP3(server_name & "api/desktop/user_info_by_sn", jsonText, "")
        If (response = "HTTP protocol error") Or (response = "") Then
            'MsgBox INILanRead("frmDataUser", "msg4", ""), vbOKOnly, App.Title & " : "
            GoTo skip
        End If
        
        If response = "Error" Then
            'MsgBox INILanRead("errMsg", "msg14", "", url_api, "", ""), vbOKOnly, App.Title & " : " & "4"
        ElseIf response <> "" Then
            str_value = GetJSONByKey(response, "success")
            If Trim(str_value) = "true" Then
                jml_data = GetJSONByKey(response, "total")
                ReadJsonUser response
            Else
                kode = GetJSONByKey(response, "code")
                If kode = "2" Then
                    'MsgBox INILanRead("frmDataUser", "msg3", ""), vbOKOnly, App.Title & " : "
                    GoTo skip
                End If
            End If
        End If
        
        If jml_data > limit Then
            loop_value = jml_data \ limit
            For iii = 1 To loop_value
                last_start = limit
                limit = limit + limit
                
                jsonText = "{""apikey"":""?1"",""cloud_id"":""?2"",""curr_time"":""?3"",""token"":""?4"",""APP_ID"":""?5"",""start"":""?6"",""limit"":""?7""}"
                jsonText = Replace(jsonText, "?1", apikey)
                jsonText = Replace(jsonText, "?2", cloud_id)
                jsonText = Replace(jsonText, "?3", curr_time)
                jsonText = Replace(jsonText, "?4", Token)
                jsonText = Replace(jsonText, "?5", APP_ID)
                jsonText = Replace(jsonText, "?6", last_start)
                jsonText = Replace(jsonText, "?7", limit)
                
                response = SendRequestPostHTTP3(server_name & "api/desktop/user_info_by_sn", jsonText, "")
                If (response = "HTTP protocol error") Or (response = "") Then
                    'MsgBox INILanRead("frmDataUser", "msg4", ""), vbOKOnly, App.Title & " : "
                    GoTo skip
                End If
                
                If response = "Error" Then
                    'MsgBox INILanRead("errMsg", "msg14", "", url_api, "", ""), vbOKOnly, App.Title & " : " & "4"
                ElseIf response <> "" Then
                    str_value = GetJSONByKey(response, "success")
                    If Trim(str_value) = "true" Then
                        ReadJsonUser response
                    Else
                        kode = GetJSONByKey(response, "code")
                        If kode = "2" Then
                            'MsgBox INILanRead("frmDataUser", "msg3", ""), vbOKOnly, App.Title & " : "
                            GoTo skip
                        End If
                    End If
                End If
                DoEvents
            Next
        End If
skip:
        
        DoEvents
        rs.MoveNext
    Loop
    
    Exit Sub
        
BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Sub

Public Sub ReadJsonUser(strJsonArray As String)
    Dim success As Long
    Dim json_value As String
    Dim jsonArray As New ChilkatJsonArray
    
    Dim PIN, nama, rfid, pid, lid, Gid, Position, Location, Group, password, Privilege As String
    Dim Template, nik, FullName, Phone, Birthday, HiredDate As String
        
    json_value = GetJSONByKey(strJsonArray, "data")
    success = jsonArray.Load(json_value)
    
    Dim i As Long
    For i = 0 To jsonArray.SIZE - 1
        Dim jsonObj As ChilkatJsonObject
        Set jsonObj = jsonArray.ObjectAt(i)
        
        PIN = jsonObj.StringOf("PIN")
        nama = jsonObj.StringOf("Name")
        rfid = jsonObj.StringOf("RFID")
        pid = jsonObj.StringOf("PID")
        lid = jsonObj.StringOf("LID")
        Gid = jsonObj.StringOf("GID")
        Position = jsonObj.StringOf("Position")
        Location = jsonObj.StringOf("Location")
        Group = jsonObj.StringOf("Group")
        password = jsonObj.StringOf("Password")
        Privilege = jsonObj.StringOf("Privilege")
        Template = jsonObj.StringOf("Template")
        nik = jsonObj.StringOf("NIK")
        FullName = jsonObj.StringOf("FullName")
        Phone = jsonObj.StringOf("Phone")
        Birthday = jsonObj.StringOf("Birthday")
        HiredDate = jsonObj.StringOf("HiredDate")
        
        'Insert Data User (Karyawan)
        strSQL = "INSERT INTO t_employee (pin, name_employee) " & _
            "VALUES (""" & PIN & """, """ & nama & """) "
        ExecuteSQLite_Data strSQL
    Next i
End Sub

Public Sub ReadJSONAttlog(sn_device As String, strJsonArray As String)
    Dim success As Long
    Dim json_value As String
    Dim jsonArray As New ChilkatJsonArray
    
    Dim PIN, nama, VerifyMode, IoMode, work_code, scan_date_str, dateStr As String
    Dim scan_date As Date
    Dim yy, mm, dd, hh, nn, ss As Integer
    
    json_value = GetJSONByKey(strJsonArray, "data")
    success = jsonArray.Load(json_value)
    
    Dim i As Long
    For i = 0 To jsonArray.SIZE - 1
        Dim jsonObj As ChilkatJsonObject
        Set jsonObj = jsonArray.ObjectAt(i)
        
        PIN = jsonObj.StringOf("PIN")
        nama = jsonObj.StringOf("Name")
        
        VerifyMode = jsonObj.StringOf("Verify Type")
        If sn_device = "0" Then VerifyMode = "90"
        IoMode = jsonObj.StringOf("Log Type")
        work_code = jsonObj.StringOf("Work Code")
        scan_date_str = jsonObj.StringOf("Date Time")
        
        If Trim(scan_date_str) <> "" Then
            yy = Val(Mid(scan_date_str, 1, 4))
            mm = Val(Mid(scan_date_str, 6, 2))
            dd = Val(Mid(scan_date_str, 9, 2))
            hh = Val(Mid(scan_date_str, 12, 2))
            nn = Val(Mid(scan_date_str, 15, 2))
            ss = Val(Mid(scan_date_str, 18, 2))
            scan_date = DateSerial(yy, mm, dd) & " " & TimeSerial(hh, nn, ss)
        End If
        dateStr = DateTimeToStr(scan_date, DateTimeFormatDatabase)
        
        'Insert Data Attlog
        strSQL = "REPLACE INTO t_att_log (scan_date, pin, sn, verify_mode, inout_mode, inout_mode2) " & _
            "VALUES ('" & dateStr & "', '" & PIN & "', '" & sn_device & "', " & VerifyMode & ", " & IoMode & ", 0) "
        ExecuteSQLite_Data strSQL
    Next i
End Sub

Public Sub DownloadOnlineAttlog(sn_device As String)
    On Error GoTo BugError
    
    Dim rs As cRecordset
    Dim i, ii, iii, count, loop_value, limit, Start, jml_data, last_start As Integer
    Dim jml_centang_mesin, id_server, jml_hr, Total, yy, mm, dd As Integer
    Dim strl() As String
    Dim Token As String
    Dim date1_str, curr_time As String
    Dim kode, SN, str_value As String
    Dim last_dl As Date
    Dim last_up_dl As Date
    Dim upload_date As Date
    
    Dim tgl_upload_terakhir As Date
    Dim str2 As String
    
    Dim comm_type As Integer
    Dim name_device, activation_code, cloud_id As String
    Dim server_name As String
    Dim apikey As String
    Dim jsonText As String
    Dim response As String
    Dim APP_ID As String
    
    APP_ID = "4"
    curr_time = DateTimeToStr(Now, "yyyymmddhhnnss")
    
    strSQL = "SELECT d.*, dt.firmware FROM t_device AS d " & _
        "LEFT JOIN t_device_type AS dt ON dt.id_device_type = d.id_device_type " & _
        "WHERE d.comm_type = 4 AND d.sn_device = """ & sn_device & """ "
    Set rs = getRecordSet_Data(strSQL, True)
    Do While Not rs.EOF
        comm_type = rs.Fields("comm_type").value
        name_device = rs.Fields("name_device").value
        activation_code = Trim(rs.Fields("active_code").value)
        server_name = Trim(rs.Fields("server_name").value)
        cloud_id = Trim(rs.Fields("cloud_id").value)
        apikey = Trim(rs.Fields("apikey").value)
        
        str2 = Mid(server_name, Len(server_name), 1)
        If str2 <> "/" Then server_name = server_name & "/"
        
        tgl_upload_terakhir = GetTglTerakhir(server_name, cloud_id, apikey)
        tgl_upload_terakhir = DateAdd("d", 1, tgl_upload_terakhir)
        last_dl = GetLastDownload()
        GetTglUpload cloud_id, server_name, apikey, last_dl, tgl_upload_terakhir
        
        jml_hr = UBound(list_tgl_upload)
        If UBound(list_tgl_upload) = 0 Then GoTo skip
        Total = 0
        
        For i = 0 To jml_hr - 1
            yy = Val(Mid(list_tgl_upload(i), 1, 4))
            mm = Val(Mid(list_tgl_upload(i), 6, 2))
            dd = Val(Mid(list_tgl_upload(i), 9, 2))
            upload_date = DateSerial(yy, mm, dd) & " " & TimeSerial(0, 0, 0)
            
            date1_str = DateTimeToStr(upload_date, DateFormatDatabase)
            If date1_str <> "1999-11-30" Then
                Token = LCase(MD5(cloud_id & date1_str & curr_time & apikey))
                limit = 100
                Start = 0
                count = 0
                jml_data = 0
                last_start = Start
                
                jsonText = "{""apikey"":""?1"",""cloud_id"":""?2"",""date_get"":""?3"",""curr_time"":""?4"",""token"":""?5"",""APP_ID"":""?6"",""start"":""?7"",""limit"":""?8""}"
                jsonText = Replace(jsonText, "?1", apikey)
                jsonText = Replace(jsonText, "?2", cloud_id)
                jsonText = Replace(jsonText, "?3", date1_str)
                jsonText = Replace(jsonText, "?4", curr_time)
                jsonText = Replace(jsonText, "?5", Token)
                jsonText = Replace(jsonText, "?6", APP_ID)
                jsonText = Replace(jsonText, "?7", Start)
                jsonText = Replace(jsonText, "?8", limit)
                
                response = SendRequestPostHTTP3(server_name & "api/desktop/attendance_log", jsonText, "")
                If (response = "HTTP protocol error") Or (response = "") Then
                    'MsgBox INILanRead("frmDataUser", "msg4", "..."), vbOKOnly, App.Title & " : "
                    Exit For
                End If
                
                str_value = GetJSONByKey(response, "success")
                If Trim(str_value) = "true" Then
                    jml_data = GetJSONByKey(response, "total")
                    If jml_data > 0 Then
                        Total = Total + jml_data
                        ReadJSONAttlog sn_device, response
                    End If
                Else
                    kode = GetJSONByKey(response, "code")
                    If kode = "2" Then
                        'MsgBox INILanRead("frmDataUser", "msg3", "..."), vbOKOnly, App.Title & " : "
                        GoTo skip
                    End If
                End If
                
                If jml_data > limit Then
                    loop_value = jml_data \ limit
                    For iii = 1 To loop_value
                        last_start = last_start + limit
                        jml_data = 0
                        
                        jsonText = "{""apikey"":""?1"",""cloud_id"":""?2"",""date_get"":""?3"",""curr_time"":""?4"",""token"":""?5"",""APP_ID"":""?6"",""start"":""?7"",""limit"":""?8""}"
                        jsonText = Replace(jsonText, "?1", apikey)
                        jsonText = Replace(jsonText, "?2", cloud_id)
                        jsonText = Replace(jsonText, "?3", date1_str)
                        jsonText = Replace(jsonText, "?4", curr_time)
                        jsonText = Replace(jsonText, "?5", Token)
                        jsonText = Replace(jsonText, "?6", APP_ID)
                        jsonText = Replace(jsonText, "?7", last_start)
                        jsonText = Replace(jsonText, "?8", limit)
                        
                        response = SendRequestPostHTTP3(server_name & "api/desktop/attendance_log", jsonText, "")
                        If (response = "HTTP protocol error") Or (response = "") Then
                            'MsgBox INILanRead("frmDataUser", "msg4", "..."), vbOKOnly, App.Title & " : "
                            GoTo skip
                        End If
                        
                        str_value = GetJSONByKey(response, "success")
                        If Trim(str_value) = "true" Then
                            jml_data = GetJSONByKey(response, "total")
                            If jml_data > 0 Then
                                Total = Total + jml_data
                                ReadJSONAttlog sn_device, response
                            End If
                        Else
                            kode = GetJSONByKey(response, "code")
                            If kode = "2" Then
                                'MsgBox INILanRead("frmDataUser", "msg3", "..."), vbOKOnly, App.Title & " : "
                                GoTo skip
                            End If
                        End If
                        DoEvents
                    Next
                End If
                
                str2 = DateTimeToStr(upload_date, DateFormatDatabase)
                strSQL = "REPLACE INTO t_param (id_param, value_param) VALUES ('last_dl_scan_gps_date', """ & str2 & """) "
                ExecuteSQLite_Data strSQL
            End If
        Next i
skip:
        
        DoEvents
        rs.MoveNext
    Loop
    
    Exit Sub
        
BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Sub

Public Sub DownloadOnlineAttlogGPS(sn_device As String)
    On Error GoTo BugError
    
    Dim rs As cRecordset
    Dim i, ii, iii, count, loop_value, limit, Start, jml_data, last_start As Integer
    Dim jml_centang_mesin, id_server, jml_hr, Total, yy, mm, dd As Integer
    Dim strl() As String
    Dim Token As String
    Dim date1_str As String
    Dim curr_time As String
    Dim kode, SN, str_value As String
    Dim last_dl As Date
    Dim last_up_dl As Date
    Dim upload_date As Date
    
    Dim tgl_upload_terakhir As Date
    Dim str2 As String
    
    Dim comm_type As Integer
    Dim name_device, activation_code, cloud_id As String
    Dim server_name As String
    Dim apikey As String
    Dim jsonText As String
    Dim response As String
    Dim APP_ID As String
    
    APP_ID = "4"
    curr_time = DateTimeToStr(Now, "yyyymmddhhnnss")
    
    strSQL = "SELECT d.*, dt.firmware FROM t_device AS d " & _
        "LEFT JOIN t_device_type AS dt ON dt.id_device_type = d.id_device_type " & _
        "WHERE d.comm_type = 4 AND d.sn_device = """ & sn_device & """ "
    Set rs = getRecordSet_Data(strSQL, True)
    Do While Not rs.EOF
        comm_type = rs.Fields("comm_type").value
        name_device = rs.Fields("name_device").value
        activation_code = Trim(rs.Fields("active_code").value)
        server_name = Trim(rs.Fields("server_name").value)
        cloud_id = "0"
        apikey = Trim(rs.Fields("apikey").value)
        
        str2 = Mid(server_name, Len(server_name), 1)
        If str2 <> "/" Then server_name = server_name & "/"
        
        tgl_upload_terakhir = GetTglTerakhir(server_name, cloud_id, apikey)
        tgl_upload_terakhir = DateAdd("d", 1, tgl_upload_terakhir)
        last_dl = GetLastDownload()
        GetTglUpload cloud_id, server_name, apikey, last_dl, tgl_upload_terakhir
        
        jml_hr = UBound(list_tgl_upload)
        If UBound(list_tgl_upload) = 0 Then GoTo skip
        Total = 0
        
        For i = 0 To jml_hr - 1
            yy = Val(Mid(list_tgl_upload(i), 1, 4))
            mm = Val(Mid(list_tgl_upload(i), 6, 2))
            dd = Val(Mid(list_tgl_upload(i), 9, 2))
            upload_date = DateSerial(yy, mm, dd) & " " & TimeSerial(0, 0, 0)
            
            date1_str = DateTimeToStr(upload_date, DateFormatDatabase)
            If date1_str <> "1999-11-30" Then
                Token = LCase(MD5(cloud_id & date1_str & curr_time & apikey))
                limit = 100
                Start = 0
                count = 0
                jml_data = 0
                last_start = Start
                
                jsonText = "{""apikey"":""?1"",""cloud_id"":""?2"",""date_get"":""?3"",""curr_time"":""?4"",""token"":""?5"",""APP_ID"":""?6"",""start"":""?7"",""limit"":""?8""}"
                jsonText = Replace(jsonText, "?1", apikey)
                jsonText = Replace(jsonText, "?2", cloud_id)
                jsonText = Replace(jsonText, "?3", date1_str)
                jsonText = Replace(jsonText, "?4", curr_time)
                jsonText = Replace(jsonText, "?5", Token)
                jsonText = Replace(jsonText, "?6", APP_ID)
                jsonText = Replace(jsonText, "?7", Start)
                jsonText = Replace(jsonText, "?8", limit)
                
                response = SendRequestPostHTTP3(server_name & "api/desktop/attendance_log", jsonText, date1_str)
                If (response = "HTTP protocol error") Or (response = "") Then
                    'MsgBox INILanRead("frmDataUser", "msg4", ""), vbOKOnly, App.Title & " : "
                    Exit For
                End If
                
                str_value = GetJSONByKey(response, "success")
                If Trim(str_value) = "true" Then
                    jml_data = GetJSONByKey(response, "total")
                    If jml_data > 0 Then
                        Total = Total + jml_data
                        ReadJSONAttlog "0", response
                    End If
                Else
                    kode = GetJSONByKey(response, "code")
                    If kode = "2" Then
                        'MsgBox INILanRead("frmDataUser", "msg3", ""), vbOKOnly, App.Title & " : "
                        GoTo skip
                    End If
                End If
                
                If jml_data > limit Then
                    loop_value = jml_data \ limit
                    For iii = 1 To loop_value
                        last_start = last_start + limit
                        jml_data = 0
                        
                        jsonText = "{""apikey"":""?1"",""cloud_id"":""?2"",""date_get"":""?3"",""curr_time"":""?4"",""token"":""?5"",""APP_ID"":""?6"",""start"":""?7"",""limit"":""?8""}"
                        jsonText = Replace(jsonText, "?1", apikey)
                        jsonText = Replace(jsonText, "?2", cloud_id)
                        jsonText = Replace(jsonText, "?3", date1_str)
                        jsonText = Replace(jsonText, "?4", curr_time)
                        jsonText = Replace(jsonText, "?5", Token)
                        jsonText = Replace(jsonText, "?6", APP_ID)
                        jsonText = Replace(jsonText, "?7", last_start)
                        jsonText = Replace(jsonText, "?8", limit)
                        
                        response = SendRequestPostHTTP3(server_name & "api/desktop/attendance_log", jsonText, date1_str & "_" & Trim(CStr(iii)))
                        If (response = "HTTP protocol error") Or (response = "") Then
                            'MsgBox INILanRead("frmDataUser", "msg4", ""), vbOKOnly, App.Title & " : "
                            GoTo skip
                        End If
                        
                        str_value = GetJSONByKey(response, "success")
                        If Trim(str_value) = "true" Then
                            jml_data = GetJSONByKey(response, "total")
                            If jml_data > 0 Then
                                Total = Total + jml_data
                                ReadJSONAttlog "0", response
                            End If
                        Else
                            kode = GetJSONByKey(response, "code")
                            If kode = "2" Then
                                'MsgBox INILanRead("frmDataUser", "msg3", ""), vbOKOnly, App.Title & " : "
                                GoTo skip
                            End If
                        End If
                        DoEvents
                    Next
                End If
                
                str2 = DateTimeToStr(upload_date, DateFormatDatabase)
                strSQL = "REPLACE INTO t_param (id_param, value_param) VALUES ('last_dl_scan_gps_date', """ & str2 & """) "
                ExecuteSQLite_Data strSQL
            End If
        Next i
skip:
        
        DoEvents
        rs.MoveNext
    Loop
    
    Exit Sub
        
BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Sub

Public Function GetTglTerakhir(server_name As String, cloud_id As String, apikey As String) As Date
    On Error GoTo BugError
            
    Dim response As String
    Dim str_value As String
    Dim Token As String
    Dim jsonText As String
    Dim kode As String
    Dim tgl As String
    Dim yy, mm, dd, hh, nn, ss As Integer
    Dim Data As String
    Dim tgl_akhir As Date
    
    GetTglTerakhir = DateAdd("d", -30, Now)
    
    Token = LCase(MD5(cloud_id + apikey))
    jsonText = "{""apikey"":""?1"",""cloud_id"":""?2"",""token"":""?3""}"
    jsonText = Replace(jsonText, "?1", apikey)
    jsonText = Replace(jsonText, "?2", cloud_id)
    jsonText = Replace(jsonText, "?3", Token)
    
    response = SendRequestPostHTTP3(server_name & "api/desktop/device_last_data", jsonText, "GetTglTerakhir")
    If (response = "HTTP protocol error") Or (response = "") Then
        'MsgBox INILanRead("frmDataUser", "msg4", ""), vbOKOnly, App.Title & " : "
        Exit Function
    End If
    
    str_value = GetJSONByKey(response, "success")
    If Trim(str_value) = "true" Then
        'data = GetJSONByKey(response, "data")
        tgl = GetJSONByKey(response, "data", "app_date_time")
        If tgl = "" Then
            tgl = GetJSONByKey(response, "data", "date_time")
        End If
        
        If tgl = "" Then
            tgl = GetJSONByKey(response, "data", "created_at")
        End If
          
        If Trim(tgl) <> "" Then
          yy = Val(Mid(tgl, 1, 4))
          mm = Val(Mid(tgl, 6, 2))
          dd = Val(Mid(tgl, 9, 2))
          hh = Val(Mid(tgl, 12, 2))
          nn = Val(Mid(tgl, 15, 2))
          ss = Val(Mid(tgl, 18, 2))
          tgl_akhir = DateSerial(yy, mm, dd) & " " & TimeSerial(0, 0, 0)
          GetTglTerakhir = DateAdd("d", 1, tgl_akhir)
        End If
    Else
        kode = GetJSONByKey(response, "code")
        If kode = "2" Then
            'MsgBox INILanRead("frmDataUser", "msg3", ""), vbOKOnly, App.Title & " : "
            GoTo skip
        End If
    End If
    
skip:
    
    Exit Function
        
BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Function

Public Function GetLastDownload() As Date
    On Error GoTo BugError
    
    Dim terakhir_dl As Date
    
    GetLastDownload = DateAdd("d", -90, Now)
    
    Dim str1 As String
    Dim tgl As Date
    Dim yy, mm, dd As Integer
    
    str1 = LoadParam("last_dl_scan_gps_date")
    If Trim(str1) <> "" Then
        yy = Val(Mid(str1, 1, 4))
        mm = Val(Mid(str1, 6, 2))
        dd = Val(Mid(str1, 9, 2))
      
        tgl = DateSerial(yy, mm, dd) & " " & TimeSerial(0, 0, 0)
        terakhir_dl = terakhir_dl = DateAdd("d", -1, tgl)
    End If
    
    Exit Function
        
BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Function

Public Sub GetTglUpload(sn_device As String, server_name As String, apikey As String, tgl1 As Date, tgl2 As Date)
    On Error GoTo BugError
            
    Dim success As Long
    Dim jsonArray As New ChilkatJsonArray
    
    Dim json_value As String
    Dim str1, str2 As String
    Dim yy, mm, dd, hh, nn, ss As Integer
    Dim jsonText As String
    Dim response As String
    Dim str_value As String
    Dim kode As String
    
    Dim i As Long
    Dim jsonObj As ChilkatJsonObject
                
    str1 = DateTimeToStr(tgl1, "yyyy-mm-dd")
    str2 = DateTimeToStr(tgl2, "yyyy-mm-dd")
    
    jsonText = "{""apikey"":""?1"",""sn_device"":""?2"",""tgl1"":""?3"",""tgl2"":""?4""}"
    jsonText = Replace(jsonText, "?1", apikey)
    jsonText = Replace(jsonText, "?2", sn_device)
    jsonText = Replace(jsonText, "?3", str1)
    jsonText = Replace(jsonText, "?4", str2)
    
    response = SendRequestPostHTTP3(server_name & "api/desktop/date_list", jsonText, "GetTglUpload")
    If (response = "HTTP protocol error") Or (response = "") Then
        'MsgBox INILanRead("frmDataUser", "msg4", ""), vbOKOnly, App.Title & " : "
        Exit Sub
    End If
    
    str_value = GetJSONByKey(response, "success")
    If Trim(str_value) = "true" Then
        json_value = GetJSONByKey(response, "data")
        success = jsonArray.Load(json_value)
        
        ReDim list_tgl_upload(jsonArray.SIZE)
        For i = 0 To jsonArray.SIZE - 1
            Set jsonObj = jsonArray.ObjectAt(i)
            'MsgBox jsonObj.StringOf("date")
            
            list_tgl_upload(i) = jsonObj.StringOf("date")
        Next i
    Else
        kode = GetJSONByKey(response, "code")
        If kode = "2" Then
            'MsgBox INILanRead("frmDataUser", "msg3", ""), vbOKOnly, App.Title & " : "
            GoTo skip
        End If
    End If
    
skip:
    
    Exit Sub
        
BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Sub

Public Function GetJSONByKey(input_data As String, key_str As String, Optional subkey_str As String = "") As String
    On Error GoTo BugError
            
    Dim JSON As New ChilkatJsonObject
    Dim jsonObj As ChilkatJsonObject
    Dim success As Long
    
    GetJSONByKey = ""
    success = JSON.Load(input_data)
    GetJSONByKey = JSON.StringOf(key_str)
    
    If GetJSONByKey = "" And subkey_str <> "" Then
        Set jsonObj = JSON.ObjectOf(key_str)
        
        GetJSONByKey = jsonObj.StringOf(subkey_str)
    End If
    
    Exit Function
        
BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Function

Public Function StringJsonForURL(jsonStr As String) As String
    On Error GoTo BugError
    
    Dim JSON As New ChilkatJsonObject

    Dim success As Long
    success = JSON.Load(jsonStr)
    If (success <> 1) Then
        Debug.Print JSON.LastErrorText
        Exit Function
    End If
    
    Dim numMembers As Long
    Dim i As Long
    
    Dim name_str As String
    Dim value_str As String
    Dim iValue As Long
    Dim paramGet As String
    
    paramGet = ""
    numMembers = JSON.SIZE
    For i = 0 To numMembers - 1
        name_str = JSON.NameAt(i)
        value_str = JSON.StringAt(i)
        'Debug.Print name_str & ": " & value_str
    
        iValue = JSON.IntAt(i)
        'Debug.Print name_str & " as integer: " & iValue
        
        If paramGet = "" Then
            paramGet = "/" & value_str
        Else
            paramGet = paramGet & "/" & value_str
        End If
    Next
    StringJsonForURL = paramGet
    
    Exit Function
        
BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Function

Public Function SendRequestPostHTTP3(url As String, jsonStr As String, no_urut As String) As String
    On Error GoTo BugError
    
    Dim http As New ChilkatHttp
    
    Dim success As Long
    
    SendRequestPostHTTP3 = ""
    success = http.UnlockComponent("FNGSPT.CB4042019_acprLRCam76f")
    If (success <> 1) Then
        Debug.Print http.LastErrorText
        Exit Function
    End If
    
    Dim str1 As String
    Dim html As String
    
    str1 = StringJsonForURL(jsonStr)
    str1 = url & str1
    html = http.QuickGetStr(str1)
    SendRequestPostHTTP3 = html
    'TextToFile str1 & vbNewLine & vbNewLine & html, "SendRequestHTTP_" & no_urut & ".txt"
    
    Exit Function
        
BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Function

Public Function SendRequestPostHTTP(url_str As String, jsonStr As String) As String
    On Error GoTo BugError
            
    Dim req As New ChilkatHttpRequest
    Dim http As New ChilkatHttp
    
    Dim success As Long
    
    SendRequestPostHTTP = ""
    success = http.UnlockComponent("FNGSPT.CB4042019_acprLRCam76f")
    If (success <> 1) Then
        Debug.Print http.LastErrorText
        Exit Function
    End If
    
    http.AcceptCharset = ""
    http.UserAgent = ""
    http.AcceptLanguage = ""
    http.AllowGzip = 0
    
    'success = http.AddQuickHeader("Cookie", "JSESSIONID=1234")
    
    Dim jsonText As String
    'jsonText = EncodeJSON(jsonStr)
    jsonText = jsonStr
    
    Dim resp As ChilkatHttpResponse
    Set resp = http.PostJson(url_str, jsonText)
    If (resp Is Nothing) Then
        Debug.Print http.LastErrorText & vbCrLf
    Else
        'TextToFile url_str & vbNewLine & vbNewLine & jsonText & vbNewLine & vbNewLine & resp.BodyStr, "SendRequestHTTP.txt"
        SendRequestPostHTTP = resp.BodyStr & vbCrLf
    End If
    
    Exit Function
        
BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Function

Public Function SendRequestPostHTTP2(url_str As String, jsonStr As String) As String
    On Error GoTo BugError
            
    Dim req As New ChilkatHttpRequest
    Dim http As New ChilkatHttp
    
    Dim success As Long
    
    SendRequestPostHTTP2 = ""
    success = http.UnlockComponent("FNGSPT.CB4042019_acprLRCam76f")
    If (success <> 1) Then
        Debug.Print http.LastErrorText
        Exit Function
    End If
    
    http.SetRequestHeader "Authorization", "my-custom-computed-auth-value"
    http.SetRequestHeader "X-Pass-Timestamp", "my-custom-computed-timestamp-value"
    http.SetRequestHeader "Accept", "application/json"
    
    http.AcceptCharset = ""
    http.UserAgent = ""
    http.AcceptLanguage = ""
    http.AllowGzip = 0
    
    'success = http.AddQuickHeader("Cookie", "JSESSIONID=1234")
    
    Dim jsonText As String
    'jsonText = EncodeJSON(jsonStr)
    jsonText = jsonStr
    
    Dim resp As ChilkatHttpResponse
    'Set resp = http.PostJson(url_str, jsonText)
    Set resp = http.PostJson2(url_str, "application/json", jsonText)
    If (resp Is Nothing) Then
        Debug.Print http.LastErrorText & vbCrLf
    Else
        'TextToFile url_str & vbNewLine & vbNewLine & jsonText & vbNewLine & vbNewLine & resp.BodyStr, "SendRequestHTTP.txt"
        SendRequestPostHTTP2 = resp.BodyStr & vbCrLf
    End If
    
    Exit Function
        
BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Function

Public Function SendRequestGetHTTP(url_str As String, jsonStr As String) As String
    On Error GoTo BugError
    
    Dim req As New ChilkatHttpRequest
    Dim http As New ChilkatHttp
    
    Dim success As Long
    
    SendRequestGetHTTP = ""
    success = http.UnlockComponent("FNGSPT.CB4042019_acprLRCam76f")
    If (success <> 1) Then
        Debug.Print http.LastErrorText
        Exit Function
    End If
    
    http.AcceptCharset = ""
    http.UserAgent = ""
    http.AcceptLanguage = ""
    
    http.AllowGzip = 0
    
    'success = http.AddQuickHeader("Cookie", "JSESSIONID=1234")
    'success = http.AddQuickHeader("Content-Encoding", "identity")
    
    ' Modify the default "Accept" header:
    http.Accept = "application/jsonrequest"
    
    Dim jsonText As String
    jsonText = EncodeJSON(jsonStr)
    
    Dim responseBody As String
    responseBody = http.PutText(url_str, jsonStr, "utf-8", "application/jsonrequest", 0, 0)
    If (http.LastMethodSuccess <> 1) Then
        Debug.Print http.LastErrorText
    Else
        'Debug.Print responseBody
        'TextToFile url_str & vbNewLine & vbNewLine & jsonText & vbNewLine & vbNewLine & responseBody, "SendRequestGetHTTP.txt"
        SendRequestGetHTTP = responseBody
    End If
    
    Exit Function
        
BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Function

Public Function SendRequestGetHTTP2(url_str As String, jsonStr As String) As String
    On Error GoTo BugError
    
    Dim http As New ChilkatHttp
    Dim success As Long
    
    SendRequestGetHTTP2 = ""
    success = http.UnlockComponent("FNGSPT.CB4042019_acprLRCam76f")
    If (success <> 1) Then
        Debug.Print http.LastErrorText
        Exit Function
    End If
    
    Dim jsonText As String
    'jsonText = EncodeJSON(jsonStr)
    jsonText = StringJsonForURL(jsonStr)
    
    ' Send the HTTP GET and return the content in a string.
    Dim html As String
    html = http.QuickGetStr(url_str & jsonText)
    SendRequestGetHTTP2 = html
    'TextToFile url_str & jsonText, "SendRequestHTTP.txt"
    
    Exit Function
        
BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Function
