Attribute VB_Name = "mdlPublic"
' ===============================================================================
' Win32 API Functions
' ===============================================================================

Public Declare Sub CopyMemory Lib "kernel32" Alias "RtlMoveMemory" (Destination As Any, Source As Any, ByVal Length As Long)
Public Declare Sub ZeroMemory Lib "kernel32" Alias "RtlZeroMemory" (Destination As Any, ByVal Length As Long)
Public Declare Sub Sleep Lib "kernel32" (ByVal dwMilliseconds As Long)


' ===============================================================================
' FingerKeeper Interface Functions
' ===============================================================================

'// Connection
Public Declare Function FK_ConnectComm Lib "FK623Attend" (ByVal nMachineNo As Long, ByVal nComPort As Long, ByVal nBaudRate As Long, ByVal pstrTelNumber As String, ByVal nWaitDialTime As Long, ByVal nLicense As Long, ByVal nComTimeOut As Long) As Long
Public Declare Function FK_ConnectNet Lib "FK623Attend" (ByVal nMachineNo As Long, ByVal strIpAddress As String, ByVal nNetPort As Long, ByVal nTimeOut As Long, ByVal nProtocolType As Long, ByVal nNetPassword As Long, ByVal nLicense As Long) As Long
Public Declare Function FK_ConnectUSB Lib "FK623Attend" (ByVal nMachineNo As Long, ByVal nLicense As Long) As Long
Public Declare Sub FK_DisConnect Lib "FK623Attend" (ByVal nHandleIndex As Long)

'// Device Setting
Public Declare Function FK_EnableDevice Lib "FK623Attend" (ByVal nHandleIndex As Long, ByVal nEnableFlag As Byte) As Long
Public Declare Sub FK_PowerOnAllDevice Lib "FK623Attend" (ByVal nHandleIndex As Long)
Public Declare Function FK_PowerOffDevice Lib "FK623Attend" (ByVal nHandleIndex As Long) As Long
Public Declare Function FK_GetDeviceStatus Lib "FK623Attend" (ByVal nHandleIndex As Long, ByVal nStatusIndex As Long, ByRef pnValue As Long) As Long
Public Declare Function FK_GetDeviceTime Lib "FK623Attend" (ByVal nHandleIndex As Long, ByRef pnDateTime As Date) As Long
Public Declare Function FK_SetDeviceTime Lib "FK623Attend" (ByVal nHandleIndex As Long, ByVal nDateTime As Date) As Long
Public Declare Function FK_GetDeviceInfo Lib "FK623Attend" (ByVal nHandleIndex As Long, ByVal nInfoIndex As Long, ByRef pnValue As Long) As Long
Public Declare Function FK_SetDeviceInfo Lib "FK623Attend" (ByVal nHandleIndex As Long, ByVal nInfoIndex As Long, ByVal nValue As Long) As Long
Public Declare Function FK_GetProductData Lib "FK623Attend" (ByVal nHandleIndex As Long, ByVal nDataIndex As Long, ByRef pstrValue As String) As Long

Public Declare Function FK_HS_ExecJsonCmd Lib "FK623Attend" (ByVal nHandleIndex As Long, ByRef pstrValue As String) As Long

'// Log Data
Public Declare Function FK_LoadSuperLogData Lib "FK623Attend" (ByVal nHandleIndex As Long, ByVal nReadMark As Long) As Long
Public Declare Function FK_USBLoadSuperLogDataFromFile Lib "FK623Attend" (ByVal nHandleIndex As Long, ByVal astrFilePath As String) As Long
Public Declare Function FK_GetSuperLogData Lib "FK623Attend" (ByVal nHandleIndex As Long, ByRef pnSEnrollNumber As Long, ByRef pnGEnrollNumber As Long, ByRef nManipulation As Long, ByRef pnBackupNumber As Long, ByRef pnDateTime As Date) As Long
Public Declare Function FK_GetSuperLogData_1 Lib "FK623Attend" (ByVal nHandleIndex As Long, ByRef pnSEnrollNumber As Long, ByRef pnGEnrollNumber As Long, ByRef nManipulation As Long, ByRef pnBackupNumber As Long, ByRef apnYear As Long, ByRef apnMonth As Long, ByRef apnDay As Long, ByRef apnHour As Long, ByRef apnMinute As Long, ByRef apnSec As Long) As Long
Public Declare Function FK_EmptySuperLogData Lib "FK623Attend" (ByVal nHandleIndex As Long) As Long

Public Declare Function FK_LoadGeneralLogData Lib "FK623Attend" (ByVal nHandleIndex As Long, ByVal nReadMark As Long) As Long
Public Declare Function FK_USBLoadGeneralLogDataFromFile Lib "FK623Attend" (ByVal nHandleIndex As Long, ByVal astrFilePath As String) As Long
Public Declare Function FK_GetGeneralLogData Lib "FK623Attend" (ByVal nHandleIndex As Long, ByRef pnEnrollNumber As Long, ByRef pnVerifyMode As Long, ByRef pnInOutMode As Long, ByRef pnDateTime As Date) As Long
Public Declare Function FK_GetGeneralLogData_1 Lib "FK623Attend" (ByVal nHandleIndex As Long, ByRef pnEnrollNumber As Long, ByRef pnVerifyMode As Long, ByRef pnInOutMode As Long, ByRef apnYear As Long, ByRef apnMonth As Long, ByRef apnDay As Long, ByRef apnHour As Long, ByRef apnMinute As Long, ByRef apnSec As Long) As Long
Public Declare Function FK_EmptyGeneralLogData Lib "FK623Attend" (ByVal nHandleIndex As Long) As Long

'// Bell Time
Public Declare Function FK_GetBellTime Lib "FK623Attend" (ByVal nHandleIndex As Long, ByRef pnBellCount As Long, ByRef ptBellInfo As Any) As Long
Public Declare Function FK_SetBellTime Lib "FK623Attend" (ByVal nHandleIndex As Long, ByVal nBellCount As Long, ByRef ptBellInfo As Any) As Long

'// Enroll Data
Public Declare Function FK_GetEnrollData Lib "FK623Attend" (ByVal nHandleIndex As Long, ByVal nEnrollNumber As Long, ByVal nBackupNumber As Long, ByRef pnMachinePrivilege As Long, ByRef pnEnrollData As Any, ByRef pnPassWord As Long) As Long
Public Declare Function FK_PutEnrollData Lib "FK623Attend" (ByVal nHandleIndex As Long, ByVal nEnrollNumber As Long, ByVal nBackupNumber As Long, ByVal nMachinePrivilege As Long, ByRef pnEnrollData As Any, ByVal nPassWord As Long) As Long
Public Declare Function FK_SaveEnrollData Lib "FK623Attend" (ByVal nHandleIndex As Long) As Long
Public Declare Function FK_DeleteEnrollData Lib "FK623Attend" (ByVal nHandleIndex As Long, ByVal nEnrollNumber As Long, ByVal nBackupNumber As Long) As Long

Public Declare Function FK_SetUSBModel Lib "FK623Attend" (ByVal nHandleIndex As Long, ByVal anModel As Long) As Long
Public Declare Function FK_SetUDiskFileFKModel Lib "FK623Attend" (ByVal nHandleIndex As Long, ByVal astrFKModel As String) As Long

Public Declare Function FK_USBReadAllEnrollDataFromFile Lib "FK623Attend" (ByVal nHandleIndex As Long, ByVal pstrFilePath As String) As Long
Public Declare Function FK_USBReadAllEnrollDataCount Lib "FK623Attend" (ByVal nHandleIndex As Long, ByVal pnValue As Long) As Long
Public Declare Function FK_USBGetOneEnrollData Lib "FK623Attend" (ByVal nHandleIndex As Long, ByRef pnEnrollNumber As Long, ByRef pnBackupNumber As Long, ByRef pnMachinePrivilege As Long, ByRef pnEnrollData As Any, ByRef pnPassWord As Long, ByRef pnEnableFlag As Long, ByRef pnEnrollName As String) As Long
Public Declare Function FK_USBSetOneEnrollData Lib "FK623Attend" (ByVal nHandleIndex As Long, ByVal nEnrollNumber As Long, ByVal nBackupNumber As Long, ByVal nMachinePrivilege As Long, ByRef pnEnrollData As Any, ByVal nPassWord As Long, ByVal nEnableFlag As Long, ByVal pnEnrollName As String) As Long
Public Declare Function FK_USBWriteAllEnrollDataToFile Lib "FK623Attend" (ByVal nHandleIndex As Long, ByVal pstrFilePath As String) As Long

Public Declare Function FK_USBReadAllEnrollDataFromFile_Color Lib "FK623Attend" (ByVal nHandleIndex As Long, ByVal pstrFilePath As String) As Long
Public Declare Function FK_USBWriteAllEnrollDataToFile_Color Lib "FK623Attend" (ByVal nHandleIndex As Long, ByVal pstrFilePath As String, ByVal anNewsKind As Long) As Long
Public Declare Function FK_USBGetOneEnrollData_Color Lib "FK623Attend" (ByVal nHandleIndex As Long, ByRef pnEnrollNumber As Long, ByRef pnBackupNumber As Long, ByRef pnMachinePrivilege As Long, ByRef pnEnrollData As Any, ByRef pnPassWord As Long, ByRef pnEnableFlag As Long, ByRef pnEnrollName As String, ByVal anNewsKind As Long) As Long
Public Declare Function FK_USBSetOneEnrollData_Color Lib "FK623Attend" (ByVal nHandleIndex As Long, ByVal nEnrollNumber As Long, ByVal nBackupNumber As Long, ByVal nMachinePrivilege As Long, ByRef pnEnrollData As Any, ByVal nPassWord As Long, ByVal nEnableFlag As Long, ByVal pnEnrollName As String, ByVal anNewsKind As Long) As Long


Public Declare Function FK_EnableUser Lib "FK623Attend" (ByVal nHandleIndex As Long, ByVal nEnrollNumber As Long, ByVal nBackupNumber As Long, ByVal nEnableFlag As Long) As Long
Public Declare Function FK_ModifyPrivilege Lib "FK623Attend" (ByVal nHandleIndex As Long, ByVal nEnrollNumber As Long, ByVal nBackupNumber As Long, ByVal nMachinePrivilege As Long) As Long
Public Declare Function FK_BenumbAllManager Lib "FK623Attend" (ByVal nHandleIndex As Long) As Long
Public Declare Function FK_ReadAllUserID Lib "FK623Attend" (ByVal nHandleIndex As Long) As Long
Public Declare Function FK_GetAllUserID Lib "FK623Attend" (ByVal nHandleIndex As Long, ByRef pnEnrollNumber As Long, ByRef pnBackupNumber As Long, ByRef pnMachinePrivilege As Long, ByRef pnEnable As Long) As Long
Public Declare Function FK_EmptyEnrollData Lib "FK623Attend" (ByVal nHandleIndex As Long) As Long
Public Declare Function FK_ClearKeeperData Lib "FK623Attend" (ByVal nHandleIndex As Long) As Long

Public Declare Function FK_SetFontName Lib "FK623Attend" (ByVal nHandleIndex As Long, ByVal aStrFontName As String, ByVal anFontType As Long) As Long

Public Declare Function FK_GetUserName Lib "FK623Attend" (ByVal nHandleIndex As Long, ByVal nEnrollNumber As Long, ByRef pstrUserName As String) As Long
Public Declare Function FK_SetUserName Lib "FK623Attend" (ByVal nHandleIndex As Long, ByVal nEnrollNumber As Long, ByVal pstrUserName As String) As Long
Public Declare Function FK_GetNewsMessage Lib "FK623Attend" (ByVal nHandleIndex As Long, ByVal nNewsId As Long, ByRef pstrNews As String) As Long
Public Declare Function FK_SetNewsMessage Lib "FK623Attend" (ByVal nHandleIndex As Long, ByVal nNewsId As Long, ByVal pstrNews As String) As Long
Public Declare Function FK_GetUserNewsID Lib "FK623Attend" (ByVal nHandleIndex As Long, ByVal nEnrollNumber As Long, ByRef pnNewsId As Long) As Long
Public Declare Function FK_SetUserNewsID Lib "FK623Attend" (ByVal nHandleIndex As Long, ByVal nEnrollNumber As Long, ByVal nNewsId As Long) As Long

'// Access Control(Haoshun FK)
Public Declare Function FK_GetDoorStatus Lib "FKAttend" (ByVal nHandleIndex As Long, ByRef apnStatusVal As Long) As Long
Public Declare Function FK_SetDoorStatus Lib "FKAttend" (ByVal nHandleIndex As Long, ByVal anStatusVal As Long) As Long
Public Declare Function FK_HS_GetTimeZone Lib "FK623Attend" (ByVal anHandleIndex As Long, ByRef apOneTimeZone As Any) As Long
Public Declare Function FK_HS_SetTimeZone Lib "FK623Attend" (ByVal anHandleIndex As Long, ByRef apOneTimeZone As Any) As Long
Public Declare Function FK_HS_GetUserWeekPassTime Lib "FK623Attend" (ByVal anHandleIndex As Long, ByRef apUserWeekPassTime As Any) As Long
Public Declare Function FK_HS_SetUserWeekPassTime Lib "FK623Attend" (ByVal anHandleIndex As Long, ByRef apUserWeekPassTime As Any) As Long

Public Declare Function FK_HS_GetWeekOpenDoorTime Lib "FK623Attend" (ByVal anHandleIndex As Long, ByRef apUserWeekPassTime As Any) As Long
Public Declare Function FK_HS_SetWeekOpenDoorTime Lib "FK623Attend" (ByVal anHandleIndex As Long, ByRef apUserWeekPassTime As Any) As Long

'// Etc Functions
Public Declare Function FK_ConnectGetIP Lib "FK623Attend" (ByRef apnComName As Any) As Long
Public Declare Function FK_GetAdjustInfo Lib "FK623Attend" (ByVal nHandleIndex As Long, ByRef dwAdjustedState As Long, ByRef dwAdjustedMonth As Long, ByRef dwAdjustedDay As Long, ByRef dwAdjustedHour As Long, ByRef dwAdjustedMinute As Long, ByRef dwRestoredState As Long, ByRef dwRestoredMonth As Long, ByRef dwRestoredDay As Long, ByRef dwRestoredHour As Long, ByRef dwRestoredMinte As Long) As Long
Public Declare Function FK_SetAdjustInfo Lib "FK623Attend" (ByVal nHandleIndex As Long, ByVal dwAdjustedState As Long, ByVal dwAdjustedMonth As Long, ByVal dwAdjustedDay As Long, ByVal dwAdjustedHour As Long, ByVal dwAdjustedMinute As Long, ByVal dwRestoredState As Long, ByVal dwRestoredMonth As Long, ByVal dwRestoredDay As Long, ByVal dwRestoredHour As Long, ByVal dwRestoredMinte As Long) As Long
Public Declare Function FK_GetAccessTime Lib "FK623Attend" (ByVal nHandleIndex As Long, ByVal anEnrollNumber As Long, ByRef apnAccessTime As Long) As Long
Public Declare Function FK_SetAccessTime Lib "FK623Attend" (ByVal nHandleIndex As Long, ByVal anEnrollNumber As Long, ByVal anAccessTime As Long) As Long
Public Declare Function FK_GetRealTimeInfo Lib "FK623Attend" (ByVal nHandleIndex As Long, ByRef apGetRealTime As Long) As Long
Public Declare Function FK_SetRealTimeInfo Lib "FK623Attend" (ByVal nHandleIndex As Long, ByRef apSetRealTime As Long) As Long
Public Declare Function FK_GetServerNetInfo Lib "FK623Attend" (ByVal nHandleIndex As Long, ByRef astrServerIPAddress As String, ByRef apServerPort As Long, ByRef apServerRequest As Long) As Long
Public Declare Function FK_SetServerNetInfo Lib "FK623Attend" (ByVal nHandleIndex As Long, ByVal astrServerIPAddress As String, ByVal anServerPort As Long, ByVal apServerReques As Long) As Long

'// Post & Shift Info (HTML version)
Public Declare Function FK_GetOneShiftInfo Lib "FK623Attend" (ByVal nHandleIndex As Long, ByVal anShiftNumber As Long, ByRef apShiftSHour As Long, ByRef apShiftSMinute As Long, ByRef apShiftEHour As Long, ByRef apShiftEMinute As Long, ByRef apstrShiftName As String) As Long
Public Declare Function FK_SetOneShiftInfo Lib "FK623Attend" (ByVal nHandleIndex As Long, ByVal anShiftNumber As Long, ByVal anShiftSHour As Long, ByVal anShiftSMinute As Long, ByVal anShiftEHour As Long, ByVal anShiftEMinute As Long, ByVal astrShiftName As String) As Long
Public Declare Function FK_GetOnePostInfo Lib "FK623Attend" (ByVal nHandleIndex As Long, ByVal anPostNumber As Long, ByRef apstrPostName As String, ByRef apShiftNumber1 As Long, ByRef apShiftNumber2 As Long, ByRef apShiftNumber3 As Long, ByRef apShiftNumber4 As Long) As Long
Public Declare Function FK_SetOnePostInfo Lib "FK623Attend" (ByVal nHandleIndex As Long, ByVal anPostNumber As Long, ByVal astrPostName As String, ByVal anShiftNumber1 As Long, ByVal anShiftNumber2 As Long, ByVal anShiftNumber3 As Long, ByVal anShiftNumber4 As Long) As Long
Public Declare Function FK_GetUserInfo Lib "FK623Attend" (ByVal nHandleIndex As Long, ByVal anEnrollNumber As Long, ByRef apstrUserName As String, ByRef apNewKind As Long, ByRef apVerifyMode As Long, ByRef apPostID As Long, ByRef apShiftNumber1 As Long, ByRef apShiftNumber2 As Long, ByRef apShiftNumber3 As Long, ByRef apShiftNumber4 As Long) As Long
Public Declare Function FK_SetUserInfo Lib "FK623Attend" (ByVal nHandleIndex As Long, ByVal anEnrollNumber As Long, ByVal astrUserName As String, ByVal anNewKind As Long, ByVal anVerifyMode As Long, ByVal anPostID As Long, ByVal anShiftNumber1 As Long, ByVal anShiftNumber2 As Long, ByVal anShiftNumber3 As Long, ByVal anShiftNumber4 As Long) As Long

'// Post & Shift Info (EXCEL version)
Public Declare Function FK_GetPostShiftInfo Lib "FK623Attend" (ByVal nHandleIndex As Long, ByRef apPostShiftInfo As Long, ByRef apPostShiftInfoLen As Long) As Long
Public Declare Function FK_SetPostShiftInfo Lib "FK623Attend" (ByVal nHandleIndex As Long, ByRef apPostShiftInfo As Long, ByVal anPostShiftInfoLen As Long) As Long
Public Declare Function FK_GetUserInfoEx Lib "FK623Attend" (ByVal nHandleIndex As Long, ByVal anEnrollNumber As Long, ByRef apUserInfo As Long, ByRef apUserInfoLen As Long) As Long
Public Declare Function FK_SetUserInfoEx Lib "FK623Attend" (ByVal nHandleIndex As Long, ByVal anEnrollNumber As Long, ByRef apUserInfo As Long, ByVal anUserInfoLen As Long) As Long

'// Photo transfer
Public Declare Function FK_GetEnrollPhoto Lib "FK623Attend" (ByVal nHandleIndex As Long, ByVal anEnrollNumber As Long, ByRef apPhotoImage As Any, ByRef anPhotoLength As Long) As Long
Public Declare Function FK_SetEnrollPhoto Lib "FK623Attend" (ByVal nHandleIndex As Long, ByVal anEnrollNumber As Long, ByRef apPhotoImage As Any, ByVal anPhotoLength As Long) As Long
Public Declare Function FK_DeleteEnrollPhoto Lib "FK623Attend" (ByVal nHandleIndex As Long, ByVal anEnrollNumber As Long) As Long
Public Declare Function FK_GetLogPhoto Lib "FK623Attend" (ByVal nHandleIndex As Long, ByVal anEnrollNumber As Long, ByVal anYear As Long, ByVal anMonth As Long, ByVal anDay As Long, ByVal anHour As Long, ByVal anMinute As Long, ByVal anSec As Long, ByRef apPhotoImage As Any, ByRef anPhotoLength As Long) As Long

'// Get supported flag of specific enroll data type
Public Declare Function FK_IsSupportedEnrollData Lib "FK623Attend" (ByVal nHandleIndex As Long, ByVal anBackupNumber As Long, ByRef anSupportFlag As Long) As Long

'// All purpose
Public Declare Function FK_ExecCommand Lib "FK623Attend" (ByVal nHandleIndex As Long, ByVal astrCommand As String, ByRef astrResult As String) As Long

Public Declare Function FK_ExtCommand Lib "FK623Attend" (ByVal nHandleIndex As Long, ByRef apCmdStruct As Any) As Long

    
    
    
    Public Declare Function FK_GetIsSupportStringID Lib "FK623Attend" ( _
        ByVal nHandleIndex As Long _
    ) As Long

    Public Declare Function FK_GetLogDataIsSupportStringID Lib "FK623Attend" ( _
        ByVal nHandleIndex As Long _
    ) As Long
    Public Declare Function FK_GetUSBEnrollDataIsSupportStringID Lib "FK623Attend" ( _
        ByVal nHandleIndex As Long _
    ) As Long

    Public Declare Function FK_GetSuperLogData_StringID Lib "FK623Attend" ( _
        ByVal nHandleIndex As Long, _
        ByRef strSEnrollNumber As String, _
        ByRef strGEnrollNumber As String, _
        ByRef nManipulation As Long, _
        ByRef nBackupNumber As Long, _
        ByRef dtLogTime As Date _
     ) As Long


    Public Declare Function FK_GetGeneralLogData_StringID_Workcode Lib "FK623Attend" ( _
        ByVal nHandleIndex As Long, _
        ByRef strEnrollNumber As String, _
        ByRef nVerifyMode As Long, _
        ByRef nInOutMode As Long, _
        ByRef dtLogTime As Date, _
        ByRef apnWorkCode As Long _
     ) As Long


    Public Declare Function FK_GetGeneralLogData_StringID Lib "FK623Attend" ( _
        ByVal nHandleIndex As Long, _
        ByRef strEnrollNumber As String, _
        ByRef nVerifyMode As Long, _
        ByRef nInOutMode As Long, _
        ByRef dtLogTime As Date _
     ) As Long


    Public Declare Function FK_EnableUser_StringID Lib "FK623Attend" ( _
        ByVal nHandleIndex As Long, _
        ByVal strEnrollNumber As String, _
        ByVal nBackupNumber As Long, _
        ByVal nEnableFlag As Long _
    ) As Long


    Public Declare Function FK_ModifyPrivilege_StringID Lib "FK623Attend" ( _
        ByVal nHandleIndex As Long, _
        ByVal strEnrollNumber As String, _
        ByVal nBackupNumber As Long, _
        ByVal nMachinePrivilege As Long _
     ) As Long


    Public Declare Function FK_GetAllUserID_StringID Lib "FK623Attend" ( _
       ByVal nHandleIndex As Long, _
       ByRef strEnrollNumber As String, _
       ByRef nBackupNumber As Long, _
       ByRef nMachinePrivilege As Long, _
       ByRef nEnable As Long _
   ) As Long

    Public Declare Function FK_GetEnrollData_StringID Lib "FK623Attend" ( _
       ByVal nHandleIndex As Long, _
       ByVal strEnrollNumber As String, _
       ByVal nBackupNumber As Long, _
       ByRef nMachinePrivilege As Long, _
       ByRef bytEnrollData As Any, _
       ByRef nPassWord As Long _
    ) As Long

    Public Declare Function FK_PutEnrollData_StringID Lib "FK623Attend" ( _
       ByVal nHandleIndex As Long, _
       ByVal strEnrollNumber As String, _
       ByVal nBackupNumber As Long, _
       ByVal nMachinePrivilege As Long, _
       ByRef bytEnrollData As Any, _
       ByVal nPassWord As Long _
    ) As Long

    Public Declare Function FK_DeleteEnrollData_StringID Lib "FK623Attend" ( _
       ByVal nHandleIndex As Long, _
       ByVal strEnrollNumber As String, _
       ByVal nBackupNumber As Long _
    ) As Long



    Public Declare Function FK_GetUserName_StringID Lib "FK623Attend" ( _
      ByVal nHandleIndex As Long, _
      ByVal strEnrollNumber As String, _
      ByRef strUserName As String _
    ) As Long


    Public Declare Function FK_SetUserName_StringID Lib "FK623Attend" ( _
      ByVal nHandleIndex As Long, _
      ByVal strEnrollNumber As String, _
      ByVal strUserName As String _
    ) As Long


    Public Declare Function FK_GetUserPassTime_StringID Lib "FK623Attend" ( _
      ByVal nHandleIndex As Long, _
      ByVal strEnrollNumber As String, _
      ByRef nGroupID As Long, _
          ByRef bytUserPassTimeInfo As Any, _
      ByVal nUserPassTimeInfoSize As Long _
    ) As Long

    Public Declare Function FK_SetUserPassTime_StringID Lib "FK623Attend" ( _
      ByVal nHandleIndex As Long, _
      ByVal strEnrollNumber As String, _
      ByVal nGroupID As Long, _
          ByVal bytUserPassTimeInfo As Any, _
          ByVal nUserPassTimeInfoSize As Long _
    ) As Long

    Public Declare Function FK_USBGetOneEnrollData_StringID Lib "FK623Attend" ( _
      ByVal nHandleIndex As Long, _
      ByRef strEnrollNumber As String, _
      ByRef nBackupNumber As Long, _
      ByRef nMachinePrivilege As Long, _
      ByRef bytEnrollData As Any, _
      ByRef nPassWord As Long, _
      ByRef nEnableFlag As Long, _
      ByRef strEnrollName As String _
    ) As Long

    Public Declare Function FK_USBSetOneEnrollData_StringID Lib "FK623Attend" ( _
        ByVal nHandleIndex As Long, _
        ByVal strEnrollNumber As String, _
        ByVal nBackupNumber As Long, _
        ByVal nMachinePrivilege As Long, _
        ByRef bytEnrollData As Any, _
        ByVal nPassWord As Long, _
        ByVal nEnableFlag As Long, _
        ByVal strEnrollName As String _
     ) As Long


    Public Declare Function FK_GetUserInfoEx_StringID Lib "FK623Attend" ( _
        ByVal nHandleIndex As Long, _
        ByVal strEnrollNumber As String, _
        ByRef abytUserInfo As Any, _
        ByRef apUserInfoLen As Long _
     ) As Long

    Public Declare Function FK_SetUserInfoEx_StringID Lib "FK623Attend" ( _
        ByVal nHandleIndex As Long, _
        ByVal strEnrollNumber As String, _
        ByRef abytUserInfo As Any, _
        ByVal anUserInfoLen As Long _
     ) As Long


    Public Declare Function FK_GetEnrollPhoto_StringID Lib "FK623Attend" ( _
        ByVal nHandleIndex As Long, _
        ByVal strEnrollNumber As String, _
                ByRef abytPhotoImage As Any, _
        ByRef anPhotoLength As Long _
     ) As Long


    Public Declare Function FK_SetEnrollPhoto_StringID Lib "FK623Attend" ( _
        ByVal nHandleIndex As Long, _
        ByVal strEnrollNumber As String, _
        ByRef abytPhotoImage As Any, _
        ByVal anPhotoLength As Long _
     ) As Long


    Public Declare Function FK_DeleteEnrollPhoto_StringID Lib "FK623Attend" ( _
        ByVal nHandleIndex As Long, _
        ByVal strEnrollNumber As String _
     ) As Long


    Public Declare Function FK_GetLogPhoto_StringID Lib "FK623Attend" ( _
        ByVal nHandleIndex As Long, _
        ByVal strEnrollNumber As String, _
        ByVal anYear As Long, _
        ByVal anMonth As Long, _
        ByVal anDay As Long, _
        ByVal anHour As Long, _
        ByVal anMinute As Long, _
        ByVal anSec As Long, _
        ByRef abytPhotoImage As Any, _
        ByRef anPhotoLength As Long _
    ) As Long
' ===============================================================================
' FingerKeeper Interface Constants & Structures
' ===============================================================================
Public Const CHINA_FONTNAME = "Arial"
Public Const JAPAN_FONTNAME = "MS PGothic"
Public Const THAI_FONTNAME = "AngsanaUPC"

'--- FK_SetUSBModel Parameters ---
Public Const FK625_FP1000 = 2001
Public Const FK625_FP2000 = 2002
Public Const FK625_FP3000 = 2003
Public Const FK625_FP5000 = 2004
Public Const FK625_FP10000 = 2005
Public Const FK625_FP30000 = 2006
Public Const FK625_ID30000 = 2007
Public Const FK635_FP700 = 3001
Public Const FK635_FP3000 = 3002
Public Const FK635_FP10000 = 3003
Public Const FK635_ID30000 = 3004
Public Const FK723_FP1000 = 4001
Public Const FK725_FP1000 = 5001
Public Const FK725_FP1500 = 5002
Public Const FK725_ID5000 = 5003
Public Const FK725_ID30000 = 5004
Public Const FK735_FP500 = 6001
Public Const FK735_FP3000 = 6002
Public Const FK735_ID30000 = 6003
Public Const FK925_FP3000 = 7001
Public Const FK935_FP3000 = 8001


'{--- Bell Time ---
Public Const MAX_BELLCOUNT_DAY = 24
Type BELLINFO
    mValid(MAX_BELLCOUNT_DAY - 1) As Byte
    mHour(MAX_BELLCOUNT_DAY - 1) As Byte
    mMinute(MAX_BELLCOUNT_DAY - 1) As Byte
End Type             '72byte
'}--- Bell Time ---


'{--- Pass Control Time ---
Public Const MAX_PASSCTRLGROUP_COUNT = 50
Public Const MAX_PASSCTRL_COUNT = 7

Type PASSTIME
    StartHour  As Byte
    StartMinute  As Byte
    EndHour  As Byte
    EndMinute  As Byte
End Type          '4byte

' Pass Control Time Infomation
Type PASSCTRLTIME
    mPassCtrlTime(MAX_PASSCTRL_COUNT - 1) As PASSTIME
End Type          '28byte

Public Const MAX_USERPASSINFO_COUNT = 3
Type USERPASSINFO
    UserPassID(MAX_USERPASSINFO_COUNT - 1) As Byte
End Type          '3byte

Public Const MAX_GROUPPASSKIND_COUNT = 5
Public Const MAX_GROUPPASSINFO_COUNT = 3
Type GROUPPASSINFO
    GroupPassID(MAX_GROUPPASSINFO_COUNT - 1) As Byte
End Type          '3byte

Public Const MAX_GROUPMATCHINFO_COUNT = 10
Type GROUPMATCHINFO
    GroupMatch(MAX_GROUPMATCHINFO_COUNT - 1) As Integer
End Type          '20byte
'}--- Pass Control Time ---


'{--- Realtime Setting ---
Public Const MAX_REAL_TIME = 4
Type REALTIMEINFO
    Valid As Byte
    AckTime As Byte
    WaitTime As Byte
    Reserve As Byte
    SendPos As Long
    Hour(MAX_REAL_TIME - 1) As Byte
    Minute(MAX_REAL_TIME - 1) As Byte
End Type     ' 16 Byte
'}--- Realtime Setting ---


'{--- Post & Shift Info ---
Public Const NAME_BYTE_COUNT = 128
Public Const MAX_SHIFT_COUNT = 24             ' maximum shift count
Public Const MAX_POST_COUNT = 16              ' maximum post count
Public Const MAX_DAY_IN_MONTH = 31            ' maximum days in a month

Type SHIFT_TIME_SLOT
    AMStartH   As Byte     ' AM time(hour)
    AMStartM   As Byte     ' AM min(minute)
    AMEndH     As Byte     ' AM time(hour)
    AMEndM     As Byte     ' AM min(minute)
    
    PMStartH   As Byte     ' PM time(hour)
    PMStartM   As Byte     ' PM min(minute)
    PMEndH     As Byte     ' PM time(hour)
    PMEndM     As Byte     ' PM min(minute)
    
    OVStartH   As Byte     ' OV time(hour)
    OVStartM   As Byte     ' OV min(minute)
    OVEndH     As Byte     ' OV time(hour)
    OVEndM     As Byte     ' OV min(minute)
End Type    ' size = 12 byte

Type POST_NAME
    PostName(NAME_BYTE_COUNT - 1) As Byte
End Type    ' size = 14 byte

Public Const VER2_POST_SHIFT_INFO = 2
Public Const SIZE_POST_SHIFT_INFO_V2 = 2476
Type POST_SHIFT_INFO
    Size As Long                                        '// 0, 4
    Ver  As Long                                        '// 4, 4
    ShiftTime(MAX_SHIFT_COUNT - 1) As SHIFT_TIME_SLOT   '// 8, 288 (=12*24)
    PostInfo(MAX_POST_COUNT - 1) As POST_NAME           '// 296, 2048 (=128*16)
    CompanyName(NAME_BYTE_COUNT - 1) As Byte            '// 2344, 128
    Reserved(4 - 1) As Byte                             '// 2472, 4
End Type  ' size = 2476 bytes

Public Const VER2_USER_INFO = 2
Public Const SIZE_USER_INFO_V2 = 184
Type USER_INFO
    Size As Long                            '// 0, 4
    Ver  As Long                            '// 4, 4
    UserID As Long                          '// 8, 4
    Reserved As Long                        '// 12, 4
    UserName(NAME_BYTE_COUNT - 1) As Byte   '// 16, 128
    PostId As Long                          '// 144, 4
    YearAssigned As Integer                 '// 148, 2
    MonthAssigned As Integer                '// 150, 2
    StartWeekdayOfMonth As Byte             '// 152, 1
    ShiftId(MAX_DAY_IN_MONTH) As Byte       '// 153, 31
End Type  ' size = 184 bytes

Public Const USER_ID_LEN13_1 = 32
Public Const USER_ID_LEN = 16
Public Const VER3_USER_INFO = 3
Public Const SIZE_USER_INFO_V3 = 196
Type USER_INFO_STRING_ID
    Size As Long                            '// 0, 4
    Ver  As Long                            '// 4, 4
    UserID(USER_ID_LEN - 1) As Byte   '// 16, 128
    Reserved As Long                        '// 12, 4
    UserName(NAME_BYTE_COUNT - 1) As Byte   '// 16, 128
    PostId As Long                          '// 144, 4
    YearAssigned As Integer                 '// 148, 2
    MonthAssigned As Integer                '// 150, 2
    StartWeekdayOfMonth As Byte             '// 152, 1
    ShiftId(MAX_DAY_IN_MONTH) As Byte       '// 153, 31
End Type  ' size = 196 bytes

'}--- Post & Shift Info ---

'{--- Access Control(Haoshun FK) ---
Public Const TIME_ZONE_COUNT = 255
Public Const TIME_SLOT_COUNT = 6

Type TIME_SLOT
    StartH   As Byte     ' AM time(hour)
    StartM   As Byte     ' AM min(minute)
    EndH     As Byte     ' AM time(hour)
    EndM     As Byte     ' AM min(minute)
End Type ' size = 4 bytes

Public Const SIZE_TIME_ZONE_STRUCT = 32
Type TIME_ZONE
    Size As Long                                '// 0, 4
    TimeZoneId  As Long                         '// 4, 4
    TimeSlots(TIME_SLOT_COUNT - 1) As TIME_SLOT '// 8, 24(=4*6)
End Type  ' size = 32 bytes

Public Const SIZE_USER_WEEK_PASS_TIME_STRUCT = 16
Type USER_WEEK_PASS_TIME
    Size As Long                '// 0, 4
    UserID  As Long             '// 4, 4
    WeekPassTime(7 - 1) As Byte '// 8, 7
    Reserved As Byte            '// 15, 1
End Type  ' size = 16 bytes


Public Const SIZE_WEEK_OPEN_DOOR_TIME_STRUCT = 16
Type WEEK_OPEN_DOOR_TIME
    Size As Long                '// 0, 4
    Type  As Long             '// 4, 4
    WeekPassTime(7 - 1) As Byte '// 8, 7
    Reserved As Byte            '// 15, 1
End Type  ' size = 16 bytes
'}--- Access Control(Haoshun FK) ---

'//============= News =============================//
Public Const NEWS_STANDARD = 1          '  character 60
Public Const NEWS_EXTEND = 2            '  character 24

'//=============== Protocol Type ===============//
Public Const PROTOCOL_TCPIP = 0               ' TCP/IP
Public Const PROTOCOL_UDP = 1                 ' UDP

'//=============== Backup Number Constant ===============//
Public Const BACKUP_FP_0 = 0                  ' Finger 0
Public Const BACKUP_FP_1 = 1                  ' Finger 1
Public Const BACKUP_FP_2 = 2                  ' Finger 2
Public Const BACKUP_FP_3 = 3                  ' Finger 3
Public Const BACKUP_FP_4 = 4                  ' Finger 4
Public Const BACKUP_FP_5 = 5                  ' Finger 5
Public Const BACKUP_FP_6 = 6                  ' Finger 6
Public Const BACKUP_FP_7 = 7                  ' Finger 7
Public Const BACKUP_FP_8 = 8                  ' Finger 8
Public Const BACKUP_FP_9 = 9                  ' Finger 9
Public Const BACKUP_PSW = 10                  ' Password
Public Const BACKUP_CARD = 11                 ' Card
Public Const BACKUP_FACE = 12                 ' Face
Public Const BACKUP_PALMVEIN_0 = 13           ' Palm Vein
Public Const BACKUP_PALMVEIN_1 = 14           ' Palm Vein
Public Const BACKUP_PALMVEIN_2 = 15           ' Palm Vein
Public Const BACKUP_PALMVEIN_3 = 16           ' Palm Vein

Public Const BACKUP_VEIN_0 = 20               ' Vein

'//=============== Manipulation of SuperLogData ===============//
Public Const LOG_ENROLL_USER = 3              ' Enroll-User
Public Const LOG_ENROLL_MANAGER = 4           ' Enroll-Manager
Public Const LOG_ENROLL_DELFP = 5             ' FP Delete
Public Const LOG_ENROLL_DELPASS = 6           ' Pass Delete
Public Const LOG_ENROLL_DELCARD = 7           ' Card Delete
Public Const LOG_LOG_ALLDEL = 8               ' LogAll Delete
Public Const LOG_SETUP_SYS = 9                ' Setup Sys
Public Const LOG_SETUP_TIME = 10              ' Setup Time
Public Const LOG_SETUP_LOG = 11               ' Setup Log
Public Const LOG_SETUP_COMM = 12              ' Setup Comm
Public Const LOG_PASSTIME = 13                ' Pass Time Set
Public Const LOG_SETUP_DOOR = 14              ' Door Set Log

'//=============== VerifyMode of GeneralLogData ===============//
Public Const LOG_FPVERIFY = 1               ' Fp Verify
Public Const LOG_PASSVERIFY = 2             ' Pass Verify
Public Const LOG_CARDVERIFY = 3             ' Card Verify
Public Const LOG_FPPASS_VERIFY = 4          ' Pass+Fp Verify
Public Const LOG_FPCARD_VERIFY = 5          ' Card+Fp Verify
Public Const LOG_PASSFP_VERIFY = 6          ' Pass+Fp Verify
Public Const LOG_CARDFP_VERIFY = 7          ' Card+Fp Verify
Public Const LOG_CARDPASS_VERIFY = 9        ' Card+Pass Verify

Public Const LOG_VER_CLOSE_DOOR = 11        'Door Close
Public Const LOG_VER_OPEN_HAND = 12         'Hand Open
Public Const LOG_VER_PROG_OPEN = 14         'Open by PC
Public Const LOG_VER_PROG_CLOSE = 15        'Close by PC
Public Const LOG_VER_OPEN_IREGAL = 16       'Iregal Open
Public Const LOG_VER_CLOSE_IREGAL = 17      'Iregal Close
Public Const LOG_VER_OPEN_COVER = 18        'Cover Open
Public Const LOG_VER_CLOSE_COVER = 19       'Cover Close

Public Const LOG_FACEVERIFY = 20            ' Face Verify
Public Const LOG_FACECARDVERIFY = 21        ' Face+Card Verify
Public Const LOG_FACEPASSVERIFY = 22        ' Face+Pass Verify
Public Const LOG_CARDFACEVERIFY = 23        ' Card+Face Verify
Public Const LOG_PASSFACEVERIFY = 24        ' Pass+Face Verify

Public Const LOG_VEINVERIFY = 30            'Vein Verify
Public Const LOG_VEINCARDVERIFY = 31        'Vein+Card Verify
Public Const LOG_VEINPASSVERIFY = 32        'Vein+Pass Verify
Public Const LOG_CARDVEINVERIFY = 33        'Card+Vein Verify
Public Const LOG_PASSVEINVERIFY = 34        'Pass+Vein Verify
Public Const LOG_OPEN_THREAT = 48           'Door Open as threat

'//=============== IOMode of GeneralLogData ===============//
Public Const LOG_IOMODE_IO = 0
Public Const LOG_IOMODE_IN1 = 1
Public Const LOG_IOMODE_OUT1 = 2
Public Const LOG_IOMODE_IN2 = 3
Public Const LOG_IOMODE_OUT2 = 4
Public Const LOG_IOMODE_IN3 = 5
Public Const LOG_IOMODE_OUT3 = 6
Public Const LOG_IOMODE_IN4 = 7
Public Const LOG_IOMODE_OUT4 = 8

'//=============== DoorMode of GeneralLogData ===============//
Public Const LOG_CLOSE_DOOR = 1                    'Door Close
Public Const LOG_OPEN_HAND = 2                     'Hand Open
Public Const LOG_PROG_OPEN = 3                     'Open by PC
Public Const LOG_PROG_CLOSE = 4                    'Close by PC
Public Const LOG_OPEN_IREGAL = 5                   'Illegal Open
Public Const LOG_CLOSE_IREGAL = 6                  'Illegal Close
Public Const LOG_OPEN_COVER = 7                    'Cover Open
Public Const LOG_CLOSE_COVER = 8                   'Cover Close
Public Const LOG_OPEN_DOOR = 9                     'Door Open
Public Const LOG_OPEN_DOOR_THREAT = 10             'Door Open
Public Const LOG_FORCE_OPEN = 11                   'Door Open
Public Const LOG_FORCE_CLOSE = 12                  'Door Open
Public Const LOG_FIRE_ALARM = 13                   'Door Open

'//=============== Verify Kind of Device ===============//
 Public Const VK_NONE = 0
 Public Const VK_FP = 1
 Public Const VK_PASS = 2
 Public Const VK_CARD = 3
 Public Const VK_FACE = 4
 Public Const VK_VEIN = 5
 Public Const VK_IRIS = 6
 Public Const VK_PV = 7
 Public Const VK_VOICE = 8
    
'//=============== IOMode of GeneralLogData ===============//
' {-- modified by lih 2013-12-25 20:22
'Public Const LOG_MODE_IO = 0 'General
'Public Const LOG_MODE_ATTEND = 1 'Normal Attend
'Public Const LOG_MODE_OVERTIME_ATTEND = 2 'OverTime Attend
'Public Const LOG_MODE_ABNORMAL_ATTEND = 3 'In after leaving a while
'Public Const LOG_MODE_LEAVE = 4 'Normal Leave
'Public Const LOG_MODE_OVERTIME_LEAVE = 5 'OverTime Leave
'Public Const LOG_MODE_ABNORMAL_LEAVE = 6 'Abnormal Leave
' }-- modified by lih 2013-12-25 20:22

' ++ add by lih 2013-12-25 20:22
' The new value range of LOG_IOMODE is between 0 ~ 8

'//=============== Machine Privilege ===============//
Public Const MP_NONE = 0                  ' General user
Public Const MP_MANAGER_1 = 1             ' Manager1 : Super Manager(MP_ALL = 1(for in version)) -+ add by lih 2013-12-25 20:15
Public Const MP_MANAGER_2 = 2             ' Manager2 : General Manager ++ add by lih 2013-12-25 20:15
Public Const MP_MANAGER_3 = 3             ' Manager3 : Capable to register user ++ add by lih 2013-12-25 20:15

'//=============== Index of  GetDeviceStatus ===============//
Public Const GET_MANAGERS = 1
Public Const GET_USERS = 2
Public Const GET_FPS = 3
Public Const GET_PSWS = 4
Public Const GET_SLOGS = 5
Public Const GET_GLOGS = 6
Public Const GET_ASLOGS = 7
Public Const GET_AGLOGS = 8
Public Const GET_CARDS = 9
Public Const GET_FACES = 10
Public Const GET_VEINS = 40

Public Const GET_MAXUSERS = 200
Public Const GET_MAXFPS = 201
Public Const GET_MAXPSWS = 202
Public Const GET_MAXCARDS = 203
Public Const GET_MAXFACES = 204
Public Const GET_MAXPVS = 205
Public Const GET_MAXSLOGS = 206
Public Const GET_MAXGLOGS = 207

'//=============== Index of  GetDeviceInfo ===============//
Public Const DI_MANAGERS = 1                  ' Numbers of Manager
Public Const DI_MACHINENUM = 2                ' Device ID
Public Const DI_LANGAUGE = 3                  ' Language
Public Const DI_POWEROFF_TIME = 4             ' Auto-PowerOff Time
Public Const DI_LOCK_CTRL = 5                 ' Lock Control
Public Const DI_GLOG_WARNING = 6              ' General-Log Warning
Public Const DI_SLOG_WARNING = 7              ' Super-Log Warning
Public Const DI_VERIFY_INTERVALS = 8          ' Verify Interval Time
Public Const DI_RSCOM_BPS = 9                 ' Comm Buadrate
Public Const DI_DATE_SEPARATE = 10            ' Date Separate Symbol
Public Const DI_VERIFY_KIND = 24              ' Verify Kind Symbol
Public Const DI_MULTIUSERS = 77               ' MultiUser

'//=============== Baudrate = value of DI_RSCOM_BPS ===============//
Public Const BPS_9600 = 3
Public Const BPS_19200 = 4
Public Const BPS_38400 = 5
Public Const BPS_57600 = 6
Public Const BPS_115200 = 7

'//=============== Product Data Index ===============//
Public Const PRODUCT_SERIALNUMBER = 1    ' Serial Number
Public Const PRODUCT_BACKUPNUMBER = 2    ' Backup Number
Public Const PRODUCT_CODE = 3            ' Product code
Public Const PRODUCT_NAME = 4            ' Product name
Public Const PRODUCT_WEB = 5             ' Product web
Public Const PRODUCT_DATE = 6            ' Product date
Public Const PRODUCT_SENDTO = 7          ' Product sendto

'//=============== Door Status ===============//
Public Const DOOR_CONROLRESET = 0
Public Const DOOR_OPEND = 1
Public Const DOOR_CLOSED = 2
Public Const DOOR_COMMNAD = 3

'//=============== Error code ===============//
Public Const RUN_SUCCESS = 1
Public Const RUNERR_NOSUPPORT = 0
Public Const RUNERR_UNKNOWNERROR = -1
Public Const RUNERR_NO_OPEN_COMM = -2
Public Const RUNERR_WRITE_FAIL = -3
Public Const RUNERR_READ_FAIL = -4
Public Const RUNERR_INVALID_PARAM = -5
Public Const RUNERR_NON_CARRYOUT = -6
Public Const RUNERR_DATAARRAY_END = -7
Public Const RUNERR_DATAARRAY_NONE = -8
Public Const RUNERR_MEMORY = -9
Public Const RUNERR_MIS_PASSWORD = -10
Public Const RUNERR_MEMORYOVER = -11
Public Const RUNERR_DATADOUBLE = -12
Public Const RUNERR_MANAGEROVER = -14
Public Const RUNERR_FPDATAVERSION = -15
Public Const RUNERR_LOGINOUTMODE = -16

'//=======================================================================================================
'// Win32 API

'The WideCharToMultiByte function maps a wide-character string to a new character string.
'The function is faster when both lpDefaultChar and lpUsedDefaultChar are NULL.

'CodePage
Private Const CP_ACP = 0 '
Private Const CP_MACCP = 2 'Mac
Private Const CP_OEMCP = 1 'OEM
Private Const CP_UTF7 = 65000
Private Const CP_UTF8 = 65001

'dwFlags
Private Const WC_NO_BEST_FIT_CHARS = &H400
Private Const WC_COMPOSITECHECK = &H200
Private Const WC_DISCARDNS = &H10
Private Const WC_SEPCHARS = &H20 'Default
Private Const WC_DEFAULTCHAR = &H40

Private Declare Function WideCharToMultiByte Lib "kernel32" ( _
    ByVal CodePage As Long, _
    ByVal dwFlags As Long, _
    ByVal lpWideCharStr As Long, _
    ByVal cchWideChar As Long, _
    ByVal lpMultiByteStr As Long, _
    ByVal cbMultiByte As Long, _
    ByVal lpDefaultChar As Long, _
    ByVal lpUsedDefaultChar As Long _
    ) As Long
                                                    
Private Declare Function MultiByteToWideChar Lib "kernel32" ( _
    ByVal CodePage As Long, _
    ByVal dwFlags As Long, _
    ByVal lpMultiByteStr As Long, _
    ByVal cchMultiByte As Long, _
    ByVal lpWideCharStr As Long, _
    ByVal cchWideChar As Long _
    ) As Long


Public Const gstrNoDevice = "No Device"

Function ReturnResultPrint(anResultCode As Long) As String
   Select Case anResultCode
        Case RUN_SUCCESS
            ReturnResultPrint = "Successful!"
        Case RUNERR_NOSUPPORT
            ReturnResultPrint = "No support"
        Case RUNERR_UNKNOWNERROR
            ReturnResultPrint = "Unknown error"
        Case RUNERR_NO_OPEN_COMM
            ReturnResultPrint = "No Open Comm"
        Case RUNERR_WRITE_FAIL
            ReturnResultPrint = "Write Error"
        Case RUNERR_READ_FAIL
            ReturnResultPrint = "Read Error"
        Case RUNERR_INVALID_PARAM
            ReturnResultPrint = "Parameter Error"
        Case RUNERR_NON_CARRYOUT
            ReturnResultPrint = "execution of command failed"
        Case RUNERR_DATAARRAY_END
            ReturnResultPrint = "End of data"
        Case RUNERR_DATAARRAY_NONE
            ReturnResultPrint = "Nonexistence data"
        Case RUNERR_MEMORY
            ReturnResultPrint = "Memory Allocating Error"
        Case RUNERR_MIS_PASSWORD
            ReturnResultPrint = "License Error"
        Case RUNERR_MEMORYOVER
            ReturnResultPrint = "full enrolldata & can`t put enrolldata"
        Case RUNERR_DATADOUBLE
            ReturnResultPrint = "this ID is already  existed."
        Case RUNERR_MANAGEROVER
            ReturnResultPrint = "full manager & can`t put manager."
        Case RUNERR_FPDATAVERSION
            ReturnResultPrint = "mistake fp data version."
        Case Else
            ReturnResultPrint = "Unknown error"
    End Select
End Function

Function ToLong(aStr As String) As Long
On Error GoTo Err_toLong
    ToLong = CInt(aStr)
    Exit Function
Err_toLong:
    ToLong = 0
End Function

'----------------------------------------------------------------
' Convert vb string to byte array in utf-16 encoding
'   !!note : string in vb are Unicode string
' The result buffer is ended by two zero

Sub StringToByteArrayUtf16(aStr As String, aByteAry() As Byte)
    Dim bytBuffer() As Byte
    Dim i As Integer, len_min As Integer
    
    If LBound(aByteAry) <> 0 Then Exit Sub
    
    bytBuffer = Trim(aStr)
    
    len_min = UBound(bytBuffer) - LBound(bytBuffer) + 1
    If len_min > (UBound(aByteAry) - LBound(aByteAry) + 1 - 2) Then
        len_min = (UBound(aByteAry) - LBound(aByteAry) + 1 - 2)
    End If
    
    For i = LBound(aByteAry) To UBound(aByteAry)
        aByteAry(i) = 0
    Next
    
    For i = 0 To len_min - 1
        aByteAry(i) = bytBuffer(i)
    Next
End Sub

'----------------------------------------------------------------
' Convert byte array in utf-16 encoding to string
'   !!note : string in vb are Unicode string

Function ByteArrayUtf16ToString(aByteAry() As Byte) As String
    Dim bytTemp() As Byte
    Dim k As Integer, nValidLen As Integer

    nValidLen = 0
    For k = 0 To UBound(aByteAry) \ 2
        If aByteAry(2 * k) = 0 And aByteAry(2 * k + 1) = 0 Then Exit For
        nValidLen = nValidLen + 1
    Next k

    If nValidLen = 0 Then
        ByteArrayUtf16ToString = ""
        Exit Function
    End If

    ReDim bytTemp(nValidLen * 2 - 1)
    CopyMemory bytTemp(0), aByteAry(0), 2 * nValidLen
    ByteArrayUtf16ToString = bytTemp
End Function

Public Function StringToUTF8(astrUnicode As String) As String
    Dim lngResult As Long
    Dim bytTemp() As Byte
    
    ReDim bytTemp(0)
    lngResult = WideCharToMultiByte(CP_UTF8, 0, StrPtr(astrUnicode), Len(astrUnicode), _
                                    0, 0, 0, 0)
    If lngResult > 0 Then
        ReDim bytTemp(lngResult - 1)
        WideCharToMultiByte CP_UTF8, 0, StrPtr(astrUnicode), Len(astrUnicode), _
                            VarPtr(bytTemp(0)), lngResult, 0, 0
        
    End If
    StringToUTF8 = bytTemp
End Function

Public Function UTF8ToString(astrUtf8 As String) As String
    Dim vByteUnicode() As Byte
    Dim k As Long
    Dim lRet As Long
    
    k = LenB(astrUtf8)
    ReDim vByteUnicode(2 * k) As Byte
    
    lRet = MultiByteToWideChar(CP_UTF8, 0, StrPtr(astrUtf8), k, ByVal VarPtr(vByteUnicode(0)), k)
    If lRet < 1 Then
        UTF8ToString = ""
        Exit Function
    End If
    
    ReDim Preserve vByteUnicode(2 * lRet - 1) As Byte
    UTF8ToString = vByteUnicode
End Function

Public Function GetHexString(ByVal aByteVal As Byte) As String
    GetHexString = Hex$(aByteVal)
    If Len(GetHexString) <> 2 Then GetHexString = "0" & GetHexString
End Function

Public Function GetByteVal_1(ByVal astrHex_1 As String) As Byte
    If Len(astrHex_1) = 1 Then
        If AscB(astrHex_1) >= AscB("0") And AscB(astrHex_1) <= AscB("9") Then
            GetByteVal_1 = AscB(astrHex_1) - AscB("0")
        ElseIf AscB(astrHex_1) = AscB("A") Or AscB(astrHex_1) = AscB("a") Then
            GetByteVal_1 = 10
        ElseIf AscB(astrHex_1) = AscB("B") Or AscB(astrHex_1) = AscB("b") Then
            GetByteVal_1 = 11
        ElseIf AscB(astrHex_1) = AscB("C") Or AscB(astrHex_1) = AscB("c") Then
            GetByteVal_1 = 12
        ElseIf AscB(astrHex_1) = AscB("D") Or AscB(astrHex_1) = AscB("d") Then
            GetByteVal_1 = 13
        ElseIf AscB(astrHex_1) = AscB("E") Or AscB(astrHex_1) = AscB("e") Then
            GetByteVal_1 = 14
        ElseIf AscB(astrHex_1) = AscB("F") Or AscB(astrHex_1) = AscB("f") Then
            GetByteVal_1 = 15
        End If
    Else
        GetByteVal_1 = 0
    End If
End Function

Public Function GetByteVal(ByVal astrHex As String) As Byte
    If Len(astrHex) = 1 Then
        GetByteVal = GetByteVal_1(astrHex)
    ElseIf Len(astrHex) = 2 Then
        GetByteVal = GetByteVal_1(Mid(astrHex, 1, 1)) * 16 + GetByteVal_1(Mid(astrHex, 2, 1))
    Else
        GetByteVal = 0
    End If
End Function

Public Function StringToUTF16HexString(ByVal astrVal As String) As String
    Dim bytUtf16() As Byte
    Dim k As Long
    Dim lRet As Long
    
    'bytUtf16 = StrConv(astrVal, vbUnicode)
    bytUtf16 = astrVal
    For k = LBound(bytUtf16) To UBound(bytUtf16)
        StringToUTF16HexString = StringToUTF16HexString & GetHexString(bytUtf16(k))
    Next k
End Function

Public Function UTF16HexStringToString(ByVal astrUtf16Hex As String) As String
    Dim bytUtf16() As Byte
    Dim k As Long, lenStr As Long
    Dim strHex As String
        
    UTF16HexStringToString = ""
    lenStr = Len(astrUtf16Hex)
    If lenStr = 0 Then Exit Function
    If (lenStr Mod 2) <> 0 Then Exit Function
        
    ReDim bytUtf16(lenStr / 2 - 1)
    For k = 0 To lenStr / 2 - 1
        strHex = Mid(astrUtf16Hex, k * 2 + 1, 2)
        bytUtf16(k) = GetByteVal(strHex)
    Next k
    
    'UTF16HexStringToString = StrConv(bytUtf16, vbFromUnicode)
    UTF16HexStringToString = bytUtf16
End Function

Function ConvertToLong(asVal As String) As Long
    On Error GoTo errl_ConvertToLong
    ConvertToLong = CLng(asVal)
    Exit Function
errl_ConvertToLong:
    ConvertToLong = 0
End Function


Public Sub String2ByteArray(aByteAry() As Byte, aStr As String)
    Dim bytBuffer() As Byte
    Dim lLenB As Long, NumChars As Long
    Dim lRet As Long
    Dim i As Long, k As Long, lenArray As Long
    
    lenArray = UBound(aByteAry, 1) - LBound(aByteAry, 1) + 1
    If lenArray = 1 Then Exit Sub
    
    aStr = Trim(aStr)
    lLenB = LenB(aStr)
    NumChars = Len(aStr)
    ReDim bytBuffer(lLenB)
    lRet = WideCharToMultiByte(CP_ACP, 0&, ByVal StrPtr(aStr), NumChars, ByVal VarPtr(bytBuffer(0)), lLenB + 1, 0&, 0&)
    If lRet < 1 Then
        Exit Sub
    End If
    
    k = 0
    Call ZeroMemory(aByteAry(0), lenArray)
    For i = 0 To UBound(bytBuffer)
        aByteAry(k) = bytBuffer(i)
        k = k + 1
        If k >= lenArray Then Exit For
    Next
End Sub

Public Function GetStringVerifyMode(anVerifyMode As Long) As String
    Dim vbyteKind(3) As Byte
    Dim nIndex, vFirstKind, vSecondKind As Byte
    Dim vRet As String
        
    BitConverter.SetLong vbyteKind, 0, anVerifyMode
    
    vRet = ""
    For nIndex = 0 To 3
    
            vFirstKind = vbyteKind(3 - nIndex)
            
            vSecondKind = vbyteKind(3 - nIndex)
            
            vFirstKind = vFirstKind And -16
            
            vSecondKind = vSecondKind And 15
            
            vFirstKind = vFirstKind / 2 ^ 4
            
            If vFirstKind = 0 Then Exit For
                
            If nIndex > 0 Then vRet = vRet + "+"
                
            Select Case vFirstKind
                Case VK_FP
                    vRet = vRet + "FP"
                Case VK_PASS
                    vRet = vRet + "PASS"
                Case VK_CARD
                    vRet = vRet + "CARD"
                Case VK_FACE
                    vRet = vRet + "FACE"
                Case VK_VEIN
                    vRet = vRet + "VEIN"
                Case VK_IRIS
                    vRet = vRet + "IRIS"
                Case VK_PV
                    vRet = vRet + "PALMVEIN"
                    
           End Select
           
           If vSecondKind = 0 Then Exit For
                
           vRet = vRet + "+"
            
            Select Case vSecondKind
                Case VK_FP
                    vRet = vRet + "FP"
                Case VK_PASS
                    vRet = vRet + "PASS"
                Case VK_CARD
                    vRet = vRet + "CARD"
                Case VK_FACE
                    vRet = vRet + "FACE"
                Case VK_VEIN
                    vRet = vRet + "VEIN"
                Case VK_IRIS
                    vRet = vRet + "IRIS"
                Case VK_PV
                    vRet = vRet + "PALMVEIN"
            End Select
           
    Next
    
        If vRet = "" Then vRet = "--"
        
        GetStringVerifyMode = vRet

End Function

Public Sub GetIoModeAndDoorMode(nIoMode As Long, ByRef vIoMode As Long, ByRef vDoorMode As Long)
    Dim vbyteKind(4) As Byte
    
    BitConverter.SetLong vbyteKind, 0, nIoMode
    
    'BitConverter.CopyArray vbyteKind, 1, vbyteDoorMode, 0, 4
    
    vDoorMode = BitConverter.GetLong(vbyteKind, 1)
    vIoMode = vbyteKind(0)
End Sub


