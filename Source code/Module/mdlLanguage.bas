Attribute VB_Name = "mdlLanguage"
Option Explicit

Public Function MsgBoxLanguageFile(errNumber As String, Optional strValue1 As String, Optional strValue2 As String, Optional strValue3 As String, Optional Buttons As VbMsgBoxStyle = vbOKOnly + vbInformation) As VbMsgBoxResult
    Dim Cnn As cConnection
    Dim rs As cRecordset
    Dim temp As String
    
    temp = ""
    temp = INILanRead("ErrMsg", errNumber, "", strValue1, strValue2, strValue3)
    'temp = Replace(temp, "?1", strValue1)
    'temp = Replace(temp, "?2", strValue2)
    'temp = Replace(temp, "?3", strValue3)
    
    If temp = errNumber Then
        temp = INILanRead2("ErrMsg", errNumber, "", strValue1, strValue2, strValue3)
    End If
    
    MsgBoxLanguageFile = MsgBox(temp, Buttons, App.Title)
End Function

Public Function deleteDataConfirmation(Question As String) As VbMsgBoxResult
    deleteDataConfirmation = MsgBox(Question, vbYesNo Or vbDefaultButton2, App.Title)
End Function

Public Function MsgboxConfirmation(Question As String, MsgTitle As String) As VbMsgBoxResult
    MsgboxConfirmation = MsgBox(Question, vbOKCancel + vbInformation, MsgTitle)
End Function

Public Function askInputData(Title As String, Prompt As String, DefaultValue As String) As String
    askInputData = InputBox(Prompt, Title, DefaultValue)
End Function

Public Sub ChangeFont(frm As Form, Optional frmName As String = "")
    On Error GoTo BugError
    
    Dim ctrl  As Control
    For Each ctrl In frm.Controls
        ctrl.Font.Name = "Century Gothic" '"Arial" '"MS Sans Serif" '
        ctrl.Font = "Century Gothic" '"Arial" '"MS Sans Serif" '
    Next
    
    Exit Sub
        
BugError:
    Select Case Err.Number
    Case 13
    Case 343
    Case 91
    Case 92
    Case 384
    Case 438
    Case Else
        'MsgBox Err.Description, , Err.Number
    End Select
    
    Err.Clear
    Resume Next
End Sub

Public Sub SetLang(frm As Form)
    On Error GoTo BugError
    
    Dim i As Long
    
    Dim labelx As Control
    For Each labelx In frm.Controls
        labelx.Font = "Century Gothic"
        labelx.Font.Name = "Century Gothic"
        'labelx.ForeColor = vbBlack
            
        If (TypeOf labelx Is Label) Then
            If labelx.ForeColor <> vbBlue And labelx.ForeColor <> vbRed And labelx.ForeColor <> &HFFFFFF Then labelx.ForeColor = vbBlack
            labelx.Caption = INILanRead(frm.Name, labelx.Name & "." & labelx.Index, "", "", "", "")
        ElseIf (TypeOf labelx Is XtremeSuiteControls.Label) Then
            If labelx.ForeColor <> vbBlue And labelx.ForeColor <> vbRed And labelx.ForeColor <> &HFFFFFF Then labelx.ForeColor = vbBlack
            labelx.Caption = INILanRead(frm.Name, labelx.Name & "." & labelx.Index, "", "", "", "")
        ElseIf (TypeOf labelx Is FlatEdit) Then
            If labelx.ForeColor <> vbBlue And labelx.ForeColor <> vbRed And labelx.ForeColor <> &HFFFFFF Then labelx.ForeColor = vbBlack
        ElseIf (TypeOf labelx Is XtremeSuiteControls.FlatEdit) Then
            If labelx.ForeColor <> vbBlue And labelx.ForeColor <> vbRed And labelx.ForeColor <> &HFFFFFF Then labelx.ForeColor = vbBlack
        ElseIf (TypeOf labelx Is TextBox) Then
            If labelx.ForeColor <> vbBlue And labelx.ForeColor <> vbRed And labelx.ForeColor <> &HFFFFFF Then labelx.ForeColor = vbBlack
        ElseIf (TypeOf labelx Is iGrid) Then
            If labelx.ForeColor <> vbBlue And labelx.ForeColor <> vbRed And labelx.ForeColor <> &HFFFFFF Then labelx.ForeColor = vbBlack
        ElseIf (TypeOf labelx Is XtremeSuiteControls.TabControl) Then
            labelx.Font.Bold = True
            'labelx.Font.SIZE = 10
            For i = 0 To labelx.ItemCount - 1
                labelx.Item(i).Caption = INILanRead(frm.Name, labelx.Name & "." & labelx.Index & "." & i, "", "", "", "")
            Next
        ElseIf (TypeOf labelx Is XtremeSuiteControls.PushButton) Then
            If labelx.ForeColor <> vbBlue And labelx.ForeColor <> vbRed And labelx.ForeColor <> &HFFFFFF Then labelx.ForeColor = vbBlack
            labelx.Font.Bold = True
            labelx.Caption = INILanRead(frm.Name, labelx.Name & "." & labelx.Index, "", "", "", "")
            labelx.ToolTipText = INILanRead(frm.Name, labelx.Name & "." & labelx.Index & "Tips", " ", "", "", "")
        ElseIf (TypeOf labelx Is PushButton) Then
            If labelx.ForeColor <> vbBlue And labelx.ForeColor <> vbRed And labelx.ForeColor <> &HFFFFFF Then labelx.ForeColor = vbBlack
            labelx.Font.Bold = True
            labelx.Caption = INILanRead(frm.Name, labelx.Name & "." & labelx.Index, "", "", "", "")
            labelx.ToolTipText = INILanRead(frm.Name, labelx.Name & "." & labelx.Index & "Tips", " ", "", "", "")
        ElseIf (TypeOf labelx Is XtremeSuiteControls.CheckBox) Then
            If labelx.ForeColor <> vbBlue And labelx.ForeColor <> vbRed And labelx.ForeColor <> &HFFFFFF Then labelx.ForeColor = vbBlack
            labelx.Caption = INILanRead(frm.Name, labelx.Name & "." & labelx.Index, "", "", "", "")
            labelx.ToolTipText = INILanRead(frm.Name, labelx.Name & "." & labelx.Index & "Tips", "", "", "", "")
        ElseIf (TypeOf labelx Is XtremeSuiteControls.RadioButton) Then
            If labelx.ForeColor <> vbBlue And labelx.ForeColor <> vbRed And labelx.ForeColor <> &HFFFFFF Then labelx.ForeColor = vbBlack
            labelx.Caption = INILanRead(frm.Name, labelx.Name & "." & labelx.Index, "", "", "", "")
            labelx.ToolTipText = INILanRead(frm.Name, labelx.Name & "." & labelx.Index & "Tips", "", "", "", "")
        ElseIf (TypeOf labelx Is XtremeSuiteControls.ListBox) Then
            If labelx.ForeColor <> vbBlue And labelx.ForeColor <> vbRed And labelx.ForeColor <> &HFFFFFF Then labelx.ForeColor = vbBlack
        ElseIf (TypeOf labelx Is XtremeSuiteControls.ComboBox) Then
            If labelx.ForeColor <> vbBlue And labelx.ForeColor <> vbRed And labelx.ForeColor <> &HFFFFFF Then labelx.ForeColor = vbBlack
        ElseIf (TypeOf labelx Is DateTimePicker) Then
        End If
    Next labelx
    
    If InStr(LCase("frmWizard"), LCase(frm.Name)) > 0 Then
        frm.Caption = "FingerspotOne UareU"
    Else
        frm.Caption = INILanRead(frm.Name, frm.Name, frm.Name, "", "", "")
    End If
    
    Exit Sub
        
BugError:
    Select Case Err.Number
    Case 13
    Case 343
    Case 91
    Case 92
    Case 384
    Case 438
    Case Else
        'MsgBox Err.Description, , Err.Number
    End Select
    
    Err.Clear
    Resume Next
End Sub
