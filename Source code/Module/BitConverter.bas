Attribute VB_Name = "BitConverter"
Private Type TwoBytes
    b(1) As Byte
End Type

Private Type FourBytes
    b(3) As Byte
End Type

Private Type EightBytes
    b(7) As Byte
End Type

Private Type BooleanValue
    value As Boolean
End Type

Private Type IntegerValue
    value As Integer
End Type

Private Type LongValue
    value As Long
End Type

Private Type SingleValue
    value As Single
End Type

Private Type DoubleValue
    value As Double
End Type

'Array bounds are not checked by the library.  The library tasks the
'programmer with ensuring the data will fit in the buffers.
Public Sub CopyArray(src() As Byte, srcIndex As Integer, dest() As Byte, destIndex As Integer, length As Integer)
    Dim i As Integer
    For i = 0 To length - 1
        dest(destIndex + i) = src(srcIndex + i)
    Next
End Sub

'It is possible to just check either byte for nonzero.  Only {0,0} returns false.
Public Function GetBoolean(b() As Byte, start As Integer) As Boolean
    Dim bytes As TwoBytes
    CopyArray b, start, bytes.b, 0, 2
    Dim v As BooleanValue
    LSet v = bytes
    GetBoolean = v.value
End Function

Public Sub SetBoolean(b() As Byte, start As Integer, value As Boolean)
    Dim v As BooleanValue
    v.value = value
    Dim bytes As TwoBytes
    LSet bytes = v
    CopyArray bytes.b, 0, b, start, 2
End Sub

Public Function GetInteger(b() As Byte, start As Integer) As Integer
    Dim bytes As TwoBytes
    CopyArray b, start, bytes.b, 0, 2
    Dim v As IntegerValue
    LSet v = bytes
    GetInteger = v.value
End Function

Public Sub SetInteger(b() As Byte, start As Integer, value As Integer)
    Dim v As IntegerValue
    v.value = value
    Dim bytes As TwoBytes
    LSet bytes = v
    CopyArray bytes.b, 0, b, start, 2
End Sub

Public Function GetLong(b() As Byte, start As Integer) As Long
    Dim bytes As FourBytes
    CopyArray b, start, bytes.b, 0, 4
    Dim v As LongValue
    LSet v = bytes
    GetLong = v.value
End Function

Public Sub SetLong(b() As Byte, start As Integer, value As Long)
    Dim v As LongValue
    v.value = value
    Dim bytes As FourBytes
    LSet bytes = v
    CopyArray bytes.b, 0, b, start, 4
End Sub

Public Function GetSingle(b() As Byte, start As Integer) As Single
    Dim bytes As FourBytes
    CopyArray b, start, bytes.b, 0, 4
    Dim v As SingleValue
    LSet v = bytes
    GetSingle = v.value
End Function

Public Sub SetSingle(b() As Byte, start As Integer, value As Single)
    Dim v As SingleValue
    v.value = value
    Dim bytes As FourBytes
    LSet bytes = v
    CopyArray bytes.b, 0, b, start, 4
End Sub

Public Function GetDouble(b() As Byte, start As Integer) As Double
    Dim bytes As EightBytes
    CopyArray b, start, bytes.b, 0, 8
    Dim v As DoubleValue
    LSet v = bytes
    GetDouble = v.value
End Function

Public Sub SetDouble(b() As Byte, start As Integer, value As Double)
    Dim v As DoubleValue
    v.value = value
    Dim bytes As EightBytes
    LSet bytes = v
    CopyArray bytes.b, 0, b, start, 8
End Sub

Public Function GetAscii(b() As Byte, start As Integer, length As Integer) As String
    Dim temp() As Byte
    ReDim temp(length - 1)
    Dim i As Integer
    For i = 0 To length - 1
        temp(i) = b(start + i)
    Next
    GetAscii = StrConv(temp, vbUnicode)
End Function

Public Sub SetAscii(b() As Byte, start As Integer, s As String)
    Dim temp() As Byte
    temp = StrConv(s, vbFromUnicode)
    Dim i As Integer
    For i = 0 To Len(s) - 1
        b(start + i) = temp(i)
    Next
End Sub





































































