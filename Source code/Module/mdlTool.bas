Attribute VB_Name = "mdlTool"
Option Explicit

Private Declare Function GetProcAddress Lib "kernel32" (ByVal hModule As Long, ByVal lpProcName As String) As Long
Private Declare Function GetModuleHandle Lib "kernel32" Alias "GetModuleHandleA" (ByVal lpModuleName As String) As Long
Private Declare Function GetCurrentProcess Lib "kernel32" () As Long
Private Declare Function IsWow64Process Lib "kernel32" (ByVal hProc As Long, bWow64Process As Boolean) As Long
    
Public Declare Function LoadLibrary Lib "kernel32" Alias "LoadLibraryA" (ByVal lpLibFileName As String) As Long
Public Declare Function DeleteFile Lib "kernel32" Alias "DeleteFileA" (ByVal lpFileName As String) As Long
Private Declare Function GetPrivateProfileString Lib "kernel32" Alias "GetPrivateProfileStringA" (ByVal lpApplicationName As String, ByVal lpKeyName As Any, ByVal lpDefault As String, ByVal lpReturnedString As String, ByVal nSize As Long, ByVal lpFileName As String) As Long
Private Declare Function WritePrivateProfileString Lib "kernel32" Alias "WritePrivateProfileStringA" (ByVal lpApplicationName As String, ByVal lpKeyName As Any, ByVal lpString As String, ByVal lpFileName As String) As Long

Public Declare Function ShellExecute Lib "Shell32.dll" Alias "ShellExecuteA" (ByVal hWnd As Long, ByVal lpOperation As String, _
    ByVal lpFile As String, ByVal lpParameters As String, ByVal lpDirectory As String, ByVal nShowCmd As Long) As Long

Public Const vnLicense = 1261
Public Const Angka As String = "0123456789"
Private AdaptersInfo() As AdapterInfo
Private AdapterCount As Long

Public id_data_privilege_login As Integer
Public id_user_login As Integer
Public name_user_login As String
Public strSQL As String

Public DateTimeFormat As String
Public DateFormat As String
Public TimeFormat As String

Public UserID As String
Public INILanFile As String
Public INILanFileMain As String
Public fieldsLang() As String
Public Language As String
'Public NumberLang As Integer

Public CloseApp As Boolean
Public zip_code_provinsi As String
Public zip_code_kota As String
Public zip_code_kecamatan As String

Public Tgl_load_str As String
Public use_sms As Boolean
Public t_att_logTxt_str As String

Public Const DateTimeFormatDatabase = "YYYY-MM-DD HH:mm:ss"
Public Const DateFormatDatabase = "YYYY-MM-DD"
Public Const TimeFormatDatabase = "HH:mm:ss"

Public RowHeightGrid As String
Public HeaderHeightGrid As String
Public DB_LANGUANGE As String

Public GRID_HEADER_BACKCOLOR As String
Public GRID_HEADER_FORECOLOR As String
Public GRID_EVENT_ROWS_BACKCOLOR As String

Public DateTimeFormatView As String
Public DateFormatView As String
Public TimeFormatView As String
Public LongDateFormatView As String
Public LongDateFormatView2 As String
Public LongDateFormatView3 As String
Public LongDateTimeFormatView As String
Public LongDateTimeFormatView2 As String

Public LoadFirstPath As String
Public PortRealtime As Long
Public PortRealtimeNeo As Long
Public Const ResponsePort As Long = 5500

Public PathDB_data As String
Public PathDB_sms As String
Public PassDB As String

Public gbOpenFlag As Boolean
Public gnCommHandleIndex As Long
Public FKFirmware As String
Public GetDevInfo As Boolean

Public vnMachineNumber As Long
Public vnCommPort As Long
Public vnCommBaudrate As Long
Public vstrTelNumber As String
Public vnWaitDialTime As Long
Public vpszIPAddress As String
Public vpszNetPort As Long
Public vpszNetPassword As Long
Public vnTimeOut As Long
Public vnProtocolType As Long
Public TCPIPFlag As Boolean
    
Private INIConfigFile As String
Public customer_id As String
Public WizardEnd As Boolean
Public fbOpenFlag As Boolean
Public firstLoad As Boolean

Public Enum Colors
   WarnaDataKosongWajibdiisi = &HC0C0FF
   WarnaDataKosongTidakWajibdiisi = &HC0FFFF
   WarnaHeader = &HFFE6CC    '&H00FFC0FF&
   
   AliceBlue = &HFFF0F8FF
   AntiqueWhite = &HFFFAEBD7
   Aqua = &HFF00FFFF
   Aquamarine = &HFF7FFFD4
   Azure = &HFFF0FFFF
   Beige = &HFFF5F5DC
   Bisque = &HFFFFE4C4
   Black = &HFF000000
   BlanchedAlmond = &HFFFFEBCD
   Blue = &HFF0000FF
   BlueViolet = &HFF8A2BE2
   Brown = &HFFA52A2A
   BurlyWood = &HFFDEB887
   CadetBlue = &HFF5F9EA0
   Chartreuse = &HFF7FFF00
   Chocolate = &HFFD2691E
   Coral = &HFFFF7F50
   CornflowerBlue = &HFF6495ED
   Cornsilk = &HFFFFF8DC
   Crimson = &HFFDC143C
   Cyan = &HFF00FFFF
   DarkBlue = &HFF00008B
   DarkCyan = &HFF008B8B
   DarkGoldenrod = &HFFB8860B
   DarkGray = &HFFA9A9A9
   DarkGreen = &HFF006400
   DarkKhaki = &HFFBDB76B
   DarkMagenta = &HFF8B008B
   DarkOliveGreen = &HFF556B2F
   DarkOrange = &HFFFF8C00
   DarkOrchid = &HFF9932CC
   DarkRed = &HFF8B0000
   DarkSalmon = &HFFE9967A
   DarkSeaGreen = &HFF8FBC8B
   DarkSlateBlue = &HFF483D8B
   DarkSlateGray = &HFF2F4F4F
   DarkTurquoise = &HFF00CED1
   DarkViolet = &HFF9400D3
   DeepPink = &HFFFF1493
   DeepSkyBlue = &HFF00BFFF
   DimGray = &HFF696969
   DodgerBlue = &HFF1E90FF
   Firebrick = &HFFB22222
   FloralWhite = &HFFFFFAF0
   ForestGreen = &HFF228B22
   Fuchsia = &HFFFF00FF
   Gainsboro = &HFFDCDCDC
   GhostWhite = &HFFF8F8FF
   Gold = &HFFFFD700
   Goldenrod = &HFFDAA520
   Gray = &HFF808080
   Green = &HFF008000
   GreenYellow = &HFFADFF2F
   Honeydew = &HFFF0FFF0
   HotPink = &HFFFF69B4
   IndianRed = &HFFCD5C5C
   Indigo = &HFF4B0082
   Ivory = &HFFFFFFF0
   Khaki = &HFFF0E68C
   Lavender = &HFFE6E6FA
   LavenderBlush = &HFFFFF0F5
   LawnGreen = &HFF7CFC00
   LemonChiffon = &HFFFFFACD
   LightBlue = &HFFADD8E6
   LightCoral = &HFFF08080
   LightCyan = &HFFE0FFFF
   LightGoldenrodYellow = &HFFFAFAD2
   LightGray = &HFFD3D3D3
   LightGreen = &HFF90EE90
   LightPink = &HFFFFB6C1
   LightSalmon = &HFFFFA07A
   LightSeaGreen = &HFF20B2AA
   LightSkyBlue = &HFF87CEFA
   LightSlateGray = &HFF778899
   LightSteelBlue = &HFFB0C4DE
   LightYellow = &HFFFFFFE0
   Lime = &HFF00FF00
   LimeGreen = &HFF32CD32
   Linen = &HFFFAF0E6
   Magenta = &HFFFF00FF
   Maroon = &HFF800000
   MediumAquamarine = &HFF66CDAA
   MediumBlue = &HFF0000CD
   MediumOrchid = &HFFBA55D3
   MediumPurple = &HFF9370DB
   MediumSeaGreen = &HFF3CB371
   MediumSlateBlue = &HFF7B68EE
   MediumSpringGreen = &HFF00FA9A
   MediumTurquoise = &HFF48D1CC
   MediumVioletRed = &HFFC71585
   MidnightBlue = &HFF191970
   MintCream = &HFFF5FFFA
   MistyRose = &HFFFFE4E1
   Moccasin = &HFFFFE4B5
   NavajoWhite = &HFFFFDEAD
   Navy = &HFF000080
   OldLace = &HFFFDF5E6
   Olive = &HFF808000
   OliveDrab = &HFF6B8E23
   Orange = &HFFFFA500
   OrangeRed = &HFFFF4500
   Orchid = &HFFDA70D6
   PaleGoldenrod = &HFFEEE8AA
   PaleGreen = &HFF98FB98
   PaleTurquoise = &HFFAFEEEE
   PaleVioletRed = &HFFDB7093
   PapayaWhip = &HFFFFEFD5
   PeachPuff = &HFFFFDAB9
   Peru = &HFFCD853F
   Pink = &HFFFFC0CB
   Plum = &HFFDDA0DD
   PowderBlue = &HFFB0E0E6
   Purple = &HFF800080
   Red = &HFFFF0000
   RosyBrown = &HFFBC8F8F
   RoyalBlue = &HFF4169E1
   SaddleBrown = &HFF8B4513
   Salmon = &HFFFA8072
   SandyBrown = &HFFF4A460
   SeaGreen = &HFF2E8B57
   SeaShell = &HFFFFF5EE
   Sienna = &HFFA0522D
   Silver = &HFFC0C0C0
   SkyBlue = &HFF87CEEB
   SlateBlue = &HFF6A5ACD
   SlateGray = &HFF708090
   Snow = &HFFFFFAFA
   SpringGreen = &HFF00FF7F
   SteelBlue = &HFF4682B4
   Tan = &HFFD2B48C
   Teal = &HFF008080
   Thistle = &HFFD8BFD8
   Tomato = &HFFFF6347
   Transparent = &HFFFFFF
   Turquoise = &HFF40E0D0
   Violet = &HFFEE82EE
   Wheat = &HFFF5DEB3
   White = &HFFFFFFFF
   WhiteSmoke = &HFFF5F5F5
   Yellow = &HFFFFFF00
   YellowGreen = &HFF9ACD32
End Enum

Private Type SHFILEOPSTRUCT
    hWnd As Long
    wFunc As Long
    pFrom As String
    pTo As String
    fFlags As Integer
    fAnyOperationsAborted As Boolean
    hNameMappings As Long
    lpszProgressTitle As String
End Type

Private Declare Function SHFileOperation Lib "Shell32.dll" Alias "SHFileOperationA" (lpFileOp As SHFILEOPSTRUCT) As Long

Private Const FO_COPY = &H2
Private Const FOF_ALLOWUNDO = &H40

Sub Main()
    On Error GoTo BugError
    
    Dim CnDB_data As cConnection
    Dim CnDB_sms As cConnection
    
    CloseApp = False
    If App.PrevInstance Then Exit Sub
        
    Dim salah As Boolean
    Dim dllHandle As Long
    
    INIConfigFile = App.Path & "\Fingerspot.ini"
    
    Language = INIConfigRead("System", "Language", "")
    INILanFile = App.Path & "\Lang\" & Language & "_uru.ini"
    INILanFileMain = App.Path & "\" & Language & ".ini"
    If INILanFile = "" Then Exit Sub
    If INILanFileMain = "" Then Exit Sub
    
    LoadFirstPath = INIConfigRead("System", "LoadFirstPath", "")
    If (LoadFirstPath = "") Or (LoadFirstPath <> App.Path) Then
        Shell "regsvr32 /s " & App.Path & "\Library\Codejock.Controls.v18.3.0.ocx", vbHide
        Sleep 1000
        Shell "regsvr32 /s " & App.Path & "\Library\vbRichClient5.dll", vbHide
        Sleep 1000
        Shell "regsvr32 /s " & App.Path & "\Library\RealSvrOcxTcp.ocx", vbHide
        Sleep 1000
        
        If salah = False Then INIConfigWrite "System", "LoadFirstPath", App.Path
    End If
    
    dllHandle = LoadLibrary(App.Path & "\Library\Codejock.Controls.v18.3.0.oca")
    dllHandle = LoadLibrary(App.Path & "\Library\Codejock.Controls.v18.3.0.ocx")
    dllHandle = LoadLibrary(App.Path & "\Library\vbRichClient5.dll")
    dllHandle = LoadLibrary(App.Path & "\Library\RealSvrOcxTcp.ocx")
    dllHandle = LoadLibrary(App.Path & "\Library\AxImage.oca")
    dllHandle = LoadLibrary(App.Path & "\Library\AxImage.ocx")
    dllHandle = LoadLibrary(App.Path & "\Library\vb_cairo_sqlite.dll")
    
    PathDB_data = INIConfigRead("System", "DBName", App.Path & "\data1.dt")
    PathDB_sms = INIConfigRead("System", "DBName_sms", App.Path & "\sms.db3")
    
    PassDB = ""
    PassDB = Mid(GetKey, 7, 15) & Mid(GetKey, 15, 2)
    
    Dim DBL As String
    DBL = INIConfigRead("System", "DBName", "") & "\" & INIConfigRead("System", "Language", "Indonesia") & ".dt"
    If FileExists(DBL) Then
        DB_LANGUANGE = DBL
    Else
        DB_LANGUANGE = App.Path & "\" & INIConfigRead("System", "Language", "Indonesia") & ".dt"
    End If
    
    DateTimeFormatView = INIConfigRead("System", "DateTimeFormatView", "dd-MM-yyyy HH:mm:ss")
    DateFormatView = INIConfigRead("System", "DateFormatView", "dd-mm-yyyy")
    TimeFormatView = INIConfigRead("System", "TimeFormatView", "HH:mm")
    LongDateFormatView = INIConfigRead("System", "LongDateFormatView", "dd MMMM yyyy")
    LongDateFormatView2 = INIConfigRead("System", "LongDateFormatView2", "dddd, dd MMMM yyyy")
    LongDateFormatView3 = INIConfigRead("System", "LongDateFormatView3", "dddd, dd-MM-yyyy")
    LongDateTimeFormatView = INIConfigRead("System", "LongDateTimeFormatView", "dd MMMM yyyy HH:mm")
    LongDateTimeFormatView2 = INIConfigRead("System", "LongDateTimeFormatView2", "dddd, dd MMMM yyyy HH:mm")
    
    DateTimeFormat = INIConfigRead("System", "DateTime", "yyyy-MM-dd hh:mm:ss")
    DateFormat = INIConfigRead("System", "DateFormat", "yyyy-MM-dd")
    TimeFormat = INIConfigRead("System", "TimeFormat", "hh:mm:ss")
    
    If Trim(PathDB_data) = "" Then
        PathDB_data = App.Path & "\data1.dt"
    Else
        If FileExists(PathDB_data) = False Then
            PathDB_data = App.Path & "\data1.dt"
        End If
    End If
    
    If Trim(PathDB_sms) = "" Then
        PathDB_sms = App.Path & "\sms.db3"
    Else
        If FileExists(PathDB_sms) = False Then
            PathDB_sms = App.Path & "\sms.db3"
        End If
    End If
    
    'Cek koneksi Database
    Set CnDB_data = New cConnection
    If FileExists(PathDB_data) Then
        CnDB_data.OpenDB PathDB_data, PassDB
        
        customer_id = getParamater("customer_id")
        If Trim(customer_id) = "" Then
            customer_id = CnDB_data.UniqueID64
            setParamater "customer_id", customer_id
        End If
    Else
        MsgBox INILanRead("errMsg", "Msg1", "", PathDB_data, "", ""), vbOKOnly, App.Title & " : " & "13"
        CloseApp = True
        Exit Sub
    End If
        
    Set CnDB_sms = New cConnection
    If FileExists(PathDB_sms) Then
        CnDB_sms.OpenDB PathDB_sms
    Else
        MsgBox INILanRead("errMsg", "Msg1", "", PathDB_sms, "", ""), vbOKOnly, App.Title & " : " & "13"
        CloseApp = True
        Exit Sub
    End If
        
    fbOpenFlag = False
    firstLoad = False
    WizardEnd = False
    
    Dim str1 As String
    str1 = getParamater("id_data_privilege_login")
    If Trim(str1) = "" Then
        id_data_privilege_login = 1
    Else
        id_data_privilege_login = CInt(str1)
    End If
    
    str1 = getParamater("name_user_login")
    If Trim(str1) = "" Then
        name_user_login = "Startup"
    Else
        name_user_login = str1
    End If
    
    str1 = getParamater("id_user_login")
    If Trim(str1) = "" Then
        id_user_login = 1
    Else
        id_user_login = CInt(str1)
    End If
    
    RowHeightGrid = getParamater("height_row_grid")
    HeaderHeightGrid = getParamater("height_header_grid")
    
    frmWizard.Show 'vbModal
    
    Exit Sub
        
BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Sub

Public Sub INIConfigWrite(Section As String, key As String, value As String)
    WritePrivateProfileString Section, key, value, INIConfigFile
End Sub

Public Function INIConfigRead(Section As String, key As String, Optional Default As String = "") As String
    Dim BufferSize As Long, Ret As Long
    
    'Increase the buffer until it's big enough
    Do
        BufferSize = BufferSize + 256
        INIConfigRead = Space(BufferSize)
        Ret = GetPrivateProfileString(Section, key, Default, INIConfigRead, BufferSize, INIConfigFile)
    Loop Until Ret < BufferSize - 1
    
    'Chop off the trailing Chr$(0)'s
    If Ret > 0 Then
        INIConfigRead = Left$(INIConfigRead, Ret)
    Else
        INIConfigRead = ""
    End If
End Function

Public Function GetBahasaHari(tgl As Date, JnsBahasa As Integer) As String
    Dim i As Integer
    
    i = Weekday(tgl)
        
    If JnsBahasa = 1 Then
        GetBahasaHari = INIConfigRead("View", "Day" & Trim(CStr(i)) & "_in", "")
    ElseIf JnsBahasa = 2 Then
        GetBahasaHari = INIConfigRead("View", "Day" & Trim(CStr(i)) & "_en", "")
    End If
End Function

Public Function GetBahasaBulan(tgl As Date, JnsBahasa As Integer) As String
    Dim i As Integer
    
    i = Month(tgl)
        
    If JnsBahasa = 1 Then
        GetBahasaBulan = INIConfigRead("View", "Month" & Trim(CStr(i)) & "_in", "")
    ElseIf JnsBahasa = 2 Then
        GetBahasaBulan = INIConfigRead("View", "Month" & Trim(CStr(i)) & "_en", "")
    End If
End Function

Public Function GetViewDateTimeTitle(tgl As Date, Title_in As String, Title_en As String, _
    ByRef value_in As String, ByRef value_en As String) As String
    
    Dim View_notif As String
    Dim notifx As String
    
    Dim str_in As String
    Dim str_en As String
    
    View_notif = INIConfigRead("View", "View_notif", "1")
    notifx = INIConfigRead("View", "notif" & View_notif, "Title, dddd dd MMMM yyyy HH:mm")
    
    str_in = notifx
    str_in = Replace(str_in, "Title", Title_in)
    str_in = Replace(str_in, "dddd", GetBahasaHari(tgl, 1))
    str_in = Replace(str_in, "dd", IIf(Len(CStr(Day(tgl))) > 1, CStr(Day(tgl)), "0" & CStr(Day(tgl))))
    str_in = Replace(str_in, "MMMM", GetBahasaBulan(tgl, 1))
    str_in = Replace(str_in, "yyyy", Year(tgl))
    str_in = Replace(str_in, "HH", IIf(Len(CStr(Hour(tgl))) > 1, CStr(Hour(tgl)), "0" & CStr(Hour(tgl))))
    str_in = Replace(str_in, "mm", IIf(Len(CStr(Minute(tgl))) > 1, CStr(Minute(tgl)), "0" & CStr(Minute(tgl))))
    str_in = Replace(str_in, "ss", IIf(Len(CStr(Second(tgl))) > 1, CStr(Second(tgl)), "0" & CStr(Second(tgl))))
    value_in = str_in
    
    str_en = notifx
    str_en = Replace(str_en, "Title", Title_en)
    str_en = Replace(str_en, "dddd", GetBahasaHari(tgl, 1))
    str_en = Replace(str_en, "dd", IIf(Len(CStr(Day(tgl))) > 1, CStr(Day(tgl)), "0" & CStr(Day(tgl))))
    str_en = Replace(str_en, "MMMM", GetBahasaBulan(tgl, 1))
    str_en = Replace(str_en, "yyyy", Year(tgl))
    str_en = Replace(str_en, "HH", IIf(Len(CStr(Hour(tgl))) > 1, CStr(Hour(tgl)), "0" & CStr(Hour(tgl))))
    str_en = Replace(str_en, "mm", IIf(Len(CStr(Minute(tgl))) > 1, CStr(Minute(tgl)), "0" & CStr(Minute(tgl))))
    str_en = Replace(str_en, "ss", IIf(Len(CStr(Second(tgl))) > 1, CStr(Second(tgl)), "0" & CStr(Second(tgl))))
    value_en = str_en
End Function
    
Public Function GetViewDateTime(tgl As Date, ByRef value_in As String, ByRef value_en As String) As String
    Dim View_date As String
    Dim datex As String
    
    Dim datex_in As String
    Dim datex_en As String
    
    View_date = INIConfigRead("View", "View_date", "1")
    datex = INIConfigRead("View", "datetime" & View_date, "dddd dd MMMM yyyy HH:mm")
    
    datex_in = datex
    datex_in = Replace(datex_in, "dddd", GetBahasaHari(tgl, 1))
    datex_in = Replace(datex_in, "dd", IIf(Len(CStr(Day(tgl))) > 1, CStr(Day(tgl)), "0" & CStr(Day(tgl))))
    datex_in = Replace(datex_in, "MMMM", GetBahasaBulan(tgl, 1))
    datex_in = Replace(datex_in, "yyyy", Year(tgl))
    datex_in = Replace(datex_in, "HH", IIf(Len(CStr(Hour(tgl))) > 1, CStr(Hour(tgl)), "0" & CStr(Hour(tgl))))
    datex_in = Replace(datex_in, "mm", IIf(Len(CStr(Minute(tgl))) > 1, CStr(Minute(tgl)), "0" & CStr(Minute(tgl))))
    datex_in = Replace(datex_in, "ss", IIf(Len(CStr(Second(tgl))) > 1, CStr(Second(tgl)), "0" & CStr(Second(tgl))))
    value_in = datex_in
    
    datex_en = datex
    datex_en = Replace(datex_en, "dddd", GetBahasaHari(tgl, 1))
    datex_en = Replace(datex_en, "dd", IIf(Len(CStr(Day(tgl))) > 1, CStr(Day(tgl)), "0" & CStr(Day(tgl))))
    datex_en = Replace(datex_en, "MMMM", GetBahasaBulan(tgl, 1))
    datex_en = Replace(datex_en, "yyyy", Year(tgl))
    datex_en = Replace(datex_en, "HH", IIf(Len(CStr(Hour(tgl))) > 1, CStr(Hour(tgl)), "0" & CStr(Hour(tgl))))
    datex_en = Replace(datex_en, "mm", IIf(Len(CStr(Minute(tgl))) > 1, CStr(Minute(tgl)), "0" & CStr(Minute(tgl))))
    datex_en = Replace(datex_en, "ss", IIf(Len(CStr(Second(tgl))) > 1, CStr(Second(tgl)), "0" & CStr(Second(tgl))))
    value_en = datex_en
End Function

Public Function GetViewDate(tgl As Date, ByRef value_in As String, ByRef value_en As String) As String
    Dim View_date As String
    Dim datex As String
    
    Dim datex_in As String
    Dim datex_en As String
    
    View_date = INIConfigRead("View", "View_date", "1")
    datex = INIConfigRead("View", "date" & View_date, "dddd dd MMMM yyyy")
    
    datex_in = datex
    datex_in = Replace(datex_in, "dddd", GetBahasaHari(tgl, 1))
    datex_in = Replace(datex_in, "dd", IIf(Len(CStr(Day(tgl))) > 1, CStr(Day(tgl)), "0" & CStr(Day(tgl))))
    datex_in = Replace(datex_in, "MMMM", GetBahasaBulan(tgl, 1))
    datex_in = Replace(datex_in, "yyyy", Year(tgl))
    value_in = datex_in
    
    datex_en = datex
    datex_en = Replace(datex_en, "dddd", GetBahasaHari(tgl, 1))
    datex_en = Replace(datex_en, "dd", IIf(Len(CStr(Day(tgl))) > 1, CStr(Day(tgl)), "0" & CStr(Day(tgl))))
    datex_en = Replace(datex_en, "MMMM", GetBahasaBulan(tgl, 1))
    datex_en = Replace(datex_en, "yyyy", Year(tgl))
    value_en = datex_en
End Function

Public Function GetKey() As String
    Dim l As Integer
    Dim aKey2(27) As Byte
    
    For l = 0 To 27
        aKey2(l) = Choose(l + 1, &H46, &H69, &H6E, &H67, &H65, &H72, &H40, &H53, &H70, &H6F, &H74, &H23, &H39, &H39, &H20, &H34, &H4A, &H4A, &H20, &H49, &H20, &H44, &H77, &H77, &H54, &H41, &H3D, &H3D)
    Next
    
    GetKey = StrConv(aKey2, vbUnicode)
End Function

Public Function GetCommandKey(command_str As String, SN As String, commandDate As String) As String
    On Error GoTo BugError
    
    Dim s As String
    Dim S2 As String
    
    'commandDate = YYYY-MM-DD HH:mm:ss
    s = Mid(GetKey, 7, 8)
    S2 = DecodeBase64(commandDate)
    
    GetCommandKey = MD5(command_str & SN & s & S2)
    
    Exit Function
        
BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Function
    
Public Function GetIPLocal() As String
    Dim Adapter As Long
    
    AdapterCount = GetAdaptersInfo(AdaptersInfo)
    
    Dim Description As String
    Dim AdapterIndex As Integer
    Dim Name As String
    Dim ip_address As String
    Dim GatewayIP As String
    Dim noteStr As String
    
    ip_address = ""
    noteStr = ""
    
    If AdapterCount = 0 Then Exit Function
    
    For Adapter = 0 To AdapterCount - 1
        Description = AdaptersInfo(Adapter).Description
        AdapterIndex = AdaptersInfo(Adapter).AdapterIndex
        Name = AdaptersInfo(Adapter).Name
        ip_address = AdaptersInfo(Adapter).IP
        GatewayIP = AdaptersInfo(Adapter).GatewayIP
        
        If AdapterIndex = 20 Then
            noteStr = noteStr & " (LAN : " & ip_address & ") "
        ElseIf AdapterIndex = 18 Then
            noteStr = noteStr & " (Wifi : " & ip_address & ") "
        End If
    Next
    
    GetIPLocal = noteStr 'ip_address
End Function
    
Public Sub TextToFile(Msg As String, FileName As String)
    Open FileName For Output As #1
    Print #1, Msg
    Close #1
End Sub
  
Public Function HexToString(ByVal HexToStr As String) As String
    Dim strTemp   As String
    Dim strReturn As String
    Dim i         As Long
    
    For i = 1 To Len(HexToStr) Step 3
        strTemp = Chr$(Val("&H" & Mid$(HexToStr, i, 2)))
        strReturn = strReturn & strTemp
    Next i
    
    HexToString = strReturn
End Function

Public Function StringToHex(ByVal StrToHex As String) As String
    Dim strTemp   As String
    Dim strReturn As String
    Dim i         As Long
    
    For i = 1 To Len(StrToHex)
        strTemp = Hex$(Asc(Mid$(StrToHex, i, 1)))
        If Len(strTemp) = 1 Then strTemp = "0" & strTemp
        strReturn = strReturn & Space$(1) & strTemp
    Next i
    
    StringToHex = strReturn
End Function

Public Function ReadFileIntoString(strFilePath As String) As String
    Dim fso As New FileSystemObject
    Dim ts As TextStream
    
    Set ts = fso.OpenTextFile(strFilePath)
    ReadFileIntoString = ts.ReadAll
End Function

Public Function GetLang(valueStr As String) As String
    Dim i As Long
    Dim j As Integer
    Dim s As String
    Dim captionStr As String
    Dim Position As Long
    
    captionStr = ""
    For i = 0 To UBound(fieldsLang)
        s = Trim(fieldsLang(i))
        
        Position = InStr(s, valueStr)
        If Position > 0 Then
            j = Position + Len(valueStr) + 1
            captionStr = Mid(s, j, Len(s) - 1)
        End If
    Next i
    
    GetLang = captionStr
End Function

Public Sub INILanWrite(Section As String, key As String, value As String)
    WritePrivateProfileString Section, key, value, INILanFile
End Sub

Public Function INILanRead(Section As String, key As String, Optional Default As String = "", _
    Optional str1 As String = "", Optional str2 As String = "", Optional str3 As String = "") As String
    
    Dim BufferSize As Long, Ret As Long
    Dim s As String
    
    'Increase the buffer until it's big enough
    Do
        BufferSize = BufferSize + 256
        INILanRead = Space(BufferSize)
        If Default = "" Then Default = key
        Ret = GetPrivateProfileString(Section, key, Default, INILanRead, BufferSize, INILanFile)
    Loop Until Ret < BufferSize - 1
    
    'Chop off the trailing Chr$(0)'s
    If Ret > 0 Then
        s = Left$(INILanRead, Ret)
        s = Replace(s, "/n", vbNewLine)
        s = Replace(s, "?1", str1)
        s = Replace(s, "?2", str2)
        s = Replace(s, "?3", str3)
        INILanRead = s
    Else
        INILanRead = ""
    End If
End Function

Public Function INILanRead2(Section As String, key As String, Optional Default As String = "", _
    Optional str1 As String = "", Optional str2 As String = "", Optional str3 As String = "") As String
    
    Dim BufferSize As Long, Ret As Long
    Dim s As String
    
    'Increase the buffer until it's big enough
    Do
        BufferSize = BufferSize + 256
        INILanRead2 = Space(BufferSize)
        If Default = "" Then Default = key
        Ret = GetPrivateProfileString(Section, key, Default, INILanRead2, BufferSize, INILanFileMain)
    Loop Until Ret < BufferSize - 1
    
    'Chop off the trailing Chr$(0)'s
    If Ret > 0 Then
        s = Left$(INILanRead2, Ret)
        s = Replace(s, "/n", vbNewLine)
        s = Replace(s, "?1", str1)
        s = Replace(s, "?2", str2)
        s = Replace(s, "?3", str3)
        INILanRead2 = s
    Else
        INILanRead2 = ""
    End If
End Function

Public Function getCurrentDT() As String
    getCurrentDT = Replace(Format(Now, DateTimeFormatDatabase), ".", ":")
End Function

Public Sub DebugText(frm As Form, txtDebug As TextBox, sText As String)
    With frm
        If Len(.txtDebug.Text) > 10000 Then .txtDebug.Text = ""
            
        .txtDebug.Text = .txtDebug.Text & sText & vbNewLine
        .txtDebug.SelStart = Len(.txtDebug) - 2
    End With
End Sub

Public Function GetDateStr(ByVal dateStr As String) As Date
    Dim nTemp As Integer
    Dim strDateTime As String
    
    On Error GoTo errL_GetDateStr
    
    strDateTime = strDateTime & Mid(dateStr, 1, 4) & "-" & Mid(dateStr, 5, 2) & "-" & Mid(dateStr, 7, 2)
    strDateTime = strDateTime & " 0:0:0"
    GetDateStr = strDateTime
    Exit Function
    
errL_GetDateStr:
    GetDateStr = "2019-01-01 0:0:0"
End Function

Public Function FileExists(ByVal Fname As String) As Boolean
    Dim fso As Object
    Set fso = CreateObject("Scripting.FileSystemObject")
    FileExists = fso.FileExists(Fname)
End Function

Public Function MD5(inputData As String) As String
    On Error GoTo BugError
    
    Dim Hash As New MD5Hash
    Dim bytBlock() As Byte
    
    bytBlock = StrConv(inputData, vbFromUnicode)
    MD5 = Hash.HashBytes(bytBlock)
    
    Exit Function
        
BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Function
    
Public Function EncryptStr(inputData As String) As String
    On Error GoTo BugError
    
    Dim crypt As New ChilkatCrypt2
    
    EncryptStr = ""
    Dim success As Long
    success = crypt.UnlockComponent("FNGSPT.CB4042019_acprLRCam76f")
    If (success <> 1) Then
        Debug.Print crypt.LastErrorText
        Exit Function
    End If
    
    crypt.CryptAlgorithm = "aes"
    crypt.CipherMode = "cbc"
    crypt.KeyLength = 256
    crypt.PaddingScheme = 0
    crypt.EncodingMode = "hex"
    
    Dim ivHex As String
    ivHex = "000102030405060708090A0B0C0D0E0F"
    crypt.SetEncodedIV ivHex, "hex"
    
    Dim keyHex As String
    keyHex = "000102030405060708090A0B0C0D0E0F101112131415161718191A1B1C1D1E1F"
    crypt.SetEncodedKey keyHex, "hex"
    
    Dim encStr As String
    encStr = crypt.EncryptStringENC(inputData)
    EncryptStr = encStr
    
    Exit Function
        
BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Function

Public Function DecryptStr(inputData As String) As String
    On Error GoTo BugError
    
    Dim crypt As New ChilkatCrypt2
    
    DecryptStr = ""
    Dim success As Long
    success = crypt.UnlockComponent("FNGSPT.CB4042019_acprLRCam76f")
    If (success <> 1) Then
        Debug.Print crypt.LastErrorText
        Exit Function
    End If
    
    crypt.CryptAlgorithm = "aes"
    crypt.CipherMode = "cbc"
    crypt.KeyLength = 256
    crypt.PaddingScheme = 0
    crypt.EncodingMode = "hex"
    
    Dim ivHex As String
    ivHex = "000102030405060708090A0B0C0D0E0F"
    crypt.SetEncodedIV ivHex, "hex"
    
    Dim keyHex As String
    keyHex = "000102030405060708090A0B0C0D0E0F101112131415161718191A1B1C1D1E1F"
    crypt.SetEncodedKey keyHex, "hex"
    
    Dim decStr As String
    decStr = crypt.DecryptStringENC(inputData)
    DecryptStr = decStr
    
    Exit Function
        
BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Function

Public Function MD5File(PathFile As String) As String
    On Error GoTo BugError
    
    Dim Hash As New MD5Hash
    
    MD5File = Hash.HashFile(PathFile)
    
    Exit Function
        
BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Function
    
Public Function EncodeBase64(inputData As String) As String
    On Error GoTo BugError
           
'    Dim crypt As New ChilkatCrypt2
'    EncodeBase64 = ""
'    Dim success As Long
'    success = crypt.UnlockComponent("FNGSPT.CB4042019_acprLRCam76f")
'    If (success <> 1) Then
'        Debug.Print crypt.LastErrorText
'        Exit Function
'    End If
'
'    crypt.CryptAlgorithm = "none"
'    crypt.EncodingMode = "base64"
'    EncodeBase64 = crypt.EncryptStringENC(inputData)
    
    Dim SB As New ChilkatStringBuilder
    Dim success As Long
    
    success = SB.Append(inputData)
    success = SB.Encode("base64", "utf-8")
    EncodeBase64 = SB.GetAsString()
    
    Exit Function
        
BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Function
    
Public Function DecodeBase64(inputData As String) As String
    On Error GoTo BugError
            
'    Dim crypt As New ChilkatCrypt2
'    Dim success As Long
'    success = crypt.UnlockComponent("FNGSPT.CB4042019_acprLRCam76f")
'    If (success <> 1) Then
'        Debug.Print crypt.LastErrorText
'        Exit Function
'    End If
'
'    crypt.CryptAlgorithm = "none"
'    crypt.EncodingMode = "base64"
'    DecodeBase64 = crypt.DecryptStringENC(inputData)
    
    Dim SB As New ChilkatStringBuilder
    Dim success As Long
    
    success = SB.Append(inputData)
    success = SB.Decode("base64", "utf-8")
    DecodeBase64 = SB.GetAsString()
    
    Exit Function
        
BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Function

Public Function EncodeJSON(inputData As String) As String
    On Error GoTo BugError
            
    Dim json64, str1, str2, str As String
    
    EncodeJSON = ""
    If Len(inputData) >= 4 Then
        json64 = EncodeBase64(inputData)
        str1 = Mid(json64, 1, 4)
        str2 = Mid(json64, Len(json64) - 3, 4)
        str = str2 & Mid(json64, 5, Len(json64) - 8) & str1
        'str = Replace(str, vbNewLine, "")
        EncodeJSON = "FIN" & str & "SPOT"
    End If
    
    Exit Function
        
BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Function

Public Function DecodeJSON(inputData As String) As String
    On Error GoTo BugError
            
    Dim JSON, jsonDecode, str1, str2, str As String
    
    DecodeJSON = ""
    If Len(inputData) >= 8 Then
        JSON = Mid(inputData, 4, Len(inputData) - 7)
        str1 = Mid(JSON, 1, 4)
        str2 = Mid(JSON, Len(JSON) - 3, 4)
        str = str2 & Mid(JSON, 5, Len(JSON) - 8) & str1
        
        jsonDecode = DecodeBase64(str)
        DecodeJSON = jsonDecode
    End If
    
    Exit Function
        
BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Function

Public Function SendRequestHTTP(use_enc As Boolean, url_str As String, jsonStr As String) As String
    On Error GoTo BugError
            
    Dim req As New ChilkatHttpRequest
    Dim http As New ChilkatHttp
    
    Dim success As Long
    
    SendRequestHTTP = ""
    success = http.UnlockComponent("FNGSPT.CB4042019_acprLRCam76f")
    If (success <> 1) Then
        Debug.Print http.LastErrorText
        Exit Function
    End If
    
    http.AcceptCharset = ""
    http.UserAgent = ""
    http.AcceptLanguage = ""
    http.AllowGzip = 0
    
    success = http.AddQuickHeader("Cookie", "JSESSIONID=1234")
    
    Dim jsonText As String
    If use_enc = True Then
        jsonText = EncodeJSON(jsonStr)
    Else
        jsonText = jsonStr
    End If
    
    Dim resp As ChilkatHttpResponse
    Set resp = http.PostJson(url_str, jsonText)
    If (resp Is Nothing) Then
        Debug.Print http.LastErrorText & vbCrLf
    Else
        SendRequestHTTP = resp.BodyStr & vbCrLf
    End If
    
    Exit Function
        
BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Function

Public Function SendRequestHTTP_TZ(url_str As String) As String
    On Error GoTo BugError
            
    Dim req As New ChilkatHttpRequest
    Dim http As New ChilkatHttp
    
    Dim success As Long
    
    SendRequestHTTP_TZ = ""
    success = http.UnlockComponent("FNGSPT.CB4042019_acprLRCam76f")
    If (success <> 1) Then
        Debug.Print http.LastErrorText
        Exit Function
    End If
    
'    http.AcceptCharset = ""
'    http.UserAgent = ""
'    http.AcceptLanguage = ""
'    http.AllowGzip = 0
'
'    success = http.AddQuickHeader("Cookie", "JSESSIONID=1234")
'
'    Dim resp As ChilkatHttpResponse
'    Set resp = http.PostUrlEncoded(url_str, req)
'    If (resp Is Nothing) Then
'        Debug.Print http.LastErrorText & vbCrLf
'    Else
'        SendRequestHTTP_TZ = resp.BodyStr & vbCrLf
'    End If
    
    Dim html As String
    html = http.QuickGetStr(url_str)
    SendRequestHTTP_TZ = html
    
    Exit Function
        
BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Function

Public Function GetRandomKey(keyValues As String) As String
    GetRandomKey = keyValues & 1 + Int(Rnd() * 10000000)
End Function

Public Function Is64bit() As Boolean
    Dim handle As Long, bolFunc As Boolean

    ' Assume initially that this is not a Wow64 process
    bolFunc = False
    ' Now check to see if IsWow64Process function exists
    handle = GetProcAddress(GetModuleHandle("kernel32"), "IsWow64Process")
    If handle > 0 Then ' IsWow64Process function exists
        ' Now use the function to determine if
        ' we are running under Wow64
        IsWow64Process GetCurrentProcess(), bolFunc
    End If
    Is64bit = bolFunc
End Function

Public Function isRunningExe(exeName As String) As Boolean
   Dim strQuery As String
   strQuery = "SELECT Name FROM Win32_Process WHERE Name=""" & exeName & """"
   isRunningExe = GetObject("winmgmts:").ExecQuery(strQuery).Count
End Function

Public Function LastSunday(tgl As Date) As Date
    Dim myDate As Date

    myDate = tgl
    While Weekday(myDate, vbSunday) <> vbSunday
        myDate = myDate - 1
    Wend
    LastSunday = myDate
End Function

Public Sub SHCopyFile(ByVal from_file As String, ByVal to_file As String)
    Dim sh_op As SHFILEOPSTRUCT

    With sh_op
        .hWnd = 0
        .wFunc = FO_COPY
        .pFrom = from_file & vbNullChar & vbNullChar
        .pTo = to_file & vbNullChar & vbNullChar
        .fFlags = FOF_NOCONFIRMATION 'FOF_ALLOWUNDO
    End With

    SHFileOperation sh_op
End Sub

Public Sub FrmUnload()
    Dim frm As Form
    
    For Each frm In Forms
        Unload frm
        Set frm = Nothing
    Next
End Sub
