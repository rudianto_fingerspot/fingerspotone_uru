VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "DNSLookup"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Private Const DNS_TYPE_PTR = &HC&
Private Const DNS_TYPE_AAAA = &H1C&
Private Const DNS_QUERY_STANDARD = &H0&
Private Const DNS_QUERY_BYPASS_CACHE = &H8&
Private Const DNS_QUERY_NO_HOSTS_FILE = &H40&
Private Const DnsFreeRecordListDeep = 1&

Private Type DnsPTRRecord
    pNext As Long
    pName As Long
    wType As Integer
    wDataLength As Integer
    Flags As Long
    dwTTL As Long
    dwReserved  As Long
    pNameHost As Long
    others(9) As Long
End Type

Private Type DnsAAAARecord
    pNext As Long
    pName As Long
    wType As Integer
    wDataLength As Integer
    Flags As Long
    dwTTL As Long
    dwReserved  As Long
    Ip6Address(15) As Byte
    others(6) As Long
End Type

Private Declare Function DnsQuery Lib "Dnsapi" Alias "DnsQuery_A" ( _
    ByVal Name As String, _
    ByVal wType As Integer, _
    ByVal Options As Long, _
    ByRef aipServers As Any, _
    ByRef ppQueryResultsSet As Long, _
    ByVal pReserved As Long) As Long
    
Private Declare Function DnsRecordListFree Lib "Dnsapi" ( _
    ByVal pDnsRecord As Long, _
    ByVal DnsFreeRecordListDeep As Long) As Long

Private Declare Sub CopyMemory Lib "kernel32" Alias "RtlMoveMemory" ( _
    ByVal pTo As Long, _
    ByVal uFrom As Long, _
    ByVal lSize As Long)
    
Private Declare Function StrCopyA Lib "kernel32" Alias "lstrcpyA" ( _
    ByVal retval As String, _
    ByVal PTR As Long) As Long
    
Private Declare Function StrLenA Lib "kernel32" Alias "lstrlenA" ( _
    ByVal PTR As Long) As Long

Public Enum DNS_STATUS
    ERROR_NO_AAAA_RETURNED = -4&
    ERROR_BAD_IP_FORMAT = -3&
    ERROR_NO_PTR_RETURNED = -2&
    DNS_STATUS_SUCCESS = 0&
    DNS_ERROR_RCODE_NAME_ERROR = 9003& 'Name does not exist.
    DNS_INFO_NO_RECORDS = 9501& 'No info returned.
End Enum

Public LeadingZeros As Boolean

Public Function IPv4ToHost(ByVal IPv4 As String, ByRef HostName As Variant) As DNS_STATUS
    Dim Octets() As String
    Dim OctX As Long
    Dim NumPart As Long
    Dim BadIPv4 As Boolean
    Dim lngDNSRec As Long
    Dim Record As DnsPTRRecord
    Dim Length As Long
    Dim HostTemp As String
    'Returns DNS_STATUS Enum values, otherwise a DNS system error code.

    IPv4 = Trim$(IPv4)
    If Len(IPv4) = 0 Then IPv4ToHost = ERROR_BAD_IP_FORMAT: Exit Function
    Octets = Split(IPv4, ".")
    If UBound(Octets) <> 3 Then IPv4ToHost = ERROR_BAD_IP_FORMAT: Exit Function
    For OctX = 0 To 3
        If IsNumeric(Octets(OctX)) Then
            NumPart = CInt(Octets(OctX))
            If 0 <= NumPart And NumPart <= 255 Then
                Octets(OctX) = CStr(NumPart)
            Else
                BadIPv4 = True
                Exit For
            End If
        Else
            BadIPv4 = True
            Exit For
        End If
    Next
    If BadIPv4 Then IPv4ToHost = ERROR_BAD_IP_FORMAT: Exit Function
    
    IPv4 = Octets(3) & "." & Octets(2) & "." & Octets(1) & "." & Octets(0) & ".IN-ADDR.ARPA"
    
    IPv4ToHost = DnsQuery(IPv4, DNS_TYPE_PTR, DNS_QUERY_STANDARD _
                                           Or DNS_QUERY_BYPASS_CACHE _
                                           Or DNS_QUERY_NO_HOSTS_FILE, ByVal 0, lngDNSRec, 0)
    If IPv4ToHost = DNS_STATUS_SUCCESS Then
        If lngDNSRec <> 0 Then
            CopyMemory VarPtr(Record), lngDNSRec, LenB(Record)
            
            With Record
                If .wType = DNS_TYPE_PTR Then
                    Length = StrLenA(.pNameHost)
                    HostTemp = String$(Length, 0)
                    StrCopyA HostTemp, .pNameHost
                    HostName = HostTemp
                Else
                    IPv4ToHost = ERROR_NO_PTR_RETURNED
                End If
            End With
            DnsRecordListFree lngDNSRec, DnsFreeRecordListDeep
        Else
            IPv4ToHost = DNS_INFO_NO_RECORDS
        End If
    'Else
        'Return with DNS error code.
    End If
End Function

Private Function HexQuad(ByVal Quad As Long) As String
    HexQuad = LCase$(Hex$(Quad))
    If LeadingZeros Then HexQuad = Right$("000" & HexQuad, 4)
End Function

Public Function HostToIPv6(ByVal HostName As String, ByRef IPv6 As Variant) As DNS_STATUS
    Dim lngDNSRec As Long
    Dim lngDNSNext As Long
    Dim Record As DnsAAAARecord
    Dim IPv6Next As Long
    Dim B As Long
    Dim IPv6Temp As String
    'Returns DNS_STATUS Enum values, otherwise a DNS system error code.

    HostName = Trim$(HostName)
    HostToIPv6 = DnsQuery(HostName, DNS_TYPE_AAAA, DNS_QUERY_STANDARD _
                                                Or DNS_QUERY_BYPASS_CACHE _
                                                Or DNS_QUERY_NO_HOSTS_FILE, ByVal 0, lngDNSRec, 0)
    If HostToIPv6 = DNS_STATUS_SUCCESS Then
        ReDim IPv6(0)
        If lngDNSRec <> 0 Then
            lngDNSNext = lngDNSRec
            Do
                CopyMemory VarPtr(Record), lngDNSNext, LenB(Record)
                
                With Record
                    If .wType = DNS_TYPE_AAAA Then
                        ReDim Preserve IPv6(IPv6Next)
                        IPv6Temp = ""
                        For B = 0 To UBound(.Ip6Address) Step 2
                            IPv6Temp = _
                                IPv6Temp & HexQuad(CLng(.Ip6Address(B)) * 256 _
                                                 + CLng(.Ip6Address(B + 1))) _
                                         & ":"
                        Next
                        IPv6(IPv6Next) = Left$(IPv6Temp, Len(IPv6Temp) - 1)
                        IPv6Next = IPv6Next + 1
                    End If
                End With
                
                lngDNSNext = Record.pNext
            Loop While lngDNSNext > 0 And lngDNSNext <> lngDNSRec
            DnsRecordListFree lngDNSRec, DnsFreeRecordListDeep
            If IPv6Next = 0 Then HostToIPv6 = ERROR_NO_AAAA_RETURNED
        Else
            HostToIPv6 = DNS_INFO_NO_RECORDS
        End If
    'Else
        'Return with DNS error code.
    End If
End Function

