cd /d %~dp0
if /i "%PROCESSOR_IDENTIFIER:~0,3%"=="X86" (
	echo system is x86
	copy .\*.dll %windir%\system32\
	copy .\*.ocx %windir%\system32\
	regsvr32 /s /c %windir%\system32\vbRichClient5.dll
	regsvr32 /s /c %windir%\system32\Codejock.Controls.v18.3.0.ocx
	regsvr32 /s /c %windir%\system32\iGrid650_10Tec.ocx	
	regsvr32 /s /c %windir%\system32\ChilkatAx-9.5.0-win32.dll
	regsvr32 /s /c %windir%\system32\MSCOMCTL.ocx
	regsvr32 /s /c %windir%\system32\MSWINSCK.ocx
	regsvr32 /s /c %windir%\system32\MSINET.ocx
	) 
else 
	(
	echo system is x64
	copy .\*.dll %windir%\SysWOW64\
	copy .\*.ocx %windir%\SysWOW64\
	regsvr32 /s /c %windir%\SysWOW64\vbRichClient5.dll
	regsvr32 /s /c %windir%\SysWOW64\Codejock.Controls.v18.3.0.ocx
	regsvr32 /s /c %windir%\SysWOW64\iGrid650_10Tec.ocx	
	regsvr32 /s /c %windir%\SysWOW64\ChilkatAx-9.5.0-win32.dll
	regsvr32 /s /c %windir%\SysWOW64\MSCOMCTL.ocx
	regsvr32 /s /c %windir%\SysWOW64\MSWINSCK.ocx
	regsvr32 /s /c %windir%\system32\MSINET.ocx
	)
