cd /d %~dp0
if /i "%PROCESSOR_IDENTIFIER:~0,3%"=="X86" (
	echo system is x86
	copy .\*.dll %windir%\system32\
	copy .\*.ocx %windir%\system32\
	regsvr32 /s /c %windir%\system32\RealSvrOcxTcp.ocx
	regsvr32 /s /c %windir%\system32\AxInterop.RealSvrOcxTcpLib.dll
	regsvr32 /s /c %windir%\system32\Interop.RealSvrOcxTcpLib.dll
	regsvr32 /s /c %windir%\system32\vbRichClient5.dll
	regsvr32 /s /c %windir%\system32\Codejock.Controls.Unicode.v18.3.0.ocx
	regsvr32 /s /c %windir%\system32\ChilkatAx-9.5.0-win32.dll
	regsvr32 /s /c %windir%\system32\iGrid650_10Tec.ocx
	regsvr32 /s /c %windir%\system32\mswinsck.ocx
	regsvr32 /s /c %windir%\system32\AnimatedGif.ocx
	regsvr32 /s /c %windir%\system32\vbalIml250_10Tec.ocx
	regsvr32 /s /c %windir%\system32\MSCOMCTL.ocx
	regsvr32 /s /c %windir%\system32\MSINET.ocx
	regsvr32 /s /c %windir%\system32\ieframe.dll
	regsvr32 /s /c %windir%\system32\ieframe.oca
	regsvr32 /s /c %windir%\system32\msador15.dll
	regsvr32 /s /c %windir%\system32\msado15.oca
	regsvr32 /s /c %windir%\system32\FTMU.dll
	) 
else 
	(
	echo system is x64
	copy .\*.dll %windir%\SysWOW64\
	copy .\*.ocx %windir%\SysWOW64\
	regsvr32 /s /c %windir%\SysWOW64\RealSvrOcxTcp.ocx
	regsvr32 /s /c %windir%\SysWOW64\AxInterop.RealSvrOcxTcpLib.dll
	regsvr32 /s /c %windir%\SysWOW64\Interop.RealSvrOcxTcpLib.dll
	regsvr32 /s /c %windir%\SysWOW64\vbRichClient5.dll
	regsvr32 /s /c %windir%\SysWOW64\Codejock.Controls.Unicode.v18.3.0.ocx
	regsvr32 /s /c %windir%\SysWOW64\ChilkatAx-9.5.0-win32.dll
	regsvr32 /s /c %windir%\SysWOW64\iGrid650_10Tec.ocx
	regsvr32 /s /c %windir%\SysWOW64\mswinsck.ocx
	regsvr32 /s /c %windir%\SysWOW64\AnimatedGif.ocx
	regsvr32 /s /c %windir%\SysWOW64\vbalIml250_10Tec.ocx
	regsvr32 /s /c %windir%\SysWOW64\MSCOMCTL.ocx
	regsvr32 /s /c %windir%\SysWOW64\MSINET.ocx
	regsvr32 /s /c %windir%\SysWOW64\ieframe.dll
	regsvr32 /s /c %windir%\SysWOW64\ieframe.oca
	regsvr32 /s /c %windir%\SysWOW64\msador15.dll
	regsvr32 /s /c %windir%\SysWOW64\msado15.oca
	regsvr32 /s /c %windir%\SysWOW64\FTMU.dll
	)
