VERSION 5.00
Object = "{86683A0C-A3FA-4412-B044-51B918DBEE5C}#1.0#0"; "RealSvrOcxTcp.ocx"
Object = "{A8E5842E-102B-4289-9D57-3B3F5B5E15D3}#18.3#0"; "CODEJO~1.OCX"
Object = "{D2BBCEE2-EBE7-4DA2-8B98-2E5F1ECA275B}#1.0#0"; "iGrid650_10Tec.ocx"
Object = "{248DD890-BB45-11CF-9ABC-0080C7E7B78D}#1.0#0"; "MSWINSCK.OCX"
Begin VB.Form frmMain 
   Appearance      =   0  'Flat
   BackColor       =   &H80000005&
   BorderStyle     =   1  'Fixed Single
   Caption         =   "FingerspotOne AddOn"
   ClientHeight    =   9870
   ClientLeft      =   45
   ClientTop       =   390
   ClientWidth     =   12750
   FillColor       =   &H00FFFFFF&
   BeginProperty Font 
      Name            =   "Segoe UI"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   ForeColor       =   &H00FFFFFF&
   Icon            =   "frmMain.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   9870
   ScaleWidth      =   12750
   StartUpPosition =   1  'CenterOwner
   Begin VB.TextBox txtTimezone 
      Appearance      =   0  'Flat
      Height          =   3435
      Left            =   7200
      Locked          =   -1  'True
      MultiLine       =   -1  'True
      ScrollBars      =   3  'Both
      TabIndex        =   70
      Text            =   "frmMain.frx":058A
      Top             =   9840
      Visible         =   0   'False
      Width           =   4215
   End
   Begin VB.Timer TimerSinkronUser 
      Enabled         =   0   'False
      Interval        =   30000
      Left            =   6600
      Top             =   600
   End
   Begin VB.TextBox t_att_logTxt 
      Appearance      =   0  'Flat
      Height          =   3435
      Left            =   13200
      Locked          =   -1  'True
      MultiLine       =   -1  'True
      ScrollBars      =   3  'Both
      TabIndex        =   13
      Text            =   "frmMain.frx":12B1
      Top             =   240
      Visible         =   0   'False
      Width           =   3375
   End
   Begin RealSvrOcxTcpLib.RealSvrOcxTcp FKRealSvrTcp 
      Height          =   480
      Left            =   5040
      TabIndex        =   0
      Top             =   600
      Visible         =   0   'False
      Width           =   480
      _Version        =   65536
      _ExtentX        =   847
      _ExtentY        =   847
      _StockProps     =   0
   End
   Begin XtremeSuiteControls.GroupBox GroupBox1TT 
      Height          =   9375
      Index           =   6
      Left            =   360
      TabIndex        =   2
      Top             =   360
      Width           =   12075
      _Version        =   1179651
      _ExtentX        =   21299
      _ExtentY        =   16536
      _StockProps     =   79
      ForeColor       =   -2147483630
      BackColor       =   16777215
      Appearance      =   15
      Begin VB.PictureBox Picture1TT 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   0  'None
         ForeColor       =   &H80000008&
         Height          =   615
         Index           =   0
         Left            =   90
         ScaleHeight     =   41
         ScaleMode       =   0  'User
         ScaleWidth      =   32
         TabIndex        =   3
         Top             =   230
         Width           =   615
      End
      Begin XtremeSuiteControls.GroupBox GroupBox3 
         Height          =   450
         Index           =   1
         Left            =   960
         TabIndex        =   4
         Tag             =   "AlignTop"
         Top             =   960
         Width           =   10590
         _Version        =   1179651
         _ExtentX        =   18680
         _ExtentY        =   794
         _StockProps     =   79
         ForeColor       =   12632256
         BackColor       =   16775142
         Appearance      =   2
         BorderStyle     =   2
         Begin XtremeSuiteControls.Label lbl 
            Height          =   210
            Index           =   20
            Left            =   120
            TabIndex        =   53
            Tag             =   "AnchorCenterHLeft"
            Top             =   120
            Width           =   240
            _Version        =   1179651
            _ExtentX        =   423
            _ExtentY        =   370
            _StockProps     =   79
            Caption         =   "20"
            ForeColor       =   16711680
            BackColor       =   -2147483633
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   9
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Transparent     =   -1  'True
            AutoSize        =   -1  'True
         End
         Begin XtremeSuiteControls.Label lbl 
            Height          =   210
            Index           =   62
            Left            =   8520
            TabIndex        =   5
            Tag             =   "AnchorCenterHLeft"
            Top             =   120
            Width           =   1905
            _Version        =   1179651
            _ExtentX        =   3360
            _ExtentY        =   370
            _StockProps     =   79
            Caption         =   "62"
            ForeColor       =   16711680
            BackColor       =   -2147483633
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   9
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Alignment       =   1
            Transparent     =   -1  'True
         End
      End
      Begin XtremeSuiteControls.GroupBox GroupBox3 
         Height          =   450
         Index           =   2
         Left            =   960
         TabIndex        =   6
         Tag             =   "AlignTop"
         Top             =   1440
         Width           =   10590
         _Version        =   1179651
         _ExtentX        =   18680
         _ExtentY        =   794
         _StockProps     =   79
         ForeColor       =   12632256
         BackColor       =   15659767
         Appearance      =   2
         BorderStyle     =   2
         Begin XtremeSuiteControls.Label lbl 
            Height          =   210
            Index           =   21
            Left            =   120
            TabIndex        =   54
            Tag             =   "AnchorCenterHLeft"
            Top             =   120
            Width           =   240
            _Version        =   1179651
            _ExtentX        =   423
            _ExtentY        =   370
            _StockProps     =   79
            Caption         =   "21"
            ForeColor       =   16711680
            BackColor       =   -2147483633
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   9
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Transparent     =   -1  'True
            AutoSize        =   -1  'True
         End
         Begin XtremeSuiteControls.Label lbl 
            Height          =   210
            Index           =   63
            Left            =   8520
            TabIndex        =   7
            Tag             =   "AnchorCenterHLeft"
            Top             =   120
            Width           =   1905
            _Version        =   1179651
            _ExtentX        =   3360
            _ExtentY        =   370
            _StockProps     =   79
            Caption         =   "63"
            ForeColor       =   16711680
            BackColor       =   -2147483633
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   9
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Alignment       =   1
            Transparent     =   -1  'True
         End
      End
      Begin XtremeSuiteControls.GroupBox GroupBox3 
         Height          =   450
         Index           =   3
         Left            =   960
         TabIndex        =   8
         Tag             =   "AlignTop"
         Top             =   1920
         Width           =   10590
         _Version        =   1179651
         _ExtentX        =   18680
         _ExtentY        =   794
         _StockProps     =   79
         ForeColor       =   12632256
         BackColor       =   16775142
         Appearance      =   2
         BorderStyle     =   2
         Begin XtremeSuiteControls.Label lbl 
            Height          =   210
            Index           =   25
            Left            =   120
            TabIndex        =   55
            Tag             =   "AnchorCenterHLeft"
            Top             =   120
            Width           =   240
            _Version        =   1179651
            _ExtentX        =   423
            _ExtentY        =   370
            _StockProps     =   79
            Caption         =   "25"
            ForeColor       =   16711680
            BackColor       =   -2147483633
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   9
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Transparent     =   -1  'True
            AutoSize        =   -1  'True
         End
         Begin XtremeSuiteControls.Label lbl 
            Height          =   210
            Index           =   64
            Left            =   8520
            TabIndex        =   9
            Tag             =   "AnchorCenterHLeft"
            Top             =   120
            Width           =   1905
            _Version        =   1179651
            _ExtentX        =   3360
            _ExtentY        =   370
            _StockProps     =   79
            Caption         =   "64"
            ForeColor       =   16711680
            BackColor       =   -2147483633
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   9
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Alignment       =   1
            Transparent     =   -1  'True
         End
      End
      Begin XtremeSuiteControls.GroupBox GroupBox3 
         Height          =   450
         Index           =   4
         Left            =   960
         TabIndex        =   10
         Tag             =   "AlignTop"
         Top             =   2400
         Width           =   10590
         _Version        =   1179651
         _ExtentX        =   18680
         _ExtentY        =   794
         _StockProps     =   79
         ForeColor       =   12632256
         BackColor       =   15659767
         Appearance      =   2
         BorderStyle     =   2
         Begin XtremeSuiteControls.Label lbl 
            Height          =   210
            Index           =   26
            Left            =   120
            TabIndex        =   56
            Tag             =   "AnchorCenterHLeft"
            Top             =   120
            Width           =   240
            _Version        =   1179651
            _ExtentX        =   423
            _ExtentY        =   370
            _StockProps     =   79
            Caption         =   "26"
            ForeColor       =   16711680
            BackColor       =   -2147483633
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   9
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Transparent     =   -1  'True
            AutoSize        =   -1  'True
         End
         Begin XtremeSuiteControls.Label lbl 
            Height          =   210
            Index           =   65
            Left            =   8520
            TabIndex        =   11
            Tag             =   "AnchorCenterHLeft"
            Top             =   120
            Width           =   1905
            _Version        =   1179651
            _ExtentX        =   3360
            _ExtentY        =   370
            _StockProps     =   79
            Caption         =   "65"
            ForeColor       =   16711680
            BackColor       =   -2147483633
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   9
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Alignment       =   1
            Transparent     =   -1  'True
         End
      End
      Begin XtremeSuiteControls.TabControl TabDetail 
         Height          =   4440
         Left            =   15
         TabIndex        =   14
         Top             =   4920
         Width           =   12045
         _Version        =   1179651
         _ExtentX        =   21246
         _ExtentY        =   7832
         _StockProps     =   68
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Appearance      =   4
         Color           =   2
         PaintManager.FixedTabWidth=   300
         PaintManager.ButtonMargin=   "20,20,20,20"
         ItemCount       =   7
         SelectedItem    =   4
         Item(0).Caption =   "0"
         Item(0).ControlCount=   6
         Item(0).Control(0)=   "tdp(0)"
         Item(0).Control(1)=   "tdp(5)"
         Item(0).Control(2)=   "tdp(6)"
         Item(0).Control(3)=   "tdp(7)"
         Item(0).Control(4)=   "tdp(8)"
         Item(0).Control(5)=   "tdp(9)"
         Item(1).Caption =   "1"
         Item(1).ControlCount=   1
         Item(1).Control(0)=   "tdp(1)"
         Item(2).Caption =   "2"
         Item(2).ControlCount=   1
         Item(2).Control(0)=   "tdp(2)"
         Item(3).Caption =   "3"
         Item(3).ControlCount=   1
         Item(3).Control(0)=   "tdp(4)"
         Item(4).Caption =   "4"
         Item(4).ControlCount=   1
         Item(4).Control(0)=   "tdp(3)"
         Item(5).Caption =   "5"
         Item(5).ControlCount=   1
         Item(5).Control(0)=   "tdp(10)"
         Item(6).Caption =   "6"
         Item(6).ControlCount=   0
         Begin XtremeSuiteControls.TabControlPage tdp 
            Height          =   3465
            Index           =   0
            Left            =   -69970
            TabIndex        =   15
            Top             =   945
            Visible         =   0   'False
            Width           =   11985
            _Version        =   1179651
            _ExtentX        =   21140
            _ExtentY        =   6112
            _StockProps     =   1
            BackColor       =   16777215
            Page            =   0
            Begin XtremeSuiteControls.FlatEdit edtPortNeo 
               Height          =   375
               Left            =   3720
               TabIndex        =   75
               Top             =   1560
               Visible         =   0   'False
               Width           =   615
               _Version        =   1179651
               _ExtentX        =   1085
               _ExtentY        =   661
               _StockProps     =   77
               ForeColor       =   0
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Tahoma"
                  Size            =   9
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Text            =   "8123"
               Alignment       =   2
               Appearance      =   2
               UseVisualStyle  =   0   'False
            End
            Begin XtremeSuiteControls.UpDown spinPort 
               Height          =   375
               Left            =   4305
               TabIndex        =   16
               TabStop         =   0   'False
               Top             =   945
               Visible         =   0   'False
               Width           =   270
               _Version        =   1179651
               _ExtentX        =   476
               _ExtentY        =   661
               _StockProps     =   64
               Appearance      =   2
               UseVisualStyle  =   0   'False
               Max             =   10000
               SyncBuddy       =   -1  'True
               AutoBuddy       =   -1  'True
               BuddyControl    =   "edtPort"
               BuddyProperty   =   ""
            End
            Begin XtremeSuiteControls.FlatEdit edtPort 
               Height          =   375
               Left            =   3720
               TabIndex        =   17
               Top             =   945
               Visible         =   0   'False
               Width           =   615
               _Version        =   1179651
               _ExtentX        =   1085
               _ExtentY        =   661
               _StockProps     =   77
               ForeColor       =   0
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Tahoma"
                  Size            =   9
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Text            =   "8123"
               Alignment       =   2
               Appearance      =   2
               UseVisualStyle  =   0   'False
            End
            Begin XtremeSuiteControls.PushButton PushButton1 
               Height          =   375
               Index           =   0
               Left            =   4800
               TabIndex        =   18
               Top             =   945
               Visible         =   0   'False
               Width           =   2295
               _Version        =   1179651
               _ExtentX        =   4048
               _ExtentY        =   661
               _StockProps     =   79
               Caption         =   "0"
               ForeColor       =   0
               BackColor       =   -2147483635
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Tahoma"
                  Size            =   9
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Appearance      =   12
            End
            Begin XtremeSuiteControls.UpDown spinPortNeo 
               Height          =   375
               Left            =   4305
               TabIndex        =   74
               TabStop         =   0   'False
               Top             =   1560
               Visible         =   0   'False
               Width           =   270
               _Version        =   1179651
               _ExtentX        =   476
               _ExtentY        =   661
               _StockProps     =   64
               Appearance      =   2
               UseVisualStyle  =   0   'False
               Max             =   10000
               SyncBuddy       =   -1  'True
               AutoBuddy       =   -1  'True
               BuddyControl    =   "edtPortNeo"
               BuddyProperty   =   ""
            End
            Begin XtremeSuiteControls.PushButton PushButton1 
               Height          =   375
               Index           =   4
               Left            =   4800
               TabIndex        =   76
               Top             =   1560
               Visible         =   0   'False
               Width           =   2295
               _Version        =   1179651
               _ExtentX        =   4048
               _ExtentY        =   661
               _StockProps     =   79
               Caption         =   "4"
               ForeColor       =   0
               BackColor       =   -2147483635
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Tahoma"
                  Size            =   9
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Appearance      =   12
            End
            Begin MSWinsockLib.Winsock win_Socket 
               Left            =   1800
               Top             =   120
               _ExtentX        =   741
               _ExtentY        =   741
               _Version        =   393216
            End
            Begin MSWinsockLib.Winsock SckUDP 
               Left            =   2400
               Top             =   120
               _ExtentX        =   741
               _ExtentY        =   741
               _Version        =   393216
            End
            Begin XtremeSuiteControls.Label lbl 
               Height          =   210
               Index           =   31
               Left            =   3360
               TabIndex        =   78
               Top             =   1590
               Width           =   210
               _Version        =   1179651
               _ExtentX        =   370
               _ExtentY        =   370
               _StockProps     =   79
               Caption         =   "31"
               ForeColor       =   16711680
               BackColor       =   -2147483633
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Tahoma"
                  Size            =   9
                  Charset         =   0
                  Weight          =   400
                  Underline       =   -1  'True
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Transparent     =   -1  'True
               AutoSize        =   -1  'True
            End
            Begin XtremeSuiteControls.Label lbl 
               Height          =   210
               Index           =   30
               Left            =   480
               TabIndex        =   77
               Top             =   1590
               Width           =   210
               _Version        =   1179651
               _ExtentX        =   370
               _ExtentY        =   370
               _StockProps     =   79
               Caption         =   "30"
               ForeColor       =   0
               BackColor       =   -2147483633
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Tahoma"
                  Size            =   9
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Transparent     =   -1  'True
               AutoSize        =   -1  'True
            End
            Begin XtremeSuiteControls.Label lbl 
               Height          =   210
               Index           =   22
               Left            =   480
               TabIndex        =   22
               Top             =   360
               Width           =   210
               _Version        =   1179651
               _ExtentX        =   370
               _ExtentY        =   370
               _StockProps     =   79
               Caption         =   "22"
               ForeColor       =   0
               BackColor       =   -2147483633
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Tahoma"
                  Size            =   9
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Transparent     =   -1  'True
               AutoSize        =   -1  'True
            End
            Begin XtremeSuiteControls.Label lbl 
               Height          =   210
               Index           =   23
               Left            =   480
               TabIndex        =   21
               Top             =   975
               Width           =   210
               _Version        =   1179651
               _ExtentX        =   370
               _ExtentY        =   370
               _StockProps     =   79
               Caption         =   "23"
               ForeColor       =   0
               BackColor       =   -2147483633
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Tahoma"
                  Size            =   9
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Transparent     =   -1  'True
               AutoSize        =   -1  'True
            End
            Begin XtremeSuiteControls.Label lbl 
               Height          =   210
               Index           =   24
               Left            =   3360
               TabIndex        =   20
               Top             =   975
               Width           =   210
               _Version        =   1179651
               _ExtentX        =   370
               _ExtentY        =   370
               _StockProps     =   79
               Caption         =   "24"
               ForeColor       =   16711680
               BackColor       =   -2147483633
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Tahoma"
                  Size            =   9
                  Charset         =   0
                  Weight          =   400
                  Underline       =   -1  'True
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Transparent     =   -1  'True
               AutoSize        =   -1  'True
            End
            Begin VB.Label lbl1 
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               Caption         =   "Scan terakhir yang masuk :"
               BeginProperty Font 
                  Name            =   "Segoe UI"
                  Size            =   9
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H00FF0000&
               Height          =   585
               Index           =   2
               Left            =   480
               TabIndex        =   19
               Top             =   2760
               Width           =   11220
               WordWrap        =   -1  'True
            End
         End
         Begin XtremeSuiteControls.TabControlPage tdp 
            Height          =   3465
            Index           =   1
            Left            =   -69970
            TabIndex        =   23
            Top             =   945
            Visible         =   0   'False
            Width           =   11985
            _Version        =   1179651
            _ExtentX        =   21140
            _ExtentY        =   6112
            _StockProps     =   1
            BackColor       =   16777215
            Page            =   6
            Begin VB.TextBox txtIsi 
               Appearance      =   0  'Flat
               BackColor       =   &H8000000F&
               BeginProperty Font 
                  Name            =   "Segoe UI"
                  Size            =   9
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H00000000&
               Height          =   1485
               Left            =   480
               MaxLength       =   1000
               MultiLine       =   -1  'True
               ScrollBars      =   2  'Vertical
               TabIndex        =   24
               Top             =   960
               Width           =   11160
            End
            Begin XtremeSuiteControls.PushButton PushButton1 
               Height          =   375
               Index           =   1
               Left            =   480
               TabIndex        =   25
               Top             =   3000
               Width           =   2295
               _Version        =   1179651
               _ExtentX        =   4048
               _ExtentY        =   661
               _StockProps     =   79
               Caption         =   "1"
               ForeColor       =   0
               BackColor       =   -2147483635
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Tahoma"
                  Size            =   9
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Appearance      =   12
            End
            Begin XtremeSuiteControls.Label lbl 
               Height          =   450
               Index           =   0
               Left            =   480
               TabIndex        =   33
               Top             =   360
               Width           =   11145
               _Version        =   1179651
               _ExtentX        =   19659
               _ExtentY        =   794
               _StockProps     =   79
               Caption         =   "0"
               ForeColor       =   0
               BackColor       =   -2147483633
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Tahoma"
                  Size            =   9
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Alignment       =   4
               Transparent     =   -1  'True
               WordWrap        =   -1  'True
            End
            Begin XtremeSuiteControls.Label lbl 
               Height          =   210
               Index           =   1
               Left            =   480
               TabIndex        =   32
               Top             =   2520
               Width           =   105
               _Version        =   1179651
               _ExtentX        =   185
               _ExtentY        =   370
               _StockProps     =   79
               Caption         =   "1"
               ForeColor       =   0
               BackColor       =   -2147483633
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Tahoma"
                  Size            =   9
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Transparent     =   -1  'True
               AutoSize        =   -1  'True
            End
            Begin XtremeSuiteControls.Label lbl 
               Height          =   210
               Index           =   2
               Left            =   840
               TabIndex        =   31
               Top             =   2520
               Width           =   105
               _Version        =   1179651
               _ExtentX        =   185
               _ExtentY        =   370
               _StockProps     =   79
               Caption         =   "2"
               ForeColor       =   16711680
               BackColor       =   -2147483633
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Tahoma"
                  Size            =   9
                  Charset         =   0
                  Weight          =   400
                  Underline       =   -1  'True
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Transparent     =   -1  'True
               AutoSize        =   -1  'True
            End
            Begin XtremeSuiteControls.Label lbl 
               Height          =   210
               Index           =   3
               Left            =   1200
               TabIndex        =   30
               Top             =   2520
               Width           =   105
               _Version        =   1179651
               _ExtentX        =   185
               _ExtentY        =   370
               _StockProps     =   79
               Caption         =   "3"
               ForeColor       =   16711680
               BackColor       =   -2147483633
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Tahoma"
                  Size            =   9
                  Charset         =   0
                  Weight          =   400
                  Underline       =   -1  'True
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Transparent     =   -1  'True
               AutoSize        =   -1  'True
            End
            Begin XtremeSuiteControls.Label lbl 
               Height          =   210
               Index           =   4
               Left            =   1560
               TabIndex        =   29
               Top             =   2520
               Width           =   105
               _Version        =   1179651
               _ExtentX        =   185
               _ExtentY        =   370
               _StockProps     =   79
               Caption         =   "4"
               ForeColor       =   16711680
               BackColor       =   -2147483633
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Tahoma"
                  Size            =   9
                  Charset         =   0
                  Weight          =   400
                  Underline       =   -1  'True
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Transparent     =   -1  'True
               AutoSize        =   -1  'True
            End
            Begin XtremeSuiteControls.Label lbl 
               Height          =   210
               Index           =   5
               Left            =   1920
               TabIndex        =   28
               Top             =   2520
               Width           =   105
               _Version        =   1179651
               _ExtentX        =   185
               _ExtentY        =   370
               _StockProps     =   79
               Caption         =   "5"
               ForeColor       =   16711680
               BackColor       =   -2147483633
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Tahoma"
                  Size            =   9
                  Charset         =   0
                  Weight          =   400
                  Underline       =   -1  'True
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Transparent     =   -1  'True
               AutoSize        =   -1  'True
            End
            Begin XtremeSuiteControls.Label lbl 
               Height          =   210
               Index           =   6
               Left            =   2280
               TabIndex        =   27
               Top             =   2520
               Width           =   105
               _Version        =   1179651
               _ExtentX        =   185
               _ExtentY        =   370
               _StockProps     =   79
               Caption         =   "6"
               ForeColor       =   16711680
               BackColor       =   -2147483633
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Tahoma"
                  Size            =   9
                  Charset         =   0
                  Weight          =   400
                  Underline       =   -1  'True
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Transparent     =   -1  'True
               AutoSize        =   -1  'True
            End
            Begin XtremeSuiteControls.Label lbl 
               Height          =   210
               Index           =   7
               Left            =   2640
               TabIndex        =   26
               Top             =   2520
               Width           =   105
               _Version        =   1179651
               _ExtentX        =   185
               _ExtentY        =   370
               _StockProps     =   79
               Caption         =   "7"
               ForeColor       =   16711680
               BackColor       =   -2147483633
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Tahoma"
                  Size            =   9
                  Charset         =   0
                  Weight          =   400
                  Underline       =   -1  'True
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Transparent     =   -1  'True
               AutoSize        =   -1  'True
            End
         End
         Begin XtremeSuiteControls.TabControlPage tdp 
            Height          =   3465
            Index           =   2
            Left            =   -69970
            TabIndex        =   34
            Top             =   945
            Visible         =   0   'False
            Width           =   11985
            _Version        =   1179651
            _ExtentX        =   21140
            _ExtentY        =   6112
            _StockProps     =   1
            BackColor       =   16777215
            Page            =   7
            Begin XtremeSuiteControls.ComboBox cmb_bln_thn 
               Height          =   345
               Left            =   480
               TabIndex        =   35
               Tag             =   "AnchorCenterHRight"
               Top             =   600
               Width           =   3000
               _Version        =   1179651
               _ExtentX        =   5292
               _ExtentY        =   609
               _StockProps     =   77
               ForeColor       =   4210752
               BackColor       =   16777215
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Segoe UI"
                  Size            =   9
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Style           =   2
               Appearance      =   2
               UseVisualStyle  =   0   'False
            End
            Begin iGrid650_10Tec.iGrid dbg1 
               Height          =   2055
               Left            =   480
               TabIndex        =   36
               Tag             =   "AlignLeft"
               Top             =   1080
               Width           =   11160
               _ExtentX        =   19685
               _ExtentY        =   3625
               BorderType      =   1
               Editable        =   0   'False
               FlatCellControls=   -1  'True
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Segoe UI"
                  Size            =   9
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   4210752
               GridLineColor   =   12632256
            End
            Begin XtremeSuiteControls.ComboBox cmb_status 
               Height          =   345
               Left            =   8400
               TabIndex        =   37
               Top             =   600
               Width           =   3240
               _Version        =   1179651
               _ExtentX        =   5715
               _ExtentY        =   609
               _StockProps     =   77
               ForeColor       =   0
               BackColor       =   16777215
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Segoe UI"
                  Size            =   9
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Style           =   2
               Appearance      =   5
               Text            =   "ComboBox1"
            End
            Begin XtremeSuiteControls.Label lbl_row_count 
               Height          =   225
               Index           =   0
               Left            =   480
               TabIndex        =   45
               Tag             =   "AnchorCenterHLeft"
               Top             =   3200
               Width           =   1110
               _Version        =   1179651
               _ExtentX        =   1958
               _ExtentY        =   397
               _StockProps     =   79
               Caption         =   "lbl_row_count"
               ForeColor       =   4210752
               BackColor       =   -2147483633
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Segoe UI"
                  Size            =   9
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Transparent     =   -1  'True
               AutoSize        =   -1  'True
            End
            Begin XtremeSuiteControls.Label lbl 
               Height          =   210
               Index           =   8
               Left            =   480
               TabIndex        =   39
               Top             =   240
               Width           =   105
               _Version        =   1179651
               _ExtentX        =   185
               _ExtentY        =   370
               _StockProps     =   79
               Caption         =   "8"
               ForeColor       =   0
               BackColor       =   -2147483633
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Tahoma"
                  Size            =   9
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Transparent     =   -1  'True
               AutoSize        =   -1  'True
            End
            Begin XtremeSuiteControls.Label lbl 
               Height          =   210
               Index           =   9
               Left            =   8400
               TabIndex        =   38
               Top             =   240
               Width           =   105
               _Version        =   1179651
               _ExtentX        =   185
               _ExtentY        =   370
               _StockProps     =   79
               Caption         =   "9"
               ForeColor       =   0
               BackColor       =   -2147483633
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Tahoma"
                  Size            =   9
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Transparent     =   -1  'True
               AutoSize        =   -1  'True
            End
         End
         Begin XtremeSuiteControls.TabControlPage tdp 
            Height          =   3465
            Index           =   5
            Left            =   -69970
            TabIndex        =   40
            Top             =   945
            Visible         =   0   'False
            Width           =   11985
            _Version        =   1179651
            _ExtentX        =   21140
            _ExtentY        =   6112
            _StockProps     =   1
            BackColor       =   16777215
            Page            =   1
         End
         Begin XtremeSuiteControls.TabControlPage tdp 
            Height          =   3465
            Index           =   6
            Left            =   -69970
            TabIndex        =   41
            Top             =   945
            Visible         =   0   'False
            Width           =   11985
            _Version        =   1179651
            _ExtentX        =   21140
            _ExtentY        =   6112
            _StockProps     =   1
            BackColor       =   16777215
            Page            =   2
         End
         Begin XtremeSuiteControls.TabControlPage tdp 
            Height          =   3465
            Index           =   7
            Left            =   -69970
            TabIndex        =   42
            Top             =   945
            Visible         =   0   'False
            Width           =   11985
            _Version        =   1179651
            _ExtentX        =   21140
            _ExtentY        =   6112
            _StockProps     =   1
            BackColor       =   16777215
            Page            =   3
         End
         Begin XtremeSuiteControls.TabControlPage tdp 
            Height          =   3465
            Index           =   8
            Left            =   -69970
            TabIndex        =   43
            Top             =   945
            Visible         =   0   'False
            Width           =   11985
            _Version        =   1179651
            _ExtentX        =   21140
            _ExtentY        =   6112
            _StockProps     =   1
            BackColor       =   16777215
            Page            =   4
         End
         Begin XtremeSuiteControls.TabControlPage tdp 
            Height          =   3465
            Index           =   9
            Left            =   -69970
            TabIndex        =   44
            Top             =   945
            Visible         =   0   'False
            Width           =   11985
            _Version        =   1179651
            _ExtentX        =   21140
            _ExtentY        =   6112
            _StockProps     =   1
            BackColor       =   16777215
            Page            =   5
         End
         Begin XtremeSuiteControls.TabControlPage tdp 
            Height          =   3465
            Index           =   4
            Left            =   -69970
            TabIndex        =   46
            Top             =   945
            Visible         =   0   'False
            Width           =   11985
            _Version        =   1179651
            _ExtentX        =   21140
            _ExtentY        =   6112
            _StockProps     =   1
            BackColor       =   16777215
            Page            =   8
            Begin VB.TextBox txtIsi2 
               Appearance      =   0  'Flat
               BackColor       =   &H8000000F&
               BeginProperty Font 
                  Name            =   "Segoe UI"
                  Size            =   9
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H00000000&
               Height          =   1725
               Left            =   1920
               Locked          =   -1  'True
               MultiLine       =   -1  'True
               ScrollBars      =   2  'Vertical
               TabIndex        =   47
               Top             =   2400
               Visible         =   0   'False
               Width           =   11160
            End
            Begin XtremeSuiteControls.ListBox ListBox 
               Height          =   1455
               Left            =   480
               TabIndex        =   48
               Top             =   600
               Width           =   11160
               _Version        =   1179651
               _ExtentX        =   19685
               _ExtentY        =   2566
               _StockProps     =   77
               ForeColor       =   -2147483640
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Segoe UI"
                  Size            =   9
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Appearance      =   5
               UseVisualStyle  =   0   'False
            End
            Begin XtremeSuiteControls.Label lbl 
               Height          =   1290
               Index           =   19
               Left            =   480
               TabIndex        =   50
               Top             =   2160
               Width           =   11145
               _Version        =   1179651
               _ExtentX        =   19659
               _ExtentY        =   2275
               _StockProps     =   79
               Caption         =   "19"
               ForeColor       =   0
               BackColor       =   -2147483633
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Tahoma"
                  Size            =   9
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Alignment       =   4
               Transparent     =   -1  'True
               WordWrap        =   -1  'True
            End
            Begin XtremeSuiteControls.Label lbl 
               Height          =   210
               Index           =   18
               Left            =   480
               TabIndex        =   49
               Top             =   240
               Width           =   240
               _Version        =   1179651
               _ExtentX        =   423
               _ExtentY        =   370
               _StockProps     =   79
               Caption         =   "18"
               ForeColor       =   0
               BackColor       =   -2147483633
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Tahoma"
                  Size            =   9
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Alignment       =   4
               Transparent     =   -1  'True
               AutoSize        =   -1  'True
            End
         End
         Begin XtremeSuiteControls.TabControlPage tdp 
            Height          =   3465
            Index           =   3
            Left            =   30
            TabIndex        =   51
            Top             =   945
            Width           =   11985
            _Version        =   1179651
            _ExtentX        =   21140
            _ExtentY        =   6112
            _StockProps     =   1
            BackColor       =   16777215
            Page            =   9
            Begin iGrid650_10Tec.iGrid dbg 
               Height          =   1725
               Left            =   480
               TabIndex        =   67
               Tag             =   "AlignLeft"
               Top             =   1080
               Width           =   11115
               _ExtentX        =   19606
               _ExtentY        =   3043
               AllowSorting    =   0   'False
               BorderType      =   1
               FlatCellControls=   -1  'True
               ForeColor       =   4210752
               GridLineColor   =   12632256
            End
            Begin XtremeSuiteControls.PushButton PushButton1 
               Height          =   375
               Index           =   3
               Left            =   480
               TabIndex        =   69
               Top             =   3000
               Width           =   2295
               _Version        =   1179651
               _ExtentX        =   4048
               _ExtentY        =   661
               _StockProps     =   79
               Caption         =   "3"
               ForeColor       =   0
               BackColor       =   -2147483635
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Tahoma"
                  Size            =   9
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Appearance      =   12
            End
            Begin XtremeSuiteControls.Label lbl 
               Height          =   570
               Index           =   27
               Left            =   480
               TabIndex        =   68
               Top             =   360
               Width           =   11145
               _Version        =   1179651
               _ExtentX        =   19659
               _ExtentY        =   1005
               _StockProps     =   79
               Caption         =   "27"
               ForeColor       =   0
               BackColor       =   -2147483633
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Tahoma"
                  Size            =   9
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Alignment       =   4
               Transparent     =   -1  'True
               WordWrap        =   -1  'True
            End
         End
         Begin XtremeSuiteControls.TabControlPage tdp 
            Height          =   3465
            Index           =   10
            Left            =   -69970
            TabIndex        =   57
            Top             =   945
            Visible         =   0   'False
            Width           =   11985
            _Version        =   1179651
            _ExtentX        =   21140
            _ExtentY        =   6112
            _StockProps     =   1
            Page            =   10
            Begin XtremeSuiteControls.Resizer Resizer1 
               Height          =   3495
               Left            =   0
               TabIndex        =   58
               Top             =   120
               Width           =   11980
               _Version        =   1179651
               _ExtentX        =   21131
               _ExtentY        =   6165
               _StockProps     =   1
               HScrollLargeChange=   1365
               HScrollSmallChange=   273
               HScrollMaximum  =   14500
               Begin XtremeSuiteControls.Label lbl 
                  Height          =   810
                  Index           =   17
                  Left            =   11160
                  TabIndex        =   66
                  Top             =   2400
                  Width           =   3105
                  _Version        =   1179651
                  _ExtentX        =   5477
                  _ExtentY        =   1429
                  _StockProps     =   79
                  Caption         =   "17"
                  ForeColor       =   0
                  BackColor       =   -2147483633
                  BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                     Name            =   "Tahoma"
                     Size            =   9
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Alignment       =   4
                  Transparent     =   -1  'True
                  WordWrap        =   -1  'True
               End
               Begin XtremeSuiteControls.Label lbl 
                  Height          =   570
                  Index           =   16
                  Left            =   11160
                  TabIndex        =   65
                  Top             =   0
                  Width           =   3000
                  _Version        =   1179651
                  _ExtentX        =   5292
                  _ExtentY        =   1005
                  _StockProps     =   79
                  Caption         =   "16"
                  ForeColor       =   0
                  BackColor       =   -2147483633
                  BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                     Name            =   "Tahoma"
                     Size            =   9
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Alignment       =   4
                  Transparent     =   -1  'True
                  WordWrap        =   -1  'True
               End
               Begin VB.Image Image1 
                  Appearance      =   0  'Flat
                  BorderStyle     =   1  'Fixed Single
                  Height          =   1755
                  Index           =   3
                  Left            =   11160
                  Picture         =   "frmMain.frx":1495
                  Top             =   600
                  Width           =   3060
               End
               Begin XtremeSuiteControls.Label lbl 
                  Height          =   810
                  Index           =   15
                  Left            =   7560
                  TabIndex        =   64
                  Top             =   2400
                  Width           =   3105
                  _Version        =   1179651
                  _ExtentX        =   5477
                  _ExtentY        =   1429
                  _StockProps     =   79
                  Caption         =   "15"
                  ForeColor       =   0
                  BackColor       =   -2147483633
                  BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                     Name            =   "Tahoma"
                     Size            =   9
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Alignment       =   4
                  Transparent     =   -1  'True
                  WordWrap        =   -1  'True
               End
               Begin XtremeSuiteControls.Label lbl 
                  Height          =   570
                  Index           =   14
                  Left            =   7560
                  TabIndex        =   63
                  Top             =   0
                  Width           =   3000
                  _Version        =   1179651
                  _ExtentX        =   5292
                  _ExtentY        =   1005
                  _StockProps     =   79
                  Caption         =   "14"
                  ForeColor       =   0
                  BackColor       =   -2147483633
                  BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                     Name            =   "Tahoma"
                     Size            =   9
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Alignment       =   4
                  Transparent     =   -1  'True
                  WordWrap        =   -1  'True
               End
               Begin VB.Image Image1 
                  Appearance      =   0  'Flat
                  BorderStyle     =   1  'Fixed Single
                  Height          =   1755
                  Index           =   2
                  Left            =   7560
                  Picture         =   "frmMain.frx":17FCF
                  Top             =   600
                  Width           =   3060
               End
               Begin XtremeSuiteControls.Label lbl 
                  Height          =   810
                  Index           =   13
                  Left            =   3960
                  TabIndex        =   62
                  Top             =   2400
                  Width           =   3105
                  _Version        =   1179651
                  _ExtentX        =   5477
                  _ExtentY        =   1429
                  _StockProps     =   79
                  Caption         =   "13"
                  ForeColor       =   0
                  BackColor       =   -2147483633
                  BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                     Name            =   "Tahoma"
                     Size            =   9
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Alignment       =   4
                  Transparent     =   -1  'True
                  WordWrap        =   -1  'True
               End
               Begin XtremeSuiteControls.Label lbl 
                  Height          =   570
                  Index           =   12
                  Left            =   3960
                  TabIndex        =   61
                  Top             =   0
                  Width           =   3000
                  _Version        =   1179651
                  _ExtentX        =   5292
                  _ExtentY        =   1005
                  _StockProps     =   79
                  Caption         =   "12"
                  ForeColor       =   0
                  BackColor       =   -2147483633
                  BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                     Name            =   "Tahoma"
                     Size            =   9
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Alignment       =   4
                  Transparent     =   -1  'True
                  WordWrap        =   -1  'True
               End
               Begin VB.Image Image1 
                  Appearance      =   0  'Flat
                  BorderStyle     =   1  'Fixed Single
                  Height          =   1755
                  Index           =   1
                  Left            =   3960
                  Picture         =   "frmMain.frx":2EB09
                  Top             =   600
                  Width           =   3060
               End
               Begin XtremeSuiteControls.Label lbl 
                  Height          =   810
                  Index           =   11
                  Left            =   360
                  TabIndex        =   60
                  Top             =   2400
                  Width           =   3105
                  _Version        =   1179651
                  _ExtentX        =   5477
                  _ExtentY        =   1429
                  _StockProps     =   79
                  Caption         =   "11"
                  ForeColor       =   0
                  BackColor       =   -2147483633
                  BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                     Name            =   "Tahoma"
                     Size            =   9
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Alignment       =   4
                  Transparent     =   -1  'True
                  WordWrap        =   -1  'True
               End
               Begin XtremeSuiteControls.Label lbl 
                  Height          =   570
                  Index           =   10
                  Left            =   360
                  TabIndex        =   59
                  Top             =   0
                  Width           =   3000
                  _Version        =   1179651
                  _ExtentX        =   5292
                  _ExtentY        =   1005
                  _StockProps     =   79
                  Caption         =   "10"
                  ForeColor       =   0
                  BackColor       =   -2147483633
                  BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                     Name            =   "Tahoma"
                     Size            =   9
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Alignment       =   4
                  Transparent     =   -1  'True
                  WordWrap        =   -1  'True
               End
               Begin VB.Image Image1 
                  Appearance      =   0  'Flat
                  BorderStyle     =   1  'Fixed Single
                  Height          =   1755
                  Index           =   0
                  Left            =   360
                  Picture         =   "frmMain.frx":45643
                  Top             =   600
                  Width           =   3060
               End
            End
         End
      End
      Begin XtremeSuiteControls.PushButton PushButton1 
         Height          =   375
         Index           =   2
         Left            =   9000
         TabIndex        =   52
         Top             =   4440
         Width           =   2535
         _Version        =   1179651
         _ExtentX        =   4471
         _ExtentY        =   661
         _StockProps     =   79
         Caption         =   "2"
         ForeColor       =   16711680
         BackColor       =   -2147483635
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Appearance      =   12
      End
      Begin XtremeSuiteControls.GroupBox GroupBox3 
         Height          =   450
         Index           =   0
         Left            =   960
         TabIndex        =   71
         Tag             =   "AlignTop"
         Top             =   2880
         Width           =   10590
         _Version        =   1179651
         _ExtentX        =   18680
         _ExtentY        =   794
         _StockProps     =   79
         ForeColor       =   12632256
         BackColor       =   16775142
         Appearance      =   2
         BorderStyle     =   2
         Begin XtremeSuiteControls.Label lbl 
            Height          =   210
            Index           =   29
            Left            =   8520
            TabIndex        =   73
            Tag             =   "AnchorCenterHLeft"
            Top             =   120
            Width           =   1905
            _Version        =   1179651
            _ExtentX        =   3360
            _ExtentY        =   370
            _StockProps     =   79
            Caption         =   "29"
            ForeColor       =   16711680
            BackColor       =   -2147483633
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   9
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Alignment       =   1
            Transparent     =   -1  'True
         End
         Begin XtremeSuiteControls.Label lbl 
            Height          =   210
            Index           =   28
            Left            =   120
            TabIndex        =   72
            Tag             =   "AnchorCenterHLeft"
            Top             =   120
            Width           =   240
            _Version        =   1179651
            _ExtentX        =   423
            _ExtentY        =   370
            _StockProps     =   79
            Caption         =   "28"
            ForeColor       =   16711680
            BackColor       =   -2147483633
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   9
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Transparent     =   -1  'True
            AutoSize        =   -1  'True
         End
      End
      Begin XtremeSuiteControls.GroupBox GroupBox3 
         Height          =   450
         Index           =   5
         Left            =   960
         TabIndex        =   79
         Tag             =   "AlignTop"
         Top             =   3360
         Width           =   10590
         _Version        =   1179651
         _ExtentX        =   18680
         _ExtentY        =   794
         _StockProps     =   79
         ForeColor       =   12632256
         BackColor       =   15659767
         Appearance      =   2
         BorderStyle     =   2
         Begin XtremeSuiteControls.Label lbl 
            Height          =   210
            Index           =   33
            Left            =   8520
            TabIndex        =   81
            Tag             =   "AnchorCenterHLeft"
            Top             =   120
            Width           =   1905
            _Version        =   1179651
            _ExtentX        =   3360
            _ExtentY        =   370
            _StockProps     =   79
            Caption         =   "33"
            ForeColor       =   16711680
            BackColor       =   -2147483633
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   9
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Alignment       =   1
            Transparent     =   -1  'True
         End
         Begin XtremeSuiteControls.Label lbl 
            Height          =   210
            Index           =   32
            Left            =   120
            TabIndex        =   80
            Tag             =   "AnchorCenterHLeft"
            Top             =   120
            Width           =   240
            _Version        =   1179651
            _ExtentX        =   423
            _ExtentY        =   370
            _StockProps     =   79
            Caption         =   "32"
            ForeColor       =   16711680
            BackColor       =   -2147483633
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   9
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Transparent     =   -1  'True
            AutoSize        =   -1  'True
         End
      End
      Begin XtremeSuiteControls.GroupBox GroupBox3 
         Height          =   450
         Index           =   6
         Left            =   960
         TabIndex        =   82
         Tag             =   "AlignTop"
         Top             =   3840
         Width           =   10590
         _Version        =   1179651
         _ExtentX        =   18680
         _ExtentY        =   794
         _StockProps     =   79
         ForeColor       =   12632256
         BackColor       =   16775142
         Appearance      =   2
         BorderStyle     =   2
         Begin XtremeSuiteControls.Label lbl 
            Height          =   210
            Index           =   35
            Left            =   120
            TabIndex        =   84
            Tag             =   "AnchorCenterHLeft"
            Top             =   120
            Width           =   240
            _Version        =   1179651
            _ExtentX        =   423
            _ExtentY        =   370
            _StockProps     =   79
            Caption         =   "35"
            ForeColor       =   16711680
            BackColor       =   -2147483633
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   9
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Transparent     =   -1  'True
            AutoSize        =   -1  'True
         End
         Begin XtremeSuiteControls.Label lbl 
            Height          =   210
            Index           =   34
            Left            =   8520
            TabIndex        =   83
            Tag             =   "AnchorCenterHLeft"
            Top             =   120
            Width           =   1905
            _Version        =   1179651
            _ExtentX        =   3360
            _ExtentY        =   370
            _StockProps     =   79
            Caption         =   "34"
            ForeColor       =   16711680
            BackColor       =   -2147483633
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   9
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Alignment       =   1
            Transparent     =   -1  'True
         End
      End
      Begin XtremeSuiteControls.Label lbl 
         Height          =   675
         Index           =   61
         Left            =   960
         TabIndex        =   12
         Tag             =   "AlignTop"
         Top             =   240
         Width           =   10740
         _Version        =   1179651
         _ExtentX        =   18944
         _ExtentY        =   1191
         _StockProps     =   79
         Caption         =   "61"
         ForeColor       =   0
         BackColor       =   8421504
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Alignment       =   4
         Transparent     =   -1  'True
         WordWrap        =   -1  'True
      End
   End
   Begin XtremeSuiteControls.TrayIcon TrayIcon 
      Left            =   5280
      Top             =   120
      _Version        =   1179651
      _ExtentX        =   423
      _ExtentY        =   423
      _StockProps     =   16
      Text            =   "Fingerspot Realtime Attlog"
      Picture         =   "frmMain.frx":5C17D
   End
   Begin XtremeSuiteControls.Label lbl 
      Height          =   270
      Index           =   60
      Left            =   480
      TabIndex        =   1
      Tag             =   "AlignTop"
      Top             =   120
      Width           =   300
      _Version        =   1179651
      _ExtentX        =   529
      _ExtentY        =   476
      _StockProps     =   79
      Caption         =   "60"
      ForeColor       =   0
      BackColor       =   8421504
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   11.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Alignment       =   4
      Transparent     =   -1  'True
      AutoSize        =   -1  'True
   End
   Begin XtremeSuiteControls.CommonDialog CommonDialog 
      Left            =   5040
      Top             =   1560
      _Version        =   1179651
      _ExtentX        =   423
      _ExtentY        =   423
      _StockProps     =   4
   End
   Begin VB.Menu mnuTray 
      Caption         =   "mnuTray"
      Visible         =   0   'False
      Begin VB.Menu mnuTrayShow 
         Caption         =   "Hide/Show"
      End
      Begin VB.Menu mnuSep 
         Caption         =   "-"
      End
      Begin VB.Menu mnuExit 
         Caption         =   "Exit"
      End
   End
End
Attribute VB_Name = "frmMain"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public CloseForm As Boolean
Public Minimized As Boolean
Private AllSNTerDaftar As String
Private AllSNTerDaftarSMS As String
Private TglTemp As String

Private NextSinkronUser As Date
Private NextValidasi As Date
Private NextSinkronDateTime As Date

Private NextSinkronUser1 As Integer
Private NextValidasi1 As Integer
Private NextSinkronDateTime1 As Integer

Private Tgl_load_str As String
Private JarakScanSMS As String

Private no_urut As Integer
Private date_temp As String
Private param1 As String
Private param2 As String
Private param3 As String
Private param4 As String
Private param5 As String
Private param6 As String
Private template_sms1 As String
    
Private beliFitur1 As Boolean
Private beliFitur2 As Boolean
Private beliFitur3 As Boolean
Private beliFitur4 As Boolean

'Mesin Neo
Private GLogPrefix As Long
Private Const GLogLen As Integer = 32
'Private Const DiscoveryPort As Long = 5057 '4111
Private Const ResponsePort As Long = 5500 '4112

Private Sub cmb_bln_thn_Click()
    AntrianSMS
End Sub

Private Sub cmb_status_Click()
    AntrianSMS
End Sub

Private Function GetPrice() As String
    On Error GoTo BugError
    
    Dim str1 As String
    Dim str2 As String
    Dim str3 As String
    Dim data_str As String
    
    Dim url_api As String
    Dim url_str As String
    Dim req_server As String
    
    Dim salah As Boolean
    Dim int_value As Integer
    
    GetPrice = ""
    TglTemp = ""
    
    lbl(62).Caption = INILanRead(Me.Name, "lbl.62", "", "0", "", "")
    lbl(63).Caption = INILanRead(Me.Name, "lbl.63", "", "0", "", "")
    lbl(64).Caption = INILanRead(Me.Name, "lbl.64", "", "0", "", "")
    lbl(65).Caption = INILanRead(Me.Name, "lbl.65", "", "0", "", "")
    lbl(29).Caption = INILanRead(Me.Name, "lbl.29", "", "0", "", "")
    lbl(33).Caption = INILanRead(Me.Name, "lbl.33", "", "0", "", "")
    lbl(34).Caption = INILanRead(Me.Name, "lbl.34", "", "0", "", "")
    
    url_str = "http://payment.fingerspot.com/api/get_price"
    str1 = "{""app_id"":""?1""}"
    str1 = Replace(str1, "?1", "1")
    
    str2 = SendRequestHTTP(False, url_str, str1)
    If str2 = "Error" Then
        frmMain.TulisLog INILanRead("errMsg", "msgLog14", "", "", "", "")
    Else
        str3 = ReadJson(str2, "success")
        If str3 = "true" Then
            data_str = ReadJson(str2, "data")
            
            str3 = ArrayJson(str2, "data")
            DoEvents
        Else
            frmMain.TulisLog INILanRead("errMsg", "msgLog14", "", "", "", "")
        End If
    End If
    
    GetPrice = str2
    
    Exit Function
        
BugError:
    'MsgBox Err.Description, , Err.Number
    salah = False
    Err.Clear
    Resume Next
End Function

Private Sub LoadTemplate()
    Dim isi_pesan As String
    
    param1 = INILanRead(Me.Name, "param1", "", "", "", "")
    param2 = INILanRead(Me.Name, "param2", "", "", "", "")
    param3 = INILanRead(Me.Name, "param3", "", "", "", "")
    param4 = INILanRead(Me.Name, "param4", "", "", "", "")
    param5 = INILanRead(Me.Name, "param5", "", "", "", "")
    param6 = INILanRead(Me.Name, "param6", "", "", "", "")
    template_sms1 = INILanRead(Me.Name, "template_sms1", "", "", "", "")
    
    isi_pesan = template_sms1
    isi_pesan = Replace(isi_pesan, "param1", param1)
    isi_pesan = Replace(isi_pesan, "param2", param2)
    isi_pesan = Replace(isi_pesan, "param3", param3)
    isi_pesan = Replace(isi_pesan, "param4", param4)
    isi_pesan = Replace(isi_pesan, "param5", param5)
    isi_pesan = Replace(isi_pesan, "param6", param6)
    
    txtIsi.Text = isi_pesan
    lbl(2).Caption = param1
    lbl(3).Caption = param2
    lbl(4).Caption = param3
    lbl(5).Caption = param4
    lbl(6).Caption = param5
    lbl(7).Caption = param6
    
    lbl(2).Left = lbl(1).Left + lbl(1).Width + 120
    lbl(3).Left = lbl(2).Left + lbl(2).Width + 120
    lbl(4).Left = lbl(3).Left + lbl(3).Width + 120
    lbl(5).Left = lbl(4).Left + lbl(4).Width + 120
    lbl(6).Left = lbl(5).Left + lbl(5).Width + 120
    lbl(7).Left = lbl(6).Left + lbl(6).Width + 120
End Sub

Private Sub SetTemplate()
    Dim isi_pesan As String
    
    param1 = INILanRead(Me.Name, "param1", "", "", "", "")
    param2 = INILanRead(Me.Name, "param2", "", "", "", "")
    param3 = INILanRead(Me.Name, "param3", "", "", "", "")
    param4 = INILanRead(Me.Name, "param4", "", "", "", "")
    param5 = INILanRead(Me.Name, "param5", "", "", "", "")
    param6 = INILanRead(Me.Name, "param6", "", "", "", "")
    
    isi_pesan = txtIsi.Text
    isi_pesan = Replace(isi_pesan, param1, "param1")
    isi_pesan = Replace(isi_pesan, param2, "param2")
    isi_pesan = Replace(isi_pesan, param3, "param3")
    isi_pesan = Replace(isi_pesan, param4, "param4")
    isi_pesan = Replace(isi_pesan, param5, "param5")
    isi_pesan = Replace(isi_pesan, param6, "param6")
    
    template_sms1 = isi_pesan
    INILanWrite Me.Name, "template_sms1", isi_pesan
End Sub

Private Sub edtPort_KeyPress(KeyAscii As Integer)
    If Not IsNumeric(Chr(KeyAscii)) And Not KeyAscii = 8 Then
        KeyAscii = 0
    End If
End Sub

Private Sub edtPortNeo_KeyPress(KeyAscii As Integer)
    If Not IsNumeric(Chr(KeyAscii)) And Not KeyAscii = 8 Then
        KeyAscii = 0
    End If
End Sub

Private Sub Form_Activate()
    On Error GoTo BugError
    
    Dim str1 As String
    
    If firstLoad = False Then
        firstLoad = True
        
        TabDetail(0).Selected = True
        
        'SetToolTip "FingerspotOne AddOn"
        'MinimizeToTray
        'ShowMessage 0, "FingerspotOne AddOn", "FingerspotOne", 1
        
        str1 = GetPrice()
        
        If CloseApp = True Then
            Unload Me
        End If
        DoEvents
    End If
    
    Exit Sub
        
BugError:
    Err.Clear
    Resume Next
End Sub

Private Sub Form_Load()
    On Error GoTo BugError
    
    Dim i As Long
    Dim PortRT As Long
    Dim s As String
    Dim rs As cRecordset
    
    fbOpenFlag = False
    CloseForm = False
    Tgl_load_str = ""
    
    ChangeFont Me
    SetLang Me
    
    TabDetail.ItemCount = 6
    
    XtremeSuiteControls.Icons.LoadBitmap App.Path & "\Img\01.png", 1, xtpImageNormal
    XtremeSuiteControls.Icons.LoadBitmap App.Path & "\Img\02.png", 2, xtpImageNormal
    XtremeSuiteControls.Icons.LoadBitmap App.Path & "\Img\03.png", 3, xtpImageNormal
    XtremeSuiteControls.Icons.LoadBitmap App.Path & "\Img\04.png", 4, xtpImageNormal
    XtremeSuiteControls.Icons.LoadBitmap App.Path & "\Img\05.png", 5, xtpImageNormal
    XtremeSuiteControls.Icons.LoadBitmap App.Path & "\Img\06.png", 6, xtpImageNormal
    
    TabDetail.EnableMarkup = True
    TabDetail(0).Caption = "<TextBlock><TextBlock HorizontalAlignment='Center' VerticalAlignment='Center' Margin='25,0,0,0'><Image Source='6' /></TextBlock>" & _
    "<LineBreak/><TextBlock HorizontalAlignment='Center' VerticalAlignment='Center'>" & INILanRead(Me.Name, "TabDetail0", "...") & "</TextBlock></TextBlock>"
    TabDetail(1).Caption = "<TextBlock><TextBlock HorizontalAlignment='Center' VerticalAlignment='Center' Margin='20,0,0,0'><Image Source='2' /></TextBlock>" & _
    "<LineBreak/><TextBlock HorizontalAlignment='Center' VerticalAlignment='Center'>" & INILanRead(Me.Name, "TabDetail1", "...") & "</TextBlock></TextBlock>"
    TabDetail(2).Caption = "<TextBlock><TextBlock HorizontalAlignment='Center' VerticalAlignment='Center' Margin='20,0,0,0'><Image Source='3' /></TextBlock>" & _
    "<LineBreak/><TextBlock HorizontalAlignment='Center' VerticalAlignment='Center'>" & INILanRead(Me.Name, "TabDetail2", "...") & "</TextBlock></TextBlock>"
    TabDetail(3).Caption = "<TextBlock><TextBlock HorizontalAlignment='Center' VerticalAlignment='Center' Margin='30,0,0,0'><Image Source='5' /></TextBlock>" & _
    "<LineBreak/><TextBlock HorizontalAlignment='Center' VerticalAlignment='Center'>" & INILanRead(Me.Name, "TabDetail3", "...") & "</TextBlock></TextBlock>"
    TabDetail(4).Caption = "<TextBlock><TextBlock HorizontalAlignment='Center' VerticalAlignment='Center' Margin='30,0,0,0'><Image Source='1' /></TextBlock>" & _
    "<LineBreak/><TextBlock HorizontalAlignment='Center' VerticalAlignment='Center'>" & INILanRead(Me.Name, "TabDetail4", "...") & "</TextBlock></TextBlock>"
    TabDetail(5).Caption = "<TextBlock><TextBlock HorizontalAlignment='Center' VerticalAlignment='Center' Margin='20,0,0,0'><Image Source='4' /></TextBlock>" & _
    "<LineBreak/><TextBlock HorizontalAlignment='Center' VerticalAlignment='Center'>" & INILanRead(Me.Name, "TabDetail5", "...") & "</TextBlock></TextBlock>"
    
    Picture1TT(0).Picture = LoadPicture(App.Path & "\Img\help_button.bmp")
    Picture1TT(0).Left = 200
    
    date_temp = ""
    LoadTemplate
    
    beliFitur1 = False
    beliFitur2 = False
    beliFitur3 = False
    beliFitur4 = False
    
    LoadDevice
    
    Dim ComboFieldID() As String
    Dim ComboFieldName() As String
    Dim ComboSQL() As String
    
    SetGrid dbg1, dbg1.Name, Me.Name, False, "", lbl_row_count(0), "", "", 0, ComboFieldID(), ComboFieldName(), ComboSQL()
    DoEvents
    
    SetGrid dbg, dbg.Name, Me.Name, False, "", lbl_row_count(0), "", txtTimezone.Text, 1, ComboFieldID(), ComboFieldName(), ComboSQL()
    DoEvents
    
    LoadCmbTanggal
    
    lbl(22).Caption = INILanRead(Me.Name, "lbl.22", "", win_Socket.LocalIP, "", "")
    
    edtPort.Text = LoadParam("PortRealtime")
    If edtPort.Text = "" Then edtPort.Text = "8123"
    PortRealtime = CLng(edtPort.Text)
    
    edtPortNeo.Text = LoadParam("PortRealtimeNeo")
    If edtPortNeo.Text = "" Then edtPortNeo.Text = "8234"
    PortRealtimeNeo = CLng(edtPortNeo.Text)
    
    lbl(24).Left = lbl(23).Left + lbl(23).Width + 120
    lbl(24).Caption = edtPort.Text
    edtPort.Left = lbl(24).Left + lbl(24).Width + 120
    spinPort.BuddyControl = ""
    spinPort.BuddyControl = "edtPort"
    PushButton1(0).Left = spinPort.Left + spinPort.Width + 120
    
    lbl(31).Left = lbl(30).Left + lbl(30).Width + 120
    lbl(31).Caption = edtPortNeo.Text
    edtPortNeo.Left = lbl(31).Left + lbl(31).Width + 120
    spinPortNeo.BuddyControl = ""
    spinPortNeo.BuddyControl = "edtPortNeo"
    PushButton1(4).Left = spinPortNeo.Left + spinPortNeo.Width + 120
    
    GLogPrefix = Val("&H" & "77" & "ED" & "C4" & "5C")
    
    If Not OpenNetWork(FKRealSvrTcp, PortRealtime) Then
        MsgBox INILanRead("errMsg", "Msg1", "", edtPort.Text, "", ""), vbOKOnly, App.Title
        lbl_Click 24
    End If
    
    If Not OpenNeoUDP(PortRealtimeNeo) Then
        MsgBox INILanRead("errMsg", "Msg1", "", edtPortNeo.Text, "", ""), vbOKOnly, App.Title
        lbl_Click 31
    End If
    
    id_userLogin = "-1"
    DoEvents
    
    NextSinkronUser1 = CInt(INILanRead(Me.Name, "NextSinkronUser", "30", "", "", ""))
    NextValidasi1 = CInt(INILanRead(Me.Name, "NextValidasi", "300", "", "", ""))
    NextSinkronDateTime1 = CInt(INILanRead(Me.Name, "NextSinkronDateTime", "10", "", "", ""))
    
    NextSinkronUser = DateAdd("n", NextSinkronUser1, Now)
    NextValidasi = DateAdd("n", NextValidasi1, Now)
    NextSinkronDateTime = DateAdd("n", NextSinkronDateTime1, Now)
    
    TimerSinkronUser.Enabled = True
    
    'lbl(28).ForeColor = &H808080
    'lbl(29).ForeColor = &H808080
    
    Exit Sub
        
BugError:
    Err.Clear
    Resume Next
End Sub

Private Sub Form_Unload(Cancel As Integer)
    On Error GoTo BugError
    
    If Not CloseForm Then
        MinimizeToTray
        Cancel = 1
        
        SetToolTip "FingerspotOne AddOn"
        ShowMessage 0, "FingerspotOne AddOn", "FingerspotOne", 1
    Else
        CloseNetWork FKRealSvrTcp
        FrmUnload
    End If
    
    Exit Sub
        
BugError:
    Err.Clear
    Resume Next
End Sub

Private Sub LoadCmbTanggal()
    On Error GoTo BugError
    
    Dim tgl_awal As Date
    Dim tgl_akhir As Date
    Dim tgl1 As Date
    Dim tgl2 As Date
    
    Dim i As Integer
    Dim j As Integer
    Dim k As Integer
    Dim s As String
    
    tgl_akhir = Now
    tgl_awal = DateAdd("m", -3, tgl_akhir)
    
    cmb_bln_thn.Clear
    For i = 1 To 3
        tgl1 = DateAdd("m", i, tgl_awal)
        j = Month(tgl1)
        k = Year(tgl1)
        
        s = INILanRead(Me.Name, "Bulan" & Trim(CStr(j)), "", "", "", "")
        s = s & " " & CStr(k)
        cmb_bln_thn.AddItem s, i - 1, i - 1
    Next i
    If cmb_bln_thn.ListCount > 0 Then cmb_bln_thn.ListIndex = cmb_bln_thn.ListCount - 1
    DoEvents
    
    cmb_status.Clear
    For i = 0 To 2
        s = INILanRead(Me.Name, "status" & Trim(CStr(i)), "", "", "", "")
        cmb_status.AddItem s, i, i
    Next i
    If cmb_status.ListCount > 0 Then cmb_status.ListIndex = 0
    DoEvents
    
    Exit Sub
        
BugError:
    Err.Clear
    Resume Next
End Sub

Private Sub AntrianSMS()
    On Error GoTo BugError
    
    Dim rs As cRecordset
    Dim str1 As String
    
    Dim tanggal_kirim As String
    Dim no_ponsel As String
    Dim isi_pesan As String
    Dim nama_kontak As String
    
    Dim tgl_awal As Date
    Dim tgl_akhir As Date
    Dim tgl1 As Date
    Dim tgl2 As Date
    Dim tgl1_str As String
    Dim tgl2_str As String
    
    dbg1.Clear
    
    tgl_akhir = Now
    tgl_awal = DateAdd("m", -3, tgl_akhir)
    tgl1 = DateAdd("m", cmb_bln_thn.ListIndex + 1, tgl_awal)
    tgl1 = DateSerial(Year(tgl1), Month(tgl1), 1)
    tgl2 = DateAdd("m", 1, tgl1)
    tgl2 = DateAdd("d", -1, tgl2)
    
    tgl1_str = DateTimeToStr(tgl1, DateFormatDatabase)
    tgl2_str = DateTimeToStr(tgl2, DateFormatDatabase)
    
    strSQL = "SELECT * FROM t_antrian_sms " & _
        "WHERE (tanggal_kirim BETWEEN '" & tgl1_str & "' AND '" & tgl2_str & "') AND status_kirim = " & CStr(cmb_status.ListIndex) & " " & _
        "ORDER BY last_update_date, no_ponsel ASC "
    'Clipboard.Clear
    'Clipboard.SetText strSQL
    Set rs = getRecordSet_SMS(strSQL, True)
    Do While Not rs.EOF
        tgl2 = rs.Fields("tanggal_kirim").value
        tanggal_kirim = DateTimeToStr(tgl2, DateFormatView)
        no_ponsel = rs.Fields("no_ponsel").value
        isi_pesan = rs.Fields("isi_pesan").value
        nama_kontak = rs.Fields("nama_kontak").value
        
        str1 = tanggal_kirim & ";" & no_ponsel & ";" & isi_pesan & ";" & nama_kontak & ";"
        InsertGrid dbg1, dbg1.RowCount + 1, lbl_row_count(0), "", False, &H0&, &H404040, False, Null, Null, "", str1
        
        DoEvents
        rs.MoveNext
    Loop
    
    Exit Sub
        
BugError:
    Err.Clear
    Resume Next
End Sub

Public Sub LoadDevice()
    Dim rs As cRecordset
    
    Dim sn_device As String
    Dim activation_code As String
    
    Dim str1 As String
    Dim str2 As String
    Dim str3 As String
    
    AllSNTerDaftar = ""
    AllSNTerDaftarSMS = ""
    
    strSQL = "SELECT * FROM t_device WHERE status_aktif = '1' AND comm_type < 4 ORDER BY sn_device ASC "
    Set rs = getRecordSet_Data(strSQL, True)
    Do While Not rs.EOF
        sn_device = rs.Fields("sn_device").value
        
        activation_code = rs.Fields("active_code_realtime").value
        If CheckKeyRevoRT2(1, sn_device, activation_code) Then
            beliFitur1 = True
            AllSNTerDaftar = AllSNTerDaftar & sn_device & ";"
        End If
        
        activation_code = rs.Fields("activation_code").value
        If CheckKeyRevoRT2(2, sn_device, activation_code) Then
            beliFitur2 = True
        End If
        
        activation_code = rs.Fields("active_code_autosync").value
        If CheckKeyRevoRT2(3, sn_device, activation_code) Then
            beliFitur3 = True
        End If
            
        activation_code = rs.Fields("active_code_sdk").value
        If CheckKeyRevoRT2(4, sn_device, activation_code) Then
            beliFitur4 = True
            AllSNTerDaftarSMS = AllSNTerDaftarSMS & sn_device & ";"
        End If
        
        DoEvents
        rs.MoveNext
    Loop
End Sub

Private Sub Image1_Click(Index As Integer)
    Select Case Index
        Case 0
            ShellExecute Me.hWnd, "open", "https://fingerspot.com/fingerspot-one-video/19", "", "", 4
        Case 1
            ShellExecute Me.hWnd, "open", "https://fingerspot.com/fingerspot-one-video/20", "", "", 4
        Case 2
            ShellExecute Me.hWnd, "open", "https://fingerspot.com/fingerspot-one-video/21", "", "", 4
        Case 3
            ShellExecute Me.hWnd, "open", "https://fingerspot.com/fingerspot-one-video/22", "", "", 4
    End Select
End Sub

Private Sub lbl_Click(Index As Integer)
    Dim i As Integer
    Dim str1 As String
    Dim str2 As String
    Dim str3 As String
    Dim inParam As Boolean
    
    inParam = False
    
    Select Case Index
    Case 2
        inParam = True
    Case 3
        inParam = True
    Case 4
        inParam = True
    Case 5
        inParam = True
    Case 6
        inParam = True
    Case 7
        inParam = True
    Case 24
        CloseNetWork FKRealSvrTcp
    
        edtPort.Visible = True
        spinPort.Visible = True
        PushButton1(0).Visible = True
    Case 31
        CloseNeoUDP
    
        edtPortNeo.Visible = True
        spinPortNeo.Visible = True
        PushButton1(4).Visible = True
    Case 62
    
    Case 63
    
    Case 64
    
    Case 65
    
    End Select
    
    If inParam = True Then
        i = txtIsi.SelStart
        str1 = Mid(txtIsi.Text, 1, i)
        str2 = Mid(txtIsi.Text, i + 1, Len(txtIsi.Text) - i)
        str3 = lbl(Index).Caption
        
        txtIsi.Text = str1 & str3 & str2
        txtIsi.SetFocus
        txtIsi.SelStart = Len(str1 & str3)
    End If
End Sub

Private Sub PushButton1_Click(Index As Integer)
    On Error GoTo Err1
    
    Dim RecRow As Long
    Dim sn_device As String
    Dim timezone As String
    Dim selisih As String
    
    PushButton1(Index).Enabled = False
    Select Case Index
    Case 0
        If OpenNetWork(FKRealSvrTcp, CLng(edtPort.Text)) = True Then
            PortRealtime = CLng(edtPort.Text)
            edtPort.Visible = False
            spinPort.Visible = False
            edtPort.Text = LoadParam("PortRealtime")
            lbl(24).Caption = edtPort.Text
            PushButton1(0).Visible = False
        Else
            MsgBox INILanRead("errMsg", "Msg1", "", edtPort.Text, "", ""), vbOKOnly, App.Title
        End If
    Case 1
        If Trim(txtIsi.Text) <> "" Then
            SetTemplate
            DoEvents
            MsgBox INILanRead("errMsg", "msg10", "", "", "", ""), vbOKOnly, App.Title
        Else
            MsgBox INILanRead("errMsg", "msg9", "", "", "", ""), vbOKOnly, App.Title
        End If
    Case 2
        'TulisLog "Line apa ....."
        'frmAktivasiAll.FormStatus = 5
        frmAktivasiAll.Show vbModal
    Case 3
        With dbg
            .BeginUpdate
            For RecRow = 1 To .RowCount
                sn_device = .CellValue(RecRow, 1)
                
                timezone = .CellValue(RecRow, 2)
                If timezone = "" Then timezone = "(UTC+07:00) Indonesia (WIB)"
                selisih = .CellValue(RecRow, 3)
                If InStr(1, selisih, "-") > 0 Then
                    selisih = getAngka(selisih, False)
                    selisih = "-" & selisih
                Else
                    selisih = getAngka(selisih, False)
                End If
                If selisih = "" Then selisih = "0"

                SetParam sn_device, timezone
                SetParam sn_device & "_diff", selisih
                
                DoEvents
            Next RecRow
            .EndUpdate
        End With
        MsgBox INILanRead("errMsg", "msg10", "", "", "", ""), vbOKOnly, App.Title
    Case 4
        If OpenNeoUDP(CLng(edtPortNeo.Text)) = True Then
            PortRealtimeNeo = CLng(edtPortNeo.Text)
            edtPortNeo.Visible = False
            spinPortNeo.Visible = False
            edtPortNeo.Text = LoadParam("PortRealtimeNeo")
            lbl(31).Caption = edtPortNeo.Text
            PushButton1(4).Visible = False
        Else
            MsgBox INILanRead("errMsg", "Msg1", "", edtPortNeo.Text, "", ""), vbOKOnly, App.Title
        End If
    End Select
    PushButton1(Index).Enabled = True
    
    Exit Sub
Err1:
    Err.Clear
    Resume Next
End Sub

Private Sub TabDetail_SelectedChanged(ByVal Item As XtremeSuiteControls.ITabControlItem)
    Dim rs As cRecordset
    Dim sn_device As String
    Dim active_code_autosync As String
    Dim timezone As String
    Dim selisih As String
    Dim str1 As String
    
    If TabDetail.Item(4).Selected = True Then
        dbg.Clear
        strSQL = "SELECT * FROM t_device Where status_aktif = '1' AND comm_type < 4 " & _
            "ORDER BY sn_device ASC "
        Set rs = getRecordSet_Data(strSQL, True)
        Do While Not rs.EOF
            sn_device = rs.Fields("sn_device").value
            active_code_autosync = rs.Fields("active_code_autosync").value
            
            If CheckKeyRevoRT2(3, sn_device, active_code_autosync) Then
                timezone = LoadParam(sn_device)
                If timezone = "" Then timezone = "(UTC+07:00) Indonesia (WIB)"
                selisih = LoadParam(sn_device & "_diff")
                If selisih = "" Then selisih = "0"
                
                str1 = sn_device & ";" & timezone & ";" & selisih & ";"
                InsertGrid dbg, dbg.RowCount + 1, lbl_row_count(0), "", False, &H0&, &H404040, False, Null, Null, "", str1
            End If
            
            DoEvents
            rs.MoveNext
        Loop
    End If
End Sub

Private Sub TimerSinkronUser_Timer()
    TimerSinkronUser.Enabled = False
    ProsesAuto
End Sub

Private Sub ProsesAuto()
    Dim rs As cRecordset
    Dim SNAutoSyncUser As String
    Dim ACAutoSyncUser As String
    
    Dim sn_device As String
    Dim name_device As String
    Dim activation_code As String
    Dim active_code_autosync As String
    Dim str1 As String
    
    Dim sukses As Boolean
    
    Dim timezone As String
    Dim selisih As String
    Dim jsonText As String
    
    If NextSinkronUser <= Now Then
        NextSinkronUser = DateAdd("n", NextSinkronUser1, Now)
        
        'Sinkron User Mesin
        SinkronUser
        DoEvents
    End If
    
    If NextSinkronDateTime <= Now Then
        NextSinkronDateTime = DateAdd("n", NextSinkronDateTime1, Now)
        
        'Sinkron Tanggal Jam Mesin
        strSQL = "SELECT d.*, dt.firmware FROM t_device AS d " & _
            "LEFT JOIN t_device_type AS dt ON dt.id_device_type = d.id_device_type " & _
            "WHERE d.status_aktif = '1' AND d.comm_type < 4 "
        Set rs = getRecordSet_Data(strSQL, True)
        Do While Not rs.EOF
            sn_device = rs.Fields("sn_device").value
            name_device = rs.Fields("name_device").value
            activation_code = rs.Fields("active_code").value
            active_code_autosync = rs.Fields("active_code_autosync").value
            
            If CheckKeyRevoRT2(3, sn_device, active_code_autosync) Then
                sukses = CekKoneksiMesin(sn_device)
                If sukses = True Then
                    frmMain.TulisLog INILanRead("errMsg", "msgLog1", "", name_device, sn_device, "")
                    cmdCloseComm
                    
                    timezone = LoadParam(sn_device)
                    If timezone = "" Then timezone = "(UTC+07:00) Indonesia (WIB)"
                    timezone = Replace(timezone, " ", "%20")
                    selisih = LoadParam(sn_device & "_diff")
                    If selisih = "" Then selisih = "0"
        
                    GetTimeServer sn_device, timezone, selisih
                Else
                    frmMain.TulisLog INILanRead("errMsg", "msgLog2", "", name_device, sn_device, "")
                End If
            End If
            DoEvents
            
            rs.MoveNext
        Loop
        DoEvents
    End If
    DoEvents
    
    TimerSinkronUser.Enabled = True
End Sub

Public Function GetTimeServer(sn_device As String, timezone As String, selisih As String) As String
    On Error GoTo BugError
    
    Dim str1 As String
    Dim str2 As String
    Dim str3 As String
    
    Dim url_api As String
    Dim url_str As String
    
    Dim server As String
    Dim api As String
    Dim req_server As String
    
    Dim tgl_svr As String
    Dim tgl_str As String
    Dim time_str As String
    Dim year1, month1, day1, hour1, minute1, Second1 As Integer
    Dim tgl_asal As Date
    Dim dwDate As Date
    
    Dim salah As Boolean
    Dim int_value As Integer
    
    salah = True
    
    server = "https://fingerspot.com/"
    api = "api/check_device"
    req_server = server & api
    url_str = req_server
    
    'str1 = EncodeJSON("http://api.fingerspotbts.com")
    'url_api = DecodeJSON(str1)
    'url_str = url_api & "/api/desktop/register"
    
    str1 = "{""sn"":""?1"",""timezone"":""?2""}"
    str1 = Replace(str1, "?1", sn_device)
    str1 = Replace(str1, "?2", timezone)
    
    url_str = url_str & "/" & sn_device & "/" & timezone
    
    str2 = SendRequestHTTP_TZ(url_str)
    If str2 = "Error" Then
        frmMain.TulisLog INILanRead("errMsg", "msgLog14", "", "", "", "")
        'MsgBox INILanRead("errMsg", "msg7", "", "", "", ""), vbOKOnly, App.Title
    Else
        str3 = ReadJson(str2, "success")
        If str3 = "true" Then
            'tgl_asal = Now
            'frmMain.TulisLog INILanRead("errMsg", "msgLog20", "", DateTimeToStr(tgl_asal, "yyyy-mm-dd hh:nn:ss"), "", "")
            'DoEvents
            
            tgl_svr = ReadJson(str2, "timestamp")
            frmMain.TulisLog INILanRead("errMsg", "msgLog13", "", "", "", "")
            frmMain.TulisLog INILanRead("errMsg", "msgLog19", "", tgl_svr, "", "")
            
            '2017-04-01 11:57:12
            '1234567890123456789
            tgl_svr = Trim(tgl_svr)
            If Len(tgl_svr) = 19 Then
                dwDate = GetDateFrom19Char(tgl_svr)
                If InStr(selisih, "-") > 0 Then
                    int_value = CInt(Mid(selisih, 2, Len(selisih))) * -1
                Else
                    int_value = CInt(selisih)
                End If
                dwDate = DateAdd("n", int_value, dwDate)
                
                SetDevTime dwDate, sn_device
            End If
            DoEvents
        Else
            frmMain.TulisLog INILanRead("errMsg", "msgLog14", "", "", "", "")
            'MsgBox INILanRead("errMsg", "msg7", "", "", "", ""), vbOKOnly, App.Title
        End If
    End If
    
    GetTimeServer = salah
    
    Exit Function
        
BugError:
    'MsgBox Err.Description, , Err.Number
    salah = False
    Err.Clear
    Resume Next
End Function

Public Function GetDateFrom19Char(ByVal char14 As String) As Date
    Dim nTemp As Integer
    Dim strDateTime As String
    
    On Error GoTo errL_GetDateFrom19Char
    
    strDateTime = strDateTime & Mid(char14, 1, 4) & "-" & Mid(char14, 6, 2) & "-" & Mid(char14, 9, 2)
    strDateTime = strDateTime & " " & Mid(char14, 12, 2) & ":" & Mid(char14, 15, 2) & ":00" '& Mid(char14, 18, 2)
    GetDateFrom19Char = strDateTime
    Exit Function
    
errL_GetDateFrom19Char:
    'GetDateFrom19Char = "1999-01-01 00:00:00"
    GetDateFrom19Char = 0
End Function

Public Function GetDateFrom14Char(ByVal char14 As String) As Date
    Dim nTemp As Integer
    Dim strDateTime As String
    
    On Error GoTo errL_GetDateFrom14Char
    
    strDateTime = strDateTime & Mid(char14, 1, 4) & "-" & Mid(char14, 5, 2) & "-" & Mid(char14, 7, 2)
    strDateTime = strDateTime & " " & Mid(char14, 9, 2) & ":" & Mid(char14, 11, 2) & ":00" '& Mid(char14, 13, 2)
    GetDateFrom14Char = strDateTime
    Exit Function
    
errL_GetDateFrom14Char:
    'GetDateFrom14Char = "1999-01-01 0:0:0"
    GetDateFrom14Char = 0
End Function

Public Function ReadJson(input_data As String, key_str As String) As String
    On Error GoTo BugError
    
    Dim JSON As New ChilkatJsonObject

    Dim success As Long
    success = JSON.Load(input_data)
    
    ReadJson = JSON.StringOf(key_str)
    
    Exit Function
        
BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Function

Public Function GetJSONByKey(input_data As String, key_str As String, Optional subkey_str As String = "") As String
    On Error GoTo BugError
            
    Dim JSON As New ChilkatJsonObject
    Dim jsonObj As ChilkatJsonObject
    Dim success As Long
    
    GetJSONByKey = ""
    success = JSON.Load(input_data)
    GetJSONByKey = JSON.StringOf(key_str)
    
    If GetJSONByKey = "" And subkey_str <> "" Then
        Set jsonObj = JSON.ObjectOf(key_str)
        
        GetJSONByKey = jsonObj.StringOf(subkey_str)
    End If
    
    Exit Function
        
BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Function

Private Function ArrayJson(jsonStr As String, key_str As String) As String
    On Error GoTo BugError
    
    ArrayJson = ""
    
    Dim JSON As New ChilkatJsonObject

    Dim success As Long
    success = JSON.Load(jsonStr)
    If (success <> 1) Then
        Debug.Print JSON.LastErrorText
        Exit Function
    End If
    
    Dim dataStr As ChilkatJsonArray
    Set dataStr = JSON.ArrayOf(key_str)
    If (JSON.LastMethodSuccess = 0) Then
        Exit Function
    End If
    
    Dim numdataStr As Long
    numdataStr = dataStr.SIZE
    Dim i As Long
    Dim addon_id As String
    Dim str1 As String
    Dim harga As Currency
    
    i = 0
    Do While i < numdataStr
    
        Dim strObj As ChilkatJsonObject
        Set strObj = dataStr.ObjectAt(i)
        
        'addon_id, addon_name_en, addon_name_id, app_id, price
        addon_id = strObj.StringOf("addon_id")
        str1 = strObj.StringOf("price")
        str1 = getAngka(str1, False)
        If Trim(str1) = "" Then str1 = "0"
        harga = CCur(str1)
        
        If addon_id = "1" Then lbl(62).Caption = INILanRead(Me.Name, "lbl.62", "", Format(harga, "#,##0;;"""""), "", "")
        If addon_id = "2" Then lbl(63).Caption = INILanRead(Me.Name, "lbl.63", "", Format(harga, "#,##0;;"""""), "", "")
        If addon_id = "3" Then lbl(64).Caption = INILanRead(Me.Name, "lbl.64", "", Format(harga, "#,##0;;"""""), "", "")
        If addon_id = "4" Then lbl(65).Caption = INILanRead(Me.Name, "lbl.65", "", Format(harga, "#,##0;;"""""), "", "")
        If addon_id = "5" Then lbl(29).Caption = INILanRead(Me.Name, "lbl.29", "", Format(harga, "#,##0;;"""""), "", "")
        If addon_id = "6" Then lbl(33).Caption = INILanRead(Me.Name, "lbl.33", "", Format(harga, "#,##0;;"""""), "", "")
        If addon_id = "7" Then lbl(34).Caption = INILanRead(Me.Name, "lbl.34", "", Format(harga, "#,##0;;"""""), "", "")
        
        i = i + 1
    Loop
    
    Exit Function
        
BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Function

Private Sub TrayIcon_DblClick()
    MinimizeToTray
End Sub

Private Sub TrayIcon_MouseUp(Button As Integer, Shift As Integer, x As Single, y As Single)
    If (Button = 2) Then Me.PopupMenu mnuTray
End Sub

Private Sub mnuTrayShow_Click()
    MinimizeToTray
End Sub

Private Sub mnuExit_Click()
    CloseForm = True
    Unload Me
End Sub

Public Sub MinimizeToTray()
    On Error GoTo BugError
    
    If Not Minimized Then
        Me.Hide
        Minimized = True
        mnuTrayShow.Caption = "Show"
    Else
        Me.Show
        Minimized = False
        mnuTrayShow.Caption = "Hide"
    End If
    
    Exit Sub
        
BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Sub

Public Sub SetToolTip(TipStr As String)
    TrayIcon.Text = TipStr
End Sub

Public Sub ShowMessage(IDMessage As Integer, StrTitle As String, StrMessage As String, StyleIndex As Integer)
    'StyleIndex = 0: None, 1: Info, 2: Warning, 3: Error
    TrayIcon.ShowBalloonTip 5, StrTitle, StrMessage, StyleIndex
End Sub

'Mesin Neo
Private Sub SckUDP_DataArrival(ByVal bytesTotal As Long)
    On Error GoTo ErrorHandle
    
    Dim strData As String
    Dim strBin As String
    Dim strHex As String
    Dim strByte() As Byte
    
    Dim i As Integer
    Dim j As Long
    Dim str1 As String
    Dim str2 As String
    Dim iLc As Long
    
    Dim iInt64 As Long
    Dim sDIN As Long
    
    Dim year1 As Long
    Dim month1 As Long
    Dim day1 As Long
    Dim hour1 As Long
    Dim minute1 As Long
    Dim Second1 As Long

    'SckUDP.GetData strData, vbString, bytesTotal
    SckUDP.GetData strByte
    
    If Not IsCorectGLog(strByte) Then Exit Sub
    
    If UBound(strByte) > 0 Then
        iInt64 = 0
        For i = 8 To 15
            iInt64 = iInt64 + ShiftLeft(strByte(i), ((i - 8) * 8))
        Next i
    End If
    If iInt64 = 0 Then Exit Sub
    
    sDIN = CStr(iInt64)
    year1 = (strByte(16) And CInt("&H" & "7F")) + 2000
    
    '((lReceiveData[16] + lReceiveData[17] shl 8) shr 7) and $0F
    month1 = (ShiftRight((strByte(16) + ShiftLeft(strByte(17), 8)), 7)) And CInt("&H" & "0F")
    
    '(lReceiveData[17] + lReceiveData[18] shl 8) shr 3 and $1F
    day1 = ShiftRight((strByte(17) + ShiftLeft(strByte(18), 8)), 3) And CInt("&H" & "1F")
    
    '(lReceiveData[21] + lReceiveData[22] shl 8) shr 7 and $1F
    hour1 = ShiftRight((strByte(21) + ShiftLeft(strByte(22), 8)), 7) And CInt("&H" & "1F")
    
    '(lReceiveData[22] + lReceiveData[23] shl 8) shr 4 and $3F
    minute1 = (ShiftRight((strByte(22) + ShiftLeft(strByte(23), 8)), 4)) And CInt("&H" & "3F")
    
    'lReceiveData[23] shr 2 and $3F
    Second1 = ShiftRight(strByte(23), 2) And CInt("&H" & "3F")
    
    Dim Verify As Long
    Dim InOutMode As Long
    Dim NoMesin As Long
    
    Verify = ShiftRight((strByte(18) + ShiftLeft(strByte(19), 8)), 4) And CInt("&H" & "1F")
    InOutMode = strByte(18) And CInt("&H" & "0F")
    'rec.DN := (lReceiveData[20] + lReceiveData[21] shl 8 + lReceiveData[22] shl 16) shr 1 and $3FFF;
    NoMesin = (ShiftRight((strByte(20) + ShiftLeft(strByte(21), 8) + ShiftLeft(strByte(22), 16)), 1)) And CInt("&H" & "3F" & "FF")
    
'    'Tes KitaID
'    str1 = ""
'    For i = 24 To UBound(strByte) - 1
'      str1 = str1 + IntToHex(strByte(i), 2)
'    Next
'    DoEvents
'
'    kunci = "FINGERSPOT"
'    str2 = ""
'    j = 0
'    For i = 1 To Len(s)
'      If i Mod 2 = 0 Then
'        j = j + 1
'        str2 = str2 + Mid(kunci, j, 1) + Mid(s, i - 1, 1) + Mid(s, i, 1)
'        If j Mod 10 = 0 Then j = 0
'      End If
'    Next
'    DoEvents
'
'    If (j <> 0) Then str2 = str2 + Mid(kunci, j + 1, Len(kunci))
'    KitaID = GetCode1(str2)
    
    Dim scanDate As Date
    Dim SN As String
    
    scanDate = DateSerial(year1, month1, day1) & " " & TimeSerial(hour1, minute1, Second1)
    strSQL = "SELECT d.* FROM t_device AS d " & _
        "LEFT JOIN t_device_type AS dt ON dt.id_device_type = d.id_device_type " & _
        "WHERE dt.dev_type = 2 AND d.dev_id = " & NoMesin & " "
    SN = GetValueField(strSQL, "sn_device")
    'lstMessages.AddItem "PIN:" & sDIN & ", ScanDate:" & DateTimeToStr(scanDate, DateTimeFormatDatabase) & ", Verify:" & Verify & ", Action:" & Action & ", IDdev:" & DN
    'Clipboard.Clear
    'Clipboard.SetText strSQL
    'MsgBox NoMesin & " ~ " & SN
    
    If InStr(AllSNTerDaftar, SN) > 0 Then
        SaveGLogDataOneRec SN, CStr(sDIN), scanDate, Verify, InOutMode, InOutMode, "", "", NoMesin, ResponsePort, ""
        'lbl1(2).Caption=lbl1(2).Caption & vbNewLine & "No Mesin : " & NoMesin
    End If
    DoEvents
    
    Dim pstrContentType As String
    Dim strOutData As String
    
    pstrContentType = "Content-type: text/html"
    strOutData = "HTTP/1.0 200 OK" & vbCrLf & _
        pstrContentType & vbCrLf & vbCrLf
    SckUDP.SendData strByte 'HexToBytes("200") 'strOutData '(HexToBytes("200"))
    
    Exit Sub
    
ErrorHandle:
    'MsgBox "Error: " + Err.Description, vbOKOnly + vbCritical, "Error"
    Resume Next
End Sub

Private Function IsCorectGLog(AData() As Byte) As Boolean
    On Error GoTo ErrorHandle
    
    IsCorectGLog = False
    
    Dim prefix As Long
    Dim sum As Integer
    Dim checksum As Integer
    Dim i As Integer

    prefix = AData(0) + ShiftLeft(AData(1), 8) + ShiftLeft(AData(2), 16) + ShiftLeft(AData(3), 24)
    If prefix <> GLogPrefix Then Exit Function

    IsCorectGLog = True

    Exit Function

ErrorHandle:
    'MsgBox "Error: " + Err.Description, vbOKOnly + vbCritical, "Error"
    Resume Next
End Function

'Mesin Revo
Private Sub FKRealSvrTcp_OnReceiveGLogDataExtend(ByVal astrRootIP As String, _
    ByVal astrDeviceIP As String, ByVal anDevicePort As Long, ByVal anDeviceID As Long, _
    ByVal anSEnrollNumber As Long, ByVal anVerifyMode As Long, ByVal anInOutMode As Long, _
    ByVal anLogDate As Date, ByVal astrSerialNo As String)
    
    On Error GoTo BugError
    
    Dim Position As Long
    Dim strQuery As String
    Dim dateStr As String
    Dim TglAktif As Date
    
    Dim Tglx As String
    Tglx = CStr(Year(anLogDate)) & CStr(Month(anLogDate)) & CStr(Day(anLogDate))
    If TglTemp <> Tglx Then
        TglTemp = Tglx
        LoadDevice
    End If
    
    astrSerialNo = check_spelling_fio_device_sn(astrSerialNo)
    
    'MsgBox "FKRealSvrTcp_OnReceiveGLogDataExtend : " & anVerifyMode
    If InStr(AllSNTerDaftar, astrSerialNo) > 0 Then
        SaveGLogDataOneRec astrSerialNo, str(anSEnrollNumber), anLogDate, anVerifyMode, anInOutMode, anInOutMode, _
            astrRootIP, astrDeviceIP, anDeviceID, anDevicePort, ""
    End If
    
    Exit Sub
        
BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Sub

Private Sub FKRealSvrTcp_OnReceiveGLogTextAndImage(ByVal astrClientIP As String, _
    ByVal anClientPort As Long, ByVal astrLogText As String, ByVal astrLogImage As String)
    
    On Error GoTo BugError
    
    Dim strLogId As String
    Dim nEnrollNumber As Long
    Dim nDeviceID As Long
    Dim strVerifyMode As String
    Dim strInOutMode As String
    Dim dwDate As Date
    
    Dim SN As String
    Dim strResponse As String
    Dim parsedObj As Object
    
    'MsgBox "FKRealSvrTcp_OnReceiveGLogTextAndImage : " & astrLogText
    Set parsedObj = JSON.parse(astrLogText)
    
    SN = parsedObj.Item("serial_number")
    strLogId = parsedObj.Item("log_id")
    nEnrollNumber = parsedObj.Item("user_id")
    nDeviceID = parsedObj.Item("fk_device_id")
    strVerifyMode = parsedObj.Item("verify_mode")
    strInOutMode = parsedObj.Item("io_mode")
    dwDate = GetDateFrom14Char(parsedObj.Item("io_time"))
    
    'saveJpgFile astrLogImage
    'showLogImage astrLogImage
    
    Dim Position As Long
    Dim strQuery As String
    Dim dateStr As String
    Dim TglAktif As Date
    Dim anVerifyMode As Long
    
    Position = InStr(UCase(strVerifyMode), "FP")
    If Position > 0 Then
        anVerifyMode = 1
    End If
    
    Position = InStr(UCase(strVerifyMode), "PASS")
    If Position > 0 Then
        anVerifyMode = 2
    End If
    
    Position = InStr(UCase(strVerifyMode), "CARD")
    If Position > 0 Then
        anVerifyMode = 3
    End If
    
    Position = InStr(UCase(strVerifyMode), "FACE")
    If Position > 0 Then
        anVerifyMode = 20
    End If
    
    Position = InStr(UCase(strVerifyMode), "VEIN")
    If Position > 0 Then
        anVerifyMode = 30
    End If
    
    Position = InStr(UCase(strVerifyMode), "PALM")
    If Position > 0 Then
        anVerifyMode = 30
    End If
    
    SN = check_spelling_fio_device_sn(SN)
    
    Dim Tglx As String
    Tglx = CStr(Year(dwDate)) & CStr(Month(dwDate)) & CStr(Day(dwDate))
    If TglTemp <> Tglx Then
        TglTemp = Tglx
        LoadDevice
    End If
    
    If InStr(AllSNTerDaftar, SN) > 0 Then
        SaveGLogDataOneRec SN, str(nEnrollNumber), dwDate, anVerifyMode, Val(strInOutMode), Val(strInOutMode), _
            astrClientIP, astrClientIP, nDeviceID, anClientPort, astrLogImage
    End If
    
    strResponse = "{""log_id"":""" & strLogId & """,""result"":""OK""}"
    Call FKRealSvrTcp.SendResponse(astrClientIP, anClientPort, strResponse)
    
    Exit Sub
        
BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Sub

Private Sub FKRealSvrTcp_OnReceiveGLogTextOnDoorOpen(ByVal astrClientIP As String, _
    ByVal anClientPort As Long, ByVal astrLogText As String, ByVal astrLogImage As String)
   
    On Error GoTo BugError
    
    Dim strLogId As String
    Dim strEnrollNumber As String
    Dim nDeviceID As Long
    Dim strVerifyMode As String
    Dim strInOutMode As String
    Dim dwDate As Date
    
    Dim strResponse As String
    Dim parsedObj As Object
    Dim strActiveLogId As String
    Dim strEmergency As String
    Dim strIsSupportStringID As String
    Dim SN As String
    
    'MsgBox "FKRealSvrTcp_OnReceiveGLogTextOnDoorOpen : " & astrLogText
    Set parsedObj = JSON.parse(astrLogText)
    
    SN = parsedObj.Item("serial_number")
    strLogId = parsedObj.Item("log_id")
    strEnrollNumber = parsedObj.Item("user_id")
    nDeviceID = parsedObj.Item("fk_device_id")
    strVerifyMode = parsedObj.Item("verify_mode")
    strInOutMode = parsedObj.Item("io_mode")
    dwDate = GetDateFrom14Char(parsedObj.Item("io_time"))
    strEmergency = parsedObj.Item("emergency")
    strIsSupportStringID = parsedObj.Item("is_support_string_id")
    
    'showLogImage astrLogImage
    
    Dim Position As Long
    Dim strQuery As String
    Dim dateStr As String
    Dim TglAktif As Date
    Dim anVerifyMode As Long
    
    Position = InStr(strVerifyMode, "FP")
    If Position > 0 Then
        anVerifyMode = 1
    End If
    
    Position = InStr(strVerifyMode, "PASS")
    If Position > 0 Then
        anVerifyMode = 2
    End If
    
    Position = InStr(strVerifyMode, "CARD")
    If Position > 0 Then
        anVerifyMode = 3
    End If
    
    Position = InStr(strVerifyMode, "FACE")
    If Position > 0 Then
        anVerifyMode = 20
    End If
    
    Position = InStr(UCase(strVerifyMode), "VEIN")
    If Position > 0 Then
        anVerifyMode = 30
    End If
    
    Position = InStr(UCase(strVerifyMode), "PALM")
    If Position > 0 Then
        anVerifyMode = 30
    End If
    
    SN = check_spelling_fio_device_sn(SN)
    
    Dim Tglx As String
    Tglx = CStr(Year(dwDate)) & CStr(Month(dwDate)) & CStr(Day(dwDate))
    If TglTemp <> Tglx Then
        TglTemp = Tglx
        LoadDevice
    End If
    
    If InStr(AllSNTerDaftar, SN) > 0 Then
        SaveGLogDataOneRec SN, strEnrollNumber, dwDate, anVerifyMode, Val(strInOutMode), Val(strInOutMode), _
            astrClientIP, astrClientIP, nDeviceID, anClientPort, astrLogImage
    End If
    
    If strEmergency = "yes" Then
        strActiveLogId = strEnrollNumber
        If strActiveLogId = strEnrollNumber Then
            strResponse = "{""log_id"":""" & strLogId & """,""result"":""OK"",""mode"":""open""}"
        Else
            strResponse = "{""log_id"":""" & strLogId & """,""result"":""OK"",""mode"":""nothing""}"
        End If
    Else
        strResponse = "{""log_id"":""" & strLogId & """,""result"":""OK"",""mode"":""nothing""}"
    End If
    
    Call FKRealSvrTcp.SendRtLogResponseV3(astrClientIP, anClientPort, strResponse)
    
    Exit Sub
        
BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Sub

Private Function SaveGLogDataOneRec(SN As String, PIN As String, scan_datetime As Date, _
    verify_type As Long, io_mode As Long, inout_mode2 As Long, ip_public As String, _
    ip_device As String, id_device As Long, port_device As Long, log_img_str As String) As Boolean
    
    On Error GoTo BugError
    
    Dim dateStr As String
    Dim tglStr As String
    Dim last_update_date As String
    
    Dim jam_scan As Date
    Dim selisih As Long
    Dim str1 As String
    Dim tgl_mulai_pakai_bts As String
    
    If scan_datetime = 0 Then Exit Function
    
    str1 = DateTimeToStr(scan_datetime, DateFormatDatabase)
    If Tgl_load_str <> str1 Then
        Tgl_load_str = str1
        JarakScanSMS = INILanRead(Me.Name, "JarakScanSMS", "60", "", "", "")
        'SetDevTime SN
        'use_sms = Val(LoadParam("use_sms"))
    End If
    
    lbl1(2).Caption = INILanRead(Me.Name, "lbl1.2", "", PIN, DateTimeToStr(scan_datetime, DateTimeFormatView), "")
    
    PIN = Trim(PIN)
    last_update_date = DateTimeToStr(Now, DateTimeFormatDatabase)
    
    dateStr = DateTimeToStr(scan_datetime, DateTimeFormatDatabase)
    dateStr = Mid(dateStr, 1, 17) & "00"
    
    tglStr = DateTimeToStr(scan_datetime, DateFormatDatabase)
    
    strSQL = "REPLACE INTO t_att_log (scan_date, pin, sn, verify_mode, inout_mode, " & _
        "inout_mode2, ip_public, ip_device, id_device, port_device) " & _
        "VALUES ('" & dateStr & "', '" & PIN & "', '" & SN & "', " & verify_type & ", " & _
        io_mode & ", " & inout_mode2 & ", '" & ip_public & "', '" & ip_device & "', " & _
        id_device & ", " & port_device & ") "
    ExecuteSQLite_Data strSQL
    
    If InStr(AllSNTerDaftarSMS, SN) > 0 Then
        PesanSMS PIN, scan_datetime
    End If
    
    SaveGLogDataOneRec = True
    
    Exit Function
        
BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Function

Private Sub PesanSMS(PIN As String, tgl_scan As Date)
    On Error GoTo BugError
    
    Dim rs As cRecordset
    Dim pesan_sms As String
    Dim dateStr As String
    Dim no_ponsel As String
    
    Dim NamaKaryawan As String
    Dim Jabatan As String
    Dim StrukturOrganisasi As String
    Dim Kantor As String
    Dim JamScan As String
    
    Dim isi_pesan As String
    
    JamScan = DateTimeToStr(tgl_scan, DateTimeFormatDatabase)
        
    strSQL = "SELECT * FROM t_antrian_sms " & _
        "WHERE nama_kontak = '" & PIN & "' AND '" & JamScan & "' < DATETIME(last_update_date, '+" & JarakScanSMS & " minutes')"
        '"WHERE nama_kontak = '" & PIN & "' AND DATETIME('now','localtime') < DATETIME(last_update_date, '+" & JarakScanSMS & " minutes')"
    'Clipboard.Clear
    'Clipboard.SetText strSQL
    Set rs = getRecordSet_SMS(strSQL, True)
    
    If rs.RecordCount = 0 Then
        dateStr = DateTimeToStr(tgl_scan, DateFormatDatabase)
            
        If date_temp <> dateStr Then
            date_temp = dateStr
            LoadTemplate
        End If
        
        strSQL = "SELECT a.name_employee AS nama, b.name_position AS jabatan, c.name_structure AS so, " & _
            "d.name_office AS kantor, e.txt_phone1 AS telp1, e.txt_phone2 AS telp2 " & _
            "FROM t_employee AS a " & _
            "LEFT JOIN t_position AS b ON a.id_position = b.id_position " & _
            "LEFT JOIN t_structure AS c ON a.id_structure = c.id_structure " & _
            "LEFT JOIN t_office AS d ON a.id_office = d.id_office " & _
            "LEFT JOIN t_employee_detail AS e ON a.id_employee = e.id_employee " & _
            "WHERE a.pin = '" & PIN & "' "
        Set rs = getRecordSet_Data(strSQL, True)
        
        If rs.RecordCount > 0 Then
            NamaKaryawan = rs.Fields("nama").value
            Jabatan = rs.Fields("jabatan").value
            StrukturOrganisasi = rs.Fields("so").value
            Kantor = rs.Fields("kantor").value
            no_ponsel = rs.Fields("telp1").value
            If Trim(no_ponsel) = "" Then
                no_ponsel = rs.Fields("telp2").value
                If Trim(no_ponsel) = "" Then Exit Sub
            End If
        Else
            Exit Sub
        End If
        
        dateStr = DateTimeToStr(tgl_scan, DateFormatDatabase)
        
        isi_pesan = template_sms1
        isi_pesan = Replace(isi_pesan, "param1", PIN)
        isi_pesan = Replace(isi_pesan, "param2", NamaKaryawan)
        isi_pesan = Replace(isi_pesan, "param3", Jabatan)
        isi_pesan = Replace(isi_pesan, "param4", StrukturOrganisasi)
        isi_pesan = Replace(isi_pesan, "param5", Kantor)
        isi_pesan = Replace(isi_pesan, "param6", JamScan)
        
        pesan_sms = EncodeJSON(isi_pesan)
        
        'No urut
        strSQL = "SELECT * FROM t_antrian_sms " & _
            "WHERE tanggal_kirim = '" & dateStr & "' AND no_ponsel = '" & no_ponsel & "' "
        Set rs = getRecordSet_SMS(strSQL, True)
        no_urut = rs.RecordCount + 1
        
        strSQL = "REPLACE INTO t_antrian_sms (no_ponsel, tanggal_kirim, no_urut, isi_pesan, app_id, status_kirim, nama_kontak, " & _
            "last_update_date, last_update_user) " & _
            "VALUES ('" & no_ponsel & "', '" & dateStr & "', " & Trim(CStr(no_urut)) & ", '" & pesan_sms & "', '0', '0', '" & PIN & "', '" & _
            JamScan & "', '-1') "
        ExecuteSQLite_SMS strSQL
    End If
    
    Exit Sub
        
BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Sub

Public Sub TulisLog(str_value As String)
    On Error GoTo BugError
    
    Dim field() As String
    Dim i As Integer
    Dim str1 As String
    Dim str2 As String
    Dim str3 As String
    Dim dateStr As String
    
    dateStr = DateTimeToStr(Now, "dd-MM-yyyy HH:mm:ss")
    
'    i = 0 'txtIsi.SelStart
'    str1 = Mid(txtIsi2.Text, 1, i)
'    str2 = Mid(txtIsi2.Text, i + 1, Len(txtIsi2.Text) - i)
'    str3 = dateStr & " : " & str_value & vbNewLine
'
'    txtIsi2.Text = str1 & str3 & str2
'    txtIsi2.SetFocus
'    txtIsi2.SelStart = Len(str1 & str3)
    
    str3 = dateStr & " : " & str_value & vbNewLine
    str1 = txtIsi2.Text
    field = Split(str1, vbNewLine)
    txtIsi2.Text = ""
    ListBox.Clear
    ListBox.AddItem str3
    For i = 0 To UBound(field) - 1
        If i <= 50 Then
            str2 = field(i)
            str3 = str3 & str2 & vbNewLine
            ListBox.AddItem str2
        Else
            Exit For
        End If
    Next i
    'MsgBox str3
    txtIsi2.Text = ""
    txtIsi2.Text = str3
    
    Exit Sub
        
BugError:
    'MsgBox Err.Description, , Err.Number
    Err.Clear
    Resume Next
End Sub

